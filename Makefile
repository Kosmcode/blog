.PHONY: up
up:
	docker compose --file=docker-compose.kosmcode.yml --env-file=.env up -d

.PHONY: down
down:
	docker compose --file=docker-compose.kosmcode.yml --env-file=.env down

.PHONY: build
build:
	docker compose --file=docker-compose.kosmcode.yml --env-file=.env build --no-cache

.PHONY: start
start:
	docker compose --file=docker-compose.kosmcode.yml --env-file=.env start

.PHONY: stop
stop:
	docker compose --file=docker-compose.kosmcode.yml --env-file=.env stop

.PHONY: restart
restart:
	docker compose --file=docker-compose.kosmcode.yml --env-file=.env restart

.PHONY: shell
shell:
	docker compose --file=docker-compose.kosmcode.yml --env-file=.env exec --user=1000 app /bin/bash

.PHONY: setup-new
setup-new:
	make start
	docker compose --file=docker-compose.kosmcode.yml --env-file=.env exec --user=1000 app php artisan migrate
	docker compose --file=docker-compose.kosmcode.yml --env-file=.env exec --user=1000 app php artisan db:seed
	docker compose --file=docker-compose.kosmcode.yml --env-file=.env exec --user=1000 app php artisan optimize:clear

.PHONY: deploy
deploy:
	php artisan maintenance:mode enable
	php artisan backup:run
	git pull
	composer install
	npm install
	php artisan migrate
	php artisan optimize:clear
	php artisan maintenance:mode disable

.PHONY: local-services-start
local-services-start:
	docker compose --file=docker-compose.local.yml --env-file=.env up -d

.PHONY: local-services-stop
local-services-stop:
	docker compose --file=docker-compose.local.yml --env-file=.env stop

.PHONY: create-admin
create-admin:
	docker compose --file=docker-compose.kosmcode.yml --env-file=.env exec --user=1000 app artisan user:create:admin

.PHONY: code-analyse
code-analyse:
	php ./vendor/bin/grumphp run

PHONY: test-unit
test-unit:
	docker compose --file=docker-compose.kosmcode.yml --env-file=.env exec --user=1000 app /var/www/html/vendor/bin/phpunit --testsuite Unit

PHONY: test-feature
test-feature:
	docker compose --file=docker-compose.kosmcode.yml --env-file=.env exec --user=1000 app /var/www/html/vendor/bin/phpunit --testsuite Feature

.PHONY: test-coverage
test-coverage:
	docker compose --file=docker-compose.kosmcode.yml --env-file=.env exec --user=1000 app /var/www/html/vendor/bin/phpunit --coverage-html /var/www/html/tests/Reports/Coverage
