# KosmCODE Core

## About:

Simple SPA CMS

## Used:

* Laravel 10
* Inertia VUE
* Laravel backpack
* GraphQL
* and more...

## Setup guide
1. Docker
```bash
make build
make up
```
2. Setup by make command
```bash
make setup-new
```
3. If you want optional docker services
```bash
make local-services-start
```
4. Make user admin
```bash
make create-admin
```
4. Enjoy :)

## Useful
* Entry to the docker app container shell
```bash
make shell 
```
* Run unit tests in docker app container
```bash
make test-unit
```
* Run unit tests with coverage in docker app container
```bash
make test-unit-coverage
```
* Make static code analyse in docker app container
```bash
make code-analyse
```

## Others:
* [Test coverage](./tests/Reports/Coverage/index.html)
