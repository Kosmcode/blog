<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */

namespace App\Models{
    /**
     * App\Models\Article
     *
     * @property int $id
     * @property int $category_id
     * @property string $title
     * @property string $content
     * @property string|null $image
     * @property int $status
     * @property int $featured
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property string|null $deleted_at
     * @property-read Category $category
     * @property-read Slug|null $slug
     * @property-read Collection|Tag[] $tags
     * @property-read int|null $tags_count
     *
     * @method static Builder|Article newModelQuery()
     * @method static Builder|Article newQuery()
     * @method static Builder|Article query()
     * @method static Builder|Article whereCategoryId($value)
     * @method static Builder|Article whereContent($value)
     * @method static Builder|Article whereCreatedAt($value)
     * @method static Builder|Article whereDeletedAt($value)
     * @method static Builder|Article whereFeatured($value)
     * @method static Builder|Article whereId($value)
     * @method static Builder|Article whereImage($value)
     * @method static Builder|Article whereStatus($value)
     * @method static Builder|Article whereTitle($value)
     * @method static Builder|Article whereUpdatedAt($value)
     *
     * @mixin Eloquent
     */
    class Article extends \Eloquent {}
}

namespace App\Models{
    /**
     * App\Models\Category
     *
     * @property int $id
     * @property int|null $parent_id
     * @property int|null $lft
     * @property int|null $rgt
     * @property int|null $depth
     * @property string $name
     * @property string|null $slug
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property string|null $deleted_at
     * @property-read Collection|Article[] $articles
     * @property-read int|null $articles_count
     * @property-read Collection|Category[] $children
     * @property-read int|null $children_count
     * @property-read mixed $slug_or_name
     * @property-read Category|null $parent
     *
     * @method static Builder|Category findSimilarSlugs(string $attribute, array $config, string $slug)
     * @method static Builder|Category firstLevelItems()
     * @method static Builder|Category newModelQuery()
     * @method static Builder|Category newQuery()
     * @method static Builder|Category query()
     * @method static Builder|Category whereCreatedAt($value)
     * @method static Builder|Category whereDeletedAt($value)
     * @method static Builder|Category whereDepth($value)
     * @method static Builder|Category whereId($value)
     * @method static Builder|Category whereLft($value)
     * @method static Builder|Category whereName($value)
     * @method static Builder|Category whereParentId($value)
     * @method static Builder|Category whereRgt($value)
     * @method static Builder|Category whereSlug($value)
     * @method static Builder|Category whereUpdatedAt($value)
     * @method static Builder|Category withUniqueSlugConstraints(Model $model, string $attribute, array $config, string $slug)
     *
     * @mixin Eloquent
     * @mixin \Eloquent
     */
    class Category extends \Eloquent {}
}

namespace App\Models{
    /**
     * App\Models\Contact
     *
     * @property int $id
     * @property string $firstname
     * @property string $lastname
     * @property string $email
     * @property string $message
     * @property string $subject
     * @property string $ip
     * @property int $queued
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     *
     * @method static Builder|Contact newModelQuery()
     * @method static Builder|Contact newQuery()
     * @method static Builder|Contact query()
     * @method static Builder|Contact whereCreatedAt($value)
     * @method static Builder|Contact whereEmail($value)
     * @method static Builder|Contact whereFirstname($value)
     * @method static Builder|Contact whereId($value)
     * @method static Builder|Contact whereLastname($value)
     * @method static Builder|Contact whereMessage($value)
     * @method static Builder|Contact whereUpdatedAt($value)
     * @method static Builder|Contact whereIp($value)
     * @method static Builder|Contact whereQueued($value)
     * @method static Builder|Contact whereSubject($value)
     *
     * @mixin Eloquent
     */
    class Contact extends \Eloquent {}
}

namespace App\Models{
    /**
     * App\Models\Menu
     *
     * @property int $id
     * @property string $name
     * @property string|null $link
     * @property int $type value from MenuItemsTypeEnum
     * @property int|null $parent_id
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property string|null $deleted_at
     * @property string|null $fa_icon Field with fa icon data
     * @property int $lft
     * @property int $rgt
     * @property int $depth
     * @property-read Collection|MenuItem[] $children
     * @property-read int|null $children_count
     * @property-read MenuItem|null $parent
     *
     * @method static Builder|MenuItem newModelQuery()
     * @method static Builder|MenuItem newQuery()
     * @method static Builder|MenuItem query()
     * @method static Builder|MenuItem whereCreatedAt($value)
     * @method static Builder|MenuItem whereDeletedAt($value)
     * @method static Builder|MenuItem whereId($value)
     * @method static Builder|MenuItem whereLink($value)
     * @method static Builder|MenuItem whereName($value)
     * @method static Builder|MenuItem whereParentId($value)
     * @method static Builder|MenuItem whereType($value)
     * @method static Builder|MenuItem whereUpdatedAt($value)
     * @method static Builder|MenuItem whereDepth($value)
     * @method static Builder|MenuItem whereFaIcon($value)
     * @method static Builder|MenuItem whereLft($value)
     * @method static Builder|MenuItem whereRgt($value)
     *
     * @mixin Eloquent
     */
    class MenuItem extends \Eloquent {}
}

namespace App\Models{
    /**
     * App\Models\ModelHasRole
     *
     * @property int $role_id
     * @property string $model_type
     * @property int $model_id
     *
     * @method static Builder|ModelHasRole newModelQuery()
     * @method static Builder|ModelHasRole newQuery()
     * @method static Builder|ModelHasRole query()
     * @method static Builder|ModelHasRole whereModelId($value)
     * @method static Builder|ModelHasRole whereModelType($value)
     * @method static Builder|ModelHasRole whereRoleId($value)
     *
     * @mixin Eloquent
     */
    class ModelHasRole extends \Eloquent {}
}

namespace App\Models{
    /**
     * App\Models\Page
     *
     * @property int $id
     * @property string $name
     * @property string $title
     * @property string|null $content
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property string|null $deleted_at
     * @property-read Slug|null $slug
     *
     * @method static Builder|Page findSimilarSlugs(string $attribute, array $config, string $slug)
     * @method static Builder|Page newModelQuery()
     * @method static Builder|Page newQuery()
     * @method static Builder|Page query()
     * @method static Builder|Page whereContent($value)
     * @method static Builder|Page whereCreatedAt($value)
     * @method static Builder|Page whereDeletedAt($value)
     * @method static Builder|Page whereId($value)
     * @method static Builder|Page whereName($value)
     * @method static Builder|Page whereSlug(string $slug)
     * @method static Builder|Page whereTitle($value)
     * @method static Builder|Page whereUpdatedAt($value)
     * @method static Builder|Page withUniqueSlugConstraints(Model $model, string $attribute, array $config, string $slug)
     *
     * @mixin Eloquent
     */
    class Page extends \Eloquent {}
}

namespace App\Models{
    /**
     * App\Models\PageElement
     *
     * @property int $id
     * @property string $title
     * @property string $description
     * @property string $content
     * @property int $enable
     *
     * @method static Builder|PageElement newModelQuery()
     * @method static Builder|PageElement newQuery()
     * @method static Builder|PageElement query()
     * @method static Builder|PageElement whereContent($value)
     * @method static Builder|PageElement whereDescription($value)
     * @method static Builder|PageElement whereEnable($value)
     * @method static Builder|PageElement whereId($value)
     * @method static Builder|PageElement whereTitle($value)
     *
     * @mixin Eloquent
     */
    class PageElement extends \Eloquent {}
}

namespace App\Models{
    /**
     * App\Models\Permission
     *
     * @property int $id
     * @property string $name
     * @property string $guard_name
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property-read Collection|OriginalPermission[] $permissions
     * @property-read int|null $permissions_count
     * @property-read Collection|\Spatie\Permission\Models\Role[] $roles
     * @property-read int|null $roles_count
     * @property-read Collection|User[] $users
     * @property-read int|null $users_count
     *
     * @method static Builder|Permission newModelQuery()
     * @method static Builder|Permission newQuery()
     * @method static Builder|Permission permission($permissions)
     * @method static Builder|Permission query()
     * @method static Builder|Permission role($roles, $guard = null)
     * @method static Builder|Permission whereCreatedAt($value)
     * @method static Builder|Permission whereGuardName($value)
     * @method static Builder|Permission whereId($value)
     * @method static Builder|Permission whereName($value)
     * @method static Builder|Permission whereUpdatedAt($value)
     *
     * @mixin Eloquent
     */
    class Permission extends \Eloquent {}
}

namespace App\Models{
    /**
     * App\Models\Role
     *
     * @property int $id
     * @property string $name
     * @property string $guard_name
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property-read Collection|\Spatie\Permission\Models\Permission[] $permissions
     * @property-read int|null $permissions_count
     * @property-read Collection|User[] $users
     * @property-read int|null $users_count
     *
     * @method static Builder|Role newModelQuery()
     * @method static Builder|Role newQuery()
     * @method static Builder|Role permission($permissions)
     * @method static Builder|Role query()
     * @method static Builder|Role whereCreatedAt($value)
     * @method static Builder|Role whereGuardName($value)
     * @method static Builder|Role whereId($value)
     * @method static Builder|Role whereName($value)
     * @method static Builder|Role whereUpdatedAt($value)
     *
     * @mixin Eloquent
     */
    class Role extends \Eloquent {}
}

namespace App\Models{
    /**
     * App\Models\Setting
     *
     * @property int $id
     * @property string $key
     * @property string $name
     * @property string|null $description
     * @property string|null $value
     * @property string $field
     * @property int $active
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     *
     * @method static Builder|Setting newModelQuery()
     * @method static Builder|Setting newQuery()
     * @method static Builder|Setting query()
     * @method static Builder|Setting whereActive($value)
     * @method static Builder|Setting whereCreatedAt($value)
     * @method static Builder|Setting whereDescription($value)
     * @method static Builder|Setting whereField($value)
     * @method static Builder|Setting whereId($value)
     * @method static Builder|Setting whereKey($value)
     * @method static Builder|Setting whereName($value)
     * @method static Builder|Setting whereUpdatedAt($value)
     * @method static Builder|Setting whereValue($value)
     *
     * @mixin Eloquent
     */
    class Setting extends \Eloquent {}
}

namespace App\Models{
    /**
     * App\Models\Slug
     *
     * @property int $id
     * @property string $slug
     * @property string $contentable_type
     * @property int $contentable_id
     * @property string|null $meta_title
     * @property string|null $meta_description
     * @property string|null $meta_keywords
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     *
     * @method static Builder|Slug newModelQuery()
     * @method static Builder|Slug newQuery()
     * @method static Builder|Slug query()
     * @method static Builder|Slug whereContentableId($value)
     * @method static Builder|Slug whereContentableType($value)
     * @method static Builder|Slug whereCreatedAt($value)
     * @method static Builder|Slug whereId($value)
     * @method static Builder|Slug whereMetaDescription($value)
     * @method static Builder|Slug whereMetaKeywords($value)
     * @method static Builder|Slug whereMetaTitle($value)
     * @method static Builder|Slug whereSlug($value)
     * @method static Builder|Slug whereUpdatedAt($value)
     *
     * @property-read Model|Eloquent $contentable
     *
     * @mixin Eloquent
     */
    class Slug extends \Eloquent {}
}

namespace App\Models{
    /**
     * App\Models\Tag
     *
     * @property int $id
     * @property string $name
     * @property string $slug
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property string|null $deleted_at
     * @property-read Collection|\Backpack\NewsCRUD\app\Models\Article[] $articles
     * @property-read int|null $articles_count
     * @property-read mixed $slug_or_name
     *
     * @method static Builder|Tag findSimilarSlugs(string $attribute, array $config, string $slug)
     * @method static Builder|Tag newModelQuery()
     * @method static Builder|Tag newQuery()
     * @method static Builder|Tag query()
     * @method static Builder|Tag whereCreatedAt($value)
     * @method static Builder|Tag whereDeletedAt($value)
     * @method static Builder|Tag whereId($value)
     * @method static Builder|Tag whereName($value)
     * @method static Builder|Tag whereSlug($value)
     * @method static Builder|Tag whereUpdatedAt($value)
     * @method static Builder|Tag withUniqueSlugConstraints(Model $model, string $attribute, array $config, string $slug)
     *
     * @mixin Eloquent
     * @mixin \Eloquent
     */
    class Tag extends \Eloquent {}
}

namespace App\Models{
    /**
     * App\Models\User
     *
     * @property int $id
     * @property string $name
     * @property string $email
     * @property Carbon|null $email_verified_at
     * @property string $password
     * @property string|null $remember_token
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
     * @property-read int|null $notifications_count
     * @property-read Collection|\Spatie\Permission\Models\Permission[] $permissions
     * @property-read int|null $permissions_count
     * @property-read Collection|\Spatie\Permission\Models\Role[] $roles
     * @property-read int|null $roles_count
     * @property-read Collection|PersonalAccessToken[] $tokens
     * @property-read int|null $tokens_count
     *
     * @method static UserFactory factory(...$parameters)
     * @method static Builder|User newModelQuery()
     * @method static Builder|User newQuery()
     * @method static Builder|User permission($permissions)
     * @method static Builder|User query()
     * @method static Builder|User role($roles, $guard = null)
     * @method static Builder|User whereCreatedAt($value)
     * @method static Builder|User whereEmail($value)
     * @method static Builder|User whereEmailVerifiedAt($value)
     * @method static Builder|User whereId($value)
     * @method static Builder|User whereName($value)
     * @method static Builder|User wherePassword($value)
     * @method static Builder|User whereRememberToken($value)
     * @method static Builder|User whereUpdatedAt($value)
     *
     * @mixin Eloquent
     */
    class User extends \Eloquent {}
}
