<?php

namespace App\Console\Commands\Mail;

use App\Mail\Mail\TestMail;
use App\Services\SettingServiceInterface;
use Illuminate\Console\Command;
use Illuminate\Mail\Mailer;

class TestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:mail:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to test send mail';

    /**
     * Execute the console command.
     */
    public function handle(
        Mailer $mailer,
        SettingServiceInterface $settingService
    ): void {
        while (true) {
            $emailToSend = $this->ask('E-m@il address to send test mail');

            if (filter_var($emailToSend, FILTER_VALIDATE_EMAIL)) {
                break;
            }

            $this->error('Wrong e-m@il address');
        }

        $mailer
            ->to($emailToSend)
            ->send(new TestMail($settingService));
    }
}
