<?php

namespace App\Console\Commands\Maintenance\Mode;

use App\Enums\Maintenance\ModeEnum;
use App\Services\Maintenance\ModeServiceInterface;
use Illuminate\Console\Command;

/**
 * Command of maintenance mode
 */
class SetCommand extends Command
{
    public const COMMAND_NAME = 'maintenance:mode';

    public const ARGUMENT_MODE_NAME = 'mode';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = self::COMMAND_NAME.' {'.self::ARGUMENT_MODE_NAME.'? : Mode of maintenance mode to set}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to set maintenance mode';

    /**
     * Execute the console command.
     */
    public function handle(ModeServiceInterface $maintenanceModeService): int
    {
        $maintenanceModeEnum = $this->getModeArgument();

        if (! $maintenanceModeEnum) {
            return self::FAILURE;
        }

        $maintenanceModeService->setMaintenanceMode((bool) $maintenanceModeEnum->value);

        $this->info('Maintenance mode app set to: '.$maintenanceModeEnum->name);

        return self::SUCCESS;
    }

    protected function getModeArgument(): ?ModeEnum
    {
        $argumentMode = $this->argument(self::ARGUMENT_MODE_NAME);

        if (! $argumentMode) {
            return $this->getModeArgumentFromSelect();
        }

        return $this->getGivenModeArgument($argumentMode);
    }

    protected function getModeArgumentFromSelect(): ?ModeEnum
    {
        $chasedMaintenanceMode = $this->choice(
            'Which mode do you want to set?',
            ModeEnum::toArray()
        );

        return ModeEnum::fromNames(
            (is_array($chasedMaintenanceMode))
                ? $chasedMaintenanceMode[0]
                : $chasedMaintenanceMode
        );
    }

    protected function getGivenModeArgument(string $argumentMode): ?ModeEnum
    {
        $maintenanceModeEnum = ModeEnum::fromNames($argumentMode);

        if (! $maintenanceModeEnum) {
            $this->error(
                'Possible values to argument `'.self::ARGUMENT_MODE_NAME.'`: '
                .implode(', ', ModeEnum::toNames())
            );

            return null;
        }

        return $maintenanceModeEnum;
    }
}
