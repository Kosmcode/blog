<?php

namespace App\Console\Commands\Sitemap;

use App\Services\Sitemap\GenerateServiceInterface;
use Illuminate\Console\Command;

class GenerateCommand extends Command
{
    public const COMMAND_SIGNATURE = 'sitemap:generate';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = self::COMMAND_SIGNATURE;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to generate sitemap';

    /**
     * Execute the console command.
     */
    public function handle(GenerateServiceInterface $sitemapGenerateService): int
    {
        if (! $sitemapGenerateService->generateSimpleTextSitemap()) {
            $this->output->error('Error generate sitemap');

            return self::FAILURE;
        }

        $this->output->success('Sitemap generated !');

        return self::SUCCESS;
    }
}
