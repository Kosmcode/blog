<?php

namespace App\Console\Commands\User\Admin;

use App\Services\LocalizationService;
use App\Services\LocalizationServiceInterface;
use App\Services\User\Admin\CreateCommandService;
use App\Services\User\Admin\CreateCommandServiceInterface;
use Illuminate\Console\Command;

/**
 * Command to create user with admin role
 */
class CreateCommand extends Command
{
    public const COMMAND_NAME = 'user:admin:create';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = self::COMMAND_NAME;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to create User with admin role';

    /**
     * Execute the console command.
     */
    public function handle(
        CreateCommandServiceInterface $userCreateAdminService,
        LocalizationService $localizationService
    ): int {
        $localizationService->setLangPattern(CreateCommandService::LANG_KEY_PATTERN);

        $this->info($localizationService->get('info.start'));

        $userEmail = $this->getUserEmail($userCreateAdminService, $localizationService);

        if (! $userEmail) {
            return self::FAILURE;
        }

        $username = $this->getUsername($userCreateAdminService, $localizationService);

        if (! $username) {
            return self::FAILURE;
        }

        $userPassword = $this->getUserPassword($userCreateAdminService, $localizationService);

        if (! $userPassword) {
            return self::FAILURE;
        }

        $result = $userCreateAdminService->createUserWithAdminRole($userEmail, $username, $userPassword);

        if (! $result) {
            $this->error($userCreateAdminService->getNotification() ?? '');

            return self::FAILURE;
        }

        $this->info($userCreateAdminService->getNotification() ?? '');

        return self::SUCCESS;
    }

    /**
     * Method to get user email
     */
    private function getUserEmail(
        CreateCommandServiceInterface $userCreateAdminService,
        LocalizationServiceInterface $localizationService
    ): ?string {
        while (true) {
            $userEmail = $this->ask($localizationService->get('asks.email'));

            if (! is_string($userEmail)) {
                return null;
            }

            if ($this->checkTypeExitInConsole($userEmail)) {
                return null;
            }

            if (! $userCreateAdminService->isCorrectUserEmail($userEmail)) {
                $this->warn($userCreateAdminService->getNotification() ?? '');

                continue;
            }

            return $userEmail;
        }
    }

    /**
     * Method to get username
     */
    private function getUsername(
        CreateCommandServiceInterface $userCreateAdminService,
        LocalizationService $localizationService
    ): ?string {
        while (true) {
            $username = $this->ask($localizationService->get('asks.username'));

            if (! is_string($username)) {
                return null;
            }

            if ($this->checkTypeExitInConsole($username)) {
                return null;
            }

            if (! $userCreateAdminService->isCorrectUsername($username)) {
                $this->warn($userCreateAdminService->getNotification() ?? '');

                continue;
            }

            return $username;
        }
    }

    /**
     * Method to get user passwords
     */
    private function getUserPassword(
        CreateCommandServiceInterface $userCreateAdminService,
        LocalizationService $localizationService
    ): ?string {
        while (true) {
            $userPassword = $this->secret($localizationService->get('asks.password'));

            if (! is_string($userPassword)) {
                return null;
            }

            if ($this->checkTypeExitInConsole($userPassword)) {
                return null;
            }

            $userPasswordRepeated = $this->secret($localizationService->get('asks.passwordConfirmation'));

            if (! is_string($userPasswordRepeated)) {
                return null;
            }

            if ($this->checkTypeExitInConsole($userPasswordRepeated)) {
                return null;
            }

            if (! $userCreateAdminService->isCorrectUserPassword($userPassword, $userPasswordRepeated)) {
                $this->warn($userCreateAdminService->getNotification() ?? '');

                continue;
            }

            return $userPassword;
        }
    }

    /**
     * Method to check if the user has passed `exit`
     */
    private function checkTypeExitInConsole(?string $value): bool
    {
        return $value === 'exit';
    }
}
