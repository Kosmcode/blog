<?php

namespace App\Console;

use App\Console\Commands\Sitemap\GenerateCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    //    /**
    //     * The Artisan commands provided by your application.
    //     *
    //     * @var array
    //     */
    //    protected $commands = [
    //        //
    //    ];

    /**
     * {@inheritDoc}
     */
    // @codeCoverageIgnoreStart
    protected function schedule(Schedule $schedule): void
    {
        $schedule
            ->command(GenerateCommand::class)
            ->daily();
        $schedule
            ->command('backup:run')
            ->daily();
    }
    // @codeCoverageIgnoreEnd

    /**
     * Register the commands for the application.
     */
    protected function commands(): void
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
