<?php

namespace App\Dto;

use App\Models\Article;
use Illuminate\Support\Collection;

class ArticleDTO
{
    protected Article $article;

    protected Collection $recentArticles;

    public function getArticle(): Article
    {
        return $this->article;
    }

    public function setArticle(Article $article): self
    {
        $this->article = $article;

        return $this;
    }

    public function getRecentArticles(): Collection
    {
        return $this->recentArticles;
    }

    public function setRecentArticles(Collection $recentArticles): self
    {
        $this->recentArticles = $recentArticles;

        return $this;
    }
}
