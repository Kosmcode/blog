<?php

namespace App\Dto;

use Illuminate\Contracts\Pagination\LengthAwarePaginator as ContractsLengthAwarePaginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

/**
 * DTO of Articles Filters
 */
class ArticlesFiltersDTO
{
    protected LengthAwarePaginator|ContractsLengthAwarePaginator $paginatedArticles;

    protected FiltersMultipleDTO $categories;

    protected FiltersMultipleDTO $tags;

    protected Collection $recentArticles;

    protected int $pageLimit;

    public function getPaginatedArticles(): LengthAwarePaginator|ContractsLengthAwarePaginator
    {
        return $this->paginatedArticles;
    }

    public function setPaginatedArticles(
        LengthAwarePaginator|ContractsLengthAwarePaginator $paginatedArticles
    ): ArticlesFiltersDTO {
        $this->paginatedArticles = $paginatedArticles;

        return $this;
    }

    public function getCategories(): FiltersMultipleDTO
    {
        return $this->categories;
    }

    public function setCategories(FiltersMultipleDTO $categories): ArticlesFiltersDTO
    {
        $this->categories = $categories;

        return $this;
    }

    public function getTags(): FiltersMultipleDTO
    {
        return $this->tags;
    }

    public function setTags(FiltersMultipleDTO $tags): ArticlesFiltersDTO
    {
        $this->tags = $tags;

        return $this;
    }

    public function getRecentArticles(): Collection
    {
        return $this->recentArticles;
    }

    public function setRecentArticles(Collection $recentArticles): self
    {
        $this->recentArticles = $recentArticles;

        return $this;
    }

    public function getPageLimit(): int
    {
        return $this->pageLimit;
    }

    public function setPageLimit(int $pageLimit): self
    {
        $this->pageLimit = $pageLimit;

        return $this;
    }
}
