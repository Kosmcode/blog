<?php

namespace App\Dto;

use App\Models\PageElement;

class ContactResourceDTO
{
    protected bool $canSend;

    protected ?PageElement $topPageElement;

    protected ?PageElement $downPageElement;

    protected ?bool $result = null;

    public function isCanSend(): bool
    {
        return $this->canSend;
    }

    public function setCanSend(bool $canSend): self
    {
        $this->canSend = $canSend;

        return $this;
    }

    public function getTopPageElement(): ?PageElement
    {
        return $this->topPageElement;
    }

    public function setTopPageElement(?PageElement $topPageElement): self
    {
        $this->topPageElement = $topPageElement;

        return $this;
    }

    public function getDownPageElement(): ?PageElement
    {
        return $this->downPageElement;
    }

    public function setDownPageElement(?PageElement $downPageElement): self
    {
        $this->downPageElement = $downPageElement;

        return $this;
    }

    public function getResult(): ?bool
    {
        return $this->result;
    }

    public function setResult(?bool $result): self
    {
        $this->result = $result;

        return $this;
    }
}
