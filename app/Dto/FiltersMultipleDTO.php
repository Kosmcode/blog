<?php

namespace App\Dto;

use App\Http\Resources\Filters\FiltersMultipleResource;

/**
 * DTO of FiltersMultiple
 */
class FiltersMultipleDTO
{
    /**
     * @var mixed[]
     */
    protected array $values = [];

    /**
     * @var mixed[]
     */
    protected array $selectedValues = [];

    /**
     * @return mixed[]
     */
    public function getValues(): array
    {
        return $this->values;
    }

    /**
     * @param  mixed[]  $values
     */
    public function setValues(array $values): FiltersMultipleDTO
    {
        $this->values = $values;

        return $this;
    }

    /**
     * @return mixed[]
     */
    public function getSelectedValues(): array
    {
        return $this->selectedValues;
    }

    /**
     * @param  mixed[]  $selectedValues
     */
    public function setSelectedValues(array $selectedValues): FiltersMultipleDTO
    {
        $this->selectedValues = $selectedValues;

        return $this;
    }

    public function getResource(): FiltersMultipleResource
    {
        return FiltersMultipleResource::make($this);
    }
}
