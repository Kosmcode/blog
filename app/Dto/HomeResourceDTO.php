<?php

namespace App\Dto;

use App\Models\PageElement;
use Illuminate\Support\Collection;

/**
 * DTO of Home ContactResource
 */
class HomeResourceDTO
{
    protected Collection $articles;

    protected ?PageElement $topPageElement;

    protected ?PageElement $downPageElement;

    public function getArticles(): Collection
    {
        return $this->articles;
    }

    public function setRecentArticles(Collection $articles): HomeResourceDTO
    {
        $this->articles = $articles;

        return $this;
    }

    public function getTopPageElement(): ?PageElement
    {
        return $this->topPageElement;
    }

    public function setTopPageElement(?PageElement $topPageElement): self
    {
        $this->topPageElement = $topPageElement;

        return $this;
    }

    public function getDownPageElement(): ?PageElement
    {
        return $this->downPageElement;
    }

    public function setDownPageElement(?PageElement $downPageElement): self
    {
        $this->downPageElement = $downPageElement;

        return $this;
    }
}
