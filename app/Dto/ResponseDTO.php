<?php

namespace App\Dto;

use App\Models\Slug;
use App\Services\App\HeadServiceInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request as LaravelRequest;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\App;
use Inertia\Response as InertiaResponse;
use Inertia\ResponseFactory as InertiaFactory;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ResponseDTO
{
    private string $inertiaComponentName;

    /**
     * @var mixed[]|null
     */
    private ?array $responseData;

    private ?string $metaTitle;

    private ?string $metaDescription;

    private ?string $metaKeywords;

    private ?object $resourceData;

    private ?string $resourceClass;

    public function setMetaDataFromSlug(Slug $slug): ResponseDTO
    {
        $this->metaTitle = $slug->getMetaTitle();
        $this->metaDescription = $slug->getMetaDescription();
        $this->metaKeywords = $slug->getMetaKeywords();

        return $this;
    }

    public function getInertiaComponentName(): string
    {
        return $this->inertiaComponentName;
    }

    public function setInertiaComponentName(string $inertiaComponentName): ResponseDTO
    {
        $this->inertiaComponentName = $inertiaComponentName;

        return $this;
    }

    /**
     * @return mixed[]|JsonResponse
     */
    public function getResponseData(bool $jsonResponse = false): array|JsonResponse
    {
        if (isset($this->resourceClass, $this->resourceData)) {
            /** @var JsonResource $resource */
            $resource = (new $this->resourceClass($this->resourceData));
            $request = App::make(LaravelRequest::class);

            if ($jsonResponse === true) {
                return $resource->toResponse($request);
            }

            return (array) $resource->toArray($request);
        }

        return $this->responseData ?? [];
    }

    /**
     * @param  mixed[]  $responseData
     */
    public function setResponseData(array $responseData): ResponseDTO
    {
        $this->responseData = $responseData;

        return $this;
    }

    public function getMetaTitle(): ?string
    {
        return $this->metaTitle;
    }

    public function setMetaTitle(?string $metaTitle): ResponseDTO
    {
        $this->metaTitle = $metaTitle;

        return $this;
    }

    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    public function setMetaDescription(?string $metaDescription): ResponseDTO
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    public function getMetaKeywords(): ?string
    {
        return $this->metaKeywords;
    }

    public function setMetaKeywords(?string $metaKeywords): ResponseDTO
    {
        $this->metaKeywords = $metaKeywords;

        return $this;
    }

    public function setResourceData(?object $resourceData, string $resourceClass): ResponseDTO
    {
        $this->resourceData = $resourceData;
        $this->resourceClass = $resourceClass;

        return $this;
    }

    public function setAppHead(): ResponseDTO
    {
        App::make(HeadServiceInterface::class)
            ->setMetas(
                $this->metaTitle,
                $this->metaDescription,
                $this->metaKeywords
            );

        return $this;
    }

    public function renderInertiaView(): InertiaResponse
    {
        return App::make(InertiaFactory::class)
            ->render($this->inertiaComponentName, (array) $this->getResponseData());
    }

    public static function createHttpException(
        int $exceptionCode = Response::HTTP_NOT_FOUND,
        string $exceptionMessage = '',
    ): HttpException {
        return new HttpException(
            statusCode: $exceptionCode,
            message: $exceptionMessage,
            code: $exceptionCode
        );
    }
}
