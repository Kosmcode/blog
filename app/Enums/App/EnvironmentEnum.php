<?php

namespace App\Enums\App;

enum EnvironmentEnum: string
{
    case local = 'local';
    case test = 'test';
    case production = 'production';
}
