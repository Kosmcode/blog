<?php

namespace App\Enums\Article;

use App\Traits\Enum\ArrayableTrait;

enum FeatureEnum: int
{
    use ArrayableTrait;

    case notFeature = 0;
    case feature = 1;
}
