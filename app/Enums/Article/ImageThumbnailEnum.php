<?php

namespace App\Enums\Article;

use App\Enums\User\DeviceTypeEnum;

enum ImageThumbnailEnum: int
{
    case desktopHomeRecent = 1;
    case desktopArticlesPagination = 2;
    case desktopArticlesRecent = 3;
    case desktopArticleMain = 4;
    case desktopArticleRecent = 5;
    case tabletHomeRecent = 6;
    case tabletArticlesPagination = 7;
    case tabletArticlesRecent = 8;
    case tabletArticleMain = 9;
    case tabletArticleRecent = 10;
    case smartphoneHomeRecent = 11;
    case smartphoneArticlesPagination = 12;
    case smartphoneArticlesRecent = 13;
    case smartphoneArticleMain = 14;
    case smartphoneArticleRecent = 15;

    public function getWidth(): int
    {
        return match ($this) {
            self::desktopHomeRecent => 306,
            self::desktopArticlesPagination => 412,
            self::desktopArticlesRecent, self::desktopArticleRecent, self::tabletArticlesRecent, self::tabletArticleRecent, self::smartphoneArticleRecent, self::smartphoneArticlesRecent => 80,
            self::desktopArticleMain => 848,
            self::tabletHomeRecent, self::tabletArticlesPagination, self::tabletArticleMain => 516,
            self::smartphoneHomeRecent, self::smartphoneArticleMain, self::smartphoneArticlesPagination => 388,
        };
    }

    public static function getHomeRecentByUserDeviceType(string $userDeviceType): ImageThumbnailEnum
    {
        return match ($userDeviceType) {
            DeviceTypeEnum::desktop->value => self::desktopHomeRecent,
            DeviceTypeEnum::tablet->value => self::tabletHomeRecent,
            DeviceTypeEnum::phone->value => self::smartphoneHomeRecent,
            default => self::desktopHomeRecent,
        };
    }

    public static function getArticleRecentByUserDeviceType(string $userDeviceType): ImageThumbnailEnum
    {
        return match ($userDeviceType) {
            DeviceTypeEnum::desktop->value => self::desktopArticleRecent,
            DeviceTypeEnum::tablet->value => self::tabletArticleRecent,
            DeviceTypeEnum::phone->value => self::smartphoneArticleRecent,
            default => self::desktopArticleRecent,
        };
    }

    public static function getArticleMainByUserDeviceType(string $userDeviceType): ImageThumbnailEnum
    {
        return match ($userDeviceType) {
            DeviceTypeEnum::desktop->value => self::desktopArticleMain,
            DeviceTypeEnum::tablet->value => self::tabletArticleMain,
            DeviceTypeEnum::phone->value => self::smartphoneArticleMain,
            default => self::desktopArticleMain,
        };
    }

    public static function getArticlesPaginationByUserDeviceType(string $userDeviceType): ImageThumbnailEnum
    {
        return match ($userDeviceType) {
            DeviceTypeEnum::desktop->value => self::desktopArticlesPagination,
            DeviceTypeEnum::tablet->value => self::tabletArticlesPagination,
            DeviceTypeEnum::phone->value => self::smartphoneArticlesPagination,
            default => self::desktopArticlesPagination,
        };
    }

    public static function getArticlesRecentByUserDeviceType(string $userDeviceType): ImageThumbnailEnum
    {
        return match ($userDeviceType) {
            DeviceTypeEnum::desktop->value => self::desktopArticlesRecent,
            DeviceTypeEnum::tablet->value => self::tabletArticlesRecent,
            DeviceTypeEnum::phone->value => self::smartphoneArticlesRecent,
            default => self::desktopArticlesRecent,
        };
    }
}
