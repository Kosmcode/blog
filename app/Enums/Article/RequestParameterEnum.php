<?php

namespace App\Enums\Article;

enum RequestParameterEnum: string
{
    case page = 'page';
    case categories = 'categories';
    case tags = 'tags';
    case categorySlug = 'categorySlug';
    case tagSlug = 'tagSlug';
}
