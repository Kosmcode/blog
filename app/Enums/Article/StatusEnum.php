<?php

namespace App\Enums\Article;

use App\Traits\Enum\ArrayableTrait;

enum StatusEnum: int
{
    use ArrayableTrait;

    case draft = 0;
    case published = 1;
}
