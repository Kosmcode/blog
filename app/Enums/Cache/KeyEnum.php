<?php

namespace App\Enums\Cache;

enum KeyEnum: string
{
    case articlesCategories = 'articles_filters_categories';
    case articlesTags = 'articles_filters_tags';
    case menuItem = 'menu_item_inertia_nav';
    case localization = 'localization_';
    case setting = 'app_setting_';
    case recentArticles = 'recent_articles';
    case sluggable = 'slug_';
    case pageElement = 'page_element';

    public static function prepareSlugCacheKey(string $slugUri): string
    {
        return sprintf(
            '%s%s',
            self::sluggable->value,
            $slugUri
        );
    }

    public static function prepareSettingCacheKey(string $settingKeyName): string
    {
        return sprintf(
            '%s%s',
            self::setting->value,
            $settingKeyName
        );
    }

    public static function preparePageElementCacheKey(string $pageElementTitle): string
    {
        return sprintf(
            '%s%s',
            self::pageElement->value,
            $pageElementTitle
        );
    }
}
