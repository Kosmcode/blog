<?php

namespace App\Enums;

enum FilesystemDiskEnum: string
{
    case local = 'local';
    case public = 'public';
    case articleImages = 'articlesImages';
}
