<?php

namespace App\Enums\Intertia;

enum ComponentEnum: string
{
    case singleArticle = 'Article';
    case articleFilters = 'Articles';
    case home = 'Home';
    case page = 'Page';
    case contact = 'Contact';
    case error = 'Error';
}
