<?php

namespace App\Enums\Log;

enum ChannelEnum: string
{
    case localization = 'localization';
    case exception = 'exception';
    case contactQueue = 'contactQueue';
    case googleReCaptcha = 'googleReCaptcha';
}
