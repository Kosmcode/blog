<?php

namespace App\Enums\Maintenance;

use App\Traits\Enum\ArrayableTrait;

enum ModeEnum: int
{
    use ArrayableTrait;

    case enable = 1;
    case disable = 0;

    public static function fromNames(string $caseName): ?ModeEnum
    {
        return match ($caseName) {
            'enable' => self::enable,
            'disable' => self::disable,
            default => null,
        };
    }
}
