<?php

namespace App\Enums\Menu\Item;

use App\Traits\Enum\ArrayableTrait;

enum TypeEnum: int
{
    use ArrayableTrait;

    case internal = 0;
    case external = 1;
}
