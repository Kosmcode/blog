<?php

namespace App\Enums\Page;

enum ElementEnum: string
{
    case homePageTop = 'HomePageTop';
    case homePageDown = 'HomePageDown';
    case contactPageTop = 'ContactPageTop';
    case contactPageDown = 'ContactPageDown';
}
