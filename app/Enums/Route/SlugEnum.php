<?php

namespace App\Enums\Route;

use Illuminate\Support\Facades\Lang;

enum SlugEnum
{
    case home;
    case articles;
    case contact;
    case dontAcceptCookies;
    case articlesCategorySlug;
    case articlesTagSlug;

    public function route(): string
    {
        return match ($this) {
            self::articles => DIRECTORY_SEPARATOR.Lang::get('routes.articles'), // @phpstan-ignore-line
            self::contact => DIRECTORY_SEPARATOR.Lang::get('routes.contact'), // @phpstan-ignore-line
            self::home => DIRECTORY_SEPARATOR.Lang::get('routes.home'), // @phpstan-ignore-line
            self::dontAcceptCookies => DIRECTORY_SEPARATOR.Lang::get('routes.dontAcceptCookies'), // @phpstan-ignore-line
            self::articlesCategorySlug => DIRECTORY_SEPARATOR.Lang::get('routes.category').'/{categorySlug}', // @phpstan-ignore-line
            self::articlesTagSlug => DIRECTORY_SEPARATOR.Lang::get('routes.tag').'/{tagSlug}', // @phpstan-ignore-line
        };
    }

    /**
     * @return string[]
     */
    public static function getRoutesToMenuItemInternalLinks(): array
    {
        return [
            DIRECTORY_SEPARATOR.Lang::get('routes.home') => 'Static: Home', // @phpstan-ignore-line
            DIRECTORY_SEPARATOR.Lang::get('routes.articles') => 'Static: Articles', // @phpstan-ignore-line
            DIRECTORY_SEPARATOR.Lang::get('routes.contact') => 'Static: Contact', // @phpstan-ignore-line
            DIRECTORY_SEPARATOR.Lang::get('routes.gallery') => 'Static: Gallery', // @phpstan-ignore-line
        ];
    }
}
