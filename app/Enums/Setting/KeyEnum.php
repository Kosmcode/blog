<?php

namespace App\Enums\Setting;

enum KeyEnum: string
{
    case articlesMetaTitle = 'articles.meta.title';
    case articlesMetaDescription = 'articles.meta.description';
    case articlesMetaKeywords = 'articles.meta.keywords';
    case articlesDateFormat = 'articles.date.format';
    case homeMetaTitle = 'home.meta.title';
    case homeMetaDescription = 'home.meta.description';
    case homeMetaKeywords = 'home.meta.keywords';
    case contactCanSendTimeInMinutes = 'contact.canSend.timeInMinutes';
    case contactSendToMailEnabled = 'contact.sendToEmailEnable';
    case contactSendEmail = 'contact.sendEmail';
    case googleReCaptchaV3SecretKey = 'google.reCaptcha.v3.secretKey';
    case googleReCaptchaV3AcceptableScore = 'google.reCaptcha.v3.acceptableScore';
    case maintenanceModeEnabled = 'app.maintenanceModeEnabled';
    case maintenanceModeMessage = 'app.maintenanceModeMessage';
    case appName = 'app.name';
    case appUrl = 'app.url';
    case contactMetaTitle = 'contact.meta.title';
    case contactMetaDescription = 'contact.meta.description';
    case contactMetaKeywords = 'contact.meta.keywords';
    case sitemapFilesystemDriver = 'sitemap.driver';
    case sitemapFilename = 'sitemap.filename';
    case articlesPageLimit = 'articles.limit';
    case recentArticleLimit = 'home.articles.limit';
}
