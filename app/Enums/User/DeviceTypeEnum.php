<?php

namespace App\Enums\User;

enum DeviceTypeEnum: string
{
    case desktop = 'desktop';
    case tablet = 'tablet';
    case phone = 'phone';
}
