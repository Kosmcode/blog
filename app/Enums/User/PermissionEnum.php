<?php

namespace App\Enums\User;

enum PermissionEnum: string
{
    case admin = 'Admin';
    case users = 'Users';
    case articles = 'Articles';
    case articlesCategory = 'Articles category';
    case articlesTag = 'Articles tag';
    case fileManager = 'File manager';
    case pages = 'Pages';
    case menu = 'Menu';
    case settings = 'Settings';
    case contact = 'Contact';
}
