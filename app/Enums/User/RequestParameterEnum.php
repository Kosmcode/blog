<?php

namespace App\Enums\User;

enum RequestParameterEnum: string
{
    case name = 'name';
    case email = 'email';
    case password = 'password';
}
