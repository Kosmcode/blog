<?php

namespace App\Enums\User;

enum RoleEnum: string
{
    case admin = 'Admin';
    case users = 'Users';
    case articles = 'Articles';
    case fileManager = 'File manager';
    case menu = 'Menu';
    case pages = 'Pages';
    case settings = 'Settings';
    case contact = 'Contact';
}
