<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Inertia\Inertia;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class Handler extends ExceptionHandler
{
    const HTTP_SESSION_EXPIRED = 419;

    const HANDLED_ERRORS = [
        Response::HTTP_NOT_FOUND,
        Response::HTTP_SERVICE_UNAVAILABLE,
        Response::HTTP_FORBIDDEN,
        Response::HTTP_INTERNAL_SERVER_ERROR,
        self::HTTP_SESSION_EXPIRED,
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->reportable(function (Throwable $e) {
            // @codeCoverageIgnoreStart
            if (app()->bound('sentry')) {
                app('sentry')->captureException($e);
            }
            // @codeCoverageIgnoreEnd
        });
    }

    /**
     * Prepare exception for rendering.
     *
     * @throws Throwable
     */
    public function render($request, Throwable $e): JsonResponse|Response|RedirectResponse
    {
        $response = parent::render($request, $e);

        if (in_array($response->getStatusCode(), self::HANDLED_ERRORS)) {
            return Inertia::render(
                'Error',
                [
                    'status' => $response->getStatusCode(),
                ]
            )
                ->toResponse($request)
                ->setStatusCode($response->getStatusCode());
        }

        return $response;
    }
}
