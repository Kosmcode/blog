<?php

declare(strict_types=1);

namespace App\GraphQL\Scalars\Article;

use Illuminate\Support\Carbon as IlluminateCarbon;
use Nuwave\Lighthouse\Schema\Types\Scalars\DateScalar as LighthouseDateScalar;

final class DateScalar extends LighthouseDateScalar
{
    protected function format(IlluminateCarbon $carbon): string
    {
        return $carbon->format('d.m.Y');
    }

    protected function parse(mixed $value): IlluminateCarbon
    {
        return IlluminateCarbon::createFromFormat(IlluminateCarbon::DEFAULT_TO_STRING_FORMAT, $value); // @phpstan-ignore-line
    }
}
