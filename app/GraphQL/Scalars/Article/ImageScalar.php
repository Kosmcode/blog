<?php

namespace App\GraphQL\Scalars\Article;

use App\Enums\FilesystemDiskEnum;
use GraphQL\Error\Error;
use GraphQL\Language\AST\Node;
use GraphQL\Language\AST\StringValueNode;
use GraphQL\Type\Definition\ScalarType;
use Storage;

final class ImageScalar extends ScalarType
{
    public function serialize(mixed $value): mixed
    {
        return Storage::disk(FilesystemDiskEnum::articleImages->value)
            ->url($value); // @phpstan-ignore-line
    }

    public function parseValue(mixed $value): mixed
    {
        return is_string($value);
    }

    public function parseLiteral(Node $valueNode, ?array $variables = null): mixed
    {
        if (! $valueNode instanceof StringValueNode) {
            throw new Error("Query error: Can only parse strings, got {$valueNode->kind}.", $valueNode);
        }

        return $valueNode->value;
    }
}
