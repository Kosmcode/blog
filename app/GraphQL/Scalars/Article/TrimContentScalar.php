<?php

declare(strict_types=1);

namespace App\GraphQL\Scalars\Article;

use App\Traits\Article\ContentTrait;
use GraphQL\Error\Error;
use GraphQL\Language\AST\Node;
use GraphQL\Language\AST\StringValueNode;
use GraphQL\Type\Definition\ScalarType;

final class TrimContentScalar extends ScalarType
{
    use ContentTrait;

    public function serialize(mixed $value): mixed
    {
        return $this->getFirstParagraphFromString($value); // @phpstan-ignore-line
    }

    public function parseValue(mixed $value): mixed
    {
        return is_string($value);
    }

    public function parseLiteral(Node $valueNode, ?array $variables = null): mixed
    {
        if (! $valueNode instanceof StringValueNode) {
            throw new Error("Query error: Can only parse strings, got {$valueNode->kind}.", $valueNode);
        }

        return $valueNode->value;
    }
}
