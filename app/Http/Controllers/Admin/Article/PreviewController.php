<?php

namespace App\Http\Controllers\Admin\Article;

use App\Http\Controllers\Web\AbstractController;
use App\Http\Requests\Admin\Article\PreviewRequest;
use App\Services\Article\GetServiceInterface as ArticleGetServiceInterface;
use Inertia\Response;

class PreviewController extends AbstractController
{
    public function __construct(
        protected readonly ArticleGetServiceInterface $articleGetService
    ) {}

    public function getPreviewArticle(PreviewRequest $previewArticleGetRequest): Response
    {
        return $this->articleGetService // @phpstan-ignore-line
            ->getArticleToAdminPreviewByArticleId($previewArticleGetRequest->articleId)
            ->setAppHead()
            ->renderInertiaView();
    }
}
