<?php

namespace App\Http\Controllers\Admin;

use App\Enums\Article\StatusEnum;
use App\Enums\FilesystemDiskEnum;
use App\Http\Requests\Admin\Article\StoreRequest;
use App\Http\Requests\Admin\Article\UpdateRequest;
use App\Models\Article;
use App\Models\Slug;
use App\Traits\CRUD\CreateOperationTrait;
use App\Traits\CRUD\UpdateOperationTrait;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;

/**
 * @SuppressWarnings(PHPMD)
 */
class ArticleCrudController extends CrudController
{
    use CreateOperationTrait {
        store as traitStore;
    }
    use DeleteOperation,
        ListOperation;
    use UpdateOperationTrait {
        update as traitUpdate;
    }

    public function setup(): void
    {
        $this->crud->setModel(Article::class);
        $this->crud->setRoute(config('backpack.base.route_prefix', 'admin').'/article');
        $this->crud->setEntityNameStrings('article', 'articles');

        $this->crud->operation('list', function () {
            $this->crud->addColumn('title');
            $this->crud->addColumn([
                'name' => 'status',
                'label' => 'Published',
                'type' => 'check',
            ]);
            $this->crud->addColumn([
                'name' => 'featured',
                'label' => 'Featured',
                'type' => 'check',
            ]);
            $this->crud->addColumn([
                'name' => 'image',
                'label' => 'Image',
                'type' => 'image',
                'disk' => FilesystemDiskEnum::articleImages->value,
            ]);
            $this->crud->addColumn([
                'label' => 'Preview',
                'type' => 'custom_html',
                'value' => function (Article $article) {
                    return '<div style="text-align: center;"><a href="'.
                        route('admin.preview.article', ['articleId' => $article->getId()])
                        .'" target="_blank">Preview</a></div>';
                },
            ]);
            $this->crud->addColumn([
                'name' => 'created_at',
                'label' => 'Created',
                'type' => 'datetime',
            ]);
            $this->crud->addColumn([
                'name' => 'updated_at',
                'label' => 'Last update',
                'type' => 'datetime',
            ]);
        });

        $this->crud->operation(['create'], function () {
            $this->crud->setValidation(StoreRequest::class);

            $this->addFieldsBasics();
            $this->addFieldsMetas(false);
        });

        $this->crud->operation(['update'], function () {
            $this->crud->setValidation(UpdateRequest::class);

            $this->addFieldsBasics();
            $this->addFieldsMetas(true);
        });
    }

    private function addFieldsBasics(): void
    {
        $this->crud->addField([
            'name' => 'title',
            'label' => 'Title',
            'type' => 'text',
            'placeholder' => 'Your title here',
        ]);
        $this->crud->addField([
            'name' => 'content',
            'label' => 'Content',
            'type' => 'summernote',
            'placeholder' => 'Your textarea text here',
        ]);
        $this->crud->addField([
            'name' => 'image',
            'label' => 'Image',
            'type' => 'upload',
            'withFiles' => [
                'disk' => FilesystemDiskEnum::articleImages->value,
            ],
        ]);
        $this->crud->addField([
            'label' => 'Category',
            'type' => 'select',
            'name' => 'category_id',
            'entity' => 'category',
            'attribute' => 'name',
        ]);
        $this->crud->addField([
            'label' => 'Tags',
            'type' => 'select_multiple',
            'name' => 'tags',
            'entity' => 'tags',
            'attribute' => 'name',
        ]);
        $this->crud->addField([
            'name' => 'status',
            'label' => 'Status',
            'type' => 'enum',
            'enum_class' => StatusEnum::class,
        ]);
        $this->crud->addField([
            'name' => 'featured',
            'label' => 'Featured item',
            'type' => 'switch',
        ]);
    }

    private function addFieldsMetas(bool $updateOperation): void
    {
        $articleSlug = null;

        if ($updateOperation === true) {
            $article = $this->crud->getCurrentEntry();
            $articleSlug = $article->slug;
        }

        $this->crud->addField([
            'name' => 'separator',
            'type' => 'custom_html',
            'value' => '<h2>Metas</h2><hr>',
        ]);

        $this->crud->addField([
            'name' => 'meta_title',
            'label' => 'Meta title',
            'type' => 'text',
            'value' => ($articleSlug instanceof Slug) ? $articleSlug->getMetaTitle() : '',
            'hint' => 'If this field is empty value will get from Title',
        ]);
        $this->crud->addField([
            'name' => 'meta_slug',
            'label' => 'Slug',
            'type' => 'text',
            'value' => ($articleSlug instanceof Slug) ? $articleSlug->getSlug() : '',
            'hint' => 'If this field is empty value will get from Meta title',
        ]);
        $this->crud->addField([
            'name' => 'meta_description',
            'label' => 'Meta description',
            'type' => 'textarea',
            'value' => ($articleSlug instanceof Slug) ? $articleSlug->getMetaDescription() : '',
        ]);
        $this->crud->addField([
            'name' => 'meta_keywords',
            'label' => 'Meta keywords',
            'type' => 'textarea',
            'value' => ($articleSlug instanceof Slug) ? $articleSlug->getMetaKeywords() : '',
        ]);
    }
}
