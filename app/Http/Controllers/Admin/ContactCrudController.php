<?php

namespace App\Http\Controllers\Admin;

use App\Models\Contact;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

class ContactCrudController extends CrudController
{
    use DeleteOperation, ListOperation, ShowOperation;

    public function setup(): void
    {
        $this->crud->setModel(Contact::class);
        $this->crud->setRoute(config('backpack.base.route_prefix').'/contact');
        $this->crud->setEntityNameStrings('contact', 'contacts');
    }

    protected function setupListOperation(): void
    {
        $this->crud->column('firstname')->type('string');
        $this->crud->column('lastname')->type('string');
        $this->crud->column('email')->type('email');
        $this->crud->column('subject')->type('string');
        $this->crud->column('created_at')->type('datetime')->label('Created');
        $this->crud->column('queued')->type('boolean');
    }

    protected function setupShowOperation(): void
    {
        $this->crud->column('firstname')->type('string');
        $this->crud->column('lastname')->type('string');
        $this->crud->column('email')->type('email');
        $this->crud->column('message')->type('text');
        $this->crud->column('subject')->type('string');
        $this->crud->column('ip')->type('ip');
        $this->crud->column('created_at')->type('datetime')->label('Created');
        $this->crud->column('queued')->type('boolean');
        $this->crud->column('updated_at')->type('datetime')->label('Queued time');
    }
}
