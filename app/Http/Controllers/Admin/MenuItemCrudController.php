<?php

namespace App\Http\Controllers\Admin;

use App\Enums\Cache\KeyEnum;
use App\Enums\Menu\Item\TypeEnum;
use App\Http\Requests\Admin\Menu\Item\StoreRequest;
use App\Http\Requests\Admin\Menu\Item\UpdateRequest;
use App\Models\MenuItem;
use App\Services\MenuItem\GetServiceInterface;
use App\Services\MenuItem\Internal\LinkFactoryInterface;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\ValidationException;

class MenuItemCrudController extends CrudController
{
    use CreateOperation {
        store as traitStore;
    }
    use DeleteOperation, ListOperation, ReorderOperation;
    use UpdateOperation {
        update as traitUpdate;
    }

    public function setup()
    {
        $this->crud->setModel(MenuItem::class);
        $this->crud->setRoute(config('backpack.base.route_prefix').'/menu-item');
        $this->crud->setEntityNameStrings('menu item', 'menu items');
    }

    public function setupListOperation()
    {
        $this->crud->addColumn([
            'name' => 'name',
            'label' => 'Label',
        ]);
        $this->crud->addColumn([
            'name' => 'fa_icon',
            'label' => 'Icon',
            'type' => 'custom_html',
            'value' => function (MenuItem $menuItem) {
                if (empty($menuItem->getFaIcon())) {
                    return '';
                }

                return '<div class="text-center"><i style="width: 10px; height: 10px;" class="'.$menuItem->getFaIcon().'"></i></div>';
            },
        ]);
        $this->crud->addColumn([
            'label' => 'Parent',
            'type' => 'select',
            'name' => 'parent_id',
            'entity' => 'parent',
            'attribute' => 'name',
            'model' => MenuItem::class,
        ]);
        $this->crud->addColumn([
            'label' => 'Type',
            'type' => 'select_from_array',
            'name' => 'type',
            'options' => TypeEnum::toArray(),
        ]);
        $this->crud->addColumn([
            'label' => 'Link',
            'type' => 'text',
            'name' => 'link',
        ]);
    }

    public function setupCreateOperation()
    {
        $this->crud->setValidation(StoreRequest::class);

        $this->addFields(false);
    }

    public function setupUpdateOperation()
    {
        $this->crud->setValidation(UpdateRequest::class);

        $this->addFields(true);
    }

    public function store(): Response|array|RedirectResponse
    {
        $this->prepareLinkToOperations();

        return $this->traitStore();
    }

    public function update(): Response|array|RedirectResponse
    {
        $this->prepareLinkToOperations();

        return $this->traitUpdate();
    }

    protected function setupReorderOperation(): void
    {
        $this->crud->set('reorder.label', 'name');
        $this->crud->set('reorder.max_level', 3);
    }

    /**
     * {@inheritDoc}
     */
    public function saveReorder()
    {
        $this->crud->hasAccessOrFail('reorder');

        $allEntries = json_decode(\Request::input('tree'), true);

        if (! count($allEntries)) {
            return false;
        }

        $count = $this->crud->updateTreeOrder($allEntries);

        $menuItemService = App::make(GetServiceInterface::class);
        $menuItemService->forgetByKeyEnum(KeyEnum::menuItem);

        return 'success for '.$count.' items';
    }

    private function prepareLinkToOperations(): void
    {
        $requestData = $this->crud->getRequest()->request->all();

        if (empty($requestData['internalLink']) && empty($requestData['externalLink'])) {
            throw ValidationException::withMessages([
                'internalLink' => 'You must choice one from this',
                'externalLink' => 'or type url',
            ]);
        }

        $this->crud->getRequest()->request->remove('externalLink');
        $this->crud->getRequest()->request->remove('internalLink');

        if (! empty($requestData['internalLink'])) {
            $this->crud->getRequest()->request->set('link', $requestData['internalLink']);
            $this->crud->getRequest()->request->set('type', TypeEnum::internal->value);

            return;
        }

        $this->crud->getRequest()->request->set('link', $requestData['externalLink']);
        $this->crud->getRequest()->request->set('type', TypeEnum::external->value);
    }

    private function getMenuItemsParentListToField(): array
    {
        $mainMenuItems = MenuItem::query()
            ->whereNull('parent_id')
            ->get();

        $menuItemsList = [];

        foreach ($mainMenuItems as $menuItem) {
            $menuItemsList[$menuItem->getId()] = $menuItem->getName();

            if ($menuItem->children_count === 0) {
                continue;
            }

            foreach ($menuItem->children as $menuItemChildren) {
                $menuItemsList[$menuItemChildren->getId()] = '- '.$menuItemChildren->getName();
            }
        }

        return $menuItemsList;
    }

    private function addFields(bool $updateOperation): void
    {
        $internalLinksList = app(LinkFactoryInterface::class)
            ->getArrayLinksToAdmin();

        $this->crud->addField([
            'name' => 'name',
            'label' => 'Label',
        ]);
        $this->crud->addField([
            'name' => 'fa_icon',
            'label' => 'Icon',
            'type' => 'text',
            'hint' => 'Give classes of fa icons library or leave blank',
        ]);
        $this->crud->addField([
            'name' => 'parent_id',
            'label' => 'Parent',
            'type' => 'select_from_array',
            'options' => $this->getMenuItemsParentListToField(),
            'allows_null' => true,
        ]);
        $this->crud->addField([
            'name' => 'type',
            'type' => 'hidden',
            'value' => '',
        ]);
        $this->crud->addField([
            'name' => 'link',
            'type' => 'hidden',
            'value' => '',
        ]);

        $internalLinkFieldData = [
            'name' => 'internalLink',
            'label' => 'Internal Link',
            'type' => 'select_from_array',
            'options' => $internalLinksList,
            'allows_null' => true,
        ];

        $externalLinkFieldData = [
            'name' => 'externalLink',
            'label' => 'External Link',
            'type' => 'url',
        ];

        if ($updateOperation === true) {
            $menuItem = $this->crud->getCurrentEntry();

            if (! $menuItem instanceof MenuItem) {
                throw new Exception('Current entry is not Menu');
            }

            if ($menuItem->getType() === TypeEnum::external->value) {
                $externalLinkFieldData['value'] = $menuItem->getLink();
            }

            if ($menuItem->getType() === TypeEnum::internal->value) {
                $internalLinkFieldData['default'] = $menuItem->getLink();
            }
        }

        $this->crud->addField($internalLinkFieldData);
        $this->crud->addField($externalLinkFieldData);
    }
}
