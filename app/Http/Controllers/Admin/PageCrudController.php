<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Page\StoreRequest;
use App\Http\Requests\Admin\Page\UpdateRequest;
use App\Models\Page;
use App\Models\Slug;
use App\Traits\CRUD\CreateOperationTrait;
use App\Traits\CRUD\UpdateOperationTrait;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;

class PageCrudController extends CrudController
{
    use CreateOperationTrait {
        store as traitStore;
    }
    use DeleteOperation,
        ListOperation;
    use UpdateOperationTrait {
        update as traitUpdate;
    }

    public function setup()
    {
        $this->crud->setModel(Page::class);
        $this->crud->setRoute(config('backpack.base.route_prefix').'/page');
        $this->crud->setEntityNameStrings(trans('backpack::pagemanager.page'), trans('backpack::pagemanager.pages'));
    }

    protected function setupListOperation()
    {
        $this->crud
            ->addColumn([
                'name' => 'name',
                'label' => trans('backpack::pagemanager.name'),
            ])
            ->addColumn([
                'name' => 'published',
                'label' => 'Published',
                'type' => 'switch',
            ])
            ->addColumn([
                'name' => 'created_at',
                'label' => 'Created At',
            ])
            ->addColumn([
                'name' => 'updated_at',
                'label' => 'Updated At',
            ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(StoreRequest::class);

        $this->addPageFields();
        $this->addMetasFields(false);
    }

    protected function setupUpdateOperation()
    {
        $this->crud->setValidation(UpdateRequest::class);

        $this->addPageFields();
        $this->addMetasFields(true);
    }

    private function addPageFields()
    {
        $this->crud
            ->addField([
                'name' => 'name',
                'label' => trans('backpack::pagemanager.page_name'),
                'type' => 'text',
            ])
            ->addField([
                'name' => 'published',
                'label' => 'Published',
                'type' => 'switch',
                'onLabel' => '✓',
                'offLabel' => '✕',
            ])
            ->addField([
                'name' => 'title',
                'label' => trans('backpack::pagemanager.page_title'),
                'type' => 'text',
            ])
            ->addField([
                'name' => 'content',
                'label' => 'Content',
                'type' => 'textarea',
            ]);
    }

    private function addMetasFields(bool $updateOperation)
    {
        $pageSlug = null;

        if ($updateOperation === true) {
            $page = $this->crud->getCurrentEntry();
            $pageSlug = $page->slug;
        }

        $this->crud->addField([
            'name' => 'separator',
            'type' => 'custom_html',
            'value' => '<h2>Metas</h2><hr>',
        ]);

        $this->crud->addField([
            'name' => 'meta_title',
            'label' => 'Meta title',
            'type' => 'text',
            'value' => ($pageSlug instanceof Slug) ? $pageSlug->getMetaTitle() : '',
            'hint' => 'If this field is empty value will get from Title',
        ]);
        $this->crud->addField([
            'name' => 'meta_slug',
            'label' => 'Slug',
            'type' => 'text',
            'value' => ($pageSlug instanceof Slug) ? $pageSlug->getSlug() : '',
            'hint' => 'If this field is empty value will get from Meta title',
        ]);
        $this->crud->addField([
            'name' => 'meta_description',
            'label' => 'Meta description',
            'type' => 'textarea',
            'value' => ($pageSlug instanceof Slug) ? $pageSlug->getMetaDescription() : '',
        ]);
        $this->crud->addField([
            'name' => 'meta_keywords',
            'label' => 'Meta keywords',
            'type' => 'textarea',
            'value' => ($pageSlug instanceof Slug) ? $pageSlug->getMetaKeywords() : '',
        ]);
    }
}
