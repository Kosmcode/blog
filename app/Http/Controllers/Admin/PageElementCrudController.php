<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\PageElementRequest;
use App\Models\PageElement;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;

class PageElementCrudController extends CrudController
{
    use ListOperation, ShowOperation, UpdateOperation;

    public function setup(): void
    {
        $this->crud->setModel(PageElement::class);
        $this->crud->setRoute(config('backpack.base.route_prefix').'/page-element');
        $this->crud->setEntityNameStrings('page element', 'page elements');
    }

    protected function setupListOperation(): void
    {
        $this->crud->addColumn('title');
        $this->crud->addColumn('description');
        $this->crud->addColumn([
            'name' => 'enable',
            'label' => 'Enabled',
            'type' => 'check',
        ]);
    }

    protected function setupUpdateOperation(): void
    {
        $this->crud->setValidation(PageElementRequest::class);

        $this->crud->field('id')->type('hidden');
        $this->crud->field('title')
            ->type('text')
            ->attributes([
                'readonly' => 'readonly',
            ]);
        $this->crud->field('content')->type('textarea');
        $this->crud->field('enable')->type('checkbox');
    }
}
