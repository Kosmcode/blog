<?php

namespace App\Http\Controllers\Admin;

use App\Models\Setting;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;

class SettingCrudController extends CrudController
{
    use ListOperation, UpdateOperation;

    public function setup()
    {
        $this->crud->setModel(Setting::class);
        $this->crud->setEntityNameStrings(
            trans('backpack::settings.setting_singular'),
            trans('backpack::settings.setting_plural')
        );
        $this->crud->setRoute(backpack_url(config('backpack.settings.route')));
    }

    public function setupListOperation()
    {
        $this->crud->addClause('where', 'active', 1);
        $this->crud->setColumns([
            [
                'name' => 'name',
                'label' => trans('backpack::settings.name'),
            ],
            [
                'name' => 'value',
                'label' => trans('backpack::settings.value'),
            ],
            [
                'name' => 'description',
                'label' => trans('backpack::settings.description'),
            ],
        ]);
    }

    public function setupUpdateOperation()
    {
        $this->crud->addField([
            'name' => 'name',
            'label' => trans('backpack::settings.name'),
            'type' => 'text',
            'attributes' => [
                'disabled' => 'disabled',
            ],
        ]);
        $this->crud->addField(json_decode($this->crud->getCurrentEntry()->field, true));
    }
}
