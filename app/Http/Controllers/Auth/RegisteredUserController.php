<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\StoreRequest;
use App\Providers\RouteServiceProvider;
use App\Services\User\RegisterServiceInterface;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector as LaravelRedirect;
use Inertia\Inertia;
use Inertia\Response;

class RegisteredUserController extends Controller
{
    private RegisterServiceInterface $userRegisterService;

    private LaravelRedirect $laravelRedirect;

    public function __construct(
        RegisterServiceInterface $userRegisterService,
        LaravelRedirect $laravelRedirect
    ) {
        $this->userRegisterService = $userRegisterService;
        $this->laravelRedirect = $laravelRedirect;
    }

    /**
     * Display the registration view.
     */
    public function create(): Response
    {
        return Inertia::render('Auth/Register');
    }

    /**
     * Handle an incoming registration request.
     */
    public function store(StoreRequest $userStoreRequest): RedirectResponse
    {
        $this->userRegisterService->createAndLogin($userStoreRequest);

        return $this->laravelRedirect->to(RouteServiceProvider::HOME);
    }
}
