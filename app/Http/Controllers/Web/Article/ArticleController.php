<?php

namespace App\Http\Controllers\Web\Article;

use App\Http\Controllers\Web\AbstractController;
use App\Http\Requests\Article\CategoryRequest;
use App\Http\Requests\Article\PaginateRequest;
use App\Http\Requests\Article\TagRequest;
use App\Services\Article\Category\GetServiceInterface as ArticleCategoryGetServiceInterface;
use App\Services\Article\GetServiceInterface as ArticleGetServiceInterface;
use App\Services\Article\Tag\GetServiceInterface as ArticleTagGetServiceInterface;
use Inertia\Response;

class ArticleController extends AbstractController
{
    public function __construct(
        protected readonly ArticleGetServiceInterface $articleGetService,
        protected readonly ArticleTagGetServiceInterface $articleTagGetService,
        protected readonly ArticleCategoryGetServiceInterface $articleCategoryGetService,
    ) {}

    public function getArticles(PaginateRequest $request): Response
    {
        return $this->articleGetService
            ->getArticlesToWeb()
            ->setAppHead()
            ->renderInertiaView();
    }

    public function getByCategorySlug(CategoryRequest $request, string $categorySlug): Response
    {
        return $this->articleCategoryGetService
            ->getArticleByCategorySlug($categorySlug)
            ->setAppHead()
            ->renderInertiaView();
    }

    public function getByTagSlug(TagRequest $request, string $tagSlug): Response
    {
        return $this->articleTagGetService
            ->getArticleByTagSlug($tagSlug)
            ->setAppHead()
            ->renderInertiaView();
    }
}
