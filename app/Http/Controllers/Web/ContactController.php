<?php

namespace App\Http\Controllers\Web;

use App\Http\Requests\Contact\CreateRequest;
use App\Services\Contact\CreateServiceInterface;
use App\Services\Contact\GetServiceInterface as ContactGetServiceInterface;
use Illuminate\Http\Request;
use Inertia\Response;
use Inertia\Response as InertiaResponse;

class ContactController extends AbstractController
{
    public function __construct(
        protected readonly CreateServiceInterface $contactCreateService,
        protected readonly ContactGetServiceInterface $contactGetService
    ) {}

    /**
     * Method to get Contact page
     */
    public function index(Request $request): Response
    {
        return $this->contactGetService
            ->getToInertia($request->getClientIp())
            ->setAppHead()
            ->renderInertiaView();
    }

    /**
     * Method to create Contact from form
     */
    public function createFromForm(CreateRequest $contactStoreRequest): InertiaResponse
    {
        return $this->contactCreateService
            ->fromCreateRequest($contactStoreRequest)
            ->setAppHead()
            ->renderInertiaView();
    }
}
