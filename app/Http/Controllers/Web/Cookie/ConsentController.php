<?php

namespace App\Http\Controllers\Web\Cookie;

use App\Http\Controllers\Web\AbstractController;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector as LaravelRedirect;
use Illuminate\Support\Facades\Cookie;

class ConsentController extends AbstractController
{
    public function __construct(
        protected readonly LaravelRedirect $laravelRedirect
    ) {}

    /**
     * Method to remove all cookies and redirect to google
     *
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function removeAllCookiesAndBye(): RedirectResponse
    {
        $cookies = [];

        foreach (Cookie::get() as $cookieKey => $cookieValue) { // @phpstan-ignore-line
            $cookies[] = Cookie::forget($cookieKey);
        }

        return $this->laravelRedirect
            ->to('https://google.com/')
            ->withCookies($cookies);
    }
}
