<?php

namespace App\Http\Controllers\Web;

use App\Services\Home\GetServiceInterface;
use Illuminate\Http\Request;
use Inertia\Response;

class HomeController extends AbstractController
{
    public function __construct(
        protected readonly GetServiceInterface $homeService
    ) {}

    public function get(Request $request): Response
    {
        return $this->homeService->getResponseDTO()
            ->setAppHead()
            ->renderInertiaView();
    }
}
