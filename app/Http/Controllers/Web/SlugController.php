<?php

namespace App\Http\Controllers\Web;

use App\Http\Requests\Slug\GetRequest;
use App\Services\Slug\GetServiceInterface;
use Inertia\Response;

class SlugController extends AbstractController
{
    public function __construct(
        protected readonly GetServiceInterface $slugRenderFactory
    ) {}

    /**
     * Method to get by Slug
     */
    public function get(GetRequest $request, string $slug): Response
    {
        return $this->slugRenderFactory->getResponseDTO($slug)
            ->setAppHead()
            ->renderInertiaView();
    }
}
