<?php

namespace App\Http\Middleware;

use App\Enums\Setting\KeyEnum;
use App\Http\Resources\Inertia\Share\UserResource;
use App\Services\MenuItem\GetServiceInterface as MenuItemGetServiceInterface;
use App\Services\SettingServiceInterface;
use App\Services\User\CheckServiceInterface;
use Illuminate\Foundation\Application as LaravelApp;
use Illuminate\Http\Request;
use Inertia\Middleware;
use Jenssegers\Agent\Agent;

class HandleInertiaRequestsMiddleware extends Middleware
{
    public function __construct(
        protected readonly MenuItemGetServiceInterface $menuItemGetService,
        protected readonly LaravelApp $laravelApp,
        protected readonly SettingServiceInterface $settingService,
        protected readonly CheckServiceInterface $userCheckService,
        protected readonly Agent $agent,
    ) {}

    /**
     * The root template that is loaded on the first page visit.
     *
     * @var string
     */
    protected $rootView = 'app';

    /**
     * {@inheritDoc}
     *
     * @return mixed[]
     */
    public function share(Request $request): array
    {
        return array_merge(parent::share($request), [
            'auth' => [
                'user' => UserResource::make(null)->toArray($request),
                'isBot' => $this->agent->isBot(),
            ],
            'nav' => $this->menuItemGetService->getToInertiaNavigation(),
            'locale' => $this->laravelApp->getLocale(),
            'maintenanceModeEnabled' => (bool) $this->settingService->get(KeyEnum::maintenanceModeEnabled->value),
        ]);
    }
}
