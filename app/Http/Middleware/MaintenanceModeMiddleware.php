<?php

namespace App\Http\Middleware;

use App\Services\Maintenance\ModeServiceInterface;
use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class of Maintenance Mode Middleware
 */
class MaintenanceModeMiddleware
{
    public function __construct(
        protected readonly ModeServiceInterface $maintenanceModeService
    ) {}

    /**
     * Handle an incoming request.
     *
     * @return RedirectResponse|Response|mixed
     */
    public function handle(Request $request, Closure $next): mixed
    {
        if ($this->maintenanceModeService->isEnabled()) {
            return $this->maintenanceModeService->getMaintenanceView();
        }

        return $next($request);
    }
}
