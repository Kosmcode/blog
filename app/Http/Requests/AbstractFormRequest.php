<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class AbstractFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    abstract public function authorize(): bool;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return mixed[]
     */
    abstract public function rules(): array;

    /**
     * @return mixed[]|null
     */
    public function validationData(): ?array
    {
        $requestData = $this->all();
        $routeParameters = $this->route()?->parameters();

        if ($routeParameters) {
            $requestData = array_merge($requestData, $routeParameters);
        }

        return $requestData;
    }
}
