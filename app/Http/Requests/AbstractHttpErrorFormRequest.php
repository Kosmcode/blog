<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\App;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractHttpErrorFormRequest extends AbstractFormRequest
{
    protected int $httpErrorCode = Response::HTTP_NOT_FOUND;

    protected string $httpErrorMessage = 'Resource Not found';

    /** {@inheritDoc} */
    public function failedValidation(Validator $validator): void
    {
        App::abort($this->httpErrorCode, $this->httpErrorMessage);
    }
}
