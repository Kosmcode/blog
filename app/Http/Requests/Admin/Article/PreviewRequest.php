<?php

namespace App\Http\Requests\Admin\Article;

use App\Enums\User\RoleEnum;
use App\Http\Requests\AbstractFormRequest;
use App\Repositories\ArticleRepositoryInterface;
use App\Rules\Article\IdExistsRule;
use App\Services\User\CheckServiceInterface;

/**
 * Article Preview Get Request
 *
 * @property int $articleId
 */
class PreviewRequest extends AbstractFormRequest
{
    private CheckServiceInterface $userCheckService;

    private ArticleRepositoryInterface $articleRepository;

    /**
     * @param  mixed[]  $query
     * @param  mixed[]  $request
     * @param  mixed[]  $attributes
     * @param  mixed[]  $cookies
     * @param  mixed[]  $files
     * @param  mixed[]  $server
     */
    public function __construct(
        CheckServiceInterface $userCheckService,
        ArticleRepositoryInterface $articleRepository,
        array $query = [],
        array $request = [],
        array $attributes = [],
        array $cookies = [],
        array $files = [],
        array $server = [],
        $content = null
    ) {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
        $this->userCheckService = $userCheckService;
        $this->articleRepository = $articleRepository;
    }

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return $this->userCheckService->userHasAdminOrRole([RoleEnum::articles->value]);
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'articleId' => [
                'bail',
                'required',
                'integer',
                new IdExistsRule($this->articleRepository),
            ],
        ];
    }
}
