<?php

namespace App\Http\Requests\Admin\Article;

use App\Enums\Article\StatusEnum;
use App\Http\Requests\Admin\AbstractRequest;
use Backpack\CRUD\app\Library\Validation\Rules\ValidUpload;
use Illuminate\Validation\Rule;

class UpdateRequest extends AbstractRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'title' => ['required', 'string', 'min:3', 'max:255'],
            'content' => ['required', 'string'],
            'image' => ValidUpload::field('required')
                ->file('file|mimes:jpeg,png,jpg,gif,svg,webp|max:2048'),
            'category_id' => ['required', 'integer', 'exists:categories,id'],
            'tags' => ['nullable', 'array'],
            'tags.*' => ['integer', 'exists:tags,id'],
            'status' => ['required', Rule::in(StatusEnum::toValues())],
            'featured' => ['nullable', 'boolean'],
        ];
    }
}
