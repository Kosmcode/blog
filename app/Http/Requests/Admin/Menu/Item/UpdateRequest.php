<?php

namespace App\Http\Requests\Admin\Menu\Item;

use App\Enums\Menu\Item\TypeEnum;
use App\Http\Requests\Admin\AbstractRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends AbstractRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'max:100'],
            'link' => ['required', 'string', 'max:255'],
            'type' => ['required', 'integer', Rule::in(TypeEnum::toValues())],
            'fa_icon' => ['nullable', 'string', 'max:255'],
        ];
    }
}
