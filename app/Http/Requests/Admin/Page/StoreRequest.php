<?php

namespace App\Http\Requests\Admin\Page;

use App\Http\Requests\Admin\AbstractRequest;

class StoreRequest extends AbstractRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'title' => ['required', 'string', 'max:255'],
            'content' => ['required', 'string'],
        ];
    }
}
