<?php

namespace App\Http\Requests\Admin;

class PageElementRequest extends AbstractRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'id' => ['required', 'exists:page_elements,id'],
            'title' => ['required'],
            'content' => ['required'],
            'enable' => ['required', 'boolean'],
        ];
    }
}
