<?php

namespace App\Http\Requests\Admin;

use Illuminate\Contracts\Validation\ValidationRule;
use Str;

class TagRequest extends AbstractRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'min:3', 'max:255'],
            'slug' => ['required', 'string', 'min:3', 'max:255'],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function passedValidation(): void
    {
        if ($this->request->get('slug')) {
            $this->request->set('slug', Str::slug($this->request->get('slug')));
        }
    }
}
