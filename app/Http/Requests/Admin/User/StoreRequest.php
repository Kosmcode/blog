<?php

namespace App\Http\Requests\Admin\User;

use App\Enums\User\RequestParameterEnum;
use App\Http\Requests\Admin\AbstractRequest;
use Illuminate\Validation\Rules\Password;

class StoreRequest extends AbstractRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            RequestParameterEnum::name->value => [
                'required',
                'string',
                'min:3',
                'max:255',
                'unique:users,name',
            ],
            RequestParameterEnum::email->value => [
                'required',
                'email',
                'unique:users,email',
            ],
            RequestParameterEnum::password->value => [
                'required',
                'confirmed',
                Password::defaults(),
            ],
        ];
    }
}
