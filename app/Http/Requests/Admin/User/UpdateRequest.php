<?php

namespace App\Http\Requests\Admin\User;

use App\Http\Requests\Admin\AbstractRequest;
use Illuminate\Support\Facades\Request;
use Illuminate\Validation\Rules\Password;

class UpdateRequest extends AbstractRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'name' => [
                'required',
                'string',
                'min:3',
                'max:255',
                'unique:users,name,'.Request::get('id'),
            ],
            'email' => [
                'required',
                'email',
                'unique:users,email,'.Request::get('id'),
            ],
            'password' => [
                'nullable',
                'confirmed',
                Password::defaults(),
            ],
        ];
    }
}
