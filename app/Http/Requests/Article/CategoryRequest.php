<?php

namespace App\Http\Requests\Article;

use App\Enums\Article\RequestParameterEnum;
use App\Http\Requests\AbstractHttpErrorFormRequest;

class CategoryRequest extends AbstractHttpErrorFormRequest
{
    /** {@inheritDoc} */
    public function authorize(): bool
    {
        return true;
    }

    /** @return mixed[] */
    public function rules(): array
    {
        return [
            RequestParameterEnum::categorySlug->value => [
                'required',
                'string',
                'exists:categories,slug',
            ],
        ];
    }
}
