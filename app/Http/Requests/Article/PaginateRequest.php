<?php

namespace App\Http\Requests\Article;

use App\Enums\Article\RequestParameterEnum;
use App\Http\Requests\AbstractFormRequest;
use App\Rules\Article\Category\NameExistRule as CategoryNameExistRule;
use App\Rules\Article\Tag\NameExistRule as TagNameExistRule;
use App\Services\Article\Filter\Category\GetServiceInterface as CategoryGetServiceInterface;
use App\Services\Article\Filter\Tag\GetServiceInterface as TagGetServiceInterface;
use Illuminate\Support\Facades\App;

class PaginateRequest extends AbstractFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            RequestParameterEnum::page->value => [
                'nullable', 'integer',
            ],
            RequestParameterEnum::categories->value => [
                'nullable',
                'array',
                new CategoryNameExistRule(
                    App::make(CategoryGetServiceInterface::class)
                ),
            ],
            RequestParameterEnum::tags->value => [
                'nullable',
                'array',
                new TagNameExistRule(
                    App::make(TagGetServiceInterface::class),
                ),
            ],
        ];
    }
}
