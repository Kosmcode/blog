<?php

namespace App\Http\Requests\Article;

use App\Enums\Article\RequestParameterEnum;
use App\Http\Requests\AbstractHttpErrorFormRequest;

class TagRequest extends AbstractHttpErrorFormRequest
{
    /** {@inheritDoc} */
    public function authorize(): bool
    {
        return true;
    }

    /** {@inheritDoc} */
    public function rules(): array
    {
        return [
            RequestParameterEnum::tagSlug->value => [
                'required',
                'string',
                'exists:tags,slug',
            ],
        ];
    }
}
