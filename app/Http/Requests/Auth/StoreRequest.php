<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\AbstractFormRequest;
use App\Rules\User\UserRulesTrait;
use Illuminate\Support\Facades\Auth;

class StoreRequest extends AbstractFormRequest
{
    use UserRulesTrait;

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return ! Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return array_merge(
            $this->getStoreUsernameRules(),
            $this->getStoreUserEmailRules(),
            $this->getStoreUserPasswordsRules(),
        );
    }
}
