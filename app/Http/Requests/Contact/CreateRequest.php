<?php

namespace App\Http\Requests\Contact;

use App\Enums\App\EnvironmentEnum;
use App\Http\Requests\AbstractFormRequest;
use App\Services\Contact\AvailableSentServiceInterface;
use App\Services\LocalizationServiceInterface;
use Illuminate\Support\Facades\App;
use Kosmcode\GoogleRecaptchaV3Rule\Rules\GoogleReCaptchaV3CheckRule;

class CreateRequest extends AbstractFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return App::make(AvailableSentServiceInterface::class)
            ->availableSendByIpAddress($this->getClientIp());
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        $rules = [
            'firstname' => ['required', 'string', 'alpha', 'min:3', 'max:15'],
            'lastname' => ['required', 'string', 'alpha', 'min:3', 'max:15'],
            'email' => ['required', 'email'],
            'subject' => ['required', 'string', 'min:3', 'max:255'],
            'message' => ['required', 'string', 'min:15'],
        ];

        if (App::environment(EnvironmentEnum::test->value)) {
            return $rules;
        }

        // @codeCoverageIgnoreStart
        $rules['captcha'] = [
            'required',
            new GoogleReCaptchaV3CheckRule($this->getClientIp()),
        ];

        return $rules;
        // @codeCoverageIgnoreEnd
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return mixed[]
     */
    public function attributes(): array
    {
        $localizationService = app(LocalizationServiceInterface::class);

        return [
            'firstname' => $localizationService->get('request.contact.create.firstname'),
            'lastname' => $localizationService->get('request.contact.create.lastname'),
            'email' => $localizationService->get('request.contact.create.email'),
            'subject' => $localizationService->get('request.contact.create.subject'),
            'message' => $localizationService->get('request.contact.create.message'),
        ];
    }
}
