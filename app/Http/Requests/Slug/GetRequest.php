<?php

namespace App\Http\Requests\Slug;

use App\Http\Requests\AbstractHttpErrorFormRequest;

class GetRequest extends AbstractHttpErrorFormRequest
{
    /** {@inheritDoc} */
    public function authorize(): bool
    {
        return true;
    }

    /** {@inheritDoc} */
    public function rules(): array
    {
        return [
            'slug' => [
                'required',
                'string',
                'exists:slugs,slug',
            ],
        ];
    }
}
