<?php

namespace App\Http\Resources\Article;

use App\Dto\ArticleDTO;
use App\Enums\Article\ImageThumbnailEnum;
use App\Traits\Article\ImageTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Jenssegers\Agent\Facades\Agent;

/**
 * ContactResource of Article
 *
 * @property ArticleDTO $resource
 */
class Resource extends JsonResource
{
    use ImageTrait;

    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return mixed[]
     */
    public function toArray($request): array
    {
        return [
            'article' => [
                'title' => $this->resource->getArticle()->getTitle(),
                'content' => $this->resource->getArticle()->getContent(),
                'image' => $this->prepareImage(
                    $this->resource->getArticle(),
                    ImageThumbnailEnum::getArticleMainByUserDeviceType(Agent::deviceType())
                ),
                'date' => $this->resource->getArticle()->getUpdatedAt()?->format('d.m.Y'),
                'tags' => TagsResource::collection($this->resource->getArticle()->tags)->toArray($request),
                'category' => CategoryResource::make($this->resource->getArticle()->category)->toArray($request),
            ],
            'recentArticles' => RecentArticlesResource::collection($this->resource->getRecentArticles()),
        ];
    }
}
