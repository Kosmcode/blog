<?php

namespace App\Http\Resources\Articles;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * ContactResource of Articles Category
 *
 * @property Category $resource
 */
class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return mixed[]
     */
    public function toArray($request): array
    {
        return [
            'name' => $this->resource->getName(),
            'slug' => $this->resource->getSlug(),
        ];
    }
}
