<?php

namespace App\Http\Resources\Articles;

use App\Dto\ArticlesFiltersDTO;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * ContactResource of Articles Filters
 *
 * @property ArticlesFiltersDTO $resource
 */
class FiltersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return mixed[]
     */
    public function toArray($request): array
    {
        return [
            'data' => [
                'articles' => Resource::collection($this->resource->getPaginatedArticles())->toArray($request),
                'current_page' => $this->resource->getPaginatedArticles()->currentPage(),
                'last_page' => $this->resource->getPaginatedArticles()->lastPage(),
            ],
            'filters' => [
                'categories' => $this->resource->getCategories()->getResource()->toArray($request),
                'tags' => $this->resource->getTags()->getResource()->toArray($request),
            ],
            'recentArticles' => RecentArticlesResource::collection($this->resource->getRecentArticles()),
            'articlesPageLimit' => $this->resource->getPageLimit(),
        ];
    }
}
