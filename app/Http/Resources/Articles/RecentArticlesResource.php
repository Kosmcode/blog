<?php

namespace App\Http\Resources\Articles;

use App\Enums\Article\ImageThumbnailEnum;
use App\Models\Article;
use App\Traits\Article\ContentTrait;
use App\Traits\Article\ImageTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Jenssegers\Agent\Facades\Agent;

/**
 * ContactResource of Home Articles
 *
 * @property Article $resource
 */
class RecentArticlesResource extends JsonResource
{
    use ContentTrait, ImageTrait;

    /**
     * Transform the resource into an array.
     *
     *
     * @return mixed[]
     */
    public function toArray(Request $request): array
    {
        return [
            'title' => $this->resource->getTitle(),
            'image' => $this->prepareImage(
                $this->resource,
                ImageThumbnailEnum::getArticlesRecentByUserDeviceType(Agent::deviceType())
            ),
            'content' => $this->getFirstParagraphFromString($this->resource->getContent()),
            'slug' => $this->resource->slug?->slug,
            'date' => $this->resource->getUpdatedAt()?->format('d.m.Y'),
        ];
    }
}
