<?php

namespace App\Http\Resources\Articles;

use App\Enums\Article\ImageThumbnailEnum;
use App\Models\Article;
use App\Traits\Article\ContentTrait;
use App\Traits\Article\ImageTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Jenssegers\Agent\Facades\Agent;

/**
 * ContactResource of Articles
 *
 * @property Article $resource
 */
class Resource extends JsonResource
{
    use ContentTrait, ImageTrait;

    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return mixed[]
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->resource->getId(),
            'category_id' => $this->resource->getCategoryId(),
            'title' => $this->resource->getTitle(),
            'slug' => $this->resource->slug()->first()?->slug,
            'content' => $this->getFirstParagraphFromString($this->resource->getContent()),
            'image' => $this->prepareImage(
                $this->resource,
                ImageThumbnailEnum::getArticlesPaginationByUserDeviceType(Agent::deviceType())
            ),
            'date' => $this->resource->getUpdatedAt()?->format('d.m.Y'),
            'tags' => TagsResource::collection($this->resource->tags)->toArray($request),
            'category' => CategoryResource::make($this->resource->category)->toArray($request),
        ];
    }
}
