<?php

namespace App\Http\Resources\Articles;

use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * ContactResource of Articles Tags
 *
 * @property Tag $resource
 */
class TagsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return mixed[]
     */
    public function toArray($request): array
    {
        return [
            'name' => $this->resource->getName(),
        ];
    }
}
