<?php

namespace App\Http\Resources;

use App\Dto\ContactResourceDTO;
use App\Enums\Page\ElementEnum;
use App\Http\Resources\Page\ElementResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Contact Inertia ContactResource
 *
 * @property ContactResourceDTO $resource
 */
class ContactResource extends JsonResource
{
    /**
     * @return mixed[]
     */
    public function toArray(Request $request): array
    {
        return [
            'canSent' => $this->resource->isCanSend(),
            'elements' => [
                ElementEnum::contactPageTop->value => ElementResource::make($this->resource->getTopPageElement()),
                ElementEnum::contactPageDown->value => ElementResource::make($this->resource->getDownPageElement()),
            ],
            'result' => $this->resource->getResult(),
        ];
    }
}
