<?php

namespace App\Http\Resources\Filters;

use App\Dto\FiltersMultipleDTO;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * ContactResource of Filters Multiple
 *
 * @property FiltersMultipleDTO $resource
 */
class FiltersMultipleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return mixed[]
     */
    public function toArray($request): array
    {
        return [
            'values' => $this->resource->getValues(),
            'selectedValues' => $this->resource->getSelectedValues(),
        ];
    }
}
