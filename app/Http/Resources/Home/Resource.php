<?php

namespace App\Http\Resources\Home;

use App\Dto\HomeResourceDTO;
use App\Enums\Page\ElementEnum;
use App\Http\Resources\Page\ElementResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * ContactResource of Home
 *
 * @property HomeResourceDTO $resource
 */
class Resource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return mixed[]
     */
    public function toArray($request): array
    {
        return [
            'articles' => RecentArticlesResource::collection($this->resource->getArticles())->toArray($request),
            'elements' => [
                ElementEnum::homePageTop->value => ElementResource::make($this->resource->getTopPageElement()),
                ElementEnum::homePageDown->value => ElementResource::make($this->resource->getDownPageElement()),
            ],
        ];
    }
}
