<?php

namespace App\Http\Resources\Inertia\Share;

use App\Models\User;
use App\Services\User\CheckServiceInterface;
use Illuminate\Contracts\Auth\Guard as LaravelAuth;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\App;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>|null
     */
    public function toArray(Request $request): ?array
    {
        $user = (App::make(LaravelAuth::class))
            ->user();

        if (! $user instanceof User) {
            return null;
        }

        return [
            'id' => $user->getId(),
            'name' => $user->getName(),
            'isAdmin' => (App::make(CheckServiceInterface::class))
                ->userHasAdminRole($user),
        ];
    }
}
