<?php

namespace App\Http\Resources;

use App\Enums\Menu\Item\TypeEnum;
use App\Models\MenuItem;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;
use JsonSerializable;

/**
 * ContactResource of Inertia Menu Items
 *
 * @property MenuItem $resource
 */
class MenuItemsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'name' => $this->resource->getName(),
            'icon' => (! empty($this->resource->getFaIcon()))
                ? $this->resource->getFaIcon()
                : null,
            'type' => TypeEnum::from($this->resource->getType())->name,
            'link' => $this->resource->getLink(),
            'childless' => $this->prepareChildless($this->resource, $request),
        ];
    }

    /** @return mixed[]|Arrayable|JsonSerializable */
    private function prepareChildless(MenuItem $menuItem, Request $request): array|Arrayable|JsonSerializable
    {
        $menuItemChildless = $menuItem->children;

        if (! $menuItemChildless instanceof Collection) {
            return [];
        }

        if ($menuItemChildless->count() === 0) {
            return [];
        }

        return MenuItemsResource::collection($menuItemChildless)->toArray($request);
    }
}
