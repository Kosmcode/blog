<?php

namespace App\Http\Resources\Page;

use App\Models\PageElement;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class of Page Element ContactResource
 *
 * @property PageElement|null $resource
 */
class ElementResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return mixed[]
     */
    public function toArray(Request $request): array
    {
        if (! $this->resource) {
            return [
                'enabled' => false,
                'content' => null,
            ];
        }

        return [
            'enabled' => $this->resource->isEnable(),
            'content' => $this->resource->isEnable() ?
                $this->resource->getContent()
                : null,
        ];
    }
}
