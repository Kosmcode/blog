<?php

namespace App\Http\Resources\Page;

use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property Page $resource
 */
class PageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return mixed[]
     */
    public function toArray(Request $request): array
    {
        return [
            'content' => $this->resource->content,
        ];
    }
}
