<?php

namespace App\Interfaces;

interface NotifiableInterface
{
    public function getNotification(): ?string;
}
