<?php

namespace App\Interfaces;

interface ValidatableInterface
{
    public function isCorrect(): bool;
}
