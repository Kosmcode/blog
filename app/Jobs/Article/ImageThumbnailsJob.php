<?php

namespace App\Jobs\Article;

use App\Exceptions\ArticleException;
use App\Services\Article\Image\ThumbnailServiceInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImageThumbnailsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct(
        protected int $articleId
    ) {}

    /**
     * Execute the job.
     *
     * @throws ArticleException
     */
    public function handle(
        ThumbnailServiceInterface $thumbnailService
    ): void {
        $thumbnailService->createOrUpdateThumbnailsByArticleId($this->articleId);
    }
}
