<?php

namespace App\Jobs\Contact;

use App\Services\Contact\Job\SendMailServiceInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendJob implements ShouldBeUnique, ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private int $contactId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $contactId)
    {
        $this->contactId = $contactId;
    }

    /**
     * Execute the job.
     */
    public function handle(
        SendMailServiceInterface $sendMailService
    ): void {
        $sendMailService->sendMailFromJobByContactId($this->contactId);
    }
}
