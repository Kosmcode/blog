<?php

namespace App\Mail\Contact;

use App\Models\Contact;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMail extends Mailable
{
    use SerializesModels;

    private Contact $contact;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Contact $contact)
    {
        $this->contact = $contact;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): static
    {
        return $this->from($this->contact->getEmail())
            ->subject($this->contact->getSubject())
            ->view('emails.contact.contactSendBody', ['contact' => $this->contact]);
    }
}
