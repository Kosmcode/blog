<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphOne;

abstract class AbstractSlugableModel extends Model implements SlugableInterface
{
    public function slug(): MorphOne
    {
        return $this->morphOne(Slug::class, 'contentable');
    }

    public function getSlug(): ?Slug
    {
        return $this->slug;
    }

    public function setSlug(?Slug $slug): self
    {
        $this->setRelation('slug', $slug);

        return $this;
    }

    abstract public function getContentableSlugableTitle(): string;
}
