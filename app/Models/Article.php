<?php

namespace App\Models;

use App\Enums\Article\StatusEnum;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Chelout\RelationshipEvents\Concerns\HasBelongsToEvents;
use Chelout\RelationshipEvents\Concerns\HasBelongsToManyEvents;
use Chelout\RelationshipEvents\Traits\HasRelationshipObservables;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\Article
 *
 * @property int $id
 * @property int $category_id
 * @property string $title
 * @property string $content
 * @property string|null $image
 * @property int $status
 * @property int $featured
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property Category $category
 * @property Slug|null $slug
 * @property Collection|Tag[]|array<Tag> $tags
 * @property int|null $tags_count
 *
 * @method static Builder|Article newModelQuery()
 * @method static Builder|Article newQuery()
 * @method static Builder|Article query()
 * @method static Builder|Article whereCategoryId($value)
 * @method static Builder|Article whereContent($value)
 * @method static Builder|Article whereCreatedAt($value)
 * @method static Builder|Article whereDeletedAt($value)
 * @method static Builder|Article whereFeatured($value)
 * @method static Builder|Article whereId($value)
 * @method static Builder|Article whereImage($value)
 * @method static Builder|Article whereStatus($value)
 * @method static Builder|Article whereTitle($value)
 * @method static Builder|Article whereUpdatedAt($value)
 *
 * @mixin Eloquent
 */
class Article extends AbstractSlugableModel
{
    use CrudTrait, HasBelongsToEvents, HasBelongsToManyEvents, HasFactory, HasRelationshipObservables;

    protected $fillable = [
        'title',
        'content',
        'image',
        'status',
        'category_id',
        'featured',
    ];

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function tags(): BelongsToMany
    {
        return $this->belongsToMany(Tag::class, 'article_tag');
    }

    public function articleImageThumbnails(): HasMany
    {
        return $this->hasMany(ArticleImageThumbnail::class);
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCategoryId(): int
    {
        return $this->category_id;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function getUpdatedAt(): ?Carbon
    {
        return $this->updated_at;
    }

    public function getImageThumbnails(): Collection
    {
        return $this->articleImageThumbnails()->get();
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function setStatusByStatusEnum(StatusEnum $statusEnum): self
    {
        return $this->setStatus($statusEnum->value);
    }

    public function getStatusEnum(): StatusEnum
    {
        return StatusEnum::from($this->status);
    }

    public function getContentableSlugableTitle(): string
    {
        return $this->getTitle();
    }
}
