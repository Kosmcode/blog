<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\ArticleImageThumbnail
 *
 * @property int $id
 * @property int $article_id ID of Article
 * @property int $width Width of thumbnail in px
 * @property string $filename Filename of thumbnail
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @method static Builder|ArticleImageThumbnail newModelQuery()
 * @method static Builder|ArticleImageThumbnail newQuery()
 * @method static Builder|ArticleImageThumbnail query()
 * @method static Builder|ArticleImageThumbnail whereArticleId($value)
 * @method static Builder|ArticleImageThumbnail whereCreatedAt($value)
 * @method static Builder|ArticleImageThumbnail whereId($value)
 * @method static Builder|ArticleImageThumbnail wherePath($value)
 * @method static Builder|ArticleImageThumbnail whereUpdatedAt($value)
 * @method static Builder|ArticleImageThumbnail whereWidth($value)
 * @method static Builder|ArticleImageThumbnail whereFilename($value)
 *
 * @mixin Eloquent
 */
class ArticleImageThumbnail extends Model
{
    public function article(): BelongsTo
    {
        return $this->belongsTo(Article::class);
    }

    public function setArticleId(int $articleId): self
    {
        $this->article_id = $articleId;

        return $this;
    }

    public function setWidth(int $width): self
    {
        $this->width = $width;

        return $this;
    }

    public function getFilename(): string
    {
        return $this->filename;
    }

    public function setFilename(string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }
}
