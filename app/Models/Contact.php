<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Contact
 *
 * @property int $id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $message
 * @property string $subject
 * @property string $ip
 * @property int $queued
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @method static Builder|Contact newModelQuery()
 * @method static Builder|Contact newQuery()
 * @method static Builder|Contact query()
 * @method static Builder|Contact whereCreatedAt($value)
 * @method static Builder|Contact whereEmail($value)
 * @method static Builder|Contact whereFirstname($value)
 * @method static Builder|Contact whereId($value)
 * @method static Builder|Contact whereLastname($value)
 * @method static Builder|Contact whereMessage($value)
 * @method static Builder|Contact whereUpdatedAt($value)
 * @method static Builder|Contact whereIp($value)
 * @method static Builder|Contact whereQueued($value)
 * @method static Builder|Contact whereSubject($value)
 *
 * @mixin Eloquent
 */
class Contact extends Model
{
    use CrudTrait;

    protected $fillable = ['firstname', 'lastname', 'email', 'message', 'subject', 'ip', 'queued'];

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getFirstname(): string
    {
        return $this->firstname;
    }

    public function getLastname(): string
    {
        return $this->lastname;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function getIp(): string
    {
        return $this->ip;
    }

    public function setQueued(bool $queued): self
    {
        $this->queued = (int) $queued;

        return $this;
    }
}
