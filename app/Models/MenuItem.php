<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\Menu
 *
 * @property int $id
 * @property string $name
 * @property string|null $link
 * @property int $type value from MenuItemsTypeEnum
 * @property int|null $parent_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property string|null $fa_icon Field with fa icon data
 * @property int $lft
 * @property int $rgt
 * @property int $depth
 * @property-read Collection|MenuItem[] $children
 * @property-read int|null $children_count
 * @property-read MenuItem|null $parent
 *
 * @method static Builder|MenuItem newModelQuery()
 * @method static Builder|MenuItem newQuery()
 * @method static Builder|MenuItem query()
 * @method static Builder|MenuItem whereCreatedAt($value)
 * @method static Builder|MenuItem whereDeletedAt($value)
 * @method static Builder|MenuItem whereId($value)
 * @method static Builder|MenuItem whereLink($value)
 * @method static Builder|MenuItem whereName($value)
 * @method static Builder|MenuItem whereParentId($value)
 * @method static Builder|MenuItem whereType($value)
 * @method static Builder|MenuItem whereUpdatedAt($value)
 * @method static Builder|MenuItem whereDepth($value)
 * @method static Builder|MenuItem whereFaIcon($value)
 * @method static Builder|MenuItem whereLft($value)
 * @method static Builder|MenuItem whereRgt($value)
 *
 * @mixin Eloquent
 */
class MenuItem extends Model
{
    use CrudTrait;

    protected $table = 'menu_items';

    protected $fillable = ['name', 'link', 'type', 'parent_id', 'fa_icon'];

    public function parent(): BelongsTo
    {
        return $this->belongsTo(MenuItem::class, 'parent_id');
    }

    public function children(): HasMany
    {
        return $this->hasMany(MenuItem::class, 'parent_id');
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function getParentId(): ?int
    {
        return $this->parent_id;
    }

    public function getFaIcon(): ?string
    {
        return $this->fa_icon;
    }
}
