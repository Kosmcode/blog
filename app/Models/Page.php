<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Page
 *
 * @property int $id
 * @property string $name
 * @property string $title
 * @property string|null $content
 * @property bool $published
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property Slug|null $slug
 *
 * @method static Builder|Page findSimilarSlugs(string $attribute, array $config, string $slug)
 * @method static Builder|Page newModelQuery()
 * @method static Builder|Page newQuery()
 * @method static Builder|Page query()
 * @method static Builder|Page whereContent($value)
 * @method static Builder|Page whereCreatedAt($value)
 * @method static Builder|Page whereDeletedAt($value)
 * @method static Builder|Page whereId($value)
 * @method static Builder|Page whereName($value)
 * @method static Builder|Page whereSlug(string $slug)
 * @method static Builder|Page whereTitle($value)
 * @method static Builder|Page whereUpdatedAt($value)
 * @method static Builder|Page withUniqueSlugConstraints(Model $model, string $attribute, array $config, string $slug)
 *
 * @mixin Eloquent
 */
class Page extends AbstractSlugableModel
{
    use CrudTrait, HasFactory;

    protected $table = 'pages';

    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = ['name', 'title', 'content', 'published'];

    /**
     * Method to get Page Slug Model
     */

    /**
     * Method to get Page ID
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function isPublished(): bool
    {
        return $this->published;
    }

    public function setPublished(bool $published): self
    {
        $this->published = $published;

        return $this;
    }

    public function getContentableSlugableTitle(): string
    {
        return $this->getName();
    }
}
