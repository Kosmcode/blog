<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PageElement
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $content
 * @property int $enable
 *
 * @method static Builder|PageElement newModelQuery()
 * @method static Builder|PageElement newQuery()
 * @method static Builder|PageElement query()
 * @method static Builder|PageElement whereContent($value)
 * @method static Builder|PageElement whereDescription($value)
 * @method static Builder|PageElement whereEnable($value)
 * @method static Builder|PageElement whereId($value)
 * @method static Builder|PageElement whereTitle($value)
 *
 * @mixin Eloquent
 */
class PageElement extends Model
{
    use CrudTrait, HasFactory;

    /**
     * Timestamps
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'title',
        'description',
        'content',
        'enable',
    ];

    /**
     * Method to get content
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * Method to get title
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Method to check is enable
     */
    public function isEnable(): bool
    {
        return (bool) $this->enable;
    }
}
