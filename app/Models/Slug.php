<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\Slug
 *
 * @property int|null $id
 * @property string $slug
 * @property string $contentable_type
 * @property int $contentable_id
 * @property string|null $meta_title
 * @property string|null $meta_description
 * @property string|null $meta_keywords
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @method static Builder|Slug newModelQuery()
 * @method static Builder|Slug newQuery()
 * @method static Builder|Slug query()
 * @method static Builder|Slug whereContentableId($value)
 * @method static Builder|Slug whereContentableType($value)
 * @method static Builder|Slug whereCreatedAt($value)
 * @method static Builder|Slug whereId($value)
 * @method static Builder|Slug whereMetaDescription($value)
 * @method static Builder|Slug whereMetaKeywords($value)
 * @method static Builder|Slug whereMetaTitle($value)
 * @method static Builder|Slug whereSlug($value)
 * @method static Builder|Slug whereUpdatedAt($value)
 *
 * @property-read Model|Eloquent $contentable
 *
 * @mixin Eloquent
 */
class Slug extends Model
{
    use HasFactory;

    protected $table = 'slugs';

    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = [
        'slug',
        'contentable_type',
        'contentable_id',
        'meta_title',
        'meta_description',
        'meta_keywords',
    ];

    public function contentable(): MorphTo
    {
        return $this->morphTo();
    }

    public function getContentable(): ?Model
    {
        return $this->contentable()->first();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getMetaTitle(): ?string
    {
        return $this->meta_title;
    }

    public function setMetaTitle(?string $metaTitle): self
    {
        $this->meta_title = $metaTitle;

        return $this;
    }

    public function getMetaDescription(): ?string
    {
        return $this->meta_description;
    }

    public function setMetaDescription(?string $metaDescription): self
    {
        $this->meta_description = $metaDescription;

        return $this;
    }

    public function getMetaKeywords(): ?string
    {
        return $this->meta_keywords;
    }

    public function setMetaKeywords(?string $metaKeywords): self
    {
        $this->meta_keywords = $metaKeywords;

        return $this;
    }

    public function setContentableId(int $contentableId): self
    {
        $this->contentable_id = $contentableId;

        return $this;
    }

    public function getContentableType(): string
    {
        return $this->contentable_type;
    }

    public function setContentableType(string $contentableType): self
    {
        $this->contentable_type = $contentableType;

        return $this;
    }
}
