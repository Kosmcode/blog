<?php

namespace App\Models;

interface SlugableInterface
{
    public function getContentableSlugableTitle(): string;
}
