<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Barryvdh\LaravelIdeHelper\Eloquent;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\Tag
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read Collection $articles
 * @property-read int|null $articles_count
 * @property-read mixed $slug_or_name
 *
 * @method static Builder|Tag findSimilarSlugs(string $attribute, array $config, string $slug)
 * @method static Builder|Tag newModelQuery()
 * @method static Builder|Tag newQuery()
 * @method static Builder|Tag query()
 * @method static Builder|Tag whereCreatedAt($value)
 * @method static Builder|Tag whereDeletedAt($value)
 * @method static Builder|Tag whereId($value)
 * @method static Builder|Tag whereName($value)
 * @method static Builder|Tag whereSlug($value)
 * @method static Builder|Tag whereUpdatedAt($value)
 * @method static Builder|Tag withUniqueSlugConstraints(Model $model, string $attribute, array $config, string $slug)
 *
 * @mixin Eloquent
 * @mixin \Eloquent
 */
class Tag extends Model
{
    use CrudTrait, HasFactory, Sluggable, SluggableScopeHelpers;

    protected $table = 'tags';

    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = ['name', 'slug'];

    public function articles(): BelongsToMany
    {
        return $this->belongsToMany(Article::class, 'article_tag');
    }

    /** @return mixed[] */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'slug_or_name',
            ],
        ];
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }
}
