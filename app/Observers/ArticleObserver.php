<?php

namespace App\Observers;

use App\Enums\Cache\KeyEnum;
use App\Jobs\Article\ImageThumbnailsJob;
use App\Models\Article;
use App\Services\Article\Image\DeleteServiceInterface;
use App\Services\Cache\AbstractService;
use Illuminate\Cache\Repository as LaravelCache;

/**
 * Class of Article Observer
 */
class ArticleObserver extends AbstractService
{
    public function __construct(
        protected LaravelCache $laravelCache,
        protected readonly DeleteServiceInterface $deleteService
    ) {
        parent::__construct($laravelCache);
    }

    public function created(Article $article): void
    {
        ImageThumbnailsJob::dispatch($article->getId()); // @phpstan-ignore-line
    }

    public function updated(Article $article): void
    {
        $this->cacheForget(KeyEnum::articlesCategories);
        $this->cacheForget(KeyEnum::articlesTags);
        $this->cacheForget(KeyEnum::recentArticles);
        if ($article->getSlug()) {
            $this->cacheForget(
                KeyEnum::prepareSlugCacheKey($article->getSlug()->getSlug())
            );
        }

        if ($article->wasChanged('image')) {
            ImageThumbnailsJob::dispatch($article->getId()); // @phpstan-ignore-line
        }
    }

    public function deleted(Article $article): void
    {
        $this->cacheForget(KeyEnum::articlesCategories);
        $this->cacheForget(KeyEnum::articlesTags);
        $this->cacheForget(KeyEnum::recentArticles);

        if ($article->slug) {
            $this->cacheForget(
                KeyEnum::prepareSlugCacheKey($article->slug->getSlug())
            );

            $article->slug->delete();
        }

        $this->deleteService->deleteAllImagesOfArticle($article);
    }

    public function restored(Article $article): void
    {
        $this->cacheForget(KeyEnum::articlesCategories);
        $this->cacheForget(KeyEnum::articlesTags);
        $this->cacheForget(KeyEnum::recentArticles);
    }

    public function forceDeleted(Article $article): void
    {
        $this->cacheForget(KeyEnum::articlesCategories);
        $this->cacheForget(KeyEnum::articlesTags);
        $this->cacheForget(KeyEnum::recentArticles);

        if ($article->slug) {
            $this->cacheForget(
                KeyEnum::prepareSlugCacheKey($article->slug->getSlug())
            );

            $article->slug->forceDelete();
        }
    }

    /**
     * @param  array<int, int>  $ids
     */
    public function belongsToManyAttached(string $relation, Article $article, array $ids): void
    {
        $this->cacheForget(KeyEnum::articlesCategories);
        $this->cacheForget(KeyEnum::articlesTags);
        $this->cacheForget(KeyEnum::recentArticles);

        if ($article->slug) {
            $this->cacheForget(
                KeyEnum::prepareSlugCacheKey($article->slug->getSlug())
            );
        }
    }

    /**
     * @param  array<int, int>  $ids
     */
    public function belongsToManyDetached(string $relation, Article $article, array $ids): void
    {
        $this->cacheForget(KeyEnum::articlesCategories);
        $this->cacheForget(KeyEnum::articlesTags);
        $this->cacheForget(KeyEnum::recentArticles);

        if ($article->slug) {
            $this->cacheForget(
                KeyEnum::prepareSlugCacheKey($article->slug->getSlug())
            );
        }
    }
}
