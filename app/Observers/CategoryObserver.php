<?php

namespace App\Observers;

use App\Enums\Cache\KeyEnum;
use App\Models\Category;
use App\Services\Cache\AbstractService;

class CategoryObserver extends AbstractService
{
    public function created(Category $category): void
    {
        $this->cacheForget(KeyEnum::articlesCategories);
    }

    public function updated(Category $category): void
    {
        $this->cacheForget(KeyEnum::articlesCategories);
    }

    public function deleted(Category $category): void
    {
        $this->cacheForget(KeyEnum::articlesCategories);
    }

    public function restored(Category $category): void
    {
        $this->cacheForget(KeyEnum::articlesCategories);
    }

    public function forceDeleted(Category $category): void
    {
        $this->cacheForget(KeyEnum::articlesCategories);
    }
}
