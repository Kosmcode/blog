<?php

namespace App\Observers;

use App\Enums\Cache\KeyEnum;
use App\Models\MenuItem;
use App\Services\MenuItem\GetServiceInterface;

class MenuItemObserver
{
    public function __construct(
        protected readonly GetServiceInterface $getService
    ) {}

    public function created(MenuItem $menuItem): void
    {
        $this->getService->cacheForget(KeyEnum::menuItem);
    }

    public function updated(MenuItem $menuItem): void
    {
        $this->getService->cacheForget(KeyEnum::menuItem);
    }

    public function deleted(MenuItem $menuItem): void
    {
        $this->getService->cacheForget(KeyEnum::menuItem);
    }

    public function restored(MenuItem $menuItem): void
    {
        $this->getService->cacheForget(KeyEnum::menuItem);
    }

    public function forceDeleted(MenuItem $menuItem): void
    {
        $this->getService->cacheForget(KeyEnum::menuItem);
    }
}
