<?php

namespace App\Observers;

use App\Enums\Cache\KeyEnum;
use App\Models\PageElement;
use App\Services\Cache\AbstractService;

/**
 * Observer class of Page Element.
 */
class PageElementObserver extends AbstractService
{
    public function updated(PageElement $pageElement): void
    {
        $this->cacheForget(KeyEnum::preparePageElementCacheKey($pageElement->getTitle()));
    }

    public function deleted(PageElement $pageElement): void
    {
        $this->cacheForget(KeyEnum::preparePageElementCacheKey($pageElement->getTitle()));
    }

    public function restored(PageElement $pageElement): void
    {
        $this->cacheForget(KeyEnum::preparePageElementCacheKey($pageElement->getTitle()));
    }

    public function forceDeleted(PageElement $pageElement): void
    {
        $this->cacheForget(KeyEnum::preparePageElementCacheKey($pageElement->getTitle()));
    }
}
