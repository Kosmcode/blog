<?php

namespace App\Observers;

use App\Enums\Cache\KeyEnum;
use App\Models\Page;
use App\Services\Cache\AbstractService;

class PageObserver extends AbstractService
{
    public function updated(Page $page): void
    {
        if ($page->slug) {
            $this->cacheForget(KeyEnum::prepareSlugCacheKey($page->slug->getSlug()));
        }
    }

    public function deleted(Page $page): void
    {
        if ($page->slug) {
            $this->cacheForget(KeyEnum::prepareSlugCacheKey($page->slug->getSlug()));
        }
    }

    public function forceDeleted(Page $page): void
    {
        if ($page->slug) {
            $this->cacheForget(KeyEnum::prepareSlugCacheKey($page->slug->getSlug()));
        }
    }
}
