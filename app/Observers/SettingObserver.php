<?php

namespace App\Observers;

use App\Enums\Cache\KeyEnum;
use App\Enums\Setting\KeyEnum as SettingKeyEnum;
use App\Models\Setting;
use App\Services\Cache\AbstractService;

class SettingObserver extends AbstractService
{
    public function updated(Setting $setting): void
    {
        $this->cacheForget(KeyEnum::prepareSettingCacheKey($setting->getSettingKeyName()));

        $this->cacheForgetArticlesRecent($setting);
    }

    public function created(Setting $setting): void
    {
        $this->cacheForget(KeyEnum::prepareSettingCacheKey($setting->getSettingKeyName()));
    }

    public function deleted(Setting $setting): void
    {
        $this->cacheForget(KeyEnum::prepareSettingCacheKey($setting->getSettingKeyName()));

        $this->cacheForgetArticlesRecent($setting);
    }

    public function restored(Setting $setting): void
    {
        $this->cacheForget(KeyEnum::prepareSettingCacheKey($setting->getSettingKeyName()));

        $this->cacheForgetArticlesRecent($setting);
    }

    public function forceDeleted(Setting $setting): void
    {
        $this->cacheForget(KeyEnum::prepareSettingCacheKey($setting->getSettingKeyName()));

        $this->cacheForgetArticlesRecent($setting);
    }

    protected function cacheForgetArticlesRecent(Setting $setting): void
    {
        if ($setting->getSettingKeyName() !== SettingKeyEnum::recentArticleLimit->value) {
            return;
        }

        $this->cacheForget(KeyEnum::recentArticles);
    }
}
