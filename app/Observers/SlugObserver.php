<?php

namespace App\Observers;

use App\Enums\Cache\KeyEnum;
use App\Models\Slug;
use App\Services\Cache\AbstractService;

class SlugObserver extends AbstractService
{
    public function updated(Slug $slug): void
    {
        $this->cacheForget(KeyEnum::prepareSlugCacheKey($slug->getSlug()));
    }

    public function deleted(Slug $slug): void
    {
        $this->cacheForget(KeyEnum::prepareSlugCacheKey($slug->getSlug()));
    }

    public function forceDeleted(Slug $slug): void
    {
        $this->cacheForget(KeyEnum::prepareSlugCacheKey($slug->getSlug()));
    }
}
