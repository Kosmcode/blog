<?php

namespace App\Observers;

use App\Enums\Cache\KeyEnum;
use App\Models\Tag;
use App\Services\Article\Filter\Tag\GetServiceInterface;

/**
 * Class of Tag Observer
 */
class TagObserver
{
    public function __construct(
        protected readonly GetServiceInterface $tagService
    ) {}

    public function created(Tag $tag): void
    {
        $this->tagService->cacheForget(KeyEnum::articlesTags);
    }

    public function updated(Tag $tag): void
    {
        $this->tagService->cacheForget(KeyEnum::articlesTags);
    }

    public function deleted(Tag $tag): void
    {
        $this->tagService->cacheForget(KeyEnum::articlesTags);
    }

    public function restored(Tag $tag): void
    {
        $this->tagService->cacheForget(KeyEnum::articlesTags);
    }

    public function forceDeleted(Tag $tag): void
    {
        $this->tagService->cacheForget(KeyEnum::articlesTags);
    }
}
