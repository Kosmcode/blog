<?php

namespace App\Providers;

use App\Services\MenuItem\Internal\LinkFactory;
use App\Services\MenuItem\Internal\LinkFactoryInterface;
use App\Services\Slug\Factory\SlugFactory;
use App\Services\Slug\Factory\SlugFactoryInterface;
use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Config;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Illuminate\Validation\Rules\Password;

class AppServiceProvider extends ServiceProvider
{
    const CONFIG_BINDINGS_KEY = 'app.bindings';

    const CONFIG_FACTORIES_SLUGGABLE_KEY = 'app.factories.sluggable';

    const CONFIG_FACTORIES_MENUITEM_INTERNAL_LINKS_KEY = 'app.factories.menuItemInternalLinks';

    /** @var array<mixed> */
    public array $bindings = [];

    /**
     * Register any application services.
     */
    public function register(): void
    {
        if ($this->app->isLocal()) {
            $this->app->register(IdeHelperServiceProvider::class);
        }

        $configBindings = Config::get(self::CONFIG_BINDINGS_KEY);

        if (is_array($configBindings)) {
            $this->bindings = $configBindings;
        }

        $this->registryFactories();
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Password::defaults(function () {
            return Password::min(8)
                ->mixedCase()
                ->numbers()
                ->letters()
                ->symbols();
        });
    }

    protected function registryFactories(): void
    {
        $this->app->bind(
            SlugFactoryInterface::class,
            function (Application $app) {
                return new SlugFactory(
                    $this->getFactoriesFromConfigByKey(self::CONFIG_FACTORIES_SLUGGABLE_KEY),
                    $app
                );
            }
        );

        $this->app->bind(
            LinkFactoryInterface::class,
            function (Application $app) {
                return new LinkFactory(
                    $this->getFactoriesFromConfigByKey(self::CONFIG_FACTORIES_MENUITEM_INTERNAL_LINKS_KEY),
                    $app
                );
            }
        );
    }

    /**
     * @return iterable<int, class-string>
     *
     * @throws Exception
     */
    protected function getFactoriesFromConfigByKey(string $configKey): iterable
    {
        $factoriesClasses = Config::get($configKey);

        if (! is_iterable($factoriesClasses)) {
            throw new Exception('Config factories `'.$configKey.'` is not array');
        }

        return $factoriesClasses;
    }
}
