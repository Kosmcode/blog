<?php

namespace App\Providers;

use App\Models\Article;
use App\Models\Category;
use App\Models\MenuItem;
use App\Models\Page;
use App\Models\PageElement;
use App\Models\Setting;
use App\Models\Slug;
use App\Models\Tag;
use App\Observers\ArticleObserver;
use App\Observers\CategoryObserver;
use App\Observers\MenuItemObserver;
use App\Observers\PageElementObserver;
use App\Observers\PageObserver;
use App\Observers\SettingObserver;
use App\Observers\SlugObserver;
use App\Observers\TagObserver;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $observers = [
        Category::class => [CategoryObserver::class],
        Article::class => [ArticleObserver::class],
        Tag::class => [TagObserver::class],
        Setting::class => [SettingObserver::class],
        MenuItem::class => [MenuItemObserver::class],
        PageElement::class => [PageElementObserver::class],
        Slug::class => [SlugObserver::class],
        Page::class => [PageObserver::class],
    ];

    //    /**
    //     * Register any events for your application.
    //     */
    //    public function boot(): void
    //    {
    //        Category::observe(CategoryObserver::class);
    //        Article::observe(ArticleObserver::class);
    //        Tag::observe(TagObserver::class);
    //        Setting::observe(SettingObserver::class);
    //        MenuItem::observe(MenuItemObserver::class);
    //        PageElement::observe(PageElementObserver::class);
    //        Slug::observe(SlugObserver::class);
    //        Page::observe(PageObserver::class);
    //    }
}
