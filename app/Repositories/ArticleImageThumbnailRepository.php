<?php

namespace App\Repositories;

use App\Models\ArticleImageThumbnail;

class ArticleImageThumbnailRepository implements ArticleImageThumbnailRepositoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function new(): ArticleImageThumbnail
    {
        return new ArticleImageThumbnail();
    }

    /**
     * {@inheritDoc}
     */
    public function save(ArticleImageThumbnail $articleImageThumbnail): bool
    {
        return $articleImageThumbnail->save();
    }

    /**
     * {@inheritDoc}
     */
    public function delete(ArticleImageThumbnail $articleImageThumbnail): ?bool
    {
        return $articleImageThumbnail->delete();
    }
}
