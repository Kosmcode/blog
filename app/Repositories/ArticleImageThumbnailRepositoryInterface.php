<?php

namespace App\Repositories;

use App\Models\ArticleImageThumbnail;

interface ArticleImageThumbnailRepositoryInterface
{
    public function new(): ArticleImageThumbnail;

    public function save(ArticleImageThumbnail $articleImageThumbnail): bool;

    public function delete(ArticleImageThumbnail $articleImageThumbnail): ?bool;
}
