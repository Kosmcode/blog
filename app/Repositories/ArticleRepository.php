<?php

namespace App\Repositories;

use App\Enums\Article\FeatureEnum;
use App\Enums\Article\RequestParameterEnum;
use App\Enums\Article\StatusEnum;
use App\Enums\Setting\KeyEnum as SettingKeyEnum;
use App\Models\Article;
use App\Services\SettingServiceInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Support\Collection;

class ArticleRepository implements ArticleRepositoryInterface
{
    public function __construct(
        protected readonly SettingServiceInterface $settingService
    ) {}

    public function getRecentArticles(): Collection
    {
        $recentArticlesLimit = $this->settingService->get(SettingKeyEnum::recentArticleLimit->value);

        if (! is_int($recentArticlesLimit)) {
            $recentArticlesLimit = self::DEFAULT_HOME_ARTICLE_LIMIT;
        }

        return Article::query()
            ->select([
                'id', 'category_id', 'title', 'content', 'image', 'status', 'updated_at',
            ])
            ->with([
                'category' => function (BelongsTo $belongsTo) {
                    $belongsTo->select(['id', 'name', 'slug']);
                },
                'tags' => function (BelongsToMany $belongsToMany) {
                    $belongsToMany->select(['name', 'slug']);
                },
                'articleImageThumbnails' => function (HasMany $hasMany) {
                    $hasMany->select(['article_id', 'width', 'filename']);
                },
            ])
            ->orderBy('id', 'DESC')
            ->where('status', '=', StatusEnum::published->value)
            ->where('featured', '=', FeatureEnum::feature->value)
            ->limit($recentArticlesLimit)
            ->get();
    }

    /** {@inheritDoc} */
    public function getArticlesToFilters(array $parameters): LengthAwarePaginator
    {
        $articlesPageLimit = $this->settingService->get(SettingKeyEnum::articlesPageLimit->value);

        if (! is_int($articlesPageLimit)) {
            $articlesPageLimit = self::DEFAULT_ARTICLES_PAGE_LIMIT;
        }

        return Article::query()
            ->select([
                'id', 'category_id', 'title', 'content', 'image', 'status', 'updated_at',
            ])
            ->with([
                'category' => function (BelongsTo $belongsTo) {
                    $belongsTo->select(['id', 'name']);
                },
                'tags' => function (BelongsToMany $belongsToMany) {
                    $belongsToMany->select(['name']);
                },
                'slug' => function (MorphOne $morphOne) {
                    $morphOne->select(['slug']);
                },
            ])
            ->when(
                isset($parameters[RequestParameterEnum::categories->value]),
                function (Builder $builder) use ($parameters) {
                    $builder->whereHas('category', function (Builder $builder) use ($parameters) {
                        $builder->whereIn('name', $parameters[RequestParameterEnum::categories->value]);
                    });
                }
            )
            ->when(isset($parameters[RequestParameterEnum::tags->value]), function (Builder $builder) use ($parameters) {
                $builder->whereHas('tags', function (Builder $builder) use ($parameters) {
                    $builder->whereIn('name', $parameters[RequestParameterEnum::tags->value]);
                });
            })
            ->where('status', '=', StatusEnum::published->value)
            ->orderBy('id', 'DESC')
            ->paginate($articlesPageLimit);
    }

    public function articleByIdExists(int $articleId): bool
    {
        return Article::query()
            ->where('id', '=', $articleId)
            ->exists();
    }

    public function getById(int $articleId): ?Article
    {
        return Article::query()
            ->where('id', '=', $articleId)
            ->first();
    }
}
