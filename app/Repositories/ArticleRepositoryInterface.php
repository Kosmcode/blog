<?php

namespace App\Repositories;

use App\Models\Article;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

interface ArticleRepositoryInterface
{
    const DEFAULT_HOME_ARTICLE_LIMIT = 4;

    const DEFAULT_ARTICLES_PAGE_LIMIT = 10;

    public function getRecentArticles(): Collection;

    /**
     * @param  array<mixed>  $parameters
     */
    public function getArticlesToFilters(array $parameters): LengthAwarePaginator;

    public function articleByIdExists(int $articleId): bool;

    public function getById(int $articleId): ?Article;
}
