<?php

namespace App\Repositories;

use App\Enums\Article\StatusEnum;
use App\Models\Category;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

class CategoryRepository implements CategoryRepositoryInterface
{
    /** {@inheritDoc} */
    public function getToArticlesFilters(): array
    {
        return Category::query()
            ->select(['name'])
            ->whereHas('articles', function (Builder $builder) {
                $builder->where('status', '=', StatusEnum::published->value);
            })
            ->get()
            ->pluck('name')
            ->toArray();
    }

    public function existBySlug(string $categorySlug): bool
    {
        return Category::query()
            ->where('slug', '=', $categorySlug)
            ->exists();
    }

    public function findBySlug(string $categorySlug): ?Category
    {
        return Category::query()
            ->where('slug', '=', $categorySlug)
            ->first();
    }

    public function getToMenuItemsLinks(): Collection
    {
        return Category::query()
            ->select(['name', 'slug'])
            ->get();
    }
}
