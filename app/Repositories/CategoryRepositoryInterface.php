<?php

namespace App\Repositories;

use App\Models\Category;
use Illuminate\Support\Collection;

interface CategoryRepositoryInterface
{
    /**
     * @return array<mixed>
     */
    public function getToArticlesFilters(): array;

    public function existBySlug(string $categorySlug): bool;

    public function findBySlug(string $categorySlug): ?Category;

    public function getToMenuItemsLinks(): Collection;
}
