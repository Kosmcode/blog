<?php

namespace App\Repositories;

use App\Http\Requests\Contact\CreateRequest;
use App\Models\Contact;

/**
 * Class of Contact Repository
 */
class ContactRepository implements ContactRepositoryInterface
{
    public function createByContactStoreRequest(CreateRequest $contactStoreRequest): ?Contact
    {
        return Contact::query()->create([
            'firstname' => $contactStoreRequest->get('firstname'),
            'lastname' => $contactStoreRequest->get('lastname'),
            'message' => $contactStoreRequest->get('message'),
            'subject' => $contactStoreRequest->get('subject'),
            'email' => $contactStoreRequest->get('email'),
            'ip' => $contactStoreRequest->ip(),
        ]);
    }

    public function existsByIpAddressAndCreatedAt(string $ipAddress, string $createdAt, string $createdAtOperator): bool
    {
        return Contact::query()
            ->where('ip', '=', $ipAddress)
            ->where('created_at', $createdAtOperator, $createdAt)
            ->exists();
    }

    public function getById(int $contactId): ?Contact
    {
        return Contact::query()
            ->where('id', '=', $contactId)
            ->first();
    }

    public function save(Contact $contact): bool
    {
        return $contact->save();
    }
}
