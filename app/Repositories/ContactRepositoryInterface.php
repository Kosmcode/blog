<?php

namespace App\Repositories;

use App\Http\Requests\Contact\CreateRequest;
use App\Models\Contact;

/**
 * Interface of Contact Repository
 */
interface ContactRepositoryInterface
{
    public function createByContactStoreRequest(CreateRequest $contactStoreRequest): ?Contact;

    public function existsByIpAddressAndCreatedAt(string $ipAddress, string $createdAt, string $createdAtOperator): bool;

    public function getById(int $contactId): ?Contact;

    public function save(Contact $contact): bool;
}
