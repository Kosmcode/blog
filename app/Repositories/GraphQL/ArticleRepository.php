<?php

namespace App\Repositories\GraphQL;

use App\Enums\Article\ImageThumbnailEnum;
use App\Enums\Article\RequestParameterEnum;
use App\Enums\Article\StatusEnum;
use App\Models\Article;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Query\JoinClause;
use Jenssegers\Agent\Facades\Agent;
use Nuwave\Lighthouse\Execution\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

final class ArticleRepository
{
    /**
     * @param  array<mixed>  $args
     */
    public function getArticlesFilters(
        mixed $root,
        array $args,
        GraphQLContext $context,
        ResolveInfo $resolveInfo
    ): Builder {
        $imageThumbnailEnum = ImageThumbnailEnum::getArticlesPaginationByUserDeviceType(Agent::deviceType());

        $sqlIfStatement = (DB::getDefaultConnection() === 'sqlite')
            ? 'IIF'
            : 'IF';

        return Article::query()
            ->select([
                'articles.id',
                'articles.category_id',
                'articles.title',
                'articles.content',
                DB::raw(
                    $sqlIfStatement.'(`article_image_thumbnails`.`filename` IS NULL, `articles`.`image`, `article_image_thumbnails`.`filename`) AS `image`'
                ),
                'articles.status',
                'articles.updated_at AS date',
                'slugs.slug',
            ])
            ->leftJoin(
                'slugs',
                function (JoinClause $joinClause) {
                    $joinClause
                        ->on('slugs.contentable_id', '=', 'articles.id')
                        ->where('slugs.contentable_type', '=', 'App\Models\Article')
                        ->select(['slugs.slug']);
                }
            )
            ->leftJoin(
                'article_image_thumbnails',
                function (JoinClause $joinClause) use ($imageThumbnailEnum) {
                    $joinClause->on('article_image_thumbnails.article_id', '=', 'articles.id')
                        ->where('article_image_thumbnails.width', '=', $imageThumbnailEnum->getWidth())
                        ->select('article_image_thumbnails.filename');
                }
            )
            ->with([
                'category' => function (BelongsTo $belongsTo) {
                    $belongsTo->select(['id', 'name']);
                },
                'tags' => function (BelongsToMany $belongsToMany) {
                    $belongsToMany->select(['name']);
                },
            ])
            ->when(
                isset($args[RequestParameterEnum::categories->value]),
                function (Builder $builder) use ($args) {
                    $builder->whereHas('category', function (Builder $builder) use ($args) {
                        $builder->whereIn('name', $args[RequestParameterEnum::categories->value]);
                    });
                }
            )
            ->when(
                isset($args[RequestParameterEnum::tags->value]),
                function (Builder $builder) use ($args) {
                    $builder->whereHas('tags', function (Builder $builder) use ($args) {
                        $builder->whereIn('name', $args[RequestParameterEnum::tags->value]);
                    });
                })
            ->where('articles.status', '=', StatusEnum::published->value)
            ->orderBy('articles.id', 'DESC');
    }
}
