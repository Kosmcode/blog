<?php

namespace App\Repositories;

use App\Models\MenuItem;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

class MenuItemRepository implements MenuItemRepositoryInterface
{
    public function getMainParentsLinksWithChildless(): Collection
    {
        return MenuItem::query()
            ->select(['id', 'name', 'type', 'link', 'parent_id', 'fa_icon'])
            ->whereNull('parent_id')
            ->with('children', function (HasMany $hasMany) {
                $hasMany->orderBy('lft')
                    ->with('children', function (HasMany $secondHasMany) {
                        $secondHasMany->orderBy('lft');
                    });
            })
            ->orderBy('lft')
            ->get();
    }
}
