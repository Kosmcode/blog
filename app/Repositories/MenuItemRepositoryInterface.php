<?php

namespace App\Repositories;

use Illuminate\Support\Collection;

interface MenuItemRepositoryInterface
{
    public function getMainParentsLinksWithChildless(): Collection;
}
