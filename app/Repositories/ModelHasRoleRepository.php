<?php

namespace App\Repositories;

use App\Models\ModelHasRole;

class ModelHasRoleRepository implements ModelHasRoleRepositoryInterface
{
    /** {@inheritDoc} */
    public function create(array $data): ModelHasRole
    {
        return ModelHasRole::query()
            ->create($data);
    }
}
