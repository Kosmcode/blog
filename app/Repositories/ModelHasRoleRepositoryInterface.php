<?php

namespace App\Repositories;

use App\Models\ModelHasRole;

interface ModelHasRoleRepositoryInterface
{
    /**
     * @param  array<mixed>  $data
     */
    public function create(array $data): ModelHasRole;
}
