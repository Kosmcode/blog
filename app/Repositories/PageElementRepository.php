<?php

namespace App\Repositories;

use App\Models\PageElement;

/**
 * Class of Page Element Repository
 */
class PageElementRepository implements PageElementRepositoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function updateOrCreate(string $title, string $description, string $content, bool $enabled): PageElement
    {
        return PageElement::query()
            ->updateOrCreate(
                [
                    'title' => $title,
                ],
                [
                    'description' => $description,
                    'content' => $content,
                    'enable' => $enabled,
                ]
            );
    }

    /**
     * {@inheritDoc}
     */
    public function getByTitle(string $title): ?PageElement
    {
        return PageElement::query()
            ->where('title', '=', $title)
            ->first();
    }
}
