<?php

namespace App\Repositories;

use App\Models\PageElement;

/**
 * Interface of Page Element Repository
 */
interface PageElementRepositoryInterface
{
    public function updateOrCreate(string $title, string $description, string $content, bool $enabled): PageElement;

    public function getByTitle(string $title): ?PageElement;
}
