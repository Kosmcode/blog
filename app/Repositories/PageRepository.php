<?php

namespace App\Repositories;

use App\Models\Page;

/**
 * Class of Page Repository
 */
class PageRepository implements PageRepositoryInterface
{
    public function findBySlug(string $slug): ?Page
    {
        return Page::query()
            ->where('slug', '=', $slug)
            ->first();
    }

    /** {@inheritDoc} */
    public function updateOrCreate(array $attributes, array $values = []): Page
    {
        return Page::query()
            ->updateOrCreate($attributes, $values);
    }
}
