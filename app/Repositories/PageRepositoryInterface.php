<?php

namespace App\Repositories;

use App\Models\Page;

/**
 * Interface of Page Repository
 */
interface PageRepositoryInterface
{
    public function findBySlug(string $slug): ?Page;

    /**
     * @param  array<mixed>  $attributes
     * @param  array<mixed>  $values
     */
    public function updateOrCreate(array $attributes, array $values = []): Page;
}
