<?php

namespace App\Repositories;

use App\Models\Role;

class RoleRepository implements RoleRepositoryInterface
{
    public function findOrCreate(string $roleName, ?string $roleGuard): \Spatie\Permission\Contracts\Role
    {
        return Role::findOrCreate($roleName, $roleGuard);
    }

    public function findByName(string $roleName): ?Role
    {
        return Role::query()
            ->where('name', '=', $roleName)
            ->first();
    }
}
