<?php

namespace App\Repositories;

use Spatie\Permission\Contracts\Role;

interface RoleRepositoryInterface
{
    public function findOrCreate(string $roleName, ?string $roleGuard): Role;

    public function findByName(string $roleName): ?\App\Models\Role;
}
