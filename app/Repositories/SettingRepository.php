<?php

namespace App\Repositories;

use App\Models\Setting;

/**
 * Repository of Setting
 */
class SettingRepository implements SettingRepositoryInterface
{
    /** {@inheritDoc} */
    public function updateOrCreateByKey(string $key, array $data): Setting
    {
        return Setting::query()
            ->updateOrCreate(
                ['key' => $key],
                $data
            );
    }

    public function findByKey(string $key): ?Setting
    {
        return Setting::query()
            ->where('key', '=', $key)
            ->first();
    }

    /** {@inheritDoc} */
    public function setValueByKey(string $key, mixed $value): bool
    {
        return (bool) Setting::query()
            ->where('key', '=', $key)
            ->update(
                ['value' => $value],
            );
    }
}
