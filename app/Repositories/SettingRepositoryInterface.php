<?php

namespace App\Repositories;

use App\Models\Setting;

/**
 * Interface of Setting Repository
 */
interface SettingRepositoryInterface
{
    /**
     * @param  array<mixed>  $data
     */
    public function updateOrCreateByKey(string $key, array $data): Setting;

    public function findByKey(string $key): ?Setting;

    /**
     * @param  array<mixed>  $value
     */
    public function setValueByKey(string $key, mixed $value): bool;
}
