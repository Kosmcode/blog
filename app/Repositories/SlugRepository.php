<?php

namespace App\Repositories;

use App\Models\Slug;
use Illuminate\Support\Collection;

/**
 * Class of Slug Repository
 */
class SlugRepository implements SlugRepositoryInterface
{
    public function findBySlug(string $slug): ?Slug
    {
        return Slug::query()
            ->where('slug', '=', $slug)
            ->first();
    }

    public function getToMenuItemsLinks(): Collection
    {
        return Slug::query()
            ->select(['contentable_type', 'contentable_id', 'slug'])
            ->get();
    }

    /** {@inheritDoc} */
    public function updateOrCreate(array $attributes, array $values = []): Slug
    {
        return Slug::query()
            ->updateOrCreate($attributes, $values);
    }

    public function save(Slug $slug): bool
    {
        return $slug->save();
    }
}
