<?php

namespace App\Repositories;

use App\Models\Slug;
use Illuminate\Support\Collection;

/**
 * Interface of Slug Repository
 */
interface SlugRepositoryInterface
{
    public function findBySlug(string $slug): ?Slug;

    public function getToMenuItemsLinks(): Collection;

    /**
     * @param  array<mixed>  $attributes
     * @param  array<mixed>  $values
     */
    public function updateOrCreate(array $attributes, array $values = []): Slug;

    public function save(Slug $slug): bool;
}
