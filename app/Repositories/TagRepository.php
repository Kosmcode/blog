<?php

namespace App\Repositories;

use App\Enums\Article\StatusEnum;
use App\Models\Tag;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

class TagRepository implements TagRepositoryInterface
{
    /** {@inheritDoc} */
    public function getForArticleFilters(): array
    {
        return Tag::query()
            ->select(['name'])
            ->whereHas('articles', function (Builder $builder) {
                $builder->where('status', '=', StatusEnum::published->value);
            })
            ->get()
            ->pluck(['name'])
            ->toArray();
    }

    public function existBySlug(string $tagSlug): bool
    {
        return Tag::query()
            ->where('slug', '=', $tagSlug)
            ->exists();
    }

    public function findBySlug(string $tagSlug): ?Tag
    {
        return Tag::query()
            ->where('slug', '=', $tagSlug)
            ->first();
    }

    public function getToMenuItemsLinks(): Collection
    {
        return Tag::query()
            ->select(['name', 'slug'])
            ->get();
    }
}
