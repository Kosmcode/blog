<?php

namespace App\Repositories;

use App\Models\Tag;
use Illuminate\Support\Collection;

interface TagRepositoryInterface
{
    /**
     * @return array<mixed>
     */
    public function getForArticleFilters(): array;

    public function existBySlug(string $tagSlug): bool;

    public function findBySlug(string $tagSlug): ?Tag;

    public function getToMenuItemsLinks(): Collection;
}
