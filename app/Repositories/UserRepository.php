<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository implements UserRepositoryInterface
{
    public function existUserByEmail(string $email): bool
    {
        return User::query()
            ->where('email', '=', $email)
            ->exists();
    }

    public function existUserByName(string $username): bool
    {
        return User::query()
            ->where('name', '=', $username)
            ->exists();
    }

    /** {@inheritDoc} */
    public function create(array $data): User
    {
        return User::query()
            ->create($data);
    }

    public function deleteByModel(User $user): bool
    {
        $deleted = $user->delete();

        if ($deleted === null || $deleted === false) {
            return false;
        }

        return true;
    }
}
