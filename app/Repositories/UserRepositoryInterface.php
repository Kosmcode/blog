<?php

namespace App\Repositories;

use App\Models\User;

interface UserRepositoryInterface
{
    public function existUserByEmail(string $email): bool;

    public function existUserByName(string $username): bool;

    /**
     * @param  array<mixed>  $data
     */
    public function create(array $data): User;

    public function deleteByModel(User $user): ?bool;
}
