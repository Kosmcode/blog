<?php

namespace App\Rules\Article\Category;

use App\Services\Article\Filter\Category\GetServiceInterface;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class NameExistRule implements ValidationRule
{
    public function __construct(
        protected readonly GetServiceInterface $categoryService
    ) {}

    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if (! is_array($value)) {
            return;
        }

        if (array_diff($value, $this->categoryService->getForFilters()) !== []) {
            $fail('rule.articleCategoryNameExistRule.categoryNotExist')->translate();
        }
    }
}
