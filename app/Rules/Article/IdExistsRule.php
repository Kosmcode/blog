<?php

namespace App\Rules\Article;

use App\Repositories\ArticleRepositoryInterface;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class IdExistsRule implements ValidationRule
{
    private ArticleRepositoryInterface $articleRepository;

    public function __construct(ArticleRepositoryInterface $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }

    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if (! is_int($value)) {
            return;
        }

        if (! $this->articleRepository->articleByIdExists($value)) {
            $fail('Article not exists');
        }
    }
}
