<?php

namespace App\Rules\Article\Tag;

use App\Services\Article\Filter\Tag\GetServiceInterface;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class NameExistRule implements ValidationRule
{
    public function __construct(
        protected readonly GetServiceInterface $tagService
    ) {}

    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if (! is_array($value)) {
            return;
        }

        if (array_diff($value, $this->tagService->getForFilters()) !== []) {
            $fail('rule.articleTagsNameExistRule.tagNotExist')->translate();
        }
    }
}
