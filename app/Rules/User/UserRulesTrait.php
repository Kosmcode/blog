<?php

namespace App\Rules\User;

use App\Enums\User\RequestParameterEnum;
use Illuminate\Validation\Rules\Password;

trait UserRulesTrait
{
    /**
     * @return array<mixed>
     */
    protected function getStoreUsernameRules(): array
    {
        return [
            RequestParameterEnum::name->value => [
                'string',
                'min:3',
                'max:255',
                'unique:users,name',
            ],
        ];
    }

    /**
     * @return array<mixed>
     */
    protected function getStoreUserEmailRules(): array
    {
        return [
            RequestParameterEnum::email->value => [
                'required',
                'email',
                'unique:users,email',
            ],
        ];
    }

    /**
     * @return array<mixed>
     */
    protected function getStoreUserPasswordsRules(): array
    {
        return [
            RequestParameterEnum::password->value => [
                'required',
                'confirmed',
                Password::defaults(),
            ],
        ];
    }
}
