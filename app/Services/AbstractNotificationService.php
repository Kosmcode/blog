<?php

namespace App\Services;

use App\Interfaces\NotifiableInterface;

abstract class AbstractNotificationService implements NotifiableInterface
{
    protected ?string $notification = null;

    public function getNotification(): ?string
    {
        return $this->notification;
    }

    protected function setNotification(string $notification): void
    {
        $this->notification = $notification;
    }
}
