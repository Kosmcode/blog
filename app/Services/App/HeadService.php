<?php

namespace App\Services\App;

use App\Enums\Intertia\PropsEnum;
use Inertia\ResponseFactory as InertiaResponseFactory;

/**
 * App Head ElementService
 */
class HeadService implements HeadServiceInterface
{
    private string $metaTitle = '';

    private string $metaDescription = '';

    private string $metaKeywords = '';

    public function __construct(
        protected readonly InertiaResponseFactory $inertiaResponseFactory
    ) {}

    /**
     * {@inheritDoc}
     */
    public function setMetas(?string $metaTitle = '', ?string $metaDescription = '', ?string $metaKeywords = ''): void
    {
        $this->metaTitle = $metaTitle ?? '';
        $this->metaDescription = $metaDescription ?? '';
        $this->metaKeywords = $metaKeywords ?? '';

        $this->shareAllMetasToInertia();
    }

    /**
     * {@inheritDoc}
     */
    public function setMetaTitle(?string $metaTitle): void
    {
        $this->metaTitle = $metaTitle ?? '';

        $this->shareAllMetasToInertia();
    }

    /**
     * {@inheritDoc}
     */
    public function setMetaDescription(?string $metaDescription): void
    {
        $this->metaDescription = $metaDescription ?? '';

        $this->shareAllMetasToInertia();
    }

    /**
     * {@inheritDoc}
     */
    public function setMetaKeywords(?string $metaKeywords): void
    {
        $this->metaKeywords = $metaKeywords ?? '';

        $this->shareAllMetasToInertia();
    }

    /**
     * {@inheritDoc}
     */
    public function getLinkCanonical(): string
    {
        return getCanonicalUrl();
    }

    /**
     * {@inheritDoc}
     */
    public function toArray(): array
    {
        return [
            'title' => $this->metaTitle,
            'description' => $this->metaDescription,
            'keywords' => $this->metaKeywords,
            'canonical' => $this->getLinkCanonical(),
        ];
    }

    /**
     * Method shares all set metas data to Inertia
     */
    protected function shareAllMetasToInertia(): void
    {
        $this->inertiaResponseFactory->share(PropsEnum::appHead->value, $this->toArray());
    }
}
