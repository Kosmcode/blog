<?php

namespace App\Services\App;

/**
 * Interface of App Head ElementService
 */
interface HeadServiceInterface
{
    /**
     * Method to set all meta head
     */
    public function setMetas(?string $metaTitle = '', ?string $metaDescription = '', ?string $metaKeywords = ''): void;

    /**
     * Method to set title meta head
     */
    public function setMetaTitle(?string $metaTitle): void;

    /**
     * Method to set description meta head
     */
    public function setMetaDescription(?string $metaDescription): void;

    /**
     * Method to set keywords meta head
     */
    public function setMetaKeywords(?string $metaKeywords): void;

    /**
     * Method to get link canonical
     */
    public function getLinkCanonical(): string;

    /**
     * Method return array with all set metas data
     *
     * @return array<mixed>
     */
    public function toArray(): array;
}
