<?php

namespace App\Services\Article\Category;

use App\Dto\ResponseDTO;
use App\Enums\Article\RequestParameterEnum;
use App\Enums\Intertia\ComponentEnum;
use App\Enums\Setting\KeyEnum;
use App\Exceptions\CategoryException;
use App\Http\Resources\Articles\FiltersResource;
use App\Repositories\CategoryRepositoryInterface;
use App\Services\Article\Filter\GetServiceInterface as ArticleFilterGetServiceInterface;
use App\Services\SettingServiceInterface;

/**
 * Article Category Get Service
 */
class GetService implements GetServiceInterface
{
    public function __construct(
        protected readonly SettingServiceInterface $settingService,
        protected readonly CategoryRepositoryInterface $categoryRepository,
        protected readonly ArticleFilterGetServiceInterface $articleFilterGetService
    ) {}

    /**
     * {@inheritDoc}
     */
    public function getArticleByCategorySlug(string $categorySlug): ResponseDTO
    {
        $category = $this->categoryRepository->findBySlug($categorySlug);

        if (! $category) {
            throw new CategoryException('Not found by categorySlug');
        }

        $articles = $this->articleFilterGetService->getArticlesFiltersDTOByParameters([
            RequestParameterEnum::categories->value => [$category->getName()],
        ]);

        return (new ResponseDTO())
            ->setInertiaComponentName(ComponentEnum::articleFilters->value)
            ->setMetaTitle($this->settingService->get(KeyEnum::articlesMetaTitle->value)) // @phpstan-ignore-line
            ->setMetaDescription($this->settingService->get(KeyEnum::articlesMetaDescription->value)) // @phpstan-ignore-line
            ->setMetaKeywords($this->settingService->get(KeyEnum::articlesMetaKeywords->value)) // @phpstan-ignore-line
            ->setResourceData($articles, FiltersResource::class);
    }
}
