<?php

namespace App\Services\Article\Category;

use App\Dto\ResponseDTO;
use App\Exceptions\CategoryException;

/**
 * Interface of Article Category Get Service
 */
interface GetServiceInterface
{
    /**
     * Method to get Article By Category Slug
     *
     * @throws CategoryException
     */
    public function getArticleByCategorySlug(string $categorySlug): ResponseDTO;
}
