<?php

namespace App\Services\Article\Filter\Category;

use App\Enums\Cache\KeyEnum;
use App\Repositories\CategoryRepositoryInterface;
use App\Services\Cache\AbstractService;
use Illuminate\Cache\Repository as LaravelCache;

class GetService extends AbstractService implements GetServiceInterface
{
    public function __construct(
        protected LaravelCache $laravelCache,
        protected readonly CategoryRepositoryInterface $categoryRepository,
    ) {
        parent::__construct($laravelCache);
    }

    /** {@inheritDoc} */
    public function getForFilters(bool $cached = true): array
    {
        if (! $cached) {
            return $this->categoryRepository->getToArticlesFilters();
        }

        $cacheValue = $this->cacheGet(KeyEnum::articlesCategories);

        if ($cacheValue) {
            return $cacheValue; // @phpstan-ignore-line
        }

        return $this->cacheForever( // @phpstan-ignore-line
            KeyEnum::articlesCategories,
            $this->categoryRepository->getToArticlesFilters()
        );
    }
}
