<?php

namespace App\Services\Article\Filter;

use App\Dto\ArticlesFiltersDTO;
use App\Dto\FiltersMultipleDTO;
use App\Enums\Article\RequestParameterEnum;
use App\Enums\Setting\KeyEnum;
use App\Repositories\ArticleRepositoryInterface;
use App\Services\Article\Filter\Category\GetServiceInterface as CategoryGetServiceInterface;
use App\Services\Article\Filter\Tag\GetServiceInterface as TagGetServiceInterface;
use App\Services\Article\Recent\GetServiceInterface as RecentGetServiceInterface;
use App\Services\SettingServiceInterface;

/**
 * Article Filter Get Service
 */
class GetService implements GetServiceInterface
{
    public function __construct(
        protected readonly ArticleRepositoryInterface $articleRepository,
        protected readonly CategoryGetServiceInterface $categoryService,
        protected readonly TagGetServiceInterface $tagService,
        protected readonly SettingServiceInterface $settingService,
        protected readonly RecentGetServiceInterface $recentArticleService,
    ) {}

    /** {@inheritDoc} */
    public function getArticlesFiltersDTOByParameters(array $parameters): ArticlesFiltersDTO
    {
        $articles = $this->articleRepository->getArticlesToFilters($parameters);

        $categories = (new FiltersMultipleDTO())->setValues($this->categoryService->getForFilters());

        if (isset($parameters[RequestParameterEnum::categories->value])) {
            $categories->setSelectedValues($parameters[RequestParameterEnum::categories->value]); // @phpstan-ignore-line
        }

        $tags = (new FiltersMultipleDTO())->setValues($this->tagService->getForFilters());

        if (isset($parameters[RequestParameterEnum::tags->value])) {
            $tags->setSelectedValues($parameters[RequestParameterEnum::tags->value]);
        }

        return (new ArticlesFiltersDTO())
            ->setPaginatedArticles($articles)
            ->setCategories($categories)
            ->setTags($tags)
            ->setRecentArticles($this->recentArticleService->getRecentArticles())
            ->setPageLimit($this->settingService->get(KeyEnum::articlesPageLimit->value)); // @phpstan-ignore-line
    }
}
