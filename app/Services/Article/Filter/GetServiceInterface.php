<?php

namespace App\Services\Article\Filter;

use App\Dto\ArticlesFiltersDTO;

/**
 * Interface of Article Filter Get Service
 */
interface GetServiceInterface
{
    /**
     * @param  array<mixed>  $parameters
     */
    public function getArticlesFiltersDTOByParameters(array $parameters): ArticlesFiltersDTO;
}
