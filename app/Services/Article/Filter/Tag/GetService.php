<?php

namespace App\Services\Article\Filter\Tag;

use App\Enums\Cache\KeyEnum;
use App\Repositories\TagRepositoryInterface;
use App\Services\Cache\AbstractService;
use Illuminate\Cache\Repository as LaravelCache;

class GetService extends AbstractService implements GetServiceInterface
{
    public function __construct(
        protected LaravelCache $laravelCache,
        protected readonly TagRepositoryInterface $tagRepository
    ) {
        parent::__construct($laravelCache);
    }

    /**
     * @return array<mixed>
     */
    public function getForFilters(bool $cached = true): array
    {
        if (! $cached) {
            return $this->tagRepository->getForArticleFilters();
        }

        $cacheValue = $this->cacheGet(KeyEnum::articlesTags);

        if ($cacheValue) {
            return $cacheValue; // @phpstan-ignore-line
        }

        return $this->cacheForever( // @phpstan-ignore-line
            KeyEnum::articlesTags,
            $this->tagRepository->getForArticleFilters()
        );
    }
}
