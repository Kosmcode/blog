<?php

namespace App\Services\Article\Filter\Tag;

use App\Services\Cache\ServiceInterface;

interface GetServiceInterface extends ServiceInterface
{
    /**
     * @return array<mixed>
     */
    public function getForFilters(bool $cached = true): array;
}
