<?php

namespace App\Services\Article;

use App\Dto\ArticleDTO;
use App\Dto\ResponseDTO;
use App\Enums\Intertia\ComponentEnum;
use App\Enums\Setting\KeyEnum;
use App\Http\Resources\Article\Resource;
use App\Http\Resources\Articles\FiltersResource;
use App\Repositories\ArticleRepositoryInterface;
use App\Services\Article\Filter\GetServiceInterface as ArticleFilterGetServiceInterface;
use App\Services\SettingServiceInterface;
use Illuminate\Support\Collection;

/**
 * Article Get Service
 */
class GetService implements GetServiceInterface
{
    public function __construct(
        protected readonly ArticleFilterGetServiceInterface $articleFilterGetService,
        protected readonly SettingServiceInterface $settingService,
        protected readonly ArticleRepositoryInterface $articleRepository,
    ) {}

    /** {@inheritDoc} */
    public function getArticlesToWeb(): ResponseDTO
    {
        return (new ResponseDTO())
            ->setInertiaComponentName(ComponentEnum::articleFilters->value)
            ->setMetaTitle($this->settingService->get(KeyEnum::articlesMetaTitle->value)) // @phpstan-ignore-line
            ->setMetaDescription($this->settingService->get(KeyEnum::articlesMetaDescription->value)) // @phpstan-ignore-line
            ->setMetaKeywords($this->settingService->get(KeyEnum::articlesMetaKeywords->value)) // @phpstan-ignore-line
            ->setResourceData(
                $this->articleFilterGetService->getArticlesFiltersDTOByParameters([]),
                FiltersResource::class
            );
    }

    /** {@inheritDoc} */
    public function getArticleToAdminPreviewByArticleId(int $articleId): ?ResponseDTO
    {
        $article = $this->articleRepository->getById($articleId);

        if (! $article) {
            return null;
        }

        if (! $article->getSlug()) {
            return null;
        }

        $articleDTO = (new ArticleDTO())
            ->setArticle($article)
            ->setRecentArticles(new Collection());

        return (new ResponseDTO())
            ->setInertiaComponentName(ComponentEnum::singleArticle->value)
            ->setMetaDataFromSlug($article->getSlug())
            ->setResourceData($articleDTO, Resource::class);
    }
}
