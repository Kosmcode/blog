<?php

namespace App\Services\Article;

use App\Dto\ResponseDTO;

/**
 * Interface of Article Get Service
 */
interface GetServiceInterface
{
    /**
     * Method to get articles to web
     */
    public function getArticlesToWeb(): ResponseDTO;

    /**
     * Method to get article to admin preview by articleId
     */
    public function getArticleToAdminPreviewByArticleId(int $articleId): ?ResponseDTO;
}
