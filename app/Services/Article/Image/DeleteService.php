<?php

namespace App\Services\Article\Image;

use App\Enums\FilesystemDiskEnum;
use App\Models\Article;
use App\Models\ArticleImageThumbnail;
use App\Repositories\ArticleImageThumbnailRepositoryInterface;
use Illuminate\Filesystem\FilesystemManager;

class DeleteService implements DeleteServiceInterface
{
    public function __construct(
        protected readonly FilesystemManager $filesystemManager,
        protected readonly ArticleImageThumbnailRepositoryInterface $articleImageThumbnailRepository,
    ) {}

    public function deleteAllImagesOfArticle(Article $article): void
    {
        $articleImagesDisk = $this->filesystemManager->disk(FilesystemDiskEnum::articleImages->value);

        if ($article->getImage()) {
            $articleImagesDisk->delete($article->getImage());
        }

        /** @var ArticleImageThumbnail $articleImageThumbnail */
        foreach ($article->getImageThumbnails() as $articleImageThumbnail) {
            $articleImagesDisk->delete($articleImageThumbnail->getFilename());

            $this->articleImageThumbnailRepository->delete($articleImageThumbnail);
        }
    }
}
