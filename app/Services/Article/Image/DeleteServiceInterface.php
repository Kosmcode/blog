<?php

namespace App\Services\Article\Image;

use App\Models\Article;

interface DeleteServiceInterface
{
    public function deleteAllImagesOfArticle(Article $article): void;
}
