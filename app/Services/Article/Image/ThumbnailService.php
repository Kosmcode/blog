<?php

namespace App\Services\Article\Image;

use App\Enums\Article\ImageThumbnailEnum;
use App\Enums\Cache\KeyEnum;
use App\Enums\FilesystemDiskEnum;
use App\Exceptions\ArticleException;
use App\Models\ArticleImageThumbnail;
use App\Repositories\ArticleImageThumbnailRepositoryInterface;
use App\Repositories\ArticleRepositoryInterface;
use App\Services\Cache\AbstractService;
use Illuminate\Cache\Repository as LaravelCache;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Filesystem\FilesystemManager;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Intervention\Image\ImageManager;

class ThumbnailService extends AbstractService implements ThumbnailServiceInterface
{
    private Filesystem $articleImagesDisk;

    public function __construct(
        protected readonly ArticleRepositoryInterface $articleRepository,
        protected readonly FilesystemManager $filesystemManager,
        protected readonly ImageManager $imageManager,
        protected readonly ArticleImageThumbnailRepositoryInterface $articleImageThumbnailRepository,
        protected LaravelCache $laravelCache,
    ) {
        parent::__construct($laravelCache);
    }

    /**
     * {@inheritDoc}
     */
    public function createOrUpdateThumbnailsByArticleId(int $articleId): void
    {
        $article = $this->articleRepository->getById($articleId);

        if (! $article) {
            throw new ArticleException('Not exists by '.$articleId.' ID');
        }

        if (! $article->getImage()) {
            return;
        }

        $articleTitleSlugged = Str::slug($article->getTitle());

        $imageManager = $this->imageManager
            ->configure(['driver' => 'imagick']);

        $this->articleImagesDisk = $this->filesystemManager->disk(FilesystemDiskEnum::articleImages->value);

        $articleImagesDiskPath = $this->articleImagesDisk->path('');

        $articleImageThumbnails = $article->getImageThumbnails();

        $generatedThumbnails = [];

        foreach (ImageThumbnailEnum::cases() as $imageThumbEnum) {
            if (isset($generatedThumbnails[$imageThumbEnum->getWidth()])) {
                continue;
            }

            $imageThumbnailFilename = $this->prepareImageThumbnailFilename(
                $articleTitleSlugged,
                $imageThumbEnum->getWidth()
            );

            $articleImageThumbnail = $imageManager->make($this->articleImagesDisk->path($article->getImage()));

            $articleImageThumbnail
                ->resize($imageThumbEnum->getWidth(),
                    null,
                    function ($constraint) {
                        // @codeCoverageIgnoreStart
                        $constraint->aspectRatio();
                        // @codeCoverageIgnoreEnd
                    }
                );
            $articleImageThumbnail
                ->save(
                    $articleImagesDiskPath.$imageThumbnailFilename,
                    self::IMAGE_THUMBNAIL_QUALITY,
                    self::IMAGE_THUMBNAIL_FORMAT
                );

            $generatedThumbnails[$imageThumbEnum->getWidth()] = $imageThumbnailFilename;

            $this->updateOrCreateArticleImageThumbnail(
                $articleImageThumbnails,
                $imageThumbEnum->getWidth(),
                $article->getId(), // @phpstan-ignore-line
                $imageThumbnailFilename
            );
        }

        $this->cacheForget(KeyEnum::recentArticles);
    }

    protected function prepareImageThumbnailFilename(
        string $articleTitle,
        int $imageThumbnailWidth
    ): string {
        return sprintf(
            '%s_%s.%s',
            $articleTitle,
            $imageThumbnailWidth,
            self::IMAGE_THUMBNAIL_FORMAT
        );
    }

    protected function updateOrCreateArticleImageThumbnail(
        Collection $articleImageThumbnails,
        int $thumbnailWidth,
        int $articleId,
        string $thumbnailFilename
    ): void {
        /** @var ArticleImageThumbnail|null $imageThumbnail */
        $imageThumbnail = $articleImageThumbnails->firstWhere('width', $thumbnailWidth);

        if ($imageThumbnail) {
            if ($thumbnailFilename !== $imageThumbnail->getFilename()) {
                $this->articleImagesDisk->delete($imageThumbnail->getFilename());
            }

            $imageThumbnail->setFilename($thumbnailFilename);

            $this->articleImageThumbnailRepository->save($imageThumbnail);

            return;
        }

        $newArticleImageThumbnail = $this->articleImageThumbnailRepository
            ->new()
            ->setWidth($thumbnailWidth)
            ->setArticleId($articleId)
            ->setFilename($thumbnailFilename);

        $this->articleImageThumbnailRepository->save($newArticleImageThumbnail);
    }
}
