<?php

namespace App\Services\Article\Image;

use App\Exceptions\ArticleException;

interface ThumbnailServiceInterface
{
    const IMAGE_THUMBNAIL_FORMAT = 'webp';

    const IMAGE_THUMBNAIL_QUALITY = 75;

    /**
     * @throws ArticleException
     */
    public function createOrUpdateThumbnailsByArticleId(int $articleId): void;
}
