<?php

namespace App\Services\Article\Recent;

use App\Enums\Cache\KeyEnum;
use App\Repositories\ArticleRepositoryInterface;
use App\Services\Cache\AbstractService;
use Illuminate\Cache\Repository as LaravelCache;
use Illuminate\Support\Collection;

class GetService extends AbstractService implements GetServiceInterface
{
    public function __construct(
        protected ArticleRepositoryInterface $articleRepository,
        protected LaravelCache $laravelCache
    ) {
        parent::__construct($laravelCache);
    }

    public function getRecentArticles(bool $cached = true): Collection
    {
        if (! $cached) {
            return $this->articleRepository->getRecentArticles();
        }

        $recentArticles = $this->cacheGet(KeyEnum::recentArticles);

        if ($recentArticles) {
            return $recentArticles; // @phpstan-ignore-line
        }

        return $this->cacheForever( // @phpstan-ignore-line
            KeyEnum::recentArticles->value,
            $this->articleRepository->getRecentArticles()
        );
    }
}
