<?php

namespace App\Services\Article\Recent;

use Illuminate\Support\Collection;

interface GetServiceInterface
{
    public function getRecentArticles(bool $cached = true): Collection;
}
