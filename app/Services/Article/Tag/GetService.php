<?php

namespace App\Services\Article\Tag;

use App\Dto\ResponseDTO;
use App\Enums\Article\RequestParameterEnum;
use App\Enums\Intertia\ComponentEnum;
use App\Enums\Setting\KeyEnum;
use App\Http\Resources\Articles\FiltersResource;
use App\Repositories\TagRepositoryInterface;
use App\Services\Article\Filter\GetServiceInterface as ArticleFilterGetServiceInterface;
use App\Services\SettingServiceInterface;

/**
 * Article Tag Get Service
 */
class GetService implements GetServiceInterface
{
    public function __construct(
        protected readonly SettingServiceInterface $settingService,
        protected readonly TagRepositoryInterface $tagRepository,
        protected readonly ArticleFilterGetServiceInterface $articleFilterGetService
    ) {}

    /** {@inheritDoc} */
    public function getArticleByTagSlug(string $tagSlug): ResponseDTO
    {
        $tag = $this->tagRepository->findBySlug($tagSlug);

        $responseDTO = new ResponseDTO();

        if (! $tag) {
            throw $responseDTO->createHttpException(exceptionMessage: 'Not found tag by slug');
        }

        $articles = $this->articleFilterGetService->getArticlesFiltersDTOByParameters([
            RequestParameterEnum::tags->value => [$tag->getName()],
        ]);

        return (new ResponseDTO())
            ->setMetaTitle($this->settingService->get(KeyEnum::articlesMetaTitle->value)) // @phpstan-ignore-line
            ->setMetaDescription($this->settingService->get(KeyEnum::articlesMetaDescription->value)) // @phpstan-ignore-line
            ->setMetaKeywords($this->settingService->get(KeyEnum::articlesMetaKeywords->value)) // @phpstan-ignore-line
            ->setInertiaComponentName(ComponentEnum::articleFilters->value)
            ->setResourceData($articles, FiltersResource::class);
    }
}
