<?php

namespace App\Services\Article\Tag;

use App\Dto\ResponseDTO;

/**
 * Interface of Article Tag Get Service
 */
interface GetServiceInterface
{
    /**
     * Method to get article by tag slug
     */
    public function getArticleByTagSlug(string $tagSlug): ResponseDTO;
}
