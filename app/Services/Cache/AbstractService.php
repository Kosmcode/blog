<?php

namespace App\Services\Cache;

use App\Enums\Cache\KeyEnum;
use Illuminate\Cache\Repository as LaravelCache;

abstract class AbstractService implements ServiceInterface
{
    public function __construct(
        protected LaravelCache $laravelCache,
    ) {}

    public function cacheForget(KeyEnum|string $key): bool
    {
        return $this->laravelCache->forget($this->getCacheKey($key));
    }

    public function cacheGet(KeyEnum|string $key): mixed
    {
        return $this->laravelCache->get($this->getCacheKey($key));
    }

    public function cacheForever(KeyEnum|string $key, mixed $value): mixed
    {
        $this->laravelCache->forever(
            $this->getCacheKey($key),
            $value
        );

        return $value;
    }

    protected function getCacheKey(KeyEnum|string $keyConfig): string
    {
        return ($keyConfig instanceof KeyEnum)
            ? $keyConfig->value
            : $keyConfig;
    }
}
