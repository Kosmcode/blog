<?php

namespace App\Services\Cache;

use App\Enums\Cache\KeyEnum;

interface ServiceInterface
{
    public function cacheForget(KeyEnum|string $key): bool;

    public function cacheGet(KeyEnum|string $key): mixed;

    public function cacheForever(KeyEnum|string $key, mixed $value): mixed;
}
