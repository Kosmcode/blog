<?php

namespace App\Services\Contact;

use App\Enums\Setting\KeyEnum;
use App\Repositories\ContactRepositoryInterface;
use App\Services\SettingServiceInterface;
use Carbon\Carbon;

class AvailableSentService implements AvailableSentServiceInterface
{
    public function __construct(
        protected readonly ContactRepositoryInterface $contactRepository,
        protected readonly SettingServiceInterface $settingService,
    ) {}

    public function availableSendByIpAddress(?string $ipAddress): bool
    {
        if (! $ipAddress) {
            return false;
        }

        $contactCanSendTimeInMinutes = $this->settingService->get(KeyEnum::contactCanSendTimeInMinutes->value);

        if (! is_numeric($contactCanSendTimeInMinutes)) {
            return true;
        }

        return ! $this->contactRepository->existsByIpAddressAndCreatedAt(
            $ipAddress,
            Carbon::now()
                ->subMinutes((int) $contactCanSendTimeInMinutes)
                ->toDateTimeString(),
            self::CREATED_AT_OPERATOR
        );
    }
}
