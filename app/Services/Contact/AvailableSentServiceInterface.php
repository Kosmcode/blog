<?php

namespace App\Services\Contact;

/**
 * Interface of Contact Check ElementService
 */
interface AvailableSentServiceInterface
{
    const CREATED_AT_OPERATOR = '>=';

    /**
     * Method to check is avaiable send message
     */
    public function availableSendByIpAddress(?string $ipAddress): bool;
}
