<?php

namespace App\Services\Contact;

use App\Dto\ContactResourceDTO;
use App\Dto\ResponseDTO;
use App\Enums\Intertia\ComponentEnum;
use App\Enums\Page\ElementEnum;
use App\Enums\Setting\KeyEnum;
use App\Http\Requests\Contact\CreateRequest;
use App\Http\Resources\ContactResource;
use App\Jobs\Contact\SendJob;
use App\Repositories\ContactRepositoryInterface;
use App\Services\Page\Element\GetServiceInterface as PageElementGetServiceInterface;
use App\Services\SettingServiceInterface;
use Illuminate\Contracts\Queue\Queue as LaravelQueue;

class CreateService implements CreateServiceInterface
{
    public function __construct(
        protected readonly ContactRepositoryInterface $contactRepository,
        protected readonly SettingServiceInterface $settingService,
        protected readonly LaravelQueue $laravelQueue,
        protected readonly PageElementGetServiceInterface $elementService
    ) {}

    public function fromCreateRequest(CreateRequest $contactCreateRequest): ResponseDTO
    {
        $contact = $this->contactRepository->createByContactStoreRequest($contactCreateRequest);

        $responseDTO = (new ResponseDTO())
            ->setInertiaComponentName(ComponentEnum::contact->value)
            ->setMetaTitle($this->settingService->get(KeyEnum::contactMetaTitle->value))// @phpstan-ignore-line
            ->setMetaDescription($this->settingService->get(KeyEnum::contactMetaDescription->value))// @phpstan-ignore-line
            ->setMetaKeywords($this->settingService->get(KeyEnum::contactMetaKeywords->value)); // @phpstan-ignore-line

        $contactResourceDTO = (new ContactResourceDTO())
            ->setTopPageElement($this->elementService->getByElementEnum(ElementEnum::contactPageTop))
            ->setDownPageElement($this->elementService->getByElementEnum(ElementEnum::contactPageDown));

        if (! $contact) {
            $contactResourceDTO
                ->setResult(false)
                ->setCanSend(true);

            return $responseDTO->setResourceData($contactResourceDTO, ContactResource::class);
        }

        if ($this->settingService->get(KeyEnum::contactSendToMailEnabled->value)) {
            $this->laravelQueue->push(new SendJob($contact->getId()));
        }

        $contactResourceDTO
            ->setResult(true)
            ->setCanSend(false);

        return $responseDTO->setResourceData($contactResourceDTO, ContactResource::class);
    }
}
