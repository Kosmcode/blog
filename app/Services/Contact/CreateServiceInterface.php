<?php

namespace App\Services\Contact;

use App\Dto\ResponseDTO;
use App\Http\Requests\Contact\CreateRequest;

interface CreateServiceInterface
{
    /**
     * Method to create contact from Contact Create Request
     */
    public function fromCreateRequest(CreateRequest $contactCreateRequest): ResponseDTO;
}
