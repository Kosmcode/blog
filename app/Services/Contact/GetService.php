<?php

namespace App\Services\Contact;

use App\Dto\ContactResourceDTO;
use App\Dto\ResponseDTO;
use App\Enums\Intertia\ComponentEnum;
use App\Enums\Page\ElementEnum;
use App\Enums\Setting\KeyEnum;
use App\Http\Resources\ContactResource;
use App\Services\Page\Element\GetServiceInterface as PageElementGetServiceInterface;
use App\Services\SettingServiceInterface;

class GetService implements GetServiceInterface
{
    public function __construct(
        protected readonly AvailableSentServiceInterface $contactCheckService,
        protected readonly SettingServiceInterface $settingService,
        protected readonly PageElementGetServiceInterface $pageElementGetService
    ) {}

    public function getToInertia(?string $clientIP): ResponseDTO
    {
        $contactResourceDTO = (new ContactResourceDTO())
            ->setCanSend($clientIP && $this->contactCheckService->availableSendByIpAddress($clientIP))
            ->setTopPageElement($this->pageElementGetService->getByElementEnum(ElementEnum::contactPageTop))
            ->setDownPageElement($this->pageElementGetService->getByElementEnum(ElementEnum::contactPageDown));

        return (new ResponseDTO())
            ->setInertiaComponentName(ComponentEnum::contact->value)
            ->setResourceData($contactResourceDTO, ContactResource::class)
            ->setMetaTitle($this->settingService->get(KeyEnum::contactMetaTitle->value)) // @phpstan-ignore-line
            ->setMetaDescription($this->settingService->get(KeyEnum::contactMetaDescription->value)) // @phpstan-ignore-line
            ->setMetaKeywords($this->settingService->get(KeyEnum::contactMetaKeywords->value)); // @phpstan-ignore-line
    }
}
