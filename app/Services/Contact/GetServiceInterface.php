<?php

namespace App\Services\Contact;

use App\Dto\ResponseDTO;

/**
 * Interface of Contact Get ElementService
 */
interface GetServiceInterface
{
    /**
     * Method to get to Inertia response
     */
    public function getToInertia(?string $clientIP): ResponseDTO;
}
