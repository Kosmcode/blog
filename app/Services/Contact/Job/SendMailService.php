<?php

namespace App\Services\Contact\Job;

use App\Enums\Log\ChannelEnum;
use App\Enums\Setting\KeyEnum;
use App\Mail\Contact\SendMail;
use App\Repositories\ContactRepositoryInterface;
use App\Services\SettingServiceInterface;
use Illuminate\Log\LogManager as LaravelLog;
use Illuminate\Mail\Mailer as LaravelMail;

class SendMailService implements SendMailServiceInterface
{
    public function __construct(
        protected readonly LaravelLog $laravelLog,
        protected readonly SettingServiceInterface $settingService,
        protected readonly ContactRepositoryInterface $contactRepository,
        protected readonly LaravelMail $laravelMail
    ) {}

    public function sendMailFromJobByContactId(int $contactId): void
    {
        $laravelLog = $this->laravelLog->channel(ChannelEnum::contactQueue->value);

        $laravelLog->info('Start', ['contactId' => $contactId]);

        if (! $this->settingService->get(KeyEnum::contactSendToMailEnabled->value)) {
            return;
        }

        $mailToSend = $this->settingService->get(KeyEnum::contactSendEmail->value);

        if (! filter_var($mailToSend, FILTER_VALIDATE_EMAIL)) {
            $laravelLog->error(
                'Address e-mail to send contact is incorrect',
                ['contactId' => $contactId]
            );

            return;
        }

        $contact = $this->contactRepository->getById($contactId);

        if (! $contact) {
            $laravelLog->error(
                'Not exists Contact by id',
                ['contactId' => $contactId]
            );

            return;
        }

        $contactSendMail = new SendMail($contact);

        $contactSendMail
            ->build()
            ->to($mailToSend) // @phpstan-ignore-line
            ->send($this->laravelMail);

        $this->contactRepository->save(
            $contact->setQueued(true)
        );

        $laravelLog->info('End', ['contactId' => $contactId]);
    }
}
