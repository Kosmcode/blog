<?php

namespace App\Services\Contact\Job;

interface SendMailServiceInterface
{
    public function sendMailFromJobByContactId(int $contactId): void;
}
