<?php

namespace App\Services\Home;

use App\Dto\HomeResourceDTO;
use App\Dto\ResponseDTO;
use App\Enums\Intertia\ComponentEnum;
use App\Enums\Page\ElementEnum;
use App\Enums\Setting\KeyEnum;
use App\Http\Resources\Home\Resource;
use App\Services\Article\Recent\GetServiceInterface as RecentGetServiceInterface;
use App\Services\Page\Element\GetServiceInterface as PageElementServiceInterface;
use App\Services\SettingServiceInterface;

/**
 * Class handle Home page
 */
class GetService implements GetServiceInterface
{
    public function __construct(
        protected readonly SettingServiceInterface $settingService,
        protected readonly PageElementServiceInterface $pageElementService,
        protected readonly RecentGetServiceInterface $recentArticleService,
    ) {}

    public function getResponseDTO(): ResponseDTO
    {
        $homeResourceDTO = (new HomeResourceDTO())
            ->setRecentArticles($this->recentArticleService->getRecentArticles())
            ->setTopPageElement($this->pageElementService->getByElementEnum(ElementEnum::homePageTop))
            ->setDownPageElement($this->pageElementService->getByElementEnum(ElementEnum::homePageDown));

        return (new ResponseDTO())
            ->setMetaTitle($this->settingService->get(KeyEnum::homeMetaTitle->value)) // @phpstan-ignore-line
            ->setMetaDescription($this->settingService->get(KeyEnum::homeMetaDescription->value)) // @phpstan-ignore-line
            ->setMetaKeywords($this->settingService->get(KeyEnum::homeMetaKeywords->value)) // @phpstan-ignore-line
            ->setInertiaComponentName(ComponentEnum::home->value)
            ->setResourceData($homeResourceDTO, Resource::class);
    }
}
