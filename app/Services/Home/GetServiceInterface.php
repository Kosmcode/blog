<?php

namespace App\Services\Home;

use App\Dto\ResponseDTO;

/**
 * Interface of Home ElementService
 */
interface GetServiceInterface
{
    /**
     * Method to get ResponseDTO
     */
    public function getResponseDTO(): ResponseDTO;
}
