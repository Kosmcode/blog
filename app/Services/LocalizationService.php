<?php

namespace App\Services;

use App\Enums\Log\ChannelEnum;
use Illuminate\Cache\Repository as LaravelCache;
use Illuminate\Log\LogManager as LaravelLog;
use Illuminate\Translation\Translator as LaravelLang;
use Psr\Log\LoggerInterface;

/**
 * Class to handle localization
 */
class LocalizationService implements LocalizationServiceInterface
{
    protected LoggerInterface $logger;

    protected ?string $langPattern = null;

    public function __construct(
        protected readonly LaravelLang $laravelLang,
        protected readonly LaravelLog $laravelLog,
        protected readonly LaravelCache $laravelCache
    ) {
        $this->logger = $laravelLog->channel(ChannelEnum::localization->value);
    }

    /** {@inheritDoc} */
    public function get(string $localizationKey, array $replace = []): string
    {
        if ($this->langPattern) {
            $localizationKey = $this->langPattern.'.'.$localizationKey;
        }

        $localization = $this->laravelLang->get(
            $localizationKey,
            $replace,
            $this->laravelLang->getLocale(),
            true
        );

        if (is_array($localization)) {
            $this->logger->error('Return array from localization key `'.$localizationKey.'`');

            return '';
        }

        if ($localization === $localizationKey) {
            $this->logger->error('Not exist localization key `'.$localizationKey.'`');

            return '';
        }

        return $localization;
    }

    /**
     * Method to set lang pattern
     */
    public function setLangPattern(string $langPattern): LocalizationService
    {
        $this->langPattern = $langPattern;

        return $this;
    }
}
