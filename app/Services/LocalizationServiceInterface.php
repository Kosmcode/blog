<?php

namespace App\Services;

interface LocalizationServiceInterface
{
    /**
     * @param  array<mixed>  $replace
     */
    public function get(string $localizationKey, array $replace = []): string;

    public function setLangPattern(string $langPattern): LocalizationService;
}
