<?php

namespace App\Services\Maintenance;

use App\Enums\Setting\KeyEnum;
use App\Services\SettingServiceInterface;
use App\Services\User\CheckServiceInterface;
use Illuminate\Contracts\Routing\ResponseFactory as LaravelResponse;
use Illuminate\Http\Response;

class ModeService implements ModeServiceInterface
{
    public function __construct(
        protected readonly SettingServiceInterface $settingService,
        protected readonly LaravelResponse $laravelResponse,
        protected readonly CheckServiceInterface $userCheckService
    ) {}

    public function isEnabled(): bool
    {
        if ($this->userCheckService->userHasAdminRole()) {
            return false;
        }

        return $this->settingService->get(KeyEnum::maintenanceModeEnabled->value); // @phpstan-ignore-line
    }

    public function getMaintenanceView(): Response
    {
        return $this->laravelResponse->view(
            'maintenance.maintenance_mode',
            [
                'appName' => $this->settingService->get(KeyEnum::appName->value),
                'message' => $this->settingService->get(KeyEnum::maintenanceModeMessage->value),
            ],
            Response::HTTP_SERVICE_UNAVAILABLE
        );
    }

    public function setMaintenanceMode(bool $enable): void
    {
        $this->settingService->setBySettingKeyEnum(KeyEnum::maintenanceModeEnabled, $enable);
    }
}
