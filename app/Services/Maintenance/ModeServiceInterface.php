<?php

namespace App\Services\Maintenance;

use Illuminate\Http\Response;

/**
 * Interface of Maintenance Mode ElementService
 */
interface ModeServiceInterface
{
    /**
     * Method to check if maintenance mode is enabled
     */
    public function isEnabled(): bool;

    /**
     * Method to render view maintenance mode
     */
    public function getMaintenanceView(): Response;

    public function setMaintenanceMode(bool $enable): void;
}
