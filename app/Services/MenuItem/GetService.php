<?php

namespace App\Services\MenuItem;

use App\Enums\Cache\KeyEnum;
use App\Http\Resources\MenuItemsResource;
use App\Repositories\MenuItemRepositoryInterface;
use App\Services\Cache\AbstractService;
use Illuminate\Cache\Repository as LaravelCache;
use Illuminate\Http\Request as LaravelRequest;

class GetService extends AbstractService implements GetServiceInterface
{
    public function __construct(
        protected LaravelCache $laravelCache,
        protected readonly MenuItemRepositoryInterface $menuItemRepository,
        protected readonly LaravelRequest $laravelRequest
    ) {
        parent::__construct($laravelCache);
    }

    /** {@inheritDoc} */
    public function getToInertiaNavigation(bool $cached = true): array
    {
        if ($cached) {
            $menuItems = $this->cacheGet(KeyEnum::menuItem);

            if ($menuItems) {
                return $menuItems; // @phpstan-ignore-line
            }

            return $this->cacheForever( // @phpstan-ignore-line
                KeyEnum::menuItem,
                $this->getMenuItemsAndPrepare()
            );
        }

        return $this->getMenuItemsAndPrepare();
    }

    /**
     * @return array<mixed>
     */
    protected function getMenuItemsAndPrepare(): array
    {
        $menuItems = $this->menuItemRepository->getMainParentsLinksWithChildless();

        return MenuItemsResource::collection($menuItems)->toArray($this->laravelRequest); // @phpstan-ignore-line
    }
}
