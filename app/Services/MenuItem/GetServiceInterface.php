<?php

namespace App\Services\MenuItem;

use App\Services\Cache\ServiceInterface;

interface GetServiceInterface extends ServiceInterface
{
    /**
     * @return array<mixed>
     */
    public function getToInertiaNavigation(bool $cached = true): array;
}
