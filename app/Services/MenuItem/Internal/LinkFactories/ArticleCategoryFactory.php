<?php

namespace App\Services\MenuItem\Internal\LinkFactories;

use App\Models\Category;
use App\Repositories\CategoryRepositoryInterface;
use Illuminate\Routing\UrlGenerator;

class ArticleCategoryFactory implements LinkFactoryInterface
{
    public function __construct(
        protected readonly CategoryRepositoryInterface $categoryRepository,
        protected readonly UrlGenerator $urlGenerator,
    ) {}

    /** {@inheritDoc} */
    public function toArrayToAdmin(): array
    {
        $categories = $this->categoryRepository->getToMenuItemsLinks();

        $preparedLinks = [];

        foreach ($categories as $category) {
            if (! $category instanceof Category) {
                continue;
            }

            $preparedLinks[$this->prepareLinksArrayKey($category)] = $this->prepareLinksArrayValue($category);
        }

        return $preparedLinks;
    }

    protected function prepareLinksArrayKey(Category $category): string
    {
        return $this->urlGenerator->route('articles.category', $category->getSlug(), false);
    }

    protected function prepareLinksArrayValue(Category $category): string
    {
        return sprintf('Article category: %s', $category->getName());
    }
}
