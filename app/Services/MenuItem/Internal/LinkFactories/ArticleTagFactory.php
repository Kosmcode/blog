<?php

namespace App\Services\MenuItem\Internal\LinkFactories;

use App\Models\Tag;
use App\Repositories\TagRepositoryInterface;
use Illuminate\Routing\UrlGenerator;

class ArticleTagFactory implements LinkFactoryInterface
{
    public function __construct(
        protected readonly TagRepositoryInterface $tagRepository,
        protected readonly UrlGenerator $urlGenerator,
    ) {}

    /** {@inheritDoc} */
    public function toArrayToAdmin(): array
    {
        $tags = $this->tagRepository->getToMenuItemsLinks();

        $preparedLinks = [];

        foreach ($tags as $tag) {
            if (! $tag instanceof Tag) {
                continue;
            }

            $preparedLinks[$this->prepareLinksArrayKey($tag)] = $this->prepareLinksArrayValue($tag);
        }

        return $preparedLinks;
    }

    protected function prepareLinksArrayKey(Tag $tag): string
    {
        return $this->urlGenerator->route('articles.tag', $tag->getSlug(), false);
    }

    protected function prepareLinksArrayValue(Tag $tag): string
    {
        return sprintf('Article tag: %s', $tag->getName());
    }
}
