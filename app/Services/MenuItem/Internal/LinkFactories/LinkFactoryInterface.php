<?php

namespace App\Services\MenuItem\Internal\LinkFactories;

use App\Exceptions\SlugException;
use Illuminate\Contracts\Container\BindingResolutionException;

interface LinkFactoryInterface
{
    /**
     * @return array<mixed>
     *
     * @throws BindingResolutionException
     * @throws SlugException
     */
    public function toArrayToAdmin(): array;
}
