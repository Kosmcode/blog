<?php

namespace App\Services\MenuItem\Internal\LinkFactories;

use App\Models\Slug;
use App\Repositories\SlugRepositoryInterface;
use App\Services\Slug\Factory\SlugFactoryInterface;
use Illuminate\Routing\UrlGenerator;

class SlugFactory implements LinkFactoryInterface
{
    public function __construct(
        protected readonly SlugRepositoryInterface $slugRepository,
        protected readonly UrlGenerator $urlGenerator,
        protected readonly SlugFactoryInterface $sluggableFactory,
    ) {}

    /** {@inheritDoc} */
    public function toArrayToAdmin(): array
    {
        $slugs = $this->slugRepository->getToMenuItemsLinks();

        $preparedLinks = [];

        foreach ($slugs as $slug) {
            if (! $slug instanceof Slug) {
                continue;
            }

            $slugFactory = $this->sluggableFactory->getBySlug($slug);

            $preparedLinks[$this->prepareLinksArrayKey($slug)] = $slugFactory->getNameToAdminLinksBySlug($slug);
        }

        return $preparedLinks;
    }

    protected function prepareLinksArrayKey(Slug $slug): string
    {
        return $this->urlGenerator->route('slug', $slug->getSlug(), false);
    }
}
