<?php

namespace App\Services\MenuItem\Internal\LinkFactories;

use App\Enums\Route\SlugEnum;

class StaticRoutesFactory implements LinkFactoryInterface
{
    /** {@inheritDoc} */
    public function toArrayToAdmin(): array
    {
        return SlugEnum::getRoutesToMenuItemInternalLinks();
    }
}
