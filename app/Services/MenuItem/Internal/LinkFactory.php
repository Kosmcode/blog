<?php

namespace App\Services\MenuItem\Internal;

use Illuminate\Contracts\Foundation\Application;

class LinkFactory implements LinkFactoryInterface
{
    /**
     * @param  iterable<int, class-string>  $factories
     */
    public function __construct(
        protected readonly iterable $factories,
        protected readonly Application $application,
    ) {}

    /** {@inheritDoc} */
    public function getArrayLinksToAdmin(): array
    {
        $preparedLinks = [];

        foreach ($this->factories as $factory) {
            $initFactory = $this->application->make($factory);

            $preparedLinks = array_merge($preparedLinks, $initFactory->toArrayToAdmin());
        }

        return $preparedLinks;
    }
}
