<?php

namespace App\Services\MenuItem\Internal;

use Illuminate\Contracts\Container\BindingResolutionException;

interface LinkFactoryInterface
{
    /**
     * @return array<string, string>
     *
     * @throws BindingResolutionException
     */
    public function getArrayLinksToAdmin(): array;
}
