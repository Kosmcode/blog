<?php

namespace App\Services\Page\Element;

use App\Enums\Cache\KeyEnum;
use App\Enums\Page\ElementEnum;
use App\Models\PageElement;
use App\Repositories\PageElementRepositoryInterface;
use App\Services\Cache\AbstractService;
use Illuminate\Cache\Repository as LaravelCache;

class GetService extends AbstractService implements GetServiceInterface
{
    public function __construct(
        protected LaravelCache $laravelCache,
        protected readonly PageElementRepositoryInterface $pageElementRepository
    ) {
        parent::__construct($laravelCache);
    }

    public function getByElementEnum(ElementEnum $elementEnum, bool $cached = true): ?PageElement
    {
        if (! $cached) {
            return $this->pageElementRepository->getByTitle($elementEnum->value);
        }

        $cacheKey = KeyEnum::preparePageElementCacheKey($elementEnum->value);

        $pageElement = $this->cacheGet($cacheKey);

        if ($pageElement) {
            return $pageElement; // @phpstan-ignore-line
        }

        $pageElement = $this->pageElementRepository->getByTitle($elementEnum->value);

        if (! $pageElement) {
            return null;
        }

        return $this->cacheForever( // @phpstan-ignore-line
            $cacheKey,
            $pageElement
        );
    }
}
