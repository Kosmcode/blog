<?php

namespace App\Services\Page\Element;

use App\Enums\Page\ElementEnum;
use App\Models\PageElement;

interface GetServiceInterface
{
    public function getByElementEnum(ElementEnum $elementEnum, bool $cached = true): ?PageElement;
}
