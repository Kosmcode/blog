<?php

namespace App\Services;

use App\Enums\Cache\KeyEnum as CacheKeyEnum;
use App\Enums\Setting\KeyEnum as SettingKeyEnum;
use App\Repositories\SettingRepositoryInterface;
use App\Services\Cache\AbstractService;
use Illuminate\Cache\Repository as LaravelCache;
use Illuminate\Config\Repository as LaravelConfig;

class SettingService extends AbstractService implements SettingServiceInterface
{
    public function __construct(
        protected LaravelCache $laravelCache,
        protected readonly SettingRepositoryInterface $settingRepository,
        protected readonly LaravelConfig $laravelConfig
    ) {
        parent::__construct($laravelCache);
    }

    public function get(string $key, bool $cached = true): mixed
    {
        if (! $cached) {
            $setting = $this->settingRepository->findByKey($key);

            return $setting?->getValue() ?? $this->laravelConfig->get($key);
        }

        $settingValue = $this->cacheGet(CacheKeyEnum::prepareSettingCacheKey($key));

        if ($settingValue) {
            return $settingValue;
        }

        $setting = $this->settingRepository->findByKey($key);

        $settingValue = $setting?->getValue() ?? $this->laravelConfig->get($key);

        return $this->cacheForever(
            CacheKeyEnum::prepareSettingCacheKey($key),
            $settingValue
        );
    }

    public function setBySettingKeyEnum(SettingKeyEnum $keyEnum, mixed $value): void
    {
        $setting = $this->settingRepository->findByKey($keyEnum->value);

        if (! $setting) {
            return;
        }

        $this->settingRepository->setValueByKey($setting->getSettingKeyName(), $value); // @phpstan-ignore-line

        $this->cacheForever(
            CacheKeyEnum::prepareSettingCacheKey($setting->getSettingKeyName()),
            $value
        );
    }
}
