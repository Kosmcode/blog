<?php

namespace App\Services;

use App\Enums\Setting\KeyEnum as SettingKeyEnum;
use App\Services\Cache\ServiceInterface;

interface SettingServiceInterface extends ServiceInterface
{
    public function get(string $key, bool $cached = true): mixed;

    public function setBySettingKeyEnum(SettingKeyEnum $keyEnum, mixed $value): void;
}
