<?php

namespace App\Services\Sitemap;

use App\Enums\FilesystemDiskEnum;
use App\Enums\Setting\KeyEnum;
use App\Services\MenuItem\Internal\LinkFactoryInterface;
use App\Services\SettingServiceInterface;
use Illuminate\Filesystem\FilesystemManager as LaravelStorage;

class GenerateService implements GenerateServiceInterface
{
    public function __construct(
        protected readonly LinkFactoryInterface $linkFactory,
        protected readonly SettingServiceInterface $settingService,
        protected readonly LaravelStorage $laravelStorage
    ) {}

    /**
     * {@inheritDoc}
     */
    public function generateSimpleTextSitemap(): bool
    {
        $siteUrl = $this->settingService->get(KeyEnum::appUrl->value);

        $slugs = array_map(
            function ($key) use ($siteUrl) {
                return $siteUrl.$key;
            },
            array_keys($this->linkFactory->getArrayLinksToAdmin())
        );

        return $this->laravelStorage
            ->drive(FilesystemDiskEnum::public->value)
            ->put(
                $this->settingService->get(KeyEnum::sitemapFilename->value), // @phpstan-ignore-line
                implode(PHP_EOL, $slugs)
            );
    }
}
