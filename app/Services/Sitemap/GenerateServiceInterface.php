<?php

namespace App\Services\Sitemap;

use Illuminate\Contracts\Container\BindingResolutionException;

/**
 * Interface of Sitemap Generate ElementService
 */
interface GenerateServiceInterface
{
    /**
     * Method to generate simple text file of sitemap
     *
     * @throws BindingResolutionException
     */
    public function generateSimpleTextSitemap(): bool;
}
