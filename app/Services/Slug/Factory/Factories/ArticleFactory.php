<?php

namespace App\Services\Slug\Factory\Factories;

use App\Dto\ArticleDTO;
use App\Dto\ResponseDTO;
use App\Enums\Article\StatusEnum;
use App\Enums\Intertia\ComponentEnum;
use App\Exceptions\SlugException;
use App\Http\Resources\Article\Resource;
use App\Models\Article;
use App\Models\Slug;
use App\Services\Article\Recent\GetServiceInterface as ArticleRecentGetServiceInterface;

/**
 * Article Factory
 */
class ArticleFactory implements ContentableFactoryInterface
{
    public function __construct(
        protected readonly ArticleRecentGetServiceInterface $articleRecentGetService,
    ) {}

    public static function supportedModel(): string
    {
        return Article::class;
    }

    /**
     * {@inheritDoc}
     */
    public function getResponseDTOBySlug(Slug $slug): ResponseDTO
    {
        $article = $slug->getContentable();

        if (! $article instanceof Article) {
            throw new SlugException('Contentable type is not Article');
        }

        $responseDTO = (new ResponseDTO());

        if ($article->getStatusEnum() !== StatusEnum::published) {
            throw $responseDTO->createHttpException(exceptionMessage: 'Article not exists');
        }

        $articleDTO = (new ArticleDTO())
            ->setArticle($article)
            ->setRecentArticles($this->articleRecentGetService->getRecentArticles());

        return $responseDTO
            ->setInertiaComponentName(ComponentEnum::singleArticle->value)
            ->setMetaDataFromSlug($slug)
            ->setResourceData($articleDTO, Resource::class);
    }

    /**
     * {@inheritDoc}
     */
    public function getNameToAdminLinksBySlug(Slug $slug): string
    {
        $slugContentable = $slug->getContentable();

        if (! $slugContentable instanceof Article) {
            throw new SlugException('Contentable type is not Article');
        }

        return sprintf('Article: %s', $slugContentable->getTitle());
    }
}
