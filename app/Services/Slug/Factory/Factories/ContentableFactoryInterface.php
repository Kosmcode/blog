<?php

namespace App\Services\Slug\Factory\Factories;

use App\Dto\ResponseDTO;
use App\Exceptions\SlugException;
use App\Models\Slug;

/**
 * Interface of Slug Factory
 */
interface ContentableFactoryInterface
{
    public static function supportedModel(): string;

    /**
     * Method getResponseDTOBySlug
     *
     * @throws SlugException
     */
    public function getResponseDTOBySlug(Slug $slug): ResponseDTO;

    /**
     * @throws SlugException
     */
    public function getNameToAdminLinksBySlug(Slug $slug): string;
}
