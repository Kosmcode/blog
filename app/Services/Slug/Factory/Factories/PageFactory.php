<?php

namespace App\Services\Slug\Factory\Factories;

use App\Dto\ResponseDTO;
use App\Enums\Intertia\ComponentEnum;
use App\Exceptions\SlugException;
use App\Http\Resources\Page\PageResource;
use App\Models\Page;
use App\Models\Slug;

/**
 * Page Factory
 */
class PageFactory implements ContentableFactoryInterface
{
    public static function supportedModel(): string
    {
        return Page::class;
    }

    /** {@inheritDoc} */
    public function getResponseDTOBySlug(Slug $slug): ResponseDTO
    {
        $page = $slug->getContentable();

        if (! $page instanceof Page) {
            throw new SlugException('Contentable type is not Page');
        }

        $responseDTO = new ResponseDTO();

        if (! $page->isPublished()) {
            throw $responseDTO->createHttpException(exceptionMessage: 'Page not found');
        }

        return $responseDTO
            ->setInertiaComponentName(ComponentEnum::page->value)
            ->setMetaDataFromSlug($slug)
            ->setResourceData($page, PageResource::class);
    }

    /**
     * {@inheritDoc}
     */
    public function getNameToAdminLinksBySlug(Slug $slug): string
    {
        $slugContentable = $slug->getContentable();

        if (! $slugContentable instanceof Page) {
            throw new SlugException('Contentable type is not Page');
        }

        return sprintf('Page: %s', $slugContentable->getName());
    }
}
