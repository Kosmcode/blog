<?php

namespace App\Services\Slug\Factory;

use App\Exceptions\SlugException;
use App\Models\Slug;
use App\Services\Slug\Factory\Factories\ContentableFactoryInterface;
use Illuminate\Contracts\Foundation\Application;

/**
 * Sluggable Factory
 */
class SlugFactory implements SlugFactoryInterface
{
    /**
     * @param  iterable<int, class-string>  $factories
     */
    public function __construct(
        protected readonly iterable $factories,
        protected readonly Application $application,
    ) {}

    /** {@inheritDoc} */
    public function getBySlug(Slug $slug): ContentableFactoryInterface
    {
        foreach ($this->factories as $factory) {
            if ($slug->getContentableType() !== $factory::supportedModel()) {
                continue;
            }

            return $this->application->make($factory);
        }

        throw new SlugException('Unsupported model in sluggable factory');
    }
}
