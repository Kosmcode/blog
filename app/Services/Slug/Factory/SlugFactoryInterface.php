<?php

namespace App\Services\Slug\Factory;

use App\Exceptions\SlugException;
use App\Models\Slug;
use App\Services\Slug\Factory\Factories\ContentableFactoryInterface;
use Illuminate\Contracts\Container\BindingResolutionException;

/**
 * Interface of Sluggable Factory
 */
interface SlugFactoryInterface
{
    /**
     * Method getBySlug
     *
     * @throws BindingResolutionException
     * @throws SlugException
     */
    public function getBySlug(Slug $slug): ContentableFactoryInterface;
}
