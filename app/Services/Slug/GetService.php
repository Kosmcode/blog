<?php

namespace App\Services\Slug;

use App\Dto\ResponseDTO;
use App\Enums\Cache\KeyEnum;
use App\Repositories\SlugRepositoryInterface;
use App\Services\Cache\AbstractService;
use App\Services\Slug\Factory\SlugFactoryInterface;
use Illuminate\Cache\Repository as LaravelCache;

/**
 * Response Service
 */
class GetService extends AbstractService implements GetServiceInterface
{
    public function __construct(
        protected readonly SlugRepositoryInterface $slugRepository,
        protected readonly SlugFactoryInterface $sluggableFactory,
        protected LaravelCache $laravelCache,
    ) {
        parent::__construct($laravelCache);
    }

    /** {@inheritDoc} */
    public function getResponseDTO(string $slugUri): ResponseDTO
    {
        $cacheKey = KeyEnum::prepareSlugCacheKey($slugUri);

        $cachedResponse = $this->cacheGet($cacheKey);

        if ($cachedResponse instanceof ResponseDTO) {
            return $cachedResponse;
        }

        $slug = $this->slugRepository->findBySlug($slugUri);

        if (! $slug) {
            throw ResponseDTO::createHttpException(exceptionMessage: 'Not found Slug by slugUri');
        }

        $sluggable = $this->sluggableFactory->getBySlug($slug);

        return $this->cacheForever( // @phpstan-ignore-line
            $cacheKey,
            $sluggable->getResponseDTOBySlug($slug)
        );
    }
}
