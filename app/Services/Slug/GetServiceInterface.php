<?php

namespace App\Services\Slug;

use App\Dto\ResponseDTO;
use App\Exceptions\SlugException;
use Illuminate\Contracts\Container\BindingResolutionException;

/**
 * Interface of Response Service
 */
interface GetServiceInterface
{
    /**
     * Method get ResponseDTO
     *
     * @throws BindingResolutionException
     * @throws SlugException
     */
    public function getResponseDTO(string $slugUri): ResponseDTO;
}
