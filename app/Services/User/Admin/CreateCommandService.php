<?php

namespace App\Services\User\Admin;

use App\Enums\User\RoleEnum;
use App\Models\User;
use App\Repositories\ModelHasRoleRepositoryInterface;
use App\Repositories\RoleRepositoryInterface;
use App\Services\AbstractNotificationService;
use App\Services\LocalizationServiceInterface;
use App\Services\User\CreateServiceInterface;
use App\Services\Validation\UserEmailValidationService;
use App\Services\Validation\UsernameValidationService;
use App\Services\Validation\UserPasswordsValidationService;
use App\Services\Validation\ValidationServiceInterface;

class CreateCommandService extends AbstractNotificationService implements CreateCommandServiceInterface
{
    public function __construct(
        protected readonly RoleRepositoryInterface $roleRepository,
        protected readonly ModelHasRoleRepositoryInterface $modelHasRoleRepository,
        protected readonly LocalizationServiceInterface $localizationService,
        protected readonly CreateServiceInterface $userCreateService,
        protected readonly ValidationServiceInterface $validationService
    ) {
        $this->localizationService->setLangPattern(self::LANG_KEY_PATTERN);
    }

    /**
     * Method to check user email
     */
    public function isCorrectUserEmail(string $userEmail): bool
    {
        if (! $this->validationService->isValid(new UserEmailValidationService($userEmail))) {
            $this->setNotification($this->validationService->getNotification()); // @phpstan-ignore-line

            return false;
        }

        return true;
    }

    public function isCorrectUsername(string $username): bool
    {
        if (! $this->validationService->isValid(new UsernameValidationService($username))) {
            $this->setNotification($this->validationService->getNotification()); // @phpstan-ignore-line

            return false;
        }

        return true;
    }

    public function isCorrectUserPassword(string $password, string $passwordConfirm): bool
    {
        if (! $this->validationService->isValid(new UserPasswordsValidationService($password, $passwordConfirm))) {
            $this->setNotification($this->validationService->getNotification()); // @phpstan-ignore-line

            return false;
        }

        return true;
    }

    public function createUserWithAdminRole(string $userEmail, string $username, string $userPassword): bool
    {
        $roleAdmin = $this->roleRepository->findByName(RoleEnum::admin->value);

        if (! $roleAdmin) {
            $this->setNotificationFromLocalization('error.roleAdminNotExists');

            return false;
        }

        $user = $this->userCreateService->create($username, $userEmail, $userPassword);

        $this->modelHasRoleRepository->create([
            'model_id' => $user->getId(),
            'role_id' => $roleAdmin->getId(),
            'model_type' => User::class,
        ]);

        $this->setNotificationFromLocalization('success');

        return true;
    }

    /**
     * Method to set notification from localization
     */
    private function setNotificationFromLocalization(string $localizationKey): void
    {
        $this->setNotification(
            $this->localizationService->get(
                $localizationKey
            )
        );
    }
}
