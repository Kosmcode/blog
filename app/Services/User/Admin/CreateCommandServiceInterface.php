<?php

namespace App\Services\User\Admin;

use App\Interfaces\NotifiableInterface;

interface CreateCommandServiceInterface extends NotifiableInterface
{
    const LANG_KEY_PATTERN = 'command.user.create.admin';

    public function isCorrectUserEmail(string $userEmail): bool;

    public function isCorrectUsername(string $username): bool;

    public function isCorrectUserPassword(string $password, string $passwordConfirm): bool;

    public function createUserWithAdminRole(string $userEmail, string $username, string $userPassword): bool;
}
