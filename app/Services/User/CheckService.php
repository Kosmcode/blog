<?php

namespace App\Services\User;

use App\Enums\User\RoleEnum;
use App\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Guard as LaravelAuth;

class CheckService implements CheckServiceInterface
{
    public function __construct(
        protected readonly LaravelAuth $laravelAuth
    ) {}

    public function userHasAdminRole(?User $user = null): bool
    {
        if (! $user) {
            $user = $this->getUser();

            if (! $user) {
                return false;
            }
        }

        return $user->hasRole(RoleEnum::admin->value);
    }

    /** {@inheritDoc} */
    public function userHasAdminOrRole(array $roles, ?User $user = null): bool
    {
        if (! $user) {
            $user = $this->getUser();

            if (! $user) {
                return false;
            }
        }

        return $user->hasRole([RoleEnum::admin->value, ...$roles]);
    }

    /**
     * Method to get user from auth
     */
    protected function getUser(): ?Authenticatable
    {
        if (! $this->laravelAuth->check()) {
            return null;
        }

        return $this->laravelAuth->user();
    }
}
