<?php

namespace App\Services\User;

use App\Models\User;

/**
 * Interface of User Check ElementService
 */
interface CheckServiceInterface
{
    /**
     * Method to check if user has admin role (when is logged or by User object)
     */
    public function userHasAdminRole(?User $user = null): bool;

    /**
     * @param  array<mixed>  $roles
     */
    public function userHasAdminOrRole(array $roles, ?User $user = null): bool;
}
