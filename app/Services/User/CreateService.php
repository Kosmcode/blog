<?php

namespace App\Services\User;

use App\Models\User;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Contracts\Hashing\Hasher as LaravelHash;

class CreateService implements CreateServiceInterface
{
    public function __construct(
        protected readonly UserRepositoryInterface $userRepository,
        protected readonly LaravelHash $laravelHash
    ) {}

    public function create(string $username, string $userEmail, string $userPassword): User
    {
        return $this->userRepository->create([
            'name' => $username,
            'email' => $userEmail,
            'password' => $this->laravelHash->make($userPassword),
        ]);
    }
}
