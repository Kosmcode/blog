<?php

namespace App\Services\User;

use App\Models\User;

/**
 * Interface of User Create ElementService
 */
interface CreateServiceInterface
{
    /**
     * Method to create User
     */
    public function create(string $username, string $userEmail, string $userPassword): User;
}
