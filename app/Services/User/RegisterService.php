<?php

namespace App\Services\User;

use App\Enums\User\RequestParameterEnum;
use App\Http\Requests\Auth\StoreRequest;
use Illuminate\Auth\Events\Registered;
use Illuminate\Events\Dispatcher as LaravelEvent;
use Illuminate\Support\Facades\Auth;

/**
 * Class to register new user
 */
class RegisterService implements RegisterServiceInterface
{
    public function __construct(
        protected readonly CreateServiceInterface $userCreateService,
        protected readonly LaravelEvent $laravelEvent,
    ) {}

    /**
     * {@inheritDoc}
     */
    public function createAndLogin(StoreRequest $userStoreRequest): void
    {
        $requestParameters = $userStoreRequest->all();

        $user = $this->userCreateService->create(
            $requestParameters[RequestParameterEnum::name->value],
            $requestParameters[RequestParameterEnum::email->value],
            $requestParameters[RequestParameterEnum::password->value]
        );

        $this->laravelEvent->dispatch(new Registered($user));

        Auth::login($user);
    }
}
