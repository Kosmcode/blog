<?php

namespace App\Services\User;

use App\Http\Requests\Auth\StoreRequest;

/**
 * Interface of User Register ElementService
 */
interface RegisterServiceInterface
{
    /**
     * Method to create new user and log in
     */
    public function createAndLogin(StoreRequest $userStoreRequest): void;
}
