<?php

namespace App\Services\Validation;

use App\Interfaces\NotifiableInterface;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\Factory as LaravelValidator;

/**
 * Abstract class to validation with notifiable
 */
abstract class AbstractValidationService implements NotifiableInterface
{
    protected LaravelValidator $laravelValidator;

    protected ?string $notification = null;

    public function __construct()
    {
        $this->laravelValidator = App::make(LaravelValidator::class);
    }

    /**
     * Method to get notification from validation result
     *
     * @return ?string
     */
    public function getNotification(): ?string
    {
        return $this->notification;
    }

    /**
     * @param  array<mixed>  $data
     * @param  array<mixed>  $rules
     */
    protected function passes(array $data, array $rules): bool
    {
        $validator = $this->laravelValidator->make($data, $rules);

        if (! $validator->passes()) {
            $this->notification = $validator->errors()->first();

            return false;
        }

        return true;
    }
}
