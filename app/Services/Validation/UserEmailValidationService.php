<?php

namespace App\Services\Validation;

use App\Rules\User\UserRulesTrait;

/**
 * Class to validation user email
 */
class UserEmailValidationService extends AbstractValidationService implements ValidatableServiceInterface
{
    use UserRulesTrait;

    private string $userEmail;

    public function __construct(string $userEmail)
    {
        parent::__construct();

        $this->userEmail = $userEmail;
    }

    /**
     * Method to passes validation
     */
    public function isCorrect(): bool
    {
        return $this->passes(
            [
                'email' => $this->userEmail,
            ],
            $this->getStoreUserEmailRules()
        );
    }
}
