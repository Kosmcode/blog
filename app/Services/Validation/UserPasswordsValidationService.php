<?php

namespace App\Services\Validation;

use App\Rules\User\UserRulesTrait;

/**
 * Class to validation user passwords
 */
class UserPasswordsValidationService extends AbstractValidationService implements ValidatableServiceInterface
{
    use UserRulesTrait;

    private string $userPassword;

    private string $userPasswordConfirm;

    public function __construct(string $userPassword, string $userPasswordConfirm)
    {
        parent::__construct();

        $this->userPassword = $userPassword;
        $this->userPasswordConfirm = $userPasswordConfirm;
    }

    /**
     * Method to passes validation
     */
    public function isCorrect(): bool
    {
        return $this->passes(
            [
                'password' => $this->userPassword,
                'password_confirmation' => $this->userPasswordConfirm,
            ],
            $this->getStoreUserPasswordsRules()
        );
    }
}
