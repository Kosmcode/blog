<?php

namespace App\Services\Validation;

use App\Rules\User\UserRulesTrait;

/**
 * Class to validation username
 */
class UsernameValidationService extends AbstractValidationService implements ValidatableServiceInterface
{
    use UserRulesTrait;

    private string $username;

    public function __construct(string $username)
    {
        parent::__construct();

        $this->username = $username;
    }

    /**
     * Method to passes validation
     */
    public function isCorrect(): bool
    {
        return $this->passes(
            [
                'username' => $this->username,
            ],
            $this->getStoreUsernameRules()
        );
    }
}
