<?php

namespace App\Services\Validation;

use App\Interfaces\NotifiableInterface;
use App\Interfaces\ValidatableInterface;

interface ValidatableServiceInterface extends NotifiableInterface, ValidatableInterface {}
