<?php

namespace App\Services\Validation;

/**
 * Class to validation
 */
class ValidationService implements ValidationServiceInterface
{
    private ?string $notification = null;

    /**
     * Method to valid by validatable service instance
     */
    public function isValid(ValidatableServiceInterface $validationService): bool
    {
        if (! $validationService->isCorrect()) {
            $this->notification = $validationService->getNotification();

            return false;
        }

        return true;
    }

    /**
     * Method to get notification from validate result
     *
     * @return ?string
     */
    public function getNotification(): ?string
    {
        return $this->notification;
    }
}
