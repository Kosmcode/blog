<?php

namespace App\Services\Validation;

use App\Interfaces\NotifiableInterface;

interface ValidationServiceInterface extends NotifiableInterface
{
    /**
     * Method to check if is valid
     */
    public function isValid(ValidatableServiceInterface $validationService): bool;
}
