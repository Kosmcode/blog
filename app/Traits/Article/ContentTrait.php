<?php

namespace App\Traits\Article;

trait ContentTrait
{
    public function getFirstParagraphFromString(string $string): string
    {
        if (empty($string)) {
            return '';
        }

        return substr($string, 0, strpos($string, '</p>') + 4);
    }
}
