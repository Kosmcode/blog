<?php

namespace App\Traits\Article;

use App\Enums\Article\ImageThumbnailEnum;
use App\Enums\FilesystemDiskEnum;
use App\Models\Article;
use App\Models\ArticleImageThumbnail;
use Illuminate\Support\Facades\Storage;

trait ImageTrait
{
    protected function prepareImage(Article $article, ImageThumbnailEnum $imageThumbnailEnum): string
    {
        /** @var ArticleImageThumbnail|null $articleThumbnailImage */
        $articleThumbnailImage = $article->getImageThumbnails()
            ->firstWhere('width', $imageThumbnailEnum->getWidth());

        $articleImagesStorageDisk = Storage::disk(FilesystemDiskEnum::articleImages->value);

        if ($articleThumbnailImage) {
            return $articleImagesStorageDisk->url($articleThumbnailImage->getFilename());
        }

        if (! $article->getImage()) {
            return '';
        }

        return $articleImagesStorageDisk->url($article->getImage());
    }
}
