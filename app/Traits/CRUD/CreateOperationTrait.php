<?php

namespace App\Traits\CRUD;

use App\Models\SlugableInterface;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Prologue\Alerts\Facades\Alert;

trait CreateOperationTrait
{
    use CreateOperation, SlugableTrait;

    public function store(): JsonResponse|RedirectResponse
    {
        $this->crud->hasAccessOrFail('create');

        $request = $this->crud->validateRequest();

        $this->crud->registerFieldEvents();

        $item = $this->crud->create($this->crud->getStrippedSaveRequest($request));

        if ($item instanceof SlugableInterface) {
            $this->prepareSlugToModel($item);

            $item
                ->slug()
                ->save(
                    $item->getSlug()
                );
        }

        $this->data['entry'] = $this->crud->entry = $item;

        Alert::success(trans('backpack::crud.insert_success'))->flash();

        $this->crud->setSaveAction();

        return $this->crud->performSaveAction($item->getKey());
    }
}
