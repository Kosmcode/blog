<?php

namespace App\Traits\CRUD;

use App\Models\Slug;
use App\Models\SlugableInterface;
use Illuminate\Support\Str;

trait SlugableTrait
{
    public function prepareSlugToModel(SlugableInterface $slugableModel): void
    {
        $slugData = [
            'slug' => Str::slug(
                $this->crud->getRequest()->request->get('meta_slug')
                ?? $slugableModel->getContentableSlugableTitle()
            ),
            'meta_title' => $this->crud->getRequest()->request->get('meta_title')
                ?? $slugableModel->getContentableSlugableTitle(),
            'meta_description' => $this->crud->getRequest()->request->get('meta_description') ?? '',
            'meta_keywords' => $this->crud->getRequest()->request->get('meta_keywords') ?? '',
        ];

        $this->crud->getRequest()->request->remove('meta_slug');
        $this->crud->getRequest()->request->remove('meta_title');
        $this->crud->getRequest()->request->remove('meta_description');
        $this->crud->getRequest()->request->remove('meta_keywords');

        if (! $slugableModel->getSlug()) {
            $slugableModel
                ->setSlug(
                    new Slug($slugData)
                );

            return;
        }

        $slugableModel
            ->getSlug()
            ->fill($slugData);
    }
}
