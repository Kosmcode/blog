<?php

namespace App\Traits\CRUD;

use App\Models\SlugableInterface;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Prologue\Alerts\Facades\Alert;

trait UpdateOperationTrait
{
    use SlugableTrait, UpdateOperation;

    public function update(): JsonResponse|RedirectResponse
    {
        $this->crud->hasAccessOrFail('update');

        $request = $this->crud->validateRequest();

        $item = $this->crud->update(
            $request->get($this->crud->model->getKeyName()),
            $this->crud->getStrippedSaveRequest($request)
        );

        if ($item instanceof SlugableInterface) {
            $this->prepareSlugToModel($item);

            $item
                ->slug()
                ->save(
                    $item->getSlug()
                );
        }

        $this->data['entry'] = $this->crud->entry = $item;

        Alert::success(trans('backpack::crud.update_success'))->flash();

        $this->crud->setSaveAction();

        return $this->crud->performSaveAction($item->getKey());
    }
}
