<?php

namespace App\Traits\Enum;

trait ArrayableTrait
{
    /**
     * @return array<int|string, int|string>
     */
    public static function toValues(): array
    {
        return array_column(self::cases(), 'value');
    }

    /**
     * @return array<int|string, int|string>
     */
    public static function toNames(): array
    {
        return array_column(self::cases(), 'name');
    }

    /**
     * @return array<int|string, int|string>
     */
    public static function toArray(): array
    {
        return array_combine(self::toValues(), self::toNames());
    }
}
