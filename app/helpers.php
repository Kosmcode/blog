<?php

use App\Enums\User\RoleEnum;
use Illuminate\Support\Env;
use Illuminate\Support\Facades\Request;

function isAdminOrHasRole(string $role): bool
{
    return backpack_user()->hasRole($role) || isAdmin();
}

function isAdminOrHasPermission(string $permission): bool
{
    return backpack_user()->hasPermissionTo($permission) || isAdmin();
}

function isAdmin(): bool
{
    return backpack_user()->hasRole(RoleEnum::admin->value);
}

function getCanonicalUrl(): string
{
    $currentUrlPath = Request::path();

    if ($currentUrlPath === DIRECTORY_SEPARATOR) {
        $currentUrlPath = '';
    }

    return Env::get('APP_URL').$currentUrlPath;
}
