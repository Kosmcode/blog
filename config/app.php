<?php

use App\Http\Controllers\Admin\MenuItemCrudController;
use App\Http\Controllers\Admin\SettingCrudController;
use App\Http\Controllers\Admin\UserCrudController;
use App\Repositories\ArticleImageThumbnailRepository;
use App\Repositories\ArticleImageThumbnailRepositoryInterface;
use App\Repositories\ArticleRepository;
use App\Repositories\ArticleRepositoryInterface;
use App\Repositories\CategoryRepository;
use App\Repositories\CategoryRepositoryInterface;
use App\Repositories\ContactRepository;
use App\Repositories\ContactRepositoryInterface;
use App\Repositories\MenuItemRepository;
use App\Repositories\MenuItemRepositoryInterface;
use App\Repositories\ModelHasRoleRepository;
use App\Repositories\ModelHasRoleRepositoryInterface;
use App\Repositories\PageElementRepository;
use App\Repositories\PageElementRepositoryInterface;
use App\Repositories\PageRepository;
use App\Repositories\PageRepositoryInterface;
use App\Repositories\RoleRepository;
use App\Repositories\RoleRepositoryInterface;
use App\Repositories\SettingRepository;
use App\Repositories\SettingRepositoryInterface;
use App\Repositories\SlugRepository;
use App\Repositories\SlugRepositoryInterface;
use App\Repositories\TagRepository;
use App\Repositories\TagRepositoryInterface;
use App\Repositories\UserRepository;
use App\Repositories\UserRepositoryInterface;
use App\Services\App\HeadService;
use App\Services\App\HeadServiceInterface;
use App\Services\Article\Category\GetService as ArticleCategoryGetService;
use App\Services\Article\Category\GetServiceInterface as ArticleCategoryGetServiceInterface;
use App\Services\Article\Filter\Category\GetService as CategoryGetService;
use App\Services\Article\Filter\Category\GetServiceInterface as CategoryGetServiceInterface;
use App\Services\Article\Filter\GetService as ArticleFilterGetService;
use App\Services\Article\Filter\GetServiceInterface as ArticleFilterGetServiceInterface;
use App\Services\Article\Filter\Tag\GetService as TagGetService;
use App\Services\Article\Filter\Tag\GetServiceInterface as TagGetServiceInterface;
use App\Services\Article\GetService as ArticleGetService;
use App\Services\Article\GetServiceInterface as ArticleGetServiceInterface;
use App\Services\Article\Image\DeleteService;
use App\Services\Article\Image\DeleteServiceInterface;
use App\Services\Article\Image\ThumbnailService;
use App\Services\Article\Image\ThumbnailServiceInterface;
use App\Services\Article\Recent\GetService as RecentGetService;
use App\Services\Article\Recent\GetServiceInterface as RecentGetServiceInterface;
use App\Services\Article\Tag\GetService as ArticleTagGetService;
use App\Services\Article\Tag\GetServiceInterface as ArticleTagGetServiceInterface;
use App\Services\Contact\AvailableSentService;
use App\Services\Contact\AvailableSentServiceInterface;
use App\Services\Contact\CreateService;
use App\Services\Contact\CreateServiceInterface;
use App\Services\Contact\GetService as ContactGetService;
use App\Services\Contact\GetServiceInterface as ContactGetServiceInterface;
use App\Services\Contact\Job\SendMailService;
use App\Services\Contact\Job\SendMailServiceInterface;
use App\Services\Home\GetService as HomeGetService;
use App\Services\Home\GetServiceInterface as HomeGetServiceInterface;
use App\Services\LocalizationService;
use App\Services\LocalizationServiceInterface;
use App\Services\Maintenance\ModeService;
use App\Services\Maintenance\ModeServiceInterface;
use App\Services\MenuItem\GetService as MenuItemGetService;
use App\Services\MenuItem\GetServiceInterface as MenuItemGetServiceInterface;
use App\Services\MenuItem\Internal\LinkFactories\ArticleCategoryFactory;
use App\Services\MenuItem\Internal\LinkFactories\ArticleTagFactory;
use App\Services\MenuItem\Internal\LinkFactories\SlugFactory;
use App\Services\MenuItem\Internal\LinkFactories\StaticRoutesFactory;
use App\Services\Page\Element\GetService as PageElementService;
use App\Services\Page\Element\GetServiceInterface as PageElementServiceInterface;
use App\Services\SettingService;
use App\Services\SettingServiceInterface;
use App\Services\Sitemap\GenerateService;
use App\Services\Sitemap\GenerateServiceInterface;
use App\Services\Slug\Factory\Factories\ArticleFactory;
use App\Services\Slug\Factory\Factories\PageFactory;
use App\Services\Slug\GetService as SlugGetService;
use App\Services\Slug\GetServiceInterface as SlugGetServiceInterface;
use App\Services\User\Admin\CreateCommandService;
use App\Services\User\Admin\CreateCommandServiceInterface;
use App\Services\User\CheckService;
use App\Services\User\CheckServiceInterface;
use App\Services\User\CreateService as UserCreateService;
use App\Services\User\CreateServiceInterface as UserCreateServiceInterface;
use App\Services\User\RegisterService;
use App\Services\User\RegisterServiceInterface;
use App\Services\Validation\ValidationService;
use App\Services\Validation\ValidationServiceInterface;
use Backpack\MenuCRUD\app\Http\Controllers\Admin\MenuItemCrudController as BackpackMenuItemCrudController;
use Backpack\PermissionManager\app\Http\Controllers\UserCrudController as BackpackUserCrudController;
use Backpack\Settings\app\Http\Controllers\SettingCrudController as BackpackSettingCrudController;

return [

    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    |
    */

    'name' => env('APP_NAME', 'Laravel'),

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services the application utilizes. Set this in your ".env" file.
    |
    */

    'env' => env('APP_ENV', 'production'),

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug' => (bool) env('APP_DEBUG', false),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

    'url' => env('APP_URL', 'http://localhost'),

    'asset_url' => env('ASSET_URL'),

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    'timezone' => env('APP_TIMEZONE', 'UTC'),

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'locale' => env('APP_LANGUAGE_DEFAULT', 'en'),

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Faker Locale
    |--------------------------------------------------------------------------
    |
    | This locale will be used by the Faker PHP library when generating fake
    | data for your database seeds. For example, this will be used to get
    | localized telephone numbers, street address information and more.
    |
    */

    'faker_locale' => 'en_US',

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key' => env('APP_KEY'),

    'cipher' => 'AES-256-CBC',

    /*
     * Maintenance mode
     */

    'maintenanceModeEnabled' => false,
    'maintenanceModeMessage' => 'We will coming soon :)',

    /*
    |--------------------------------------------------------------------------
    | Autoloaded ElementService Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'providers' => [

        /*
         * Laravel Framework ElementService Providers...
         */
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Notifications\NotificationServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,

        /*
         * Package ElementService Providers...
         */
        Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class,

        /*
         * Application ElementService Providers...
         */
        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        // App\Providers\BroadcastServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,
        Jenssegers\Agent\AgentServiceProvider::class,
        Intervention\Image\ImageServiceProvider::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => [

        'App' => Illuminate\Support\Facades\App::class,
        'Arr' => Illuminate\Support\Arr::class,
        'Artisan' => Illuminate\Support\Facades\Artisan::class,
        'Auth' => Illuminate\Support\Facades\Auth::class,
        'Blade' => Illuminate\Support\Facades\Blade::class,
        'Broadcast' => Illuminate\Support\Facades\Broadcast::class,
        'Bus' => Illuminate\Support\Facades\Bus::class,
        'Cache' => Illuminate\Support\Facades\Cache::class,
        'Config' => Illuminate\Support\Facades\Config::class,
        'Cookie' => Illuminate\Support\Facades\Cookie::class,
        'Crypt' => Illuminate\Support\Facades\Crypt::class,
        'Date' => Illuminate\Support\Facades\Date::class,
        'DB' => Illuminate\Support\Facades\DB::class,
        'Eloquent' => Illuminate\Database\Eloquent\Model::class,
        'Event' => Illuminate\Support\Facades\Event::class,
        'File' => Illuminate\Support\Facades\File::class,
        'Gate' => Illuminate\Support\Facades\Gate::class,
        'Hash' => Illuminate\Support\Facades\Hash::class,
        'Http' => Illuminate\Support\Facades\Http::class,
        'Lang' => Illuminate\Support\Facades\Lang::class,
        'Log' => Illuminate\Support\Facades\Log::class,
        'Mail' => Illuminate\Support\Facades\Mail::class,
        'Notification' => Illuminate\Support\Facades\Notification::class,
        'Password' => Illuminate\Support\Facades\Password::class,
        'Queue' => Illuminate\Support\Facades\Queue::class,
        'RateLimiter' => Illuminate\Support\Facades\RateLimiter::class,
        'Redirect' => Illuminate\Support\Facades\Redirect::class,
        // 'Redis' => Illuminate\Support\Facades\Redis::class,
        'Request' => Illuminate\Support\Facades\Request::class,
        'Response' => Illuminate\Support\Facades\Response::class,
        'Route' => Illuminate\Support\Facades\Route::class,
        'Schema' => Illuminate\Support\Facades\Schema::class,
        'Session' => Illuminate\Support\Facades\Session::class,
        'Storage' => Illuminate\Support\Facades\Storage::class,
        'Str' => Illuminate\Support\Str::class,
        'URL' => Illuminate\Support\Facades\URL::class,
        'Validator' => Illuminate\Support\Facades\Validator::class,
        'View' => Illuminate\Support\Facades\View::class,
        'Agent' => Jenssegers\Agent\Facades\Agent::class,
        'Image' => Intervention\Image\Facades\Image::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Class Bindings
    |--------------------------------------------------------------------------
    */

    'bindings' => [
        HomeGetServiceInterface::class => HomeGetService::class,
        HeadServiceInterface::class => HeadService::class,
        SettingServiceInterface::class => SettingService::class,
        LocalizationServiceInterface::class => LocalizationService::class,
        ValidationServiceInterface::class => ValidationService::class,
        PageElementServiceInterface::class => PageElementService::class,
        MenuItemGetServiceInterface::class => MenuItemGetService::class,
        CreateServiceInterface::class => CreateService::class,
        AvailableSentServiceInterface::class => AvailableSentService::class,
        ModeServiceInterface::class => ModeService::class,
        ContactGetServiceInterface::class => ContactGetService::class,
        GenerateServiceInterface::class => GenerateService::class,
        ArticleGetServiceInterface::class => ArticleGetService::class,
        CategoryGetServiceInterface::class => CategoryGetService::class,
        SlugGetServiceInterface::class => SlugGetService::class,
        RecentGetServiceInterface::class => RecentGetService::class,
        CreateCommandServiceInterface::class => CreateCommandService::class,
        UserCreateServiceInterface::class => UserCreateService::class,
        RegisterServiceInterface::class => RegisterService::class,
        CheckServiceInterface::class => CheckService::class,
        BackpackUserCrudController::class => UserCrudController::class,
        BackpackSettingCrudController::class => SettingCrudController::class,
        BackpackMenuItemCrudController::class => MenuItemCrudController::class,
        ArticleRepositoryInterface::class => ArticleRepository::class,
        CategoryRepositoryInterface::class => CategoryRepository::class,
        TagRepositoryInterface::class => TagRepository::class,
        UserRepositoryInterface::class => UserRepository::class,
        SettingRepositoryInterface::class => SettingRepository::class,
        RoleRepositoryInterface::class => RoleRepository::class,
        ModelHasRoleRepositoryInterface::class => ModelHasRoleRepository::class,
        PageRepositoryInterface::class => PageRepository::class,
        SlugRepositoryInterface::class => SlugRepository::class,
        MenuItemRepositoryInterface::class => MenuItemRepository::class,
        PageElementRepositoryInterface::class => PageElementRepository::class,
        ContactRepositoryInterface::class => ContactRepository::class,
        ArticleFilterGetServiceInterface::class => ArticleFilterGetService::class,
        ArticleTagGetServiceInterface::class => ArticleTagGetService::class,
        ArticleCategoryGetServiceInterface::class => ArticleCategoryGetService::class,
        TagGetServiceInterface::class => TagGetService::class,
        ThumbnailServiceInterface::class => ThumbnailService::class,
        ArticleImageThumbnailRepositoryInterface::class => ArticleImageThumbnailRepository::class,
        DeleteServiceInterface::class => DeleteService::class,
        SendMailServiceInterface::class => SendMailService::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Factories instance
    |--------------------------------------------------------------------------
    */

    'factories' => [
        'sluggable' => [
            ArticleFactory::class,
            PageFactory::class,
        ],
        'menuItemInternalLinks' => [
            StaticRoutesFactory::class,
            SlugFactory::class,
            ArticleCategoryFactory::class,
            ArticleTagFactory::class,
        ],
    ],

];
