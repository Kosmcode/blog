<?php

return [
    'date' => [
        'format' => 'j.n.Y',
    ],
    'limit' => 10,
    'meta' => [
        'title' => 'Articles',
        'description' => 'Page Articles',
        'keywords' => 'articles, article, page',
    ],
];
