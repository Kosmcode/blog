<?php

return [
    'canSend' => [
        'timeInMinutes' => 3600,
    ],
    'sendToEmailEnable' => true,
    'sendEmail' => 'contact@test.com',
    'meta' => [
        'title' => 'Contact page',
        'description' => 'This is contact page',
        'keywords' => 'contact, page',
    ],
];
