<?php

use App\Enums\FilesystemDiskEnum;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "sftp", "s3"
    |
    */

    'disks' => [
        FilesystemDiskEnum::local->value => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        FilesystemDiskEnum::public->value => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL'),
            'visibility' => 'public',
        ],

        FilesystemDiskEnum::articleImages->value => [
            'driver' => 'local',
            'root' => storage_path('app/public/images/articles'),
            'url' => '/images/articles',
            'visibility' => 'public',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Symbolic Links
    |--------------------------------------------------------------------------
    |
    | Here you may configure the symbolic links that will be created when the
    | `storage:link` Artisan command is executed. The array keys should be
    | the locations of the links and the values should be their targets.
    |
    */

    'links' => [
        public_path('images') => storage_path('app/public/images'),
        public_path('basset') => storage_path('app/public/basset'),
        public_path('sitemap.txt') => storage_path('app/public/sitemap.txt'),
    ],

];
