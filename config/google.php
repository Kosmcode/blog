<?php

return [
    'reCaptcha' => [
        'v3' => [
            'siteKey' => env('MIX_GOOGLE_RECAPTCHA_V3_SITE_KEY'),
            'secretKey' => env('GOOGLE_RECAPTCHA_V3_SECRET_KEY'),
            'acceptableScore' => 0.8,
        ],
    ],
];
