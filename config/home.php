<?php

return [
    'articles' => [
        'limit' => 5,
    ],
    'meta' => [
        'title' => 'Home page',
        'description' => 'This is home page',
        'keywords' => 'home, page',
    ],
];
