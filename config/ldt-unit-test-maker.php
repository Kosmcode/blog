<?php

return [
    'test' => [
        'unit' => [
            'testPath' => base_path('tests/Unit/app'),
            'testNamespace' => 'Tests\Unit\app',
            'templateName' => 'vendor.kosmcode.test.unit.template',
        ],
    ],
];
