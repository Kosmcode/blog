<?php

namespace Database\Factories;

use App\Models\Article;
use App\Models\Category;
use App\Models\Slug;
use App\Models\Tag;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Article>
 */
class ArticleFactory extends Factory
{
    protected $model = Article::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'category_id' => Category::factory(),
            'title' => $this->faker->text(15),
            'content' => $this->faker->text(),
            'image' => null,
        ];
    }

    public function configure(): static
    {
        return $this->afterCreating(function (Article $article) {
            $article->slug()
                ->save(Slug::factory()->create());

            $article->tags()
                ->saveMany(
                    Tag::factory(3)->create()
                );
        });
    }
}
