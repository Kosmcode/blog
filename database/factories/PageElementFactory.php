<?php

namespace Database\Factories;

use App\Models\PageElement;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<PageElement>
 */
class PageElementFactory extends Factory
{
    protected $model = PageElement::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'title' => $this->faker->title(),
            'description' => $this->faker->text(),
            'content' => $this->faker->randomHtml(),
            'enable' => (int) $this->faker->boolean(),
        ];
    }
}
