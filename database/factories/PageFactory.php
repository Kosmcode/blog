<?php

namespace Database\Factories;

use App\Models\Page;
use App\Models\Slug;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Page>
 */
class PageFactory extends Factory
{
    protected $model = Page::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->text(),
            'title' => $this->faker->text(),
            'content' => $this->faker->randomHtml(),
            'published' => $this->faker->boolean(100),
        ];
    }

    public function configure(): static
    {
        return $this->afterCreating(function (Page $page) {
            $slug = Slug::factory()
                ->create([
                    'contentable_type' => Page::class,
                    'contentable_id' => $page->getId(),
                ]);

            $page->setSlug($slug);
        });
    }
}
