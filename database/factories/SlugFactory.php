<?php

namespace Database\Factories;

use App\Models\Article;
use App\Models\Page;
use App\Models\Slug;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Slug>
 */
class SlugFactory extends Factory
{
    protected $model = Slug::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $commentable = $this->getRandomContentable();

        return [
            'slug' => $this->faker->slug,
            'contentable_type' => $commentable,
            'contentable_id' => $commentable::factory(),
            'meta_title' => $this->faker->text(),
            'meta_description' => $this->faker->text(),
            'meta_keywords' => $this->faker->text(),
        ];
    }

    protected function getRandomContentable(): mixed
    {
        return $this->faker->randomElement([
            Article::class,
            Page::class,
        ]);
    }
}
