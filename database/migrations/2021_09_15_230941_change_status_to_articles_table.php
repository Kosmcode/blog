<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeStatusToArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->dropColumn('status');
        });

        Schema::table('articles', function (Blueprint $table) {
            $table->tinyInteger('status')
                ->after('image')
                ->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->dropColumn('status');
        });

        Schema::table('articles', function (Blueprint $table) {
            $table->enum('status', ['PUBLISHED', 'DRAFT'])
                ->default('PUBLISHED');
        });
    }
}
