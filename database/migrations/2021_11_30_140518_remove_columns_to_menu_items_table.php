<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveColumnsToMenuItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('menu_items', function (Blueprint $table) {
            $table->dropColumn(['page_id', 'lft', 'rgt', 'depth']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('menu_items', function (Blueprint $table) {
            $table->integer('page_id')
                ->unsigned()
                ->nullable();
            $table->integer('lft')
                ->unsigned()
                ->nullable();
            $table->integer('rgt')
                ->unsigned()
                ->nullable();
            $table->integer('depth')
                ->unsigned()
                ->nullable();
        });
    }
}
