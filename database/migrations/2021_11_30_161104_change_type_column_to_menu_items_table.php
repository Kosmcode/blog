<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTypeColumnToMenuItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('menu_items', function (Blueprint $table) {
            $table->dropColumn('type');
        });

        Schema::table('menu_items', function (Blueprint $table) {
            $table->tinyInteger('type')
                ->nullable(false)
                ->after('link')
                ->comment('value from MenuItemsTypeEnum');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('menu_items', function (Blueprint $table) {
            $table->dropColumn('type');
        });

        Schema::table('menu_items', function (Blueprint $table) {
            $table->string('type', 20)->nullable();
        });
    }
}
