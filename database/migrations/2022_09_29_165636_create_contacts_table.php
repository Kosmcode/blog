<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->id();
            $table->string('firstname', 15)
                ->nullable(false);
            $table->string('lastname', 15)
                ->nullable(false);
            $table->string('email', 320)
                ->nullable(false);
            $table->string('subject', 255)
                ->nullable(false);
            $table->text('message')
                ->nullable(false);
            $table->ipAddress('ip')
                ->nullable(false);
            $table->boolean('queued')
                ->nullable(false)
                ->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('contacts');
    }
}
