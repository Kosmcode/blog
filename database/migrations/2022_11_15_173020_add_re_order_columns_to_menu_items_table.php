<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddReOrderColumnsToMenuItemsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('menu_items', function (Blueprint $table) {
            $table->integer('lft')
                ->after('parent_id')
                ->default(0);
            $table
                ->integer('rgt')
                ->after('lft')
                ->default(0);
            $table
                ->integer('depth')
                ->after('rgt')
                ->default(0);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('menu_items', function (Blueprint $table) {
            $table->dropColumn(['lft', 'rgt', 'depth']);
        });
    }
}
