<?php

use App\Models\Article;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('article_image_thumbnails', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Article::class)
                ->nullable(false)
                ->comment('Id of Article');
            $table->smallInteger('width')
                ->nullable(false)
                ->comment('Width of thumbnail in px');
            $table->char('filename', 255)
                ->nullable(false)
                ->comment('Filename of thumbnail');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('article_image_thumbnails');
    }
};
