<?php

namespace Database\Seeders;

use App\Enums\Page\ElementEnum;
use App\Repositories\PageElementRepositoryInterface;
use Illuminate\Database\Seeder;

/**
 * Class of Page Element Seeder
 */
class PageElementSeeder extends Seeder
{
    public function __construct(
        protected readonly PageElementRepositoryInterface $pageElementRepository
    ) {}

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $pageElementSeederData = [
            [
                'title' => ElementEnum::homePageTop->value,
                'description' => 'Home Page element top',
                'content' => view('seeder.page.element.home_page_top')->render(),
                'enable' => false,
            ],
            [
                'title' => ElementEnum::homePageDown->value,
                'description' => 'Home Page element down',
                'content' => view('seeder.page.element.home_page_down')->render(),
                'enable' => false,
            ],
            [
                'title' => ElementEnum::contactPageTop->value,
                'description' => 'Contact Page element top',
                'content' => '',
                'enable' => false,
            ],
            [
                'title' => ElementEnum::contactPageDown->value,
                'description' => 'Contact Page element down',
                'content' => '',
                'enable' => false,
            ],
        ];

        foreach ($pageElementSeederData as $seederData) {
            $this->pageElementRepository->updateOrCreate(
                $seederData['title'],
                $seederData['description'],
                $seederData['content'],
                $seederData['enable']
            );
        }
    }
}
