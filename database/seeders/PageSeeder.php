<?php

namespace Database\Seeders;

use App\Models\Page;
use App\Repositories\PageRepositoryInterface;
use App\Repositories\SlugRepositoryInterface;
use App\Services\SettingServiceInterface;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\View\Factory as LaravelView;

class PageSeeder extends Seeder
{
    private SettingServiceInterface $settingService;

    private LaravelView $laravelView;

    private PageRepositoryInterface $pageRepository;

    private SlugRepositoryInterface $slugRepository;

    public function __construct(
        SettingServiceInterface $settingService,
        LaravelView $laravelView,
        PageRepositoryInterface $pageRepository,
        SlugRepositoryInterface $slugRepository
    ) {
        $this->settingService = $settingService;
        $this->laravelView = $laravelView;
        $this->pageRepository = $pageRepository;
        $this->slugRepository = $slugRepository;
    }

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $this->seedPagePrivacyPolicy();
    }

    private function seedPagePrivacyPolicy(): void
    {
        $content = $this->laravelView->make(
            'seeder.page.privacy_policy',
            [
                'siteName' => $this->settingService->get('app.name'),
                'dateUpdated' => Carbon::now()->format('Y - m - d H:i'),
                'pageUrl' => $this->settingService->get('app.url'),
                'contactEmail' => $this->settingService->get('contact.sendEmail'),
                'pageCountry' => '<< COUNTRY >>',
            ]
        );

        $page = $this->pageRepository->updateOrCreate(
            [
                'name' => 'Privacy Policy',
            ],
            [
                'title' => 'Privacy Policy',
                'content' => $content,
                'published' => true,
            ]
        );

        $this->slugRepository->updateOrCreate(
            [
                'contentable_type' => Page::class,
                'contentable_id' => $page->getId(),
            ],
            [
                'slug' => 'privacy-policy',
                'meta_title' => 'Privacy policy',
                'meta_description' => 'Page privacy policy',
                'meta_keywords' => 'page, privacy, policy',
            ]
        );
    }
}
