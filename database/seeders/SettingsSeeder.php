<?php

namespace Database\Seeders;

use App\Enums\Setting\KeyEnum;
use App\Repositories\SettingRepositoryInterface;
use Illuminate\Config\Repository as LaravelConfig;
use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    private SettingRepositoryInterface $settingRepository;

    private LaravelConfig $laravelConfig;

    public function __construct(
        SettingRepositoryInterface $settingRepository,
        LaravelConfig $laravelConfig
    ) {
        $this->settingRepository = $settingRepository;
        $this->laravelConfig = $laravelConfig;
    }

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $appSettingsData = [
            [
                'key' => KeyEnum::appName->value,
                'name' => 'Name of application',
                'description' => 'Name of application added to title page etc.',
                'value' => $this->laravelConfig->get(KeyEnum::appName->value),
                'active' => 1,
                'field' => [
                    'name' => 'value',
                    'label' => 'App name',
                    'type' => 'text',
                ],
            ],
            [
                'key' => KeyEnum::appUrl->value,
                'name' => 'URL of application',
                'description' => 'URL of application',
                'value' => $this->laravelConfig->get(KeyEnum::appUrl->value),
                'active' => 1,
                'field' => [
                    'name' => 'value',
                    'label' => 'App url',
                    'type' => 'url',
                ],
            ],
            [
                'key' => KeyEnum::articlesDateFormat->value,
                'name' => 'Date format in articles',
                'description' => 'Date time format in php standard (https://www.php.net/manual/en/datetime.format.php)',
                'value' => $this->laravelConfig->get(KeyEnum::articlesDateFormat->value),
                'active' => 1,
                'field' => [
                    'name' => 'value',
                    'label' => 'Articles date format',
                    'type' => 'text',
                ],
            ],
            [
                'key' => 'home.articles.limit',
                'name' => 'Limit of articles on home page slider',
                'description' => 'Limit of articles on home page slider',
                'value' => $this->laravelConfig->get('home.articles.limit'),
                'active' => 1,
                'field' => [
                    'name' => 'value',
                    'label' => 'Home articles limit on slider',
                    'type' => 'number',
                ],
            ],
            [
                'key' => 'articles.limit',
                'name' => 'Limit of articles on articles page',
                'description' => 'Limit of articles on articles page',
                'value' => $this->laravelConfig->get('articles.limit'),
                'active' => 1,
                'field' => [
                    'name' => 'value',
                    'label' => 'Articles limit on page',
                    'type' => 'number',
                ],
            ],
            [
                'key' => KeyEnum::articlesMetaTitle->value,
                'name' => 'Articles title',
                'description' => 'Added title on articles page',
                'value' => $this->laravelConfig->get(KeyEnum::articlesMetaTitle->value),
                'active' => 1,
                'field' => [
                    'name' => 'value',
                    'label' => 'Articles title',
                    'type' => 'text',
                ],
            ],
            [
                'key' => KeyEnum::articlesMetaDescription->value,
                'name' => 'Articles description',
                'description' => 'Description on articles page',
                'value' => $this->laravelConfig->get(KeyEnum::articlesMetaDescription->value),
                'active' => 1,
                'field' => [
                    'name' => 'value',
                    'label' => 'Articles description',
                    'type' => 'text',
                ],
            ],
            [
                'key' => KeyEnum::articlesMetaKeywords->value,
                'name' => 'Articles keywords',
                'description' => 'Keywords on articles page',
                'value' => $this->laravelConfig->get(KeyEnum::articlesMetaKeywords->value),
                'active' => 1,
                'field' => [
                    'name' => 'value',
                    'label' => 'Articles keywords',
                    'type' => 'text',
                ],
            ],
            [
                'key' => KeyEnum::homeMetaTitle->value,
                'name' => 'Home page title',
                'description' => 'Title added on home page',
                'value' => $this->laravelConfig->get(KeyEnum::homeMetaTitle->value),
                'active' => 1,
                'field' => [
                    'name' => 'value',
                    'label' => 'Home page title',
                    'type' => 'text',
                ],
            ],
            [
                'key' => KeyEnum::homeMetaDescription->value,
                'name' => 'Home page description',
                'description' => 'Home page description',
                'value' => $this->laravelConfig->get(KeyEnum::homeMetaDescription->value),
                'active' => 1,
                'field' => [
                    'name' => 'value',
                    'label' => 'Home page description',
                    'type' => 'text',
                ],
            ],
            [
                'key' => KeyEnum::homeMetaKeywords->value,
                'name' => 'Home page keywords',
                'description' => 'Home page keywords',
                'value' => $this->laravelConfig->get(KeyEnum::homeMetaKeywords->value),
                'active' => 1,
                'field' => [
                    'name' => 'value',
                    'label' => 'Home page keywords',
                    'type' => 'text',
                ],
            ],
            [
                'key' => KeyEnum::contactCanSendTimeInMinutes->value,
                'name' => 'Contact time in minutes when can send next message',
                'description' => 'Contact time can send in minutes',
                'value' => $this->laravelConfig->get(KeyEnum::contactCanSendTimeInMinutes->value),
                'active' => 1,
                'field' => [
                    'name' => 'value',
                    'label' => 'Contact time can send in minutes',
                    'type' => 'number',
                ],
            ],
            [
                'key' => KeyEnum::contactSendToMailEnabled->value,
                'name' => 'Contact send to mail enabled',
                'description' => 'Contact send to mail enabled',
                'value' => $this->laravelConfig->get(KeyEnum::contactSendToMailEnabled->value),
                'active' => 1,
                'field' => [
                    'name' => 'value',
                    'label' => 'Contact send to mail enabled',
                    'type' => 'boolean',
                ],
            ],
            [
                'key' => KeyEnum::contactSendEmail->value,
                'name' => 'Contact address e-m@il to send message',
                'description' => 'Contact address e-m@il to send message',
                'value' => $this->laravelConfig->get(KeyEnum::contactSendEmail->value),
                'active' => 1,
                'field' => [
                    'name' => 'value',
                    'label' => 'Contact address e-m@il to send message',
                    'type' => 'email',
                ],
            ],
            [
                'key' => KeyEnum::maintenanceModeEnabled->value,
                'name' => 'Maintenance mode enable',
                'description' => 'Maintenance mode enable',
                'value' => $this->laravelConfig->get(KeyEnum::maintenanceModeEnabled->value),
                'active' => 1,
                'field' => [
                    'name' => 'value',
                    'label' => 'Maintenance mode enable',
                    'type' => 'boolean',
                ],
            ],
            [
                'key' => KeyEnum::maintenanceModeMessage->value,
                'name' => 'Maintenance mode message',
                'description' => 'Maintenance mode message',
                'value' => $this->laravelConfig->get(KeyEnum::maintenanceModeMessage->value),
                'active' => 1,
                'field' => [
                    'name' => 'value',
                    'label' => 'Maintenance mode message',
                    'type' => 'text',
                ],
            ],
            [
                'key' => KeyEnum::contactMetaTitle->value,
                'name' => 'Contact page title',
                'description' => 'Title added on contact page',
                'value' => $this->laravelConfig->get(KeyEnum::contactMetaTitle->value),
                'active' => 1,
                'field' => [
                    'name' => 'value',
                    'label' => 'Contact page title',
                    'type' => 'text',
                ],
            ],
            [
                'key' => KeyEnum::contactMetaDescription->value,
                'name' => 'Contact page description',
                'description' => 'Contact page description',
                'value' => $this->laravelConfig->get(KeyEnum::contactMetaDescription->value),
                'active' => 1,
                'field' => [
                    'name' => 'value',
                    'label' => 'Contact page description',
                    'type' => 'text',
                ],
            ],
            [
                'key' => KeyEnum::contactMetaKeywords->value,
                'name' => 'Contact page keywords',
                'description' => 'Contact page keywords',
                'value' => $this->laravelConfig->get(KeyEnum::contactMetaKeywords->value),
                'active' => 1,
                'field' => [
                    'name' => 'value',
                    'label' => 'Contact page keywords',
                    'type' => 'text',
                ],
            ],
        ];

        foreach ($appSettingsData as $appSettingDatum) {
            $appSettingDatum['field'] = json_encode($appSettingDatum['field']);

            $key = $appSettingDatum['key'];

            unset($appSettingDatum['key']);

            $this->settingRepository->updateOrCreateByKey($key, $appSettingDatum);
        }
    }
}
