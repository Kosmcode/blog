<?php

namespace Database\Seeders;

use App\Enums\User\PermissionEnum;
use App\Enums\User\RoleEnum;
use App\Repositories\RoleRepositoryInterface;
use Illuminate\Database\Seeder;

class UserRolePermissionsSeeder extends Seeder
{
    private const GUARD_NAME = 'web';

    private RoleRepositoryInterface $roleRepository;

    public function __construct(RoleRepositoryInterface $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $roles = [
            [
                'role' => RoleEnum::admin->value,
                'permissions' => [PermissionEnum::admin->value],
            ],
            [
                'role' => RoleEnum::users->value,
                'permissions' => [PermissionEnum::users->value],
            ],
            [
                'role' => RoleEnum::articles->value,
                'permissions' => [
                    PermissionEnum::articles->value,
                    PermissionEnum::articlesCategory->value,
                    PermissionEnum::articlesTag->value,
                ],
            ],
            [
                'role' => RoleEnum::fileManager->value,
                'permissions' => [PermissionEnum::fileManager->value],
            ],
            [
                'role' => RoleEnum::pages->value,
                'permissions' => [PermissionEnum::pages->value],
            ],
            [
                'role' => RoleEnum::menu->value,
                'permissions' => [PermissionEnum::menu->value],
            ],
            [
                'role' => RoleEnum::settings->value,
                'permissions' => [PermissionEnum::settings->value],
            ],
            [
                'role' => RoleEnum::contact->value,
                'permissions' => [PermissionEnum::contact->value],
            ],
        ];

        foreach ($roles as $role) {
            $roleModel = $this->roleRepository->findOrCreate($role['role'], self::GUARD_NAME);

            foreach ($role['permissions'] as $permission) {
                $roleModel->permissions()
                    ->firstOrCreate(['name' => $permission, 'guard_name' => self::GUARD_NAME]);
            }
        }
    }
}
