import BaseRepository from "@/Repository/GraphQL/BaseRepository";

const QUERY_ARTICLES_FILTERS_PAGINATE = `
    query ARTICLES($first: Int!, $page: Int!, $categories: [String], $tags: [String]){
        articles(first: $first, page: $page, categories: $categories, tags: $tags) {
            data{
                id,
                title,
                content,
                image,
                slug,
                date,
                category {
                    id,
                    name,
                    slug,
                }
                tags {
                    id,
                    name,
                    slug,
                }
            },
            paginatorInfo {
                currentPage,
                total,
                lastPage,
            }
        }
    }
    `

export default class ArticleRepository extends BaseRepository{
    async getArticleFiltersPaginate(first = 10, page = 1, categories = [], tags = []) {
        if (categories.length === 0) {
            categories = null;
        }

        if (tags.length === 0) {
            tags = null;
        }

        return await this.sendGraphQLRequest(this.prepareRequestData(
            QUERY_ARTICLES_FILTERS_PAGINATE, {
                first: first,
                page: page,
                categories: categories,
                tags: tags,
            }
        )).then((result) => {
            return result.articles;
        });
    }
}
