import axios from 'axios';

const GRAPHQL_ENDPOINT = '/graphql';
const HEADERS = {
    'Content-Type': 'application/json'
};

export default class BaseRepository {
    async sendGraphQLRequest(data) {
        try {
            const response = await axios({
                url: GRAPHQL_ENDPOINT,
                method: 'POST',
                headers: HEADERS,
                data: data
            });

            return response.data.data;
        } catch (error) {
            console.log(error)
        }
    }

    prepareRequestData(query, variables = {})
    {
        return {
            query: query,
            variables: variables
        }
    }
}

