import { createI18n } from "vue-i18n";
import localeMessages from "./vue-i18n-locales.generated";
import { createApp, h } from 'vue';
import { createInertiaApp } from '@inertiajs/inertia-vue3';
import { InertiaProgress } from '@inertiajs/progress';
import VueCookies from 'vue-cookies'
import { Inertia } from "@inertiajs/inertia";
import AOS from 'aos'
import '@fortawesome/fontawesome-free/js/all.js';

const appName = window.document.getElementsByTagName('title')[0]?.innerText || 'Laravel';

createInertiaApp({
    title: (title) => `${title} - ${appName}`,
    resolve: (name) => require(`./Pages/${name}.vue`),
    setup({ el, app, props, plugin }) {
        const i18n = createI18n({
            locale: props.initialPage.props.locale,
            fallbackLocale: "en",
            messages: localeMessages,
        });

        return createApp(
            {
                render: () => h(app, props),
                mounted() {
                    AOS.init()
                }
            })
            .use(plugin)
            .use(i18n)
            .use(VueCookies)
            .mixin({ methods: { route } })
            .mount(el);
    },

});

InertiaProgress.init({ color: '#4B5563' });

Inertia.on('navigate', (event) => {
    gtag('js', new Date());
    gtag('config', process.env.GOOGLE_GA4_GID);
});

Inertia.on('success', (event) => {
    window.scrollTo({ top: 0, left: 0, behavior: 'instant' })
})
