export default {
    "en": {
        "auth": {
            "failed": "These credentials do not match our records.",
            "password": "The provided password is incorrect.",
            "throttle": "Too many login attempts. Please try again in {seconds} seconds."
        },
        "command": {
            "user": {
                "create": {
                    "admin": {
                        "info": {
                            "start": "If you want to quit command type: `exit`"
                        },
                        "asks": {
                            "email": "Type user E-m@ail",
                            "username": "Type username",
                            "password": "Type password",
                            "passwordConfirmation": "Type password confirmation"
                        },
                        "error": {
                            "roleAdminNotExists": "Not exist Role Admin."
                        },
                        "success": "User with admin role created successfully !"
                    }
                }
            }
        },
        "exception": {
            "pageElement": {
                "notFoundByTitle": "PageElement not found by title `{pageElementTitle}`"
            }
        },
        "frontend": {
            "cookieConsent": {
                "text": "We use third party cookies to personalize content, ads and analyze site traffic.",
                "linkLabel": "Learn more",
                "acceptButtonLabel": "Accept",
                "notAcceptButtonLabel": "Don't accept"
            },
            "contact": {
                "title": "Contact Us",
                "form": {
                    "firstnameLabel": "First name",
                    "lastnameLabel": "Last name",
                    "emailLabel": "Email address",
                    "subjectLabel": "Subject",
                    "messageLabel": "Message",
                    "submitButtonLabel": "Send"
                },
                "actions": {
                    "afterSend": "Thanks for message :)",
                    "whenSent": "You recently sent a message ;)",
                    "errorSend": "Error sent message :("
                }
            },
            "articles": {
                "title": "Articles",
                "filters": {
                    "categoriesLabel": "Categories",
                    "tagsLabel": "Tags"
                }
            },
            "home": {
                "recentArticles": "Recent articles",
                "readMore": "Read more"
            },
            "error": {
                "403": {
                    "title": "Forbidden",
                    "description": "Sorry, you are forbidden from accessing this page."
                },
                "404": {
                    "title": "Page Not Found",
                    "description": "Sorry, the page you are looking for could not be found."
                },
                "500": {
                    "title": "Server Error",
                    "description": "Whoops, something went wrong on our servers."
                },
                "503": {
                    "title": "ElementService Unavailable",
                    "description": "Sorry, we are doing some maintenance. Please check back soon."
                }
            }
        },
        "pagination": {
            "previous": "&laquo; Previous",
            "next": "Next &raquo;"
        },
        "passwords": {
            "reset": "Your password has been reset!",
            "sent": "We have emailed your password reset link!",
            "throttled": "Please wait before retrying.",
            "token": "This password reset token is invalid.",
            "user": "We can't find a user with that email address."
        },
        "request": {
            "contact": {
                "create": {
                    "firstname": "Firstname",
                    "lastname": "Lastname",
                    "email": "E-mail",
                    "subject": "Subject",
                    "message": "Message"
                }
            }
        },
        "routes": {
            "articles": "articles",
            "contact": "contact",
            "gallery": "gallery",
            "home": "",
            "dontAcceptCookies": "dont-accept-cookies",
            "category": "category",
            "tag": "tag"
        },
        "rule": {
            "articleCategoryNameExistRule": {
                "categoryNotExist": "Category `{value}` not exist."
            },
            "articleTagsNameExistRule": {
                "tagNotExist": "Tag `{value}` not exist."
            },
            "contactCanSendRule": {
                "cantSend": "You cant send next contact message now."
            }
        },
        "validation": {
            "accepted": "The {attribute} must be accepted.",
            "accepted_if": "The {attribute} must be accepted when {other} is {value}.",
            "active_url": "The {attribute} is not a valid URL.",
            "after": "The {attribute} must be a date after {date}.",
            "after_or_equal": "The {attribute} must be a date after or equal to {date}.",
            "alpha": "The {attribute} must only contain letters.",
            "alpha_dash": "The {attribute} must only contain letters, numbers, dashes and underscores.",
            "alpha_num": "The {attribute} must only contain letters and numbers.",
            "array": "The {attribute} must be an array.",
            "before": "The {attribute} must be a date before {date}.",
            "before_or_equal": "The {attribute} must be a date before or equal to {date}.",
            "between": {
                "numeric": "The {attribute} must be between {min} and {max}.",
                "file": "The {attribute} must be between {min} and {max} kilobytes.",
                "string": "The {attribute} must be between {min} and {max} characters.",
                "array": "The {attribute} must have between {min} and {max} items."
            },
            "boolean": "The {attribute} field must be true or false.",
            "confirmed": "The {attribute} confirmation does not match.",
            "current_password": "The password is incorrect.",
            "date": "The {attribute} is not a valid date.",
            "date_equals": "The {attribute} must be a date equal to {date}.",
            "date_format": "The {attribute} does not match the format {format}.",
            "different": "The {attribute} and {other} must be different.",
            "digits": "The {attribute} must be {digits} digits.",
            "digits_between": "The {attribute} must be between {min} and {max} digits.",
            "dimensions": "The {attribute} has invalid image dimensions.",
            "distinct": "The {attribute} field has a duplicate value.",
            "email": "The {attribute} must be a valid email address.",
            "ends_with": "The {attribute} must end with one of the following: {values}.",
            "exists": "The selected {attribute} is invalid.",
            "file": "The {attribute} must be a file.",
            "filled": "The {attribute} field must have a value.",
            "gt": {
                "numeric": "The {attribute} must be greater than {value}.",
                "file": "The {attribute} must be greater than {value} kilobytes.",
                "string": "The {attribute} must be greater than {value} characters.",
                "array": "The {attribute} must have more than {value} items."
            },
            "gte": {
                "numeric": "The {attribute} must be greater than or equal {value}.",
                "file": "The {attribute} must be greater than or equal {value} kilobytes.",
                "string": "The {attribute} must be greater than or equal {value} characters.",
                "array": "The {attribute} must have {value} items or more."
            },
            "image": "The {attribute} must be an image.",
            "in": "The selected {attribute} is invalid.",
            "in_array": "The {attribute} field does not exist in {other}.",
            "integer": "The {attribute} must be an integer.",
            "ip": "The {attribute} must be a valid IP address.",
            "ipv4": "The {attribute} must be a valid IPv4 address.",
            "ipv6": "The {attribute} must be a valid IPv6 address.",
            "json": "The {attribute} must be a valid JSON string.",
            "lt": {
                "numeric": "The {attribute} must be less than {value}.",
                "file": "The {attribute} must be less than {value} kilobytes.",
                "string": "The {attribute} must be less than {value} characters.",
                "array": "The {attribute} must have less than {value} items."
            },
            "lte": {
                "numeric": "The {attribute} must be less than or equal {value}.",
                "file": "The {attribute} must be less than or equal {value} kilobytes.",
                "string": "The {attribute} must be less than or equal {value} characters.",
                "array": "The {attribute} must not have more than {value} items."
            },
            "max": {
                "numeric": "The {attribute} must not be greater than {max}.",
                "file": "The {attribute} must not be greater than {max} kilobytes.",
                "string": "The {attribute} must not be greater than {max} characters.",
                "array": "The {attribute} must not have more than {max} items."
            },
            "mimes": "The {attribute} must be a file of type: {values}.",
            "mimetypes": "The {attribute} must be a file of type: {values}.",
            "min": {
                "numeric": "The {attribute} must be at least {min}.",
                "file": "The {attribute} must be at least {min} kilobytes.",
                "string": "The {attribute} must be at least {min} characters.",
                "array": "The {attribute} must have at least {min} items."
            },
            "multiple_of": "The {attribute} must be a multiple of {value}.",
            "not_in": "The selected {attribute} is invalid.",
            "not_regex": "The {attribute} format is invalid.",
            "numeric": "The {attribute} must be a number.",
            "password": "The password is incorrect.",
            "present": "The {attribute} field must be present.",
            "regex": "The {attribute} format is invalid.",
            "required": "The {attribute} field is required.",
            "required_if": "The {attribute} field is required when {other} is {value}.",
            "required_unless": "The {attribute} field is required unless {other} is in {values}.",
            "required_with": "The {attribute} field is required when {values} is present.",
            "required_with_all": "The {attribute} field is required when {values} are present.",
            "required_without": "The {attribute} field is required when {values} is not present.",
            "required_without_all": "The {attribute} field is required when none of {values} are present.",
            "prohibited": "The {attribute} field is prohibited.",
            "prohibited_if": "The {attribute} field is prohibited when {other} is {value}.",
            "prohibited_unless": "The {attribute} field is prohibited unless {other} is in {values}.",
            "prohibits": "The {attribute} field prohibits {other} from being present.",
            "same": "The {attribute} and {other} must match.",
            "size": {
                "numeric": "The {attribute} must be {size}.",
                "file": "The {attribute} must be {size} kilobytes.",
                "string": "The {attribute} must be {size} characters.",
                "array": "The {attribute} must contain {size} items."
            },
            "starts_with": "The {attribute} must start with one of the following: {values}.",
            "string": "The {attribute} must be a string.",
            "timezone": "The {attribute} must be a valid timezone.",
            "unique": "The {attribute} has already been taken.",
            "uploaded": "The {attribute} failed to upload.",
            "url": "The {attribute} must be a valid URL.",
            "uuid": "The {attribute} must be a valid UUID.",
            "custom": {
                "attribute-name": {
                    "rule-name": "custom-message"
                }
            },
            "attributes": [],
            "rules": [],
            "rule": {
                "GoogleReCaptchaV3CheckRule": {
                    "verifyIsNotValid": "The {attribute} is not valid after validation"
                }
            }
        }
    },
    "pl": {
        "auth": {
            "failed": "Te dane uwierzytelniające nie pasują do naszych danych.",
            "password": "Podane hasło jest nieprawidłowe.",
            "throttle": "Zbyt wiele prób logowania. Spróbuj ponownie za {seconds} sekund."
        },
        "command": {
            "user": {
                "create": {
                    "admin": {
                        "info": {
                            "start": "Jeżeli chcesz zakończyć wpisz: `exit`"
                        },
                        "asks": {
                            "email": "Podaj adres E-m@ail",
                            "username": "Podaj nazwe użytkownika",
                            "password": "Podaj hasło",
                            "passwordConfirmation": "Powtórz hasło"
                        },
                        "error": {
                            "roleAdminNotExists": "Rola Admina nie istnieje"
                        },
                        "success": "Konto administratora utworzono pomyślnie"
                    }
                }
            }
        },
        "exception": {
            "pageElement": {
                "notFoundByTitle": "Nie znaleziono elementu strony `{pageElementTitle}`"
            }
        },
        "frontend": {
            "cookieConsent": {
                "text": "Używamy plików cookie stron trzecich do personalizowania treści, reklam i analizowania ruchu w witrynie.",
                "linkLabel": "Czytaj więcej",
                "acceptButtonLabel": "Akceptuje",
                "notAcceptButtonLabel": "Nie akceptuje"
            },
            "contact": {
                "title": "Kontakt",
                "form": {
                    "firstnameLabel": "Imie",
                    "lastnameLabel": "Nazwisko",
                    "emailLabel": "Adres e-mail",
                    "subjectLabel": "Temat",
                    "messageLabel": "Wiadomość",
                    "submitButtonLabel": "Wyślij"
                },
                "actions": {
                    "afterSend": "Dziękuje za wiadomość :)",
                    "whenSent": "Już wysłałeś wiadomość - poczekaj na odpowiedź ;)",
                    "errorSend": "Nie udało się wysłać wiadomości :(. Spróbuj ponownie później"
                }
            },
            "articles": {
                "title": "Artykuły",
                "filters": {
                    "categoriesLabel": "Kategorie",
                    "tagsLabel": "Tagi"
                }
            },
            "home": {
                "recentArticles": "Polecane artykuły",
                "readMore": "Czytaj więcej"
            },
            "error": {
                "403": {
                    "title": "Dostęp zabroniony",
                    "description": "Przepraszam, nie masz dostępu do tej strony."
                },
                "404": {
                    "title": "Nie znaleziono strony",
                    "description": "Przepraszam, strona, której szukasz, nie została znaleziona."
                },
                "500": {
                    "title": "Server Error",
                    "description": "Ups, coś poszło nie tak na serwerze"
                },
                "503": {
                    "title": "ElementService Unavailable",
                    "description": "Przepraszam, przeprowadzamy prace konserwacyjne. Sprawdź ponownie wkrótce."
                }
            }
        },
        "pagination": {
            "previous": "&laquo; Poprzednia",
            "next": "Następna &raquo;"
        },
        "passwords": {
            "reset": "Twoje hasło zostało zresetowane!",
            "sent": "Wysłaliśmy e-mail z linkiem do resetowania hasła!",
            "throttled": "Proszę czekać przed ponowną próbą.",
            "token": "Ten token resetowania hasła jest nieprawidłowy.",
            "user": "Nie możemy znaleźć użytkownika o tym adresie e-mail."
        },
        "request": {
            "contact": {
                "create": {
                    "firstname": "`Imie`",
                    "lastname": "`Nazwisko`",
                    "email": "`Adres e-mail`",
                    "subject": "`Temat`",
                    "message": "`Wiadomość`"
                }
            }
        },
        "routes": {
            "articles": "artykuly",
            "contact": "kontakt",
            "gallery": "galeria",
            "home": "",
            "dontAcceptCookies": "nie-akceptuje-ciasteczek",
            "category": "kategoria",
            "tag": "tag"
        },
        "rule": {
            "articleCategoryNameExistRule": {
                "categoryNotExist": "Kategoria `{value}` nie istnieje."
            },
            "articleTagsNameExistRule": {
                "tagNotExist": "Tag `{value}` nie istnieje."
            },
            "contactCanSendRule": {
                "cantSend": "Nie możesz wysłać wiadomości teraz."
            }
        },
        "validation": {
            "accepted": "{attribute} musi zostać zaakceptowane.",
            "active_url": "{attribute} nie jest prawidłowym adresem URL.",
            "after": "{attribute} musi zawierać datę, która jest po {date}.",
            "alpha": "{attribute} może zawierać jedynie litery.",
            "alpha_dash": "{attribute} może zawierać jedynie litery, cyfry i myślniki.",
            "alpha_num": "{attribute} może zawierać jedynie litery i cyfry.",
            "array": "{attribute} musi zawierać jakieś elementy.",
            "before": "{attribute} musi zawierać datę, która jest przed {date}.",
            "between": {
                "numeric": "{attribute} musi mieścić się w granicach {min} - {max}.",
                "file": "{attribute} musi mieć {min} - {max} kilobajtów.",
                "string": "{attribute} musi mieć {min} - {max} znaków.",
                "array": "{attribute} musi być pomiędzy {min}, a {max}."
            },
            "boolean": "{attribute} możę być tylko true lub false.",
            "confirmed": "Potwierdzenie {attribute} się nie zgadza.",
            "date": "{attribute} nie jest prawidłową datą.",
            "date_format": "{attribute} nie jest datą w formacie: {format}.",
            "different": "{attribute} i {other} muszą się od siebie różnić.",
            "digits": "{attribute} musi być {digits} numerem.",
            "digits_between": "{attribute} musi być pomiędzy numerami {min} i {max}.",
            "email": "{attribute} nie jest adresem email.",
            "filled": "Pole {attribute} jest wymagane.",
            "exists": "{attribute} nie istnieje.",
            "image": "{attribute} musi być obrazkiem.",
            "in": "Zaznaczona opcja {attribute} jest nieprawidłowa.",
            "integer": "{attribute} musi być liczbą całkowitą.",
            "ip": "{attribute} musi być prawidłowym adresem IP.",
            "max": {
                "numeric": "{attribute} musi być poniżej {max}.",
                "file": "{attribute} musi mieć poniżej {max} kilobajtów.",
                "string": "{attribute} musi mieć poniżej {max} znaków.",
                "array": "{attribute} nie może zawierać więcej niż {max} elementów."
            },
            "mimes": "{attribute} musi być plikiem rodzaju {values}.",
            "min": {
                "numeric": "{attribute} musi być co najmniej {min}.",
                "file": "Plik {attribute} musi mieć co najmniej {min} kilobajtów.",
                "string": "{attribute} musi mieć co najmniej {min} znaki.",
                "array": "{attribute} musi mieć conajmniej {min} elementów."
            },
            "not_in": "Zaznaczona opcja {attribute} jest nieprawidłowa.",
            "numeric": "{attribute} musi być numeryczne.",
            "regex": "Pole {attribute} jest nieprawidłowego formatu.",
            "required": "Pole {attribute} jest wymagane.",
            "required_if": "Pole {attribute} jest wymagane jeśli pole {other} ma wartość {value}.",
            "required_with": "Pole {attribute} jest wymagane wraz z polem {values}.",
            "required_with_all": "Pole {attribute} jest wymagane wraz z polem {values}.",
            "required_without": "Pole {attribute} jest wymagane jeśli nie ma pola {values}.",
            "required_without_all": "Pole {attribute} jest wymagane jeśli żadna z wartości ({values}) nie jest podana.",
            "same": "{attribute} i {other} muszą być takie same.",
            "size": {
                "numeric": "{attribute} musi mieć rozmiary {size}.",
                "file": "{attribute} musi mieć {size} kilobajtów.",
                "string": "{attribute} musi mieć {size} znaków.",
                "array": "{attribute} musi zawierać {size} elementów."
            },
            "string": "{attribute} musi być tekstem.",
            "unique": "{attribute} zostało już użyte.",
            "url": "{attribute} - to nieprawidłowy adres URL.",
            "timezone": "{attribute} jest nieprawidłową strefą czasową.",
            "accepted_if": "The {attribute} must be accepted when {other} is {value}.",
            "after_or_equal": "The {attribute} must be a date after or equal to {date}.",
            "before_or_equal": "The {attribute} must be a date before or equal to {date}.",
            "current_password": "The password is incorrect.",
            "date_equals": "The {attribute} must be a date equal to {date}.",
            "dimensions": "The {attribute} has invalid image dimensions.",
            "distinct": "The {attribute} field has a duplicate value.",
            "ends_with": "The {attribute} must end with one of the following: {values}.",
            "file": "The {attribute} must be a file.",
            "gt": {
                "numeric": "The {attribute} must be greater than {value}.",
                "file": "The {attribute} must be greater than {value} kilobytes.",
                "string": "The {attribute} must be greater than {value} characters.",
                "array": "The {attribute} must have more than {value} items."
            },
            "gte": {
                "numeric": "The {attribute} must be greater than or equal {value}.",
                "file": "The {attribute} must be greater than or equal {value} kilobytes.",
                "string": "The {attribute} must be greater than or equal {value} characters.",
                "array": "The {attribute} must have {value} items or more."
            },
            "in_array": "The {attribute} field does not exist in {other}.",
            "ipv4": "The {attribute} must be a valid IPv4 address.",
            "ipv6": "The {attribute} must be a valid IPv6 address.",
            "json": "The {attribute} must be a valid JSON string.",
            "lt": {
                "numeric": "The {attribute} must be less than {value}.",
                "file": "The {attribute} must be less than {value} kilobytes.",
                "string": "The {attribute} must be less than {value} characters.",
                "array": "The {attribute} must have less than {value} items."
            },
            "lte": {
                "numeric": "The {attribute} must be less than or equal {value}.",
                "file": "The {attribute} must be less than or equal {value} kilobytes.",
                "string": "The {attribute} must be less than or equal {value} characters.",
                "array": "The {attribute} must not have more than {value} items."
            },
            "mimetypes": "The {attribute} must be a file of type: {values}.",
            "multiple_of": "The {attribute} must be a multiple of {value}.",
            "not_regex": "The {attribute} format is invalid.",
            "password": "The password is incorrect.",
            "present": "The {attribute} field must be present.",
            "required_unless": "The {attribute} field is required unless {other} is in {values}.",
            "prohibited": "The {attribute} field is prohibited.",
            "prohibited_if": "The {attribute} field is prohibited when {other} is {value}.",
            "prohibited_unless": "The {attribute} field is prohibited unless {other} is in {values}.",
            "prohibits": "The {attribute} field prohibits {other} from being present.",
            "starts_with": "The {attribute} must start with one of the following: {values}.",
            "uploaded": "The {attribute} failed to upload.",
            "uuid": "The {attribute} must be a valid UUID.",
            "custom": {
                "attribute-name": {
                    "rule-name": "custom-message"
                }
            },
            "attributes": [],
            "rules": [],
            "rule": {
                "GoogleReCaptchaV3CheckRule": {
                    "verifyIsNotValid": "`{attribute}` jest nieprawidłowy po sprawdzeniu poprawności"
                }
            }
        }
    }
}
