<?php

return [
    'user' => [
        'create' => [
            'admin' => [
                'info' => [
                    'start' => 'If you want to quit command type: `exit`',
                ],
                'asks' => [
                    'email' => 'Type user E-m@ail',
                    'username' => 'Type username',
                    'password' => 'Type password',
                    'passwordConfirmation' => 'Type password confirmation',
                ],
                'error' => [
                    'roleAdminNotExists' => 'Not exist Role Admin.',
                ],
                'success' => 'User with admin role created successfully !',
            ],
        ],
    ],
];
