<?php

return [
    'pageElement' => [
        'notFoundByTitle' => 'PageElement not found by title `:pageElementTitle`',
    ],
];
