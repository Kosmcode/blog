<?php

use App\Exceptions\Handler;
use Symfony\Component\HttpFoundation\Response;

return [
    'cookieConsent' => [
        'text' => 'We use third party cookies to personalize content, ads and analyze site traffic.',
        'linkLabel' => 'Learn more',
        'acceptButtonLabel' => 'Accept',
        'notAcceptButtonLabel' => 'Don\'t accept',
    ],
    'contact' => [
        'title' => 'Contact Us',
        'form' => [
            'firstnameLabel' => 'First name',
            'lastnameLabel' => 'Last name',
            'emailLabel' => 'Email address',
            'subjectLabel' => 'Subject',
            'messageLabel' => 'Message',
            'submitButtonLabel' => 'Send',
        ],
        'actions' => [
            'afterSend' => 'Thanks for message :)',
            'whenSent' => 'You recently sent a message ;)',
            'errorSend' => 'Error sent message :(',
        ],
    ],
    'articles' => [
        'title' => 'Articles',
        'filters' => [
            'categoriesLabel' => 'Categories',
            'tagsLabel' => 'Tags',
        ],
    ],
    'home' => [
        'recentArticles' => 'Recent articles',
        'readMore' => 'Read more',
    ],
    'error' => [
        Response::HTTP_FORBIDDEN => [
            'title' => 'Forbidden',
            'description' => 'Sorry, you are forbidden from accessing this page.',
        ],
        Response::HTTP_NOT_FOUND => [
            'title' => 'Page Not Found',
            'description' => 'Sorry, the page you are looking for could not be found.',
        ],
        Response::HTTP_INTERNAL_SERVER_ERROR => [
            'title' => 'Server Error',
            'description' => 'Whoops, something went wrong on our servers.',
        ],
        Response::HTTP_SERVICE_UNAVAILABLE => [
            'title' => 'Service Unavailable',
            'description' => 'Sorry, we are doing some maintenance. Please check back soon.',
        ],
        Handler::HTTP_SESSION_EXPIRED => [
            'title' => 'Session expired',
            'description' => 'The page session expired, please try again.',
        ],
    ],
];
