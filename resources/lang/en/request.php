<?php

return [
    'contact' => [
        'create' => [
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'email' => 'E-mail',
            'subject' => 'Subject',
            'message' => 'Message',
        ],
    ],
];
