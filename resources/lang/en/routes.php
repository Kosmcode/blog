<?php

return [
    'articles' => 'articles',
    'contact' => 'contact',
    'gallery' => 'gallery',
    'home' => '',
    'dontAcceptCookies' => 'dont-accept-cookies',
    'category' => 'category',
    'tag' => 'tag',
];
