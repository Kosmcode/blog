<?php

return [
    'articleCategoryNameExistRule' => [
        'categoryNotExist' => 'Category `:value` not exist.',
    ],
    'articleTagsNameExistRule' => [
        'tagNotExist' => 'Tag `:value` not exist.',
    ],
    'contactCanSendRule' => [
        'cantSend' => 'You cant send next contact message now.',
    ],
];
