<?php

return [
    'user' => [
        'create' => [
            'admin' => [
                'info' => [
                    'start' => 'Jeżeli chcesz zakończyć wpisz: `exit`',
                ],
                'asks' => [
                    'email' => 'Podaj adres E-m@ail',
                    'username' => 'Podaj nazwe użytkownika',
                    'password' => 'Podaj hasło',
                    'passwordConfirmation' => 'Powtórz hasło',
                ],
                'error' => [
                    'roleAdminNotExists' => 'Rola Admina nie istnieje',
                ],
                'success' => 'Konto administratora utworzono pomyślnie',
            ],
        ],
    ],
];
