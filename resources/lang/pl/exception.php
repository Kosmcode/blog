<?php

return [
    'pageElement' => [
        'notFoundByTitle' => 'Nie znaleziono elementu strony `:pageElementTitle`',
    ],
];
