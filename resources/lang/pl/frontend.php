<?php

use App\Exceptions\Handler;
use Symfony\Component\HttpFoundation\Response;

return [
    'cookieConsent' => [
        'text' => 'Używamy plików cookie stron trzecich do personalizowania treści, reklam i analizowania ruchu w witrynie.',
        'linkLabel' => 'Czytaj więcej',
        'acceptButtonLabel' => 'Akceptuje',
        'notAcceptButtonLabel' => 'Nie akceptuje',
    ],
    'contact' => [
        'title' => 'Kontakt',
        'form' => [
            'firstnameLabel' => 'Imie',
            'lastnameLabel' => 'Nazwisko',
            'emailLabel' => 'Adres e-mail',
            'subjectLabel' => 'Temat',
            'messageLabel' => 'Wiadomość',
            'submitButtonLabel' => 'Wyślij',
        ],
        'actions' => [
            'afterSend' => 'Dziękuje za wiadomość :)',
            'whenSent' => 'Już wysłałeś wiadomość - poczekaj na odpowiedź ;)',
            'errorSend' => 'Nie udało się wysłać wiadomości :(. Spróbuj ponownie później',
        ],
    ],
    'articles' => [
        'title' => 'Artykuły',
        'filters' => [
            'categoriesLabel' => 'Kategorie',
            'tagsLabel' => 'Tagi',
        ],
    ],
    'home' => [
        'recentArticles' => 'Polecane artykuły',
        'readMore' => 'Czytaj więcej',
    ],
    'error' => [
        Response::HTTP_FORBIDDEN => [
            'title' => 'Dostęp zabroniony',
            'description' => 'Przepraszam, nie masz dostępu do tej strony.',
        ],
        Response::HTTP_NOT_FOUND => [
            'title' => 'Nie znaleziono strony',
            'description' => 'Przepraszam, strona, której szukasz, nie została znaleziona.',
        ],
        Response::HTTP_INTERNAL_SERVER_ERROR => [
            'title' => 'Server Error',
            'description' => 'Ups, coś poszło nie tak na serwerze',
        ],
        Response::HTTP_SERVICE_UNAVAILABLE => [
            'title' => 'Serwis niedostępny',
            'description' => 'Przepraszam, przeprowadzamy prace konserwacyjne. Sprawdź ponownie wkrótce.',
        ],
        Handler::HTTP_SESSION_EXPIRED => [
            'title' => 'Sesja wygasła',
            'description' => 'Sesja strony wygasła, spróbuj ponownie',
        ],
    ],
];
