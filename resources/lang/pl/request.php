<?php

return [
    'contact' => [
        'create' => [
            'firstname' => '`Imie`',
            'lastname' => '`Nazwisko`',
            'email' => '`Adres e-mail`',
            'subject' => '`Temat`',
            'message' => '`Wiadomość`',
        ],
    ],
];
