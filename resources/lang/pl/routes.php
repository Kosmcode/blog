<?php

return [
    'articles' => 'artykuly',
    'contact' => 'kontakt',
    'gallery' => 'galeria',
    'home' => '',
    'dontAcceptCookies' => 'nie-akceptuje-ciasteczek',
    'category' => 'kategoria',
    'tag' => 'tag',
];
