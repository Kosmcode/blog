<?php

return [
    'articleCategoryNameExistRule' => [
        'categoryNotExist' => 'Kategoria `:value` nie istnieje.',
    ],
    'articleTagsNameExistRule' => [
        'tagNotExist' => 'Tag `:value` nie istnieje.',
    ],
    'contactCanSendRule' => [
        'cantSend' => 'Nie możesz wysłać wiadomości teraz.',
    ],
];
