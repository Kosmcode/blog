<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <title inertia>{{ config('app.name', 'Laravel') }}</title>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="description" content="" inertia>
        <meta name="keywords" content="" inertia>

        <link rel="canonical" href="{{ getCanonicalUrl() }}" inertia>
        <link rel="icon" type="image/x-icon" href="{{ asset('/images/favicon.ico') }}">

        <!-- Styles -->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">

        <!-- Scripts -->
        @routes
        <script type="text/javascript" src="{{ asset('js/app.js') }}" defer></script>
        <script type="text/javascript" src="{{ asset('js/main.js') }}" defer></script>

    </head>
    <body class="page-index">
        @inertia

        <script async src="https://www.googletagmanager.com/gtag/js?id={{ env('GOOGLE_GA4_GID') }}"></script>
        <script>
            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }
        </script>

    </body>
</html>
