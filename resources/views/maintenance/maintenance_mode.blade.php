<!doctype html>
<title>{{ $appName }} - Maintenance</title>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
<style>
    html, body { padding: 0; margin: 0; width: 100%; height: 100%; }
    * {box-sizing: border-box;}
    body { text-align: center; padding: 0; background: white; color: black; font-family: Open Sans; }
    h1 { font-size: 50px; font-weight: 100; text-align: center;}
    body { font-family: Open Sans; font-weight: 100; font-size: 20px; color: black; text-align: center; display: -webkit-box; display: -ms-flexbox; display: flex; -webkit-box-pack: center; -ms-flex-pack: center; justify-content: center; -webkit-box-align: center; -ms-flex-align: center; align-items: center;}
    article { display: block; width: 700px; padding: 50px; margin: 0 auto; }
    a { color: black; font-weight: bold;}
    a:hover { text-decoration: none; }
    .rotate { animation: rotation 15s infinite linear; }
    @keyframes rotation {
        from {
            transform: rotate(0deg);
        }
        to {
            transform: rotate(359deg);
        }
    }
</style>

<article>
    <img class="rotate" style="width: 150px; height: 150px;" src="{{ asset('/images/kosmcode_logo_company.webp') }}" alt="KosmCODE logo" />
    <h1>We&rsquo;ll be back soon!</h1>
    <div>
        <p>{{ $message }}</p>
    </div>
</article>
