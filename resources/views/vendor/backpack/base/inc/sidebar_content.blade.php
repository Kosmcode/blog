@php use App\Enums\User\PermissionEnum;use App\Enums\User\RoleEnum; @endphp
@php @endphp
    <!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item">
    <a class="nav-link" href="{{ backpack_url('dashboard') }}">
        <i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}
    </a>
</li>

@if(isAdminOrHasPermission(RoleEnum::users->value))
    <li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}">
            <i class="nav-icon la la-user"></i> <span>Users</span></a>
    </li>
@endif

@if(isAdminOrHasPermission(PermissionEnum::pages->value))
    <li class='nav-item'>
        <a class='nav-link' href='{{ backpack_url('page') }}'><i
                class='nav-icon la la-file-o'></i><span>Pages</span></a>
        <a class='nav-link' href='{{ backpack_url('page-element') }}'><i
                class='nav-icon la la-file-o'></i><span>Page Elements</span></a>
    </li>
@endif

@if(isAdminOrHasPermission(PermissionEnum::menu->value))
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('menu-item') }}'>
            <i class='nav-icon la la-list'></i><span>Menu</span></a>
    </li>
@endif

@if(isAdminOrHasRole(RoleEnum::articles->value))
    <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-newspaper-o"></i>Articles</a>

        <ul class="nav-dropdown-items">

            @if(isAdminOrHasPermission(PermissionEnum::articles->value))
                <li class="nav-item"><a class="nav-link" href="{{ backpack_url('article') }}">
                        <i class="nav-icon la la-newspaper-o"></i> Articles</a>
                </li>
            @endif

            @if(isAdminOrHasPermission(PermissionEnum::articlesCategory->value))
                <li class="nav-item">
                    <a class="nav-link" href="{{ backpack_url('category') }}"><i class="nav-icon la la-list"></i>
                        Categories</a>
                </li>
            @endif

            @if(isAdminOrHasPermission(PermissionEnum::articlesTag->value))
                <li class="nav-item">
                    <a class="nav-link" href="{{ backpack_url('tag') }}"><i class="nav-icon la la-tag"></i> Tags</a>
                </li>
            @endif

        </ul>

    </li>
@endif

@if(isAdminOrHasPermission(PermissionEnum::fileManager->value))
    <li class="nav-item">
        <a class="nav-link" href="{{ backpack_url('elfinder') }}\">
            <i class="nav-icon la la-files-o"></i><span>{{ trans('backpack::crud.file_manager') }}</span>
        </a>
    </li>
@endif


@if(isAdminOrHasRole(PermissionEnum::settings->value))
    <li class='nav-item'>
        <a class='nav-link' href='{{ backpack_url('setting') }}'>
            <i class='nav-icon la la-cog'></i> <span>Settings</span>
        </a>
    </li>
@endif

@if(isAdminOrHasRole(PermissionEnum::contact->value))
    <li class='nav-item'>
        <a class='nav-link' href='{{ backpack_url('contact') }}'>
            <i class='nav-icon la la-mail-bulk'></i> Contact
        </a>
    </li>
@endif

