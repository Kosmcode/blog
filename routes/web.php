<?php

use App\Enums\Route\SlugEnum;
use App\Http\Controllers\Admin\Article\PreviewController;
use App\Http\Controllers\Web\Article\ArticleController;
use App\Http\Controllers\Web\ContactController;
use App\Http\Controllers\Web\Cookie\ConsentController;
use App\Http\Controllers\Web\HomeController;
use App\Http\Controllers\Web\SlugController;
use App\Http\Middleware\MaintenanceModeMiddleware;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware([
    MaintenanceModeMiddleware::class,
])
    ->group(function () {
        Route::name('admin')
            ->prefix('admin')
            ->group(function () {
                Route::name('.preview')
                    ->prefix('preview')
                    ->group(function () {
                        Route::get('article/{articleId}', [PreviewController::class, 'getPreviewArticle'])
                            ->name('.article');
                    });
            });

        Route::get(SlugEnum::home->route(), [HomeController::class, 'get'])
            ->name(SlugEnum::home->name);

        Route::name(SlugEnum::articles->name)
            ->prefix(SlugEnum::articles->route())
            ->group(function () {
                Route::get('/', [ArticleController::class, 'getArticles']);
                Route::get(SlugEnum::articlesCategorySlug->route(), [ArticleController::class, 'getByCategorySlug'])
                    ->name('.category');
                Route::get(SlugEnum::articlesTagSlug->route(), [ArticleController::class, 'getByTagSlug'])
                    ->name('.tag');
            });

        Route::name(SlugEnum::contact->name)
            ->prefix(SlugEnum::contact->route())
            ->group(function () {
                Route::get('', [ContactController::class, 'index']);
                Route::post('', [ContactController::class, 'createFromForm'])->name('.store');
            });

        Route::get(SlugEnum::dontAcceptCookies->route(), [ConsentController::class, 'removeAllCookiesAndBye'])
            ->name(SlugEnum::dontAcceptCookies->name);

        Route::get('{slug}', [SlugController::class, 'get'])
            ->name('slug');
    });

//require __DIR__.'/auth.php';
