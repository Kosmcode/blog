<?php

namespace Tests\Feature;

use App\Enums\Intertia\PropsEnum;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Inertia\Testing\AssertableInertia as Assert;
use Nuwave\Lighthouse\Testing\MakesGraphQLRequests;
use Tests\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use MakesGraphQLRequests, RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->refreshInMemoryDatabase();
        $this->seed();
    }

    protected function assertInertiaShareProps(Assert $page): void
    {
        $page->has('auth');
        $page->has('auth.user');
        $page->has('nav');
        $page->has('locale');
        $page->has('maintenanceModeEnabled');
    }

    protected function assertInertiaAppHeadProps(
        Assert $page,
        string $title,
        string $description,
        string $keywords,
        string $canonical
    ): void {
        $page->has(PropsEnum::appHead->value, 4);
        $page->has(
            PropsEnum::appHead->value,
            fn (Assert $page) => $page
                ->where('title', $title)
                ->where('description', $description)
                ->where('keywords', $keywords)
                ->where('canonical', $canonical)
        );
    }
}
