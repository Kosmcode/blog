<?php

namespace Tests\Feature\Trait;

use App\Enums\Article\FeatureEnum;
use App\Enums\Article\StatusEnum;
use App\Models\Article;
use Illuminate\Database\Eloquent\Collection;
use Inertia\Testing\AssertableInertia as Assert;
use Tests\TestCase;

trait ArticleTrait
{
    protected function createArticles(
        StatusEnum $statusEnum,
        FeatureEnum $featureEnum,
        int $count = 1,
        bool $withImage = false,
    ): Collection {
        return Article::factory()
            ->count($count)
            ->create([
                'status' => $statusEnum->value,
                'featured' => $featureEnum->value,
                'image' => ($withImage) ? TestCase::ARTICLE_IMAGE_TEST_FILENAME : null,
            ]);
    }

    protected function assertArticlesResent(
        Assert $page,
        string $assertKey,
        int $count
    ): void {
        $page
            ->has(
                $assertKey,
                $count,
                function (Assert $page) {
                    $page
                        ->has('title')
                        ->has('image')
                        ->has('content')
                        ->has('slug')
                        ->has('date');
                }
            );
    }
}
