<?php

namespace Tests\Feature\Trait;

use App\Enums\Page\ElementEnum;
use App\Models\PageElement;
use Inertia\Testing\AssertableInertia as Assert;

trait PageElementTrait
{
    protected function updatePageElement(
        ElementEnum $pageElementEnum,
        ?string $content,
        bool $enable
    ): void {
        PageElement::query()
            ->where(['title' => $pageElementEnum->value])
            ->first()
            ->update([
                'content' => $content,
                'enable' => $enable,
            ]);
    }

    protected function assertPageElement(
        Assert $page,
        string $assertKey,
        bool $enabled,
        ?string $content
    ): void {
        $page->has(
            $assertKey,
            fn (Assert $page) => $page
                ->where('enabled', $enabled)
                ->where('content', $content)
        );
    }
}
