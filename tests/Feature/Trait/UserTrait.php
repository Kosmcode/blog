<?php

namespace Tests\Feature\Trait;

use App\Enums\User\RoleEnum;
use App\Models\Role;
use App\Models\User;

trait UserTrait
{
    protected function createUserWithAdminRole(): User
    {
        $adminRole = Role::query()
            ->where('name', RoleEnum::admin->value)
            ->first();

        return User::factory()
            ->hasAttached(
                $adminRole,
                [],
                'roles'
            )
            ->count(1)
            ->create()
            ->first();
    }
}
