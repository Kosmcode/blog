<?php

namespace Tests\Feature\Web\Articles\Category;

use App\Enums\Article\FeatureEnum;
use App\Enums\Article\StatusEnum;
use App\Enums\Intertia\ComponentEnum;
use App\Enums\Route\SlugEnum;
use App\Models\Article;
use App\Models\Category;
use Inertia\Testing\AssertableInertia as Assert;
use Symfony\Component\HttpFoundation\Response;
use Tests\Feature\TestCase;
use Tests\Feature\Trait\ArticleTrait;

class GetTest extends TestCase
{
    use ArticleTrait;

    public function testGetArticlesByCategorySlugWhenNotExists(): void
    {
        $uri = str_replace(
            '{categorySlug}',
            'not-exists-category-slug',
            SlugEnum::articles->route().SlugEnum::articlesCategorySlug->route()
        );

        $this->get($uri)
            ->assertInertia(function (Assert $page) {
                $page->component(ComponentEnum::error->value);
            })
            ->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testGetArticlesByCategorySlugFirstPage(): void
    {
        $categoryFirst = Category::factory()
            ->count(1)
            ->create([
                'id' => 123456789,
                'slug' => 'test-category-slug',
            ])
            ->first();
        $categorySecound = Category::factory()
            ->count(1)
            ->create([
                'id' => 987654321,
                'slug' => 'test-category-slug-secound',
            ])
            ->first();

        Article::factory(5)
            ->create([
                'category_id' => $categoryFirst->getId(),
                'status' => StatusEnum::published->value,
                'featured' => FeatureEnum::notFeature->value,
                'image' => null,
            ]);
        Article::factory(5)
            ->create([
                'category_id' => $categorySecound->getId(),
                'status' => StatusEnum::published->value,
                'featured' => FeatureEnum::feature->value,
                'image' => null,
            ]);

        $uri = str_replace(
            '{categorySlug}',
            'test-category-slug',
            SlugEnum::articles->route().SlugEnum::articlesCategorySlug->route()
        );

        $this->get($uri)
            ->assertInertia(function (Assert $page) use ($uri) {
                $page->component(ComponentEnum::articleFilters->value);
                $this->assertInertiaShareProps($page);
                $this->assertInertiaAppHeadProps(
                    $page,
                    'Articles',
                    'Page Articles',
                    'articles, article, page',
                    env('APP_URL').ltrim($uri, DIRECTORY_SEPARATOR)
                );
                $page
                    ->has(
                        'data',
                        fn (Assert $page) => $page
                            ->has('articles', 5)
                            ->where('current_page', 1)
                            ->where('last_page', 1)
                    )->has(
                        'filters',
                        fn (Assert $page) => $page
                            ->has('categories')
                            ->has('categories.values')
                            ->has('categories.selectedValues')
                            ->has('tags')
                            ->has('tags.values')
                            ->has('categories.selectedValues')
                    )
                    ->where('articlesPageLimit', 10);
                $this->assertArticlesResent(
                    $page,
                    'recentArticles.data',
                    4
                );
            })->assertStatus(Response::HTTP_OK);
    }
}
