<?php

namespace Tests\Feature\Web\Articles;

use App\Enums\Article\FeatureEnum;
use App\Enums\Article\StatusEnum;
use App\Enums\Intertia\ComponentEnum;
use App\Enums\Route\SlugEnum;
use App\Models\Article;
use App\Models\Category;
use App\Models\Tag;
use Illuminate\Testing\Fluent\AssertableJson;
use Inertia\Testing\AssertableInertia as Assert;
use Symfony\Component\HttpFoundation\Response;
use Tests\Feature\TestCase;
use Tests\Feature\Trait\ArticleTrait;

class GetTest extends TestCase
{
    use ArticleTrait;

    public function testGetArticlesFirstPageWithoutArticles(): void
    {
        $this->get(SlugEnum::articles->route())
            ->assertInertia(function (Assert $page) {
                $page->component(ComponentEnum::articleFilters->value);
                $this->assertInertiaShareProps($page);
                $this->assertInertiaAppHeadProps(
                    $page,
                    'Articles',
                    'Page Articles',
                    'articles, article, page',
                    env('APP_URL').ltrim(SlugEnum::articles->route(), DIRECTORY_SEPARATOR)
                );
                $page
                    ->has(
                        'data',
                        fn (Assert $page) => $page
                            ->has('articles', 0)
                            ->where('current_page', 1)
                            ->where('last_page', 1)
                    )->has(
                        'filters',
                        fn (Assert $page) => $page
                            ->has('categories')
                            ->has('categories.values')
                            ->has('categories.selectedValues')
                            ->has('tags')
                            ->has('tags.values')
                            ->has('categories.selectedValues')
                    )
                    ->has('recentArticles.data', 0)
                    ->where('articlesPageLimit', 10);
            })->assertStatus(Response::HTTP_OK);
    }

    public function testGetArticlesFirstPageWithArticles(): void
    {
        $this->createArticles(
            StatusEnum::published,
            FeatureEnum::notFeature,
            10
        );
        $this->createArticles(
            StatusEnum::published,
            FeatureEnum::feature,
            4
        );
        $this->createArticles(
            StatusEnum::draft,
            FeatureEnum::notFeature,
            5
        );

        $this->get(SlugEnum::articles->route())
            ->assertInertia(function (Assert $page) {
                $page->component(ComponentEnum::articleFilters->value);
                $this->assertInertiaShareProps($page);
                $this->assertInertiaAppHeadProps(
                    $page,
                    'Articles',
                    'Page Articles',
                    'articles, article, page',
                    env('APP_URL').ltrim(SlugEnum::articles->route(), DIRECTORY_SEPARATOR)
                );
                $page
                    ->has(
                        'data',
                        fn (Assert $page) => $page
                            ->has('articles', 10)
                            ->where('current_page', 1)
                            ->where('last_page', 2)
                    )->has(
                        'filters',
                        fn (Assert $page) => $page
                            ->has('categories')
                            ->has('categories.values')
                            ->has('categories.selectedValues')
                            ->has('tags')
                            ->has('tags.values')
                            ->has('categories.selectedValues')
                    )
                    ->where('articlesPageLimit', 10);
                $this->assertArticlesResent(
                    $page,
                    'recentArticles.data',
                    4
                );
            })->assertStatus(Response::HTTP_OK);
    }

    public function testGetArticlesByGraphQLEndpoint()
    {
        $this->createArticles(
            StatusEnum::published,
            FeatureEnum::notFeature,
            10
        );
        $this->createArticles(
            StatusEnum::published,
            FeatureEnum::feature,
            4
        );
        $this->createArticles(
            StatusEnum::draft,
            FeatureEnum::notFeature,
            5
        );

        $this->graphQL(/** @lang GraphQL */ '
            query ARTICLES(
  $first: Int!
  $page: Int!
  $categories: [String]
  $tags: [String]
) {
  articles(first: $first, page: $page, categories: $categories, tags: $tags) {
    data {
      id
      title
      content
      image
      slug
      date
      category {
        id
        name
        slug
      }
      tags {
        id
        name
        slug
      }
    }
    paginatorInfo {
      currentPage
      total
      lastPage
    }
  }
}
            ',
            [
                'categories' => null,
                'tags' => null,
                'first' => 10,
                'page' => 1,
            ]
        )
            ->assertStatus(Response::HTTP_OK)
            ->assertJson(function (AssertableJson $assertableJson) {
                $assertableJson
                    ->has(
                        'data.articles.data.0',
                        fn (AssertableJson $assertableJson) => $assertableJson
                            ->has('id')
                            ->has('title')
                            ->has('content')
                            ->has('image')
                            ->has('slug')
                            ->has('date')
                            ->has(
                                'category',
                                fn (AssertableJson $assertableJson) => $assertableJson
                                    ->has('id')
                                    ->has('name')
                                    ->has('slug')
                            )
                            ->has(
                                'tags.0',
                                fn (AssertableJson $assertableJson) => $assertableJson
                                    ->has('id')
                                    ->has('name')
                                    ->has('slug')
                            )
                    )
                    ->has(
                        'data.articles.paginatorInfo',
                        fn (AssertableJson $assertableJson) => $assertableJson
                            ->has('currentPage')
                            ->has('total')
                            ->has('lastPage')
                    );
            });
    }

    public function testGetArticlesGraphQLEndpointByCategoryFilter(): void
    {
        $categoryFirst = Category::factory()
            ->count(1)
            ->create([
                'id' => 123456789,
                'slug' => 'test-category-slug',
            ])
            ->first();
        $categorySecound = Category::factory()
            ->count(1)
            ->create([
                'id' => 987654321,
                'slug' => 'test-category-slug-secound',
            ])
            ->first();

        Article::factory(5)
            ->create([
                'category_id' => $categoryFirst->getId(),
                'status' => StatusEnum::published->value,
                'featured' => FeatureEnum::notFeature->value,
                'image' => null,
            ]);
        Article::factory(5)
            ->create([
                'category_id' => $categorySecound->getId(),
                'status' => StatusEnum::published->value,
                'featured' => FeatureEnum::feature->value,
                'image' => null,
            ]);

        $this->graphQL(/** @lang GraphQL */ '
            query ARTICLES(
  $first: Int!
  $page: Int!
  $categories: [String]
  $tags: [String]
) {
  articles(first: $first, page: $page, categories: $categories, tags: $tags) {
    data {
      id
      title
      content
      image
      slug
      date
      category {
        id
        name
        slug
      }
      tags {
        id
        name
        slug
      }
    }
    paginatorInfo {
      currentPage
      total
      lastPage
    }
  }
}
            ',
            [
                'categories' => [$categoryFirst->getName(), $categorySecound->getName()],
                'tags' => null,
                'first' => 10,
                'page' => 1,
            ]
        )
            ->assertStatus(Response::HTTP_OK)
            ->assertJson(function (AssertableJson $assertableJson) {
                $assertableJson
                    ->has(
                        'data.articles.data.0',
                        fn (AssertableJson $assertableJson) => $assertableJson
                            ->has('id')
                            ->has('title')
                            ->has('content')
                            ->has('image')
                            ->has('slug')
                            ->has('date')
                            ->has(
                                'category',
                                fn (AssertableJson $assertableJson) => $assertableJson
                                    ->has('id')
                                    ->has('name')
                                    ->has('slug')
                            )
                            ->has(
                                'tags.0',
                                fn (AssertableJson $assertableJson) => $assertableJson
                                    ->has('id')
                                    ->has('name')
                                    ->has('slug')
                            )
                    )
                    ->has(
                        'data.articles.paginatorInfo',
                        fn (AssertableJson $assertableJson) => $assertableJson
                            ->has('currentPage')
                            ->has('total')
                            ->has('lastPage')
                    );
            });
    }

    public function testGetArticlesGraphQLEndpointByTagsFilter(): void
    {
        Article::factory()
            ->count(5)
            ->hasAttached(
                Tag::factory()
                    ->count(1)
                    ->create([
                        'id' => 123456789,
                        'name' => 'test tag',
                        'slug' => 'test-tag-slug',
                    ]),
                relationship: 'tags'
            )
            ->create([
                'status' => StatusEnum::published->value,
                'featured' => FeatureEnum::notFeature->value,
                'image' => self::ARTICLE_IMAGE_TEST_FILENAME,
            ]);

        Article::factory(5)
            ->count(5)
            ->hasAttached(
                Tag::factory()
                    ->count(1)
                    ->create([
                        'id' => 987654321,
                        'name' => 'test tag secound',
                        'slug' => 'test-tag-secound-slug',
                    ]),
                relationship: 'tags'
            )
            ->create([
                'status' => StatusEnum::published->value,
                'featured' => FeatureEnum::feature->value,
                'image' => null,
            ]);

        $this->graphQL(/** @lang GraphQL */ '
            query ARTICLES(
  $first: Int!
  $page: Int!
  $categories: [String]
  $tags: [String]
) {
  articles(first: $first, page: $page, categories: $categories, tags: $tags) {
    data {
      id
      title
      content
      image
      slug
      date
      category {
        id
        name
        slug
      }
      tags {
        id
        name
        slug
      }
    }
    paginatorInfo {
      currentPage
      total
      lastPage
    }
  }
}
            ',
            [
                'categories' => null,
                'tags' => ['test tag', 'test tag secound'],
                'first' => 10,
                'page' => 1,
            ]
        )
            ->assertStatus(Response::HTTP_OK)
            ->assertJson(function (AssertableJson $assertableJson) {
                $assertableJson
                    ->has(
                        'data.articles.data.0',
                        fn (AssertableJson $assertableJson) => $assertableJson
                            ->has('id')
                            ->has('title')
                            ->has('content')
                            ->has('image')
                            ->has('slug')
                            ->has('date')
                            ->has(
                                'category',
                                fn (AssertableJson $assertableJson) => $assertableJson
                                    ->has('id')
                                    ->has('name')
                                    ->has('slug')
                            )
                            ->has(
                                'tags.0',
                                fn (AssertableJson $assertableJson) => $assertableJson
                                    ->has('id')
                                    ->has('name')
                                    ->has('slug')
                            )
                    )
                    ->has(
                        'data.articles.paginatorInfo',
                        fn (AssertableJson $assertableJson) => $assertableJson
                            ->has('currentPage')
                            ->has('total')
                            ->has('lastPage')
                    );
            });
    }
}
