<?php

namespace Tests\Feature\Web\Contact\Form;

use App\Enums\Intertia\ComponentEnum;
use App\Enums\Route\SlugEnum;
use App\Models\Contact;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Inertia\Testing\AssertableInertia as Assert;
use Tests\Feature\TestCase;

class PostTest extends TestCase
{
    public function testSendFormWithEmptyData(): void
    {
        $url = env('APP_URL').ltrim(SlugEnum::contact->route(), DIRECTORY_SEPARATOR);

        $this->get($url);

        $this
            ->followingRedirects()
            ->post(
                $url,
                [
                    'firstname' => null,
                    'lastname' => null,
                    'email' => null,
                    'subject' => null,
                    'message' => null,
                ],
                [
                    'X-Inertia' => true,
                    'X-CSRF-TOKEN' => csrf_token(),
                    'Referer' => $url,
                ]
            )
            ->assertInertia(function (Assert $page) {
                $page->component(ComponentEnum::contact->value);
                $page->has('errors', fn (Assert $page) => $page
                    ->has('firstname')
                    ->has('lastname')
                    ->has('email')
                    ->has('subject')
                    ->has('message')
                );
                $page->where('canSent', true);
                $page->where('result', null);
            })
            ->assertStatus(Response::HTTP_OK);
    }

    public function testSendFormWithCorrectDataWithResendTry(): void
    {
        $url = env('APP_URL').ltrim(SlugEnum::contact->route(), DIRECTORY_SEPARATOR);

        $this->get($url);

        $this
            ->followingRedirects()
            ->postJson(
                $url,
                [
                    'firstname' => 'Firstnametest',
                    'lastname' => 'LastnameTest',
                    'email' => 'test@test.com',
                    'subject' => 'Test subject of contact',
                    'message' => 'Test message of contact. Test message of contact. Test message of contact. Test message of contact. ',
                ],
                [
                    'X-Inertia' => true,
                    'X-CSRF-TOKEN' => csrf_token(),
                    'Referer' => $url,
                ]
            )
            ->assertJson(function (AssertableJson $assertableJson) {
                $assertableJson
                    ->where('component', ComponentEnum::contact->value)
                    ->has('url')
                    ->has('version')
                    ->where('props.errors', [])
                    ->where('props.canSent', false)
                    ->where('props.result', true);
            })
            ->assertStatus(Response::HTTP_OK);

        $this->assertSame(
            1,
            Contact::query()->select(['id'])->count()
        );

        $this->get($url)
            ->assertStatus(Response::HTTP_OK)
            ->assertInertia(
                function (Assert $page) {
                    $page
                        ->component(ComponentEnum::contact->value)
                        ->where('canSent', false)
                        ->where('result', null);
                }
            );

        $this
            ->followingRedirects()
            ->postJson(
                $url,
                [
                    'firstname' => 'Firstnametest',
                    'lastname' => 'LastnameTest',
                    'email' => 'test@test.com',
                    'subject' => 'Test subject of contact',
                    'message' => 'Test message of contact. Test message of contact. Test message of contact. Test message of contact. ',
                ],
                [
                    'X-Inertia' => true,
                    'X-CSRF-TOKEN' => csrf_token(),
                    'Referer' => $url,
                ]
            )
            ->assertJson(function (AssertableJson $assertableJson) {
                $assertableJson
                    ->where('component', ComponentEnum::error->value)
                    ->has('props')
                    ->has('url')
                    ->has('version');
            })
            ->assertStatus(Response::HTTP_FORBIDDEN);

        $this->assertSame(
            1,
            Contact::query()->select(['id'])->count()
        );
    }
}
