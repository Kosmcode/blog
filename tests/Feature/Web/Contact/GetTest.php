<?php

namespace Tests\Feature\Web\Contact;

use App\Enums\Intertia\ComponentEnum;
use App\Enums\Page\ElementEnum;
use App\Enums\Route\SlugEnum;
use Inertia\Testing\AssertableInertia as Assert;
use Symfony\Component\HttpFoundation\Response;
use Tests\Feature\TestCase;
use Tests\Feature\Trait\PageElementTrait;

class GetTest extends TestCase
{
    use PageElementTrait;

    public function testGetContactPageWithoutPageElements(): void
    {
        $this->get(SlugEnum::contact->route())
            ->assertInertia(function (Assert $page) {
                $page->component(ComponentEnum::contact->value);
                $this->assertInertiaShareProps($page);
                $this->assertInertiaAppHeadProps(
                    $page,
                    'Contact page',
                    'This is contact page',
                    'contact, page',
                    env('APP_URL').ltrim(SlugEnum::contact->route(), DIRECTORY_SEPARATOR)
                );
                $this->assertPageElement(
                    $page,
                    'elements.'.ElementEnum::contactPageTop->value.'.data',
                    false,
                    null
                );
                $this->assertPageElement(
                    $page,
                    'elements.'.ElementEnum::contactPageDown->value.'.data',
                    false,
                    null
                );
                $page->has('result');
                $page->has('canSent');
            })
            ->assertStatus(Response::HTTP_OK);
    }

    public function testGetContactPageWithPageElements(): void
    {
        $this->updatePageElement(
            ElementEnum::contactPageTop,
            'contactPageTop content',
            true
        );
        $this->updatePageElement(
            ElementEnum::contactPageDown,
            'contactPageDown content',
            true
        );

        $this->get(SlugEnum::contact->route())
            ->assertInertia(function (Assert $page) {
                $page->component(ComponentEnum::contact->value);
                $this->assertInertiaShareProps($page);
                $this->assertInertiaAppHeadProps(
                    $page,
                    'Contact page',
                    'This is contact page',
                    'contact, page',
                    env('APP_URL').ltrim(SlugEnum::contact->route(), DIRECTORY_SEPARATOR)
                );
                $this->assertPageElement(
                    $page,
                    'elements.'.ElementEnum::contactPageTop->value.'.data',
                    true,
                    'contactPageTop content'
                );
                $this->assertPageElement(
                    $page,
                    'elements.'.ElementEnum::contactPageDown->value.'.data',
                    true,
                    'contactPageDown content'
                );
                $page->has('result');
            })
            ->assertStatus(Response::HTTP_OK);
    }
}
