<?php

namespace Tests\Feature\Web\Cookie;

use App\Enums\Route\SlugEnum;
use Tests\Feature\TestCase;

class ConsentTest extends TestCase
{
    public function testDontAgreeCookieConsent(): void
    {
        $this
            ->withCookies(['test' => true])
            ->get(SlugEnum::dontAcceptCookies->route())
            ->assertRedirect('https://google.com/');
    }
}
