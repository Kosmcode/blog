<?php

namespace Feature\Web\Home;

use App\Enums\Article\FeatureEnum;
use App\Enums\Article\StatusEnum;
use App\Enums\Intertia\ComponentEnum;
use App\Enums\Page\ElementEnum;
use Inertia\Testing\AssertableInertia as Assert;
use Symfony\Component\HttpFoundation\Response;
use Tests\Feature\TestCase;
use Tests\Feature\Trait\ArticleTrait;
use Tests\Feature\Trait\PageElementTrait;

class GetTest extends TestCase
{
    use ArticleTrait, PageElementTrait;

    public function testGetHomeWithoutAll(): void
    {
        $this->get('/')
            ->assertInertia(function (Assert $page) {
                $page->component(ComponentEnum::home->value);
                $this->assertInertiaShareProps($page);
                $this->assertInertiaAppHeadProps(
                    $page,
                    'Home page',
                    'This is home page',
                    'home, page',
                    'http://localhost/'
                );
                $page->has('articles', 0);
                $page->has('elements', 2);
                $this->assertPageElement(
                    $page,
                    $this->preparePageElementAssertKey(ElementEnum::homePageTop),
                    false,
                    null
                );
                $this->assertPageElement(
                    $page,
                    $this->preparePageElementAssertKey(ElementEnum::homePageDown),
                    false,
                    null
                );
            }
            )->assertStatus(Response::HTTP_OK);
    }

    public function testGetHomeWhenHasPageElement(): void
    {
        $this->updatePageElement(
            ElementEnum::homePageTop,
            'HomePageElementTopContent',
            true
        );
        $this->updatePageElement(
            ElementEnum::homePageDown,
            'HomePageElementDownContent',
            true
        );

        $this->get('/')
            ->assertInertia(function (Assert $page) {
                $page->component(ComponentEnum::home->value);
                $this->assertInertiaShareProps($page);
                $this->assertInertiaAppHeadProps(
                    $page,
                    'Home page',
                    'This is home page',
                    'home, page',
                    'http://localhost/'
                );
                $page->has('articles', 0);
                $page->has('elements', 2);
                $this->assertPageElement(
                    $page,
                    $this->preparePageElementAssertKey(ElementEnum::homePageTop),
                    true,
                    'HomePageElementTopContent'
                );
                $this->assertPageElement(
                    $page,
                    $this->preparePageElementAssertKey(ElementEnum::homePageDown),
                    true,
                    'HomePageElementDownContent'
                );
            }
            )->assertStatus(Response::HTTP_OK);
    }

    public function testGetHomeWhenHasRecentArticles(): void
    {
        $this->createArticles(
            StatusEnum::draft,
            FeatureEnum::feature,
            2
        );
        $this->createArticles(
            StatusEnum::published,
            FeatureEnum::notFeature,
            2
        );
        $this->createArticles(
            StatusEnum::published,
            FeatureEnum::feature,
            4,
            true
        );

        $this->get('/')
            ->assertInertia(function (Assert $page) {
                $page->component(ComponentEnum::home->value);
                $this->assertInertiaShareProps($page);
                $this->assertInertiaAppHeadProps(
                    $page,
                    'Home page',
                    'This is home page',
                    'home, page',
                    'http://localhost/'
                );
                $page->has('elements', 2);
                $this->assertArticlesResent($page, 'articles', 4);
            }
            )->assertStatus(Response::HTTP_OK);
    }

    protected function preparePageElementAssertKey(ElementEnum $pageElementEnum): string
    {
        return sprintf(
            'elements.%s.data',
            $pageElementEnum->value
        );
    }
}
