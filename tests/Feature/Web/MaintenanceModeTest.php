<?php

namespace Tests\Feature\Web;

use App\Enums\Article\StatusEnum;
use App\Enums\Route\SlugEnum;
use App\Enums\Setting\KeyEnum;
use App\Models\Article;
use App\Models\Category;
use App\Models\Page;
use App\Models\Tag;
use Illuminate\Http\Response;
use Inertia\Testing\AssertableInertia as Assert;
use Setting;
use Tests\Feature\TestCase;
use Tests\Feature\Trait\UserTrait;

class MaintenanceModeTest extends TestCase
{
    use UserTrait;

    public function testMaintenanceModeOnPages(): void
    {
        $category = Category::factory()
            ->count(1)
            ->create([
                'slug' => 'article-category',
            ])
            ->first();

        $article = Article::factory()
            ->count(1)
            ->hasAttached(
                Tag::factory()
                    ->count(1)
                    ->create([
                        'slug' => 'article-tag',
                    ]),
                relationship: 'tags'
            )
            ->create([
                'category_id' => $category->getId(),
                'status' => StatusEnum::published->value,
                'title' => 'article',
            ])
            ->first();

        $page = Page::factory()
            ->create()
            ->first();

        $pagesToTest = [
            SlugEnum::home->route(),
            SlugEnum::contact->route(),
            SlugEnum::articles->route(),
            DIRECTORY_SEPARATOR.'not-exist-page',
            str_replace(
                '{categorySlug}',
                'article-category',
                SlugEnum::articles->route().SlugEnum::articlesCategorySlug->route()
            ),
            str_replace(
                '{tagSlug}',
                'article-tag',
                SlugEnum::articles->route().SlugEnum::articlesTagSlug->route()
            ),
            DIRECTORY_SEPARATOR.$article->slug()->first()->getSlug(),
            DIRECTORY_SEPARATOR.$page->getSlug()->getSlug(),
        ];

        Setting::query()
            ->where('key', KeyEnum::maintenanceModeEnabled->value)
            ->first()
            ->update(['value' => 1]);

        foreach ($pagesToTest as $pageUri) {
            $this
                ->get($pageUri)
                ->assertStatus(Response::HTTP_SERVICE_UNAVAILABLE);
        }
    }

    public function testMaintenanceModeOnPagesWhenUserIsLoggedAndIsAdmin(): void
    {
        $category = Category::factory()
            ->count(1)
            ->create([
                'slug' => 'article-category',
            ])
            ->first();

        $article = Article::factory()
            ->count(1)
            ->hasAttached(
                Tag::factory()
                    ->count(1)
                    ->create([
                        'slug' => 'article-tag',
                    ]),
                relationship: 'tags'
            )
            ->create([
                'category_id' => $category->getId(),
                'status' => StatusEnum::published->value,
                'title' => 'article',
            ])
            ->first();

        $page = Page::factory()
            ->create()
            ->first();

        $pagesToTest = [
            SlugEnum::home->route(),
            SlugEnum::contact->route(),
            SlugEnum::articles->route(),
            str_replace(
                '{categorySlug}',
                'article-category',
                SlugEnum::articles->route().SlugEnum::articlesCategorySlug->route()
            ),
            str_replace(
                '{tagSlug}',
                'article-tag',
                SlugEnum::articles->route().SlugEnum::articlesTagSlug->route()
            ),
            DIRECTORY_SEPARATOR.$article->slug()->first()->getSlug(),
            DIRECTORY_SEPARATOR.$page->getSlug()->getSlug(),
        ];

        Setting::query()
            ->where('key', KeyEnum::maintenanceModeEnabled->value)
            ->first()
            ->update(['value' => 1]);

        $userAdmin = $this->createUserWithAdminRole();

        foreach ($pagesToTest as $pageUri) {
            $this
                ->actingAs($userAdmin)
                ->get($pageUri)
                ->assertStatus(Response::HTTP_OK)
                ->assertInertia(function (Assert $page) use ($userAdmin) {
                    $this->assertInertiaShareProps($page);

                    $page->has(
                        'auth.user',
                        fn (Assert $page) => $page
                            ->where('id', $userAdmin->getId())
                            ->where('name', $userAdmin->getName())
                            ->where('isAdmin', true)
                    );
                    $page->where('maintenanceModeEnabled', true);
                });
        }

        $this
            ->actingAs($userAdmin)
            ->get(DIRECTORY_SEPARATOR.'not-exist-page')
            ->assertStatus(Response::HTTP_NOT_FOUND)
            ->assertInertia(function (Assert $page) use ($userAdmin) {
                $this->assertInertiaShareProps($page);

                $page->has(
                    'auth.user',
                    fn (Assert $page) => $page
                        ->where('id', $userAdmin->getId())
                        ->where('name', $userAdmin->getName())
                        ->where('isAdmin', true)
                );
                $page->where('maintenanceModeEnabled', true);
            });
    }
}
