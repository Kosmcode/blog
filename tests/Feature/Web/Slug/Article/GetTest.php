<?php

namespace Tests\Feature\Web\Slug\Article;

use App\Enums\Article\FeatureEnum;
use App\Enums\Article\StatusEnum;
use App\Enums\Intertia\ComponentEnum;
use App\Models\Article;
use Inertia\Testing\AssertableInertia as Assert;
use Symfony\Component\HttpFoundation\Response;
use Tests\Feature\TestCase;
use Tests\Feature\Trait\ArticleTrait;

class GetTest extends TestCase
{
    use ArticleTrait;

    public function testGetArticleWhenNotExists(): void
    {
        $this->get('/not-exists-article')
            ->assertInertia(function (Assert $page) {
                $page->component(ComponentEnum::error->value);
                $this->assertInertiaShareProps($page);
            })
            ->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testGetArticleWhenIsNotPublished(): void
    {
        /** @var Article $article */
        $article = $this->createArticles(
            StatusEnum::draft,
            FeatureEnum::feature
        )->first();

        $this->get('/'.$article->first()->getSlug()->getSlug())
            ->assertInertia(function (Assert $page) {
                $page->component(ComponentEnum::error->value);
                $this->assertInertiaShareProps($page);
            })
            ->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testGetArticleWhenIsPublished(): void
    {
        /** @var Article $article */
        $article = $this->createArticles(
            StatusEnum::published,
            FeatureEnum::feature,
            4
        )->first();

        $this->get('/'.$article->first()->getSlug()->getSlug())
            ->assertInertia(function (Assert $page) {
                $page->component(ComponentEnum::singleArticle->value);
                $this->assertInertiaShareProps($page);
                $this->assertInertiaShareProps($page);
                $this->assertArticlesResent($page, 'recentArticles.data', 4);
                $page->has(
                    'article',
                    fn (Assert $page) => $page
                        ->has('title')
                        ->has('content')
                        ->has('image')
                        ->has('date')
                        ->has(
                            'category',
                            fn (Assert $page) => $page
                                ->has('name')
                                ->has('slug')
                        )
                        ->has(
                            'tags',
                            3,
                            function (Assert $page) {
                                $page
                                    ->has('name')
                                    ->has('slug');
                            }
                        )
                );
            })
            ->assertStatus(Response::HTTP_OK);
    }
}
