<?php

namespace Tests\Feature\Web\Slug\Page;

use App\Enums\Intertia\ComponentEnum;
use App\Models\Page;
use Inertia\Testing\AssertableInertia as Assert;
use Symfony\Component\HttpFoundation\Response;
use Tests\Feature\TestCase;

class GetTest extends TestCase
{
    public function testGetPageWhenNotExists(): void
    {
        $this->get('/not-exists-page')
            ->assertInertia(function (Assert $page) {
                $page->component(ComponentEnum::error->value);
                $this->assertInertiaShareProps($page);
            })
            ->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testGetPageWhenExistsButIsNotPublished(): void
    {
        /** @var Page $pageModel */
        $pageModel = Page::factory()
            ->count(1)
            ->create([
                'name' => 'Test',
                'title' => 'Test',
                'content' => 'Content page test',
                'published' => false,
            ])
            ->first();

        $this->get('/'.$pageModel->getSlug()->getSlug())
            ->assertInertia(function (Assert $page) {
                $page->component(ComponentEnum::error->value);
                $this->assertInertiaShareProps($page);
            })
            ->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testGetPageWhenExists(): void
    {
        /** @var Page $pageModel */
        $pageModel = Page::factory()
            ->create(['published' => true])
            ->first();

        $this->get('/'.$pageModel->getSlug()->getSlug())
            ->assertInertia(function (Assert $page) use ($pageModel) {
                $page->component(ComponentEnum::page->value);
                $this->assertInertiaShareProps($page);
                $this->assertInertiaAppHeadProps(
                    $page,
                    $pageModel->getSlug()->getMetaTitle(),
                    $pageModel->getSlug()->getMetaDescription(),
                    $pageModel->getSlug()->getMetaKeywords(),
                    env('APP_URL').$pageModel->getSlug()->getSlug()
                );
                $page->has('content');
            })
            ->assertStatus(Response::HTTP_OK);
    }
}
