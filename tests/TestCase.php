<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    const ARTICLE_IMAGE_TEST_FILENAME = 'test.jpg';

    use CreatesApplication;
}
