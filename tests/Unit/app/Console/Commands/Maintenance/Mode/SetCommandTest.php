<?php

namespace Tests\Unit\app\Console\Commands\Maintenance\Mode;

use App\Console\Commands\Maintenance\Mode\SetCommand;
use App\Enums\Maintenance\ModeEnum;
use App\Services\Maintenance\ModeServiceInterface;
use Illuminate\Console\Command;
use Mockery\MockInterface;
use Tests\Unit\TestCase;

class SetCommandTest extends TestCase
{
    private MockInterface $maintenanceModeServiceMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->maintenanceModeServiceMock = $this->mock(ModeServiceInterface::class);
    }

    public function testHandleWhenNotPutModeArgumentAndSelectDisable(): void
    {
        $this->maintenanceModeServiceMock
            ->shouldReceive('setMaintenanceMode')
            ->once()
            ->with((bool) ModeEnum::disable->value);

        $this
            ->artisan(SetCommand::COMMAND_NAME)
            ->expectsChoice(
                'Which mode do you want to set?',
                ModeEnum::disable->name,
                ModeEnum::toArray()
            )
            ->assertExitCode(Command::SUCCESS);
    }

    public function testHandleWhenNotPutModeArgumentAndSelectEnable(): void
    {
        $this->maintenanceModeServiceMock
            ->shouldReceive('setMaintenanceMode')
            ->once()
            ->with((bool) ModeEnum::enable->value);

        $this
            ->artisan(SetCommand::COMMAND_NAME)
            ->expectsChoice(
                'Which mode do you want to set?',
                [ModeEnum::enable->name],
                ModeEnum::toArray()
            )
            ->assertExitCode(Command::SUCCESS);
    }

    public function testHandleWhenPutEnableModeArgument(): void
    {
        $this->maintenanceModeServiceMock
            ->shouldReceive('setMaintenanceMode')
            ->once()
            ->with((bool) ModeEnum::enable->value);

        $this
            ->artisan(
                SetCommand::COMMAND_NAME,
                [SetCommand::ARGUMENT_MODE_NAME => ModeEnum::enable->name]
            )
            ->assertExitCode(Command::SUCCESS);
    }

    public function testHandleWhenPutDisableModeArgument(): void
    {
        $this->maintenanceModeServiceMock
            ->shouldReceive('setMaintenanceMode')
            ->once()
            ->with((bool) ModeEnum::disable->value);

        $this
            ->artisan(
                SetCommand::COMMAND_NAME,
                [SetCommand::ARGUMENT_MODE_NAME => ModeEnum::disable->name]
            )
            ->assertExitCode(Command::SUCCESS);
    }

    public function testHandleWhenPutWrongModeArgument(): void
    {
        $this
            ->artisan(
                SetCommand::COMMAND_NAME,
                [SetCommand::ARGUMENT_MODE_NAME => 'wrongValue']
            )
            ->assertExitCode(Command::FAILURE);
    }
}
