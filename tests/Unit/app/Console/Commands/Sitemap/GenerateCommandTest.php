<?php

namespace Tests\Unit\app\Console\Commands\Sitemap;

use App\Console\Commands\Sitemap\GenerateCommand;
use App\Services\Sitemap\GenerateServiceInterface;
use Illuminate\Console\Command;
use Mockery\MockInterface;
use Tests\Unit\TestCase;

class GenerateCommandTest extends TestCase
{
    private MockInterface $sitemapGenerateService;

    public function setUp(): void
    {
        parent::setUp();

        $this->sitemapGenerateService = $this->mock(GenerateServiceInterface::class);
    }

    public function testHandleWhenFail()
    {
        $this->sitemapGenerateService
            ->shouldReceive('generateSimpleTextSitemap')
            ->withNoArgs()
            ->once()
            ->andReturnFalse();

        $this->artisan(GenerateCommand::COMMAND_SIGNATURE)
            ->assertExitCode(Command::FAILURE);
    }

    public function testHandleWhenSuccess()
    {
        $this->sitemapGenerateService
            ->shouldReceive('generateSimpleTextSitemap')
            ->withNoArgs()
            ->once()
            ->andReturnTrue();

        $this->artisan(GenerateCommand::COMMAND_SIGNATURE)
            ->assertExitCode(Command::SUCCESS);
    }
}
