<?php

namespace Tests\Unit\app\Console\Commands\User\Admin;

use App\Console\Commands\User\Admin\CreateCommand;
use App\Services\LocalizationService;
use App\Services\User\Admin\CreateCommandService;
use App\Services\User\Admin\CreateCommandServiceInterface;
use Illuminate\Console\Command;
use Mockery\MockInterface;
use Tests\Unit\TestCase;

class CreateCommandTest extends TestCase
{
    private MockInterface $userCreateAdminServiceMock;

    private MockInterface $localizationServiceMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->userCreateAdminServiceMock = $this->mock(CreateCommandServiceInterface::class);
        $this->localizationServiceMock = $this->mock(LocalizationService::class);

        $this->localizationServiceMock
            ->shouldReceive('setLangPattern')
            ->once()
            ->with(CreateCommandService::LANG_KEY_PATTERN);
        $this->localizationServiceMock
            ->shouldReceive('get')
            ->once()
            ->with('info.start')
            ->andReturn('info.start');
    }

    public function testHandleWhenAskEmailUserNotPutString(): void
    {
        $this->localizationServiceMock
            ->shouldReceive('get')
            ->once()
            ->with('asks.email')
            ->andReturn('asks.email');

        $this
            ->artisan(CreateCommand::COMMAND_NAME)
            ->expectsQuestion('asks.email', 0)
            ->assertExitCode(Command::FAILURE);
    }

    public function testHandleWhenAskEmailUserPutExit(): void
    {
        $this->localizationServiceMock
            ->shouldReceive('get')
            ->once()
            ->with('asks.email')
            ->andReturn('asks.email');

        $this
            ->artisan(CreateCommand::COMMAND_NAME)
            ->expectsQuestion('asks.email', 'exit')
            ->assertExitCode(Command::FAILURE);
    }

    public function testHandleWhenAskEmailUserPutIncorrectEmail(): void
    {
        $this->localizationServiceMock
            ->shouldReceive('get')
            ->twice()
            ->with('asks.email')
            ->andReturn('asks.email');

        $this->userCreateAdminServiceMock
            ->shouldReceive('isCorrectUserEmail')
            ->once()
            ->with('notCorrectEmail')
            ->andReturnFalse();

        $this->userCreateAdminServiceMock
            ->shouldReceive('getNotification')
            ->once()
            ->andReturn('error notification');

        $this
            ->artisan(CreateCommand::COMMAND_NAME)
            ->expectsQuestion('asks.email', 'notCorrectEmail')
            ->expectsQuestion('asks.email', 'exit')
            ->assertExitCode(Command::FAILURE);
    }

    public function testHandleWhenAskEmailUserPutCorrectEmailAndUsernamePutNotString(): void
    {
        $this->localizationServiceMock
            ->shouldReceive('get')
            ->once()
            ->with('asks.email')
            ->andReturn('asks.email');

        $this->userCreateAdminServiceMock
            ->shouldReceive('isCorrectUserEmail')
            ->once()
            ->with('correct@email.com')
            ->andReturnTrue();

        $this->localizationServiceMock
            ->shouldReceive('get')
            ->once()
            ->with('asks.username')
            ->andReturn('asks.username');

        $this
            ->artisan(CreateCommand::COMMAND_NAME)
            ->expectsQuestion('asks.email', 'correct@email.com')
            ->expectsQuestion('asks.username', 0)
            ->assertExitCode(Command::FAILURE);
    }

    public function testHandleWhenAskEmailUserPutCorrectEmailAndUsernamePutExit(): void
    {
        $this->localizationServiceMock
            ->shouldReceive('get')
            ->once()
            ->with('asks.email')
            ->andReturn('asks.email');

        $this->userCreateAdminServiceMock
            ->shouldReceive('isCorrectUserEmail')
            ->once()
            ->with('correct@email.com')
            ->andReturnTrue();

        $this->localizationServiceMock
            ->shouldReceive('get')
            ->once()
            ->with('asks.username')
            ->andReturn('asks.username');

        $this
            ->artisan(CreateCommand::COMMAND_NAME)
            ->expectsQuestion('asks.email', 'correct@email.com')
            ->expectsQuestion('asks.username', 'exit')
            ->assertExitCode(Command::FAILURE);
    }

    public function testHandleWhenAskEmailUserPutCorrectEmailAndPutIncorrectUsername(): void
    {
        $this->localizationServiceMock
            ->shouldReceive('get')
            ->once()
            ->with('asks.email')
            ->andReturn('asks.email');

        $this->userCreateAdminServiceMock
            ->shouldReceive('isCorrectUserEmail')
            ->once()
            ->with('correct@email.com')
            ->andReturnTrue();

        $this->localizationServiceMock
            ->shouldReceive('get')
            ->twice()
            ->with('asks.username')
            ->andReturn('asks.username');

        $this->userCreateAdminServiceMock
            ->shouldReceive('isCorrectUsername')
            ->once()
            ->with('incorrectUsername')
            ->andReturnFalse();

        $this->userCreateAdminServiceMock
            ->shouldReceive('getNotification')
            ->once()
            ->andReturn('error notification');

        $this
            ->artisan(CreateCommand::COMMAND_NAME)
            ->expectsQuestion('asks.email', 'correct@email.com')
            ->expectsQuestion('asks.username', 'incorrectUsername')
            ->expectsQuestion('asks.username', 'exit')
            ->assertExitCode(Command::FAILURE);
    }

    public function testHandleWhenAskEmailUserPutCorrectEmailUsernameAndPasswordPutNonString(): void
    {
        $this->localizationServiceMock
            ->shouldReceive('get')
            ->once()
            ->with('asks.email')
            ->andReturn('asks.email');

        $this->userCreateAdminServiceMock
            ->shouldReceive('isCorrectUserEmail')
            ->once()
            ->with('correct@email.com')
            ->andReturnTrue();

        $this->localizationServiceMock
            ->shouldReceive('get')
            ->once()
            ->with('asks.username')
            ->andReturn('asks.username');

        $this->userCreateAdminServiceMock
            ->shouldReceive('isCorrectUsername')
            ->once()
            ->with('correctUsername')
            ->andReturnTrue();

        $this->localizationServiceMock
            ->shouldReceive('get')
            ->once()
            ->with('asks.password')
            ->andReturn('asks.password');

        $this
            ->artisan(CreateCommand::COMMAND_NAME)
            ->expectsQuestion('asks.email', 'correct@email.com')
            ->expectsQuestion('asks.username', 'correctUsername')
            ->expectsQuestion('asks.password', 0)
            ->assertExitCode(Command::FAILURE);
    }

    public function testHandleWhenAskEmailUserPutCorrectEmailUsernameAndPasswordPutExit(): void
    {
        $this->localizationServiceMock
            ->shouldReceive('get')
            ->once()
            ->with('asks.email')
            ->andReturn('asks.email');

        $this->userCreateAdminServiceMock
            ->shouldReceive('isCorrectUserEmail')
            ->once()
            ->with('correct@email.com')
            ->andReturnTrue();

        $this->localizationServiceMock
            ->shouldReceive('get')
            ->once()
            ->with('asks.username')
            ->andReturn('asks.username');

        $this->userCreateAdminServiceMock
            ->shouldReceive('isCorrectUsername')
            ->once()
            ->with('correctUsername')
            ->andReturnTrue();

        $this->localizationServiceMock
            ->shouldReceive('get')
            ->once()
            ->with('asks.password')
            ->andReturn('asks.password');

        $this
            ->artisan(CreateCommand::COMMAND_NAME)
            ->expectsQuestion('asks.email', 'correct@email.com')
            ->expectsQuestion('asks.username', 'correctUsername')
            ->expectsQuestion('asks.password', 'exit')
            ->assertExitCode(Command::FAILURE);
    }

    public function testHandleWhenAskEmailUserPutCorrectEmailUsernamePasswordAndRepeatPutNotString(): void
    {
        $this->localizationServiceMock
            ->shouldReceive('get')
            ->once()
            ->with('asks.email')
            ->andReturn('asks.email');

        $this->userCreateAdminServiceMock
            ->shouldReceive('isCorrectUserEmail')
            ->once()
            ->with('correct@email.com')
            ->andReturnTrue();

        $this->localizationServiceMock
            ->shouldReceive('get')
            ->once()
            ->with('asks.username')
            ->andReturn('asks.username');

        $this->userCreateAdminServiceMock
            ->shouldReceive('isCorrectUsername')
            ->once()
            ->with('correctUsername')
            ->andReturnTrue();

        $this->localizationServiceMock
            ->shouldReceive('get')
            ->once()
            ->with('asks.password')
            ->andReturn('asks.password');

        $this->localizationServiceMock
            ->shouldReceive('get')
            ->once()
            ->with('asks.passwordConfirmation')
            ->andReturn('asks.passwordConfirmation');

        $this
            ->artisan(CreateCommand::COMMAND_NAME)
            ->expectsQuestion('asks.email', 'correct@email.com')
            ->expectsQuestion('asks.username', 'correctUsername')
            ->expectsQuestion('asks.password', 'password')
            ->expectsQuestion('asks.passwordConfirmation', 0)
            ->assertExitCode(Command::FAILURE);
    }

    public function testHandleWhenAskEmailUserPutCorrectEmailUsernamePasswordAndRepeatPutExit(): void
    {
        $this->localizationServiceMock
            ->shouldReceive('get')
            ->once()
            ->with('asks.email')
            ->andReturn('asks.email');

        $this->userCreateAdminServiceMock
            ->shouldReceive('isCorrectUserEmail')
            ->once()
            ->with('correct@email.com')
            ->andReturnTrue();

        $this->localizationServiceMock
            ->shouldReceive('get')
            ->once()
            ->with('asks.username')
            ->andReturn('asks.username');

        $this->userCreateAdminServiceMock
            ->shouldReceive('isCorrectUsername')
            ->once()
            ->with('correctUsername')
            ->andReturnTrue();

        $this->localizationServiceMock
            ->shouldReceive('get')
            ->once()
            ->with('asks.password')
            ->andReturn('asks.password');

        $this->localizationServiceMock
            ->shouldReceive('get')
            ->once()
            ->with('asks.passwordConfirmation')
            ->andReturn('asks.passwordConfirmation');

        $this
            ->artisan(CreateCommand::COMMAND_NAME)
            ->expectsQuestion('asks.email', 'correct@email.com')
            ->expectsQuestion('asks.username', 'correctUsername')
            ->expectsQuestion('asks.password', 'password')
            ->expectsQuestion('asks.passwordConfirmation', 'exit')
            ->assertExitCode(Command::FAILURE);
    }

    public function testHandleWhenAskEmailUserPutCorrectEmailUsernamePasswordsIsNotCorrect(): void
    {
        $this->localizationServiceMock
            ->shouldReceive('get')
            ->once()
            ->with('asks.email')
            ->andReturn('asks.email');

        $this->userCreateAdminServiceMock
            ->shouldReceive('isCorrectUserEmail')
            ->once()
            ->with('correct@email.com')
            ->andReturnTrue();

        $this->localizationServiceMock
            ->shouldReceive('get')
            ->once()
            ->with('asks.username')
            ->andReturn('asks.username');

        $this->userCreateAdminServiceMock
            ->shouldReceive('isCorrectUsername')
            ->once()
            ->with('correctUsername')
            ->andReturnTrue();

        $this->localizationServiceMock
            ->shouldReceive('get')
            ->twice()
            ->with('asks.password')
            ->andReturn('asks.password');

        $this->localizationServiceMock
            ->shouldReceive('get')
            ->once()
            ->with('asks.passwordConfirmation')
            ->andReturn('asks.passwordConfirmation');

        $this->userCreateAdminServiceMock
            ->shouldReceive('isCorrectUserPassword')
            ->once()
            ->with('password', 'notCorrectPassword')
            ->andReturnFalse();

        $this->userCreateAdminServiceMock
            ->shouldReceive('getNotification')
            ->once()
            ->andReturn('error notification');

        $this
            ->artisan(CreateCommand::COMMAND_NAME)
            ->expectsQuestion('asks.email', 'correct@email.com')
            ->expectsQuestion('asks.username', 'correctUsername')
            ->expectsQuestion('asks.password', 'password')
            ->expectsQuestion('asks.passwordConfirmation', 'notCorrectPassword')
            ->expectsQuestion('asks.password', 'exit')
            ->assertExitCode(Command::FAILURE);
    }

    public function testHandleWhenUserPutCorrectDataAndNotCreatedNewUserAdmin(): void
    {
        $this->localizationServiceMock
            ->shouldReceive('get')
            ->once()
            ->with('asks.email')
            ->andReturn('asks.email');

        $this->userCreateAdminServiceMock
            ->shouldReceive('isCorrectUserEmail')
            ->once()
            ->with('correct@email.com')
            ->andReturnTrue();

        $this->localizationServiceMock
            ->shouldReceive('get')
            ->once()
            ->with('asks.username')
            ->andReturn('asks.username');

        $this->userCreateAdminServiceMock
            ->shouldReceive('isCorrectUsername')
            ->once()
            ->with('correctUsername')
            ->andReturnTrue();

        $this->localizationServiceMock
            ->shouldReceive('get')
            ->once()
            ->with('asks.password')
            ->andReturn('asks.password');

        $this->localizationServiceMock
            ->shouldReceive('get')
            ->once()
            ->with('asks.passwordConfirmation')
            ->andReturn('asks.passwordConfirmation');

        $this->userCreateAdminServiceMock
            ->shouldReceive('isCorrectUserPassword')
            ->once()
            ->with('password', 'password')
            ->andReturnTrue();

        $this->userCreateAdminServiceMock
            ->shouldReceive('createUserWithAdminRole')
            ->with(
                'correct@email.com',
                'correctUsername',
                'password'
            )
            ->andReturnFalse();

        $this->userCreateAdminServiceMock
            ->shouldReceive('getNotification')
            ->once()
            ->andReturn('error notification');

        $this
            ->artisan(CreateCommand::COMMAND_NAME)
            ->expectsQuestion('asks.email', 'correct@email.com')
            ->expectsQuestion('asks.username', 'correctUsername')
            ->expectsQuestion('asks.password', 'password')
            ->expectsQuestion('asks.passwordConfirmation', 'password')
            ->assertExitCode(Command::FAILURE);
    }

    public function testHandleWhenUserPutCorrectDataAndCreatedNewUserAdmin(): void
    {
        $this->localizationServiceMock
            ->shouldReceive('get')
            ->once()
            ->with('asks.email')
            ->andReturn('asks.email');

        $this->userCreateAdminServiceMock
            ->shouldReceive('isCorrectUserEmail')
            ->once()
            ->with('correct@email.com')
            ->andReturnTrue();

        $this->localizationServiceMock
            ->shouldReceive('get')
            ->once()
            ->with('asks.username')
            ->andReturn('asks.username');

        $this->userCreateAdminServiceMock
            ->shouldReceive('isCorrectUsername')
            ->once()
            ->with('correctUsername')
            ->andReturnTrue();

        $this->localizationServiceMock
            ->shouldReceive('get')
            ->once()
            ->with('asks.password')
            ->andReturn('asks.password');

        $this->localizationServiceMock
            ->shouldReceive('get')
            ->once()
            ->with('asks.passwordConfirmation')
            ->andReturn('asks.passwordConfirmation');

        $this->userCreateAdminServiceMock
            ->shouldReceive('isCorrectUserPassword')
            ->once()
            ->with('password', 'password')
            ->andReturnTrue();

        $this->userCreateAdminServiceMock
            ->shouldReceive('createUserWithAdminRole')
            ->with(
                'correct@email.com',
                'correctUsername',
                'password'
            )
            ->andReturnTrue();

        $this->userCreateAdminServiceMock
            ->shouldReceive('getNotification')
            ->once()
            ->andReturn('success notification');

        $this
            ->artisan(CreateCommand::COMMAND_NAME)
            ->expectsQuestion('asks.email', 'correct@email.com')
            ->expectsQuestion('asks.username', 'correctUsername')
            ->expectsQuestion('asks.password', 'password')
            ->expectsQuestion('asks.passwordConfirmation', 'password')
            ->assertExitCode(Command::SUCCESS);
    }
}
