<?php

namespace Tests\Unit\app\Rules\Article;

use App\Repositories\ArticleRepositoryInterface;
use App\Rules\Article\IdExistsRule;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use Tests\Unit\TestCase;

class ArticleIdExistsRuleTest extends TestCase
{
    use ProphecyTrait;

    private const EXAMPLE_RULE_ATTRIBUTE = 'attribute';
    private const EXAMPLE_RULE_VALUE = '1';

    private ObjectProphecy $articleRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->articleRepository = $this->prophesize(ArticleRepositoryInterface::class);
    }

    public function testPassesWhenArticleNotExistsById()
    {
        $this->articleRepository
            ->articleByIdExists(Argument::type('Integer'))
            ->willReturn(false);

        $mockedArticleIdExistsRule = $this->getMockedArticleIdExistsRule();

        $this->assertFalse(
            $mockedArticleIdExistsRule->passes(self::EXAMPLE_RULE_ATTRIBUTE, self::EXAMPLE_RULE_VALUE)
        );
    }

    public function testPassesWhenArticleExistsById()
    {
        $this->articleRepository
            ->articleByIdExists(Argument::type('Integer'))
            ->willReturn(true);

        $mockedArticleIdExistsRule = $this->getMockedArticleIdExistsRule();

        $this->assertTrue(
            $mockedArticleIdExistsRule->passes(self::EXAMPLE_RULE_ATTRIBUTE, self::EXAMPLE_RULE_VALUE)
        );
    }

    private function getMockedArticleIdExistsRule(): IdExistsRule
    {
        return new IdExistsRule(
            $this->articleRepository->reveal()
        );
    }
}
