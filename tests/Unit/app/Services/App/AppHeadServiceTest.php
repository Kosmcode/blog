<?php

namespace Tests\Unit\app\Services\App;

use App\Services\App\HeadService;
use Inertia\ResponseFactory as InertiaResponseFactory;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use Tests\Unit\TestCase;

class AppHeadServiceTest extends TestCase
{
    private const EXAMPLE_CANONICAL_URL = 'http://localhost/';

    use ProphecyTrait;

    private ObjectProphecy $inertiaResponseFactoryMock;

    public function setUp(): void
    {
        parent::setUp();

        $this->inertiaResponseFactoryMock = $this->prophesize(InertiaResponseFactory::class);
    }

    public function testSetMetas(): void
    {
        $appHeadServiceMock = $this->getAppHeadServiceMock();

        $this->assertNull($appHeadServiceMock->setMetas(null, null, null));
        $this->assertIsArray($appHeadServiceMock->toArray());
        $this->assertEquals(
            $this->prepareMetasArray(null, null, null),
            $appHeadServiceMock->toArray()
        );

        $this->assertNull($appHeadServiceMock->setMetas('metaTitle', null, null));
        $this->assertIsArray($appHeadServiceMock->toArray());
        $this->assertEquals(
            $this->prepareMetasArray('metaTitle', null, null),
            $appHeadServiceMock->toArray()
        );

        $this->assertNull($appHeadServiceMock->setMetas(null, 'metaDescription', null));
        $this->assertIsArray($appHeadServiceMock->toArray());
        $this->assertEquals(
            $this->prepareMetasArray(null, 'metaDescription', null),
            $appHeadServiceMock->toArray()
        );

        $this->assertNull($appHeadServiceMock->setMetas(null, null, 'metaKeywords'));
        $this->assertIsArray($appHeadServiceMock->toArray());
        $this->assertEquals(
            $this->prepareMetasArray(null, null, 'metaKeywords'),
            $appHeadServiceMock->toArray()
        );

        $this->assertNull($appHeadServiceMock->setMetas('metaTitle', 'metaDescription', 'metaKeywords'));
        $this->assertIsArray($appHeadServiceMock->toArray());
        $this->assertEquals(
            $this->prepareMetasArray('metaTitle', 'metaDescription', 'metaKeywords'),
            $appHeadServiceMock->toArray()
        );
    }

    public function testSetMetaTitle(): void
    {
        $appHeadServiceMock = $this->getAppHeadServiceMock();

        $this->assertNull($appHeadServiceMock->setMetaTitle(null));
        $this->assertIsArray($appHeadServiceMock->toArray());
        $this->assertEquals(
            $this->prepareMetasArray(null, null, null),
            $appHeadServiceMock->toArray()
        );

        $this->assertNull($appHeadServiceMock->setMetaTitle('metaTitle'));
        $this->assertIsArray($appHeadServiceMock->toArray());
        $this->assertEquals(
            $this->prepareMetasArray('metaTitle', null, null),
            $appHeadServiceMock->toArray()
        );
    }

    public function testSetMetaDescription(): void
    {
        $appHeadServiceMock = $this->getAppHeadServiceMock();

        $this->assertNull($appHeadServiceMock->setMetaDescription(null));
        $this->assertIsArray($appHeadServiceMock->toArray());
        $this->assertEquals(
            $this->prepareMetasArray(null, null, null),
            $appHeadServiceMock->toArray()
        );

        $this->assertNull($appHeadServiceMock->setMetaDescription('metaDescription'));
        $this->assertIsArray($appHeadServiceMock->toArray());
        $this->assertEquals(
            $this->prepareMetasArray(null, 'metaDescription', null),
            $appHeadServiceMock->toArray()
        );
    }

    public function testSetMetaKeywords(): void
    {
        $appHeadServiceMock = $this->getAppHeadServiceMock();

        $this->assertNull($appHeadServiceMock->setMetaKeywords(null));
        $this->assertIsArray($appHeadServiceMock->toArray());
        $this->assertEquals(
            $this->prepareMetasArray(null, null, null),
            $appHeadServiceMock->toArray()
        );

        $this->assertNull($appHeadServiceMock->setMetaKeywords('metaKeywords'));
        $this->assertIsArray($appHeadServiceMock->toArray());
        $this->assertEquals(
            $this->prepareMetasArray(null, null, 'metaKeywords'),
            $appHeadServiceMock->toArray()
        );
    }

    public function testToArray(): void
    {
        $appHeadServiceMock = $this->getAppHeadServiceMock();

        $this->assertNull($appHeadServiceMock->setMetas('metaTitle', 'metaDescription', 'metaKeywords'));
        $this->assertIsArray($appHeadServiceMock->toArray());
        $this->assertEquals(
            $this->prepareMetasArray('metaTitle', 'metaDescription', 'metaKeywords'),
            $appHeadServiceMock->toArray()
        );
    }

    private function getAppHeadServiceMock(): HeadService
    {
        $this->inertiaResponseFactoryMock
            ->share(Argument::type('String'), Argument::type('Array'));

        return new HeadService(
            $this->inertiaResponseFactoryMock->reveal(),
        );
    }

    private function prepareMetasArray(
        ?string $metaTitle,
        ?string $metaDescription,
        ?string $metaKeywords
    ): array {
        return [
            'title' => $metaTitle,
            'description' => $metaDescription,
            'keywords' => $metaKeywords,
            'canonical' => self::EXAMPLE_CANONICAL_URL,
        ];
    }
}
