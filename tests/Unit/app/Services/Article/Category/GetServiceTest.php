<?php

namespace Tests\Unit\app\Services\Article\Category;

use App\Dto\ArticlesFiltersDTO;
use App\Dto\ResponseDTO;
use App\Enums\Article\RequestParameterEnum;
use App\Enums\Intertia\ComponentEnum;
use App\Exceptions\CategoryException;
use App\Models\Category;
use App\Repositories\CategoryRepositoryInterface;
use App\Services\Article\Category\GetService;
use App\Services\Article\Filter\GetServiceInterface;
use App\Services\SettingServiceInterface;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;

class GetServiceTest extends TestCase
{
    private MockObject $settingServiceMock;

    private MockObject $categoryRepositoryMock;

    private MockObject $articleFilterGetServiceMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->settingServiceMock = $this->createMock(SettingServiceInterface::class);
        $this->categoryRepositoryMock = $this->createMock(CategoryRepositoryInterface::class);
        $this->articleFilterGetServiceMock = $this->createMock(GetServiceInterface::class);
    }

    public function testGetArticleByCategorySlugWhenNotFoundCategoryBySlug(): void
    {
        $this->categoryRepositoryMock
            ->expects($this->once())
            ->method('findBySlug')
            ->with('notExistsCategorySlug')
            ->willReturn(null);

        $getServiceMock = $this->getGetServiceMock();

        $this->expectException(CategoryException::class);

        $getServiceMock->getArticleByCategorySlug('notExistsCategorySlug');
    }

    public function testGetArticleByCategorySlugWhenFoundCategory(): void
    {
        $categoryMock = (new Category())
            ->setName('existCategory');

        $this->categoryRepositoryMock
            ->expects($this->once())
            ->method('findBySlug')
            ->with('existCategorySlug')
            ->willReturn($categoryMock);

        $this->articleFilterGetServiceMock
            ->expects($this->once())
            ->method('getArticlesFiltersDTOByParameters')
            ->with([
                RequestParameterEnum::categories->value => ['existCategory'],
            ])
            ->willReturn(new ArticlesFiltersDTO());

        $this->settingServiceMock
            ->method('get')
            ->withAnyParameters()
            ->willReturn('configValue');

        $getServiceMock = $this->getGetServiceMock();
        $result = $getServiceMock->getArticleByCategorySlug('existCategorySlug');

        $this->assertInstanceOf(ResponseDTO::class, $result);
        $this->assertSame(ComponentEnum::articleFilters->value, $result->getInertiaComponentName());
        $this->assertSame('configValue', $result->getMetaTitle());
        $this->assertSame('configValue', $result->getMetaDescription());
        $this->assertSame('configValue', $result->getMetaKeywords());
    }

    private function getGetServiceMock(): GetService
    {
        return new GetService(
            $this->settingServiceMock,
            $this->categoryRepositoryMock,
            $this->articleFilterGetServiceMock,
        );
    }
}
