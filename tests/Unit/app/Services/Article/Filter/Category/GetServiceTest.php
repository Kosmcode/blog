<?php

namespace Tests\Unit\app\Services\Article\Filter\Category;

use App\Enums\Cache\KeyEnum as CacheKeyEnum;
use App\Repositories\CategoryRepositoryInterface;
use App\Services\Article\Filter\Category\GetService;
use Illuminate\Cache\Repository;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;

class GetServiceTest extends TestCase
{
    private MockObject $laravelCacheMock;

    private MockObject $categoryRepositoryMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->laravelCacheMock = $this->createMock(Repository::class);
        $this->categoryRepositoryMock = $this->createMock(CategoryRepositoryInterface::class);
    }

    public function testGetForFiltersWhenArgumentCachesIsNotSet(): void
    {
        $this->categoryRepositoryMock
            ->expects($this->once())
            ->method('getToArticlesFilters')
            ->willReturn(['categoryToArticlesFilters']);

        $getServiceMock = $this->getGetServiceMock();
        $result = $getServiceMock->getForFilters(false);

        $this->assertIsArray($result);
        $this->assertSame(['categoryToArticlesFilters'], $result);
    }

    public function testGetForFiltersWhenArgumentCachesIsSetAndCacheExists(): void
    {
        $this->laravelCacheMock
            ->expects($this->once())
            ->method('get')
            ->with(CacheKeyEnum::articlesCategories->value)
            ->willReturn(['categoryToArticlesFilters']);

        $getServiceMock = $this->getGetServiceMock();
        $result = $getServiceMock->getForFilters();

        $this->assertIsArray($result);
        $this->assertSame(['categoryToArticlesFilters'], $result);
    }

    public function testGetForFiltersWhenArgumentCachesIsSetAndCacheNotExists(): void
    {
        $this->laravelCacheMock
            ->expects($this->once())
            ->method('get')
            ->with(CacheKeyEnum::articlesCategories->value)
            ->willReturn(null);

        $this->categoryRepositoryMock
            ->expects($this->once())
            ->method('getToArticlesFilters')
            ->willReturn(['categoryToArticlesFilters']);

        $this->laravelCacheMock
            ->expects($this->once())
            ->method('forever')
            ->with(
                CacheKeyEnum::articlesCategories->value,
                ['categoryToArticlesFilters']
            );

        $getServiceMock = $this->getGetServiceMock();
        $result = $getServiceMock->getForFilters();

        $this->assertIsArray($result);
        $this->assertSame(['categoryToArticlesFilters'], $result);
    }

    private function getGetServiceMock(): GetService
    {
        return new GetService(
            $this->laravelCacheMock,
            $this->categoryRepositoryMock,
        );
    }
}
