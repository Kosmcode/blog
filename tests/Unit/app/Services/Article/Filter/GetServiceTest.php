<?php

namespace Tests\Unit\app\Services\Article\Filter;

use App\Dto\ArticlesFiltersDTO;
use App\Enums\Article\RequestParameterEnum;
use App\Enums\Setting\KeyEnum;
use App\Repositories\ArticleRepositoryInterface;
use App\Services\Article\Filter\Category\GetServiceInterface as CategoryGetServiceInterface;
use App\Services\Article\Filter\GetService;
use App\Services\Article\Filter\Tag\GetServiceInterface as TagGetServiceInterface;
use App\Services\Article\Recent\GetServiceInterface;
use App\Services\SettingServiceInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;

class GetServiceTest extends TestCase
{
    private MockObject $articleRepositoryMock;

    private MockObject $categoryServiceMock;

    private MockObject $tagServiceMock;

    private MockObject $settingServiceMock;

    private MockObject $recentArticleServiceMock;

    private MockObject $lengthAwarePaginatorMock;

    /**
     * @throws Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->articleRepositoryMock = $this->createMock(ArticleRepositoryInterface::class);
        $this->categoryServiceMock = $this->createMock(CategoryGetServiceInterface::class);
        $this->tagServiceMock = $this->createMock(TagGetServiceInterface::class);
        $this->settingServiceMock = $this->createMock(SettingServiceInterface::class);
        $this->recentArticleServiceMock = $this->createMock(GetServiceInterface::class);

        $this->lengthAwarePaginatorMock = $this->createMock(LengthAwarePaginator::class);
    }

    public function testGetArticlesFiltersDTOByParameters(): void
    {
        $requestParameters = [
            RequestParameterEnum::categories->value => ['category'],
            RequestParameterEnum::tags->value => ['tag', 'tag2'],
        ];

        $this->articleRepositoryMock
            ->expects($this->once())
            ->method('getArticlesToFilters')
            ->with($requestParameters)
            ->willReturn($this->lengthAwarePaginatorMock);

        $this->categoryServiceMock
            ->expects($this->once())
            ->method('getForFilters')
            ->willReturn([]);

        $this->tagServiceMock
            ->expects($this->once())
            ->method('getForFilters')
            ->willReturn([]);

        $this->recentArticleServiceMock
            ->expects($this->once())
            ->method('getRecentArticles')
            ->willReturn(new Collection());

        $this->settingServiceMock
            ->expects($this->once())
            ->method('get')
            ->with(KeyEnum::articlesPageLimit->value)
            ->willReturn(1);

        $getServiceMock = $this->getGetServiceMock();
        $result = $getServiceMock->getArticlesFiltersDTOByParameters($requestParameters);

        $this->assertInstanceOf(ArticlesFiltersDTO::class, $result);
    }

    private function getGetServiceMock(): GetService
    {
        return new GetService(
            $this->articleRepositoryMock,
            $this->categoryServiceMock,
            $this->tagServiceMock,
            $this->settingServiceMock,
            $this->recentArticleServiceMock,
        );
    }
}
