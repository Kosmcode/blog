<?php

namespace Tests\Unit\app\Services\Article\Filter\Tag;

use App\Enums\Cache\KeyEnum;
use App\Repositories\TagRepositoryInterface;
use App\Services\Article\Filter\Tag\GetService;
use Illuminate\Cache\Repository;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;

class GetServiceTest extends TestCase
{
    private MockObject $laravelCacheMock;

    private MockObject $tagRepositoryMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->laravelCacheMock = $this->createMock(Repository::class);
        $this->tagRepositoryMock = $this->createMock(TagRepositoryInterface::class);
    }

    public function testGetForFiltersWhenNotSetCachedArgument(): void
    {
        $this->tagRepositoryMock
            ->expects($this->once())
            ->method('getForArticleFilters')
            ->willReturn(['articleFiltersTags']);

        $getServiceMock = $this->getGetServiceMock();
        $result = $getServiceMock->getForFilters(false);

        $this->assertIsArray($result);
        $this->assertSame(['articleFiltersTags'], $result);
    }

    public function testGetForFiltersWhenSetCachedArgumentAndCacheExists(): void
    {
        $this->laravelCacheMock
            ->expects($this->once())
            ->method('get')
            ->with(KeyEnum::articlesTags->value)
            ->willReturn(['articleFiltersTags']);

        $getServiceMock = $this->getGetServiceMock();
        $result = $getServiceMock->getForFilters();

        $this->assertIsArray($result);
        $this->assertSame(['articleFiltersTags'], $result);
    }

    public function testGetForFiltersWhenSetCachedArgumentAndCacheNotExists(): void
    {
        $this->laravelCacheMock
            ->expects($this->once())
            ->method('get')
            ->with(KeyEnum::articlesTags->value)
            ->willReturn(null);

        $this->tagRepositoryMock
            ->expects($this->once())
            ->method('getForArticleFilters')
            ->willReturn(['articleFiltersTags']);

        $this->laravelCacheMock
            ->expects($this->once())
            ->method('forever')
            ->with(
                KeyEnum::articlesTags->value,
                ['articleFiltersTags']
            );

        $getServiceMock = $this->getGetServiceMock();
        $result = $getServiceMock->getForFilters();

        $this->assertIsArray($result);
        $this->assertSame(['articleFiltersTags'], $result);
    }

    private function getGetServiceMock(): GetService
    {
        return new GetService(
            $this->laravelCacheMock,
            $this->tagRepositoryMock,
        );
    }
}
