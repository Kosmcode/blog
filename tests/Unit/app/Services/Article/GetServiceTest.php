<?php

namespace Tests\Unit\app\Services\Article;

use App\Dto\ArticlesFiltersDTO;
use App\Dto\ResponseDTO;
use App\Models\Article;
use App\Models\Slug;
use App\Repositories\ArticleRepositoryInterface;
use App\Services\Article\Filter\GetServiceInterface;
use App\Services\Article\GetService;
use App\Services\SettingServiceInterface;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;

class GetServiceTest extends TestCase
{
    private MockObject $articleFilterGetServiceMock;

    private MockObject $settingServiceMock;

    private MockObject $articleRepositoryMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->articleFilterGetServiceMock = $this->createMock(GetServiceInterface::class);
        $this->settingServiceMock = $this->createMock(SettingServiceInterface::class);
        $this->articleRepositoryMock = $this->createMock(ArticleRepositoryInterface::class);
    }

    public function testGetArticlesToWeb(): void
    {
        $this->settingServiceMock
            ->method('get')
            ->withAnyParameters()
            ->willReturn('settingValue');

        $this->articleFilterGetServiceMock
            ->expects($this->once())
            ->method('getArticlesFiltersDTOByParameters')
            ->with([])
            ->willReturn(new ArticlesFiltersDTO());

        $getServiceMock = $this->getGetServiceMock();
        $result = $getServiceMock->getArticlesToWeb();

        $this->assertInstanceOf(ResponseDTO::class, $result);
    }

    public function testGetArticleToAdminPreviewByArticleIdWhenArticleNotExists(): void
    {
        $this->articleRepositoryMock
            ->expects($this->once())
            ->method('getById')
            ->with(1)
            ->willReturn(null);

        $getServiceMock = $this->getGetServiceMock();

        $this->assertNull(
            $getServiceMock->getArticleToAdminPreviewByArticleId(1)
        );
    }

    public function testGetArticleToAdminPreviewByArticleIdWhenArticleNotHaveSlug(): void
    {
        $articleMock = (new Article())
            ->setSlug(null);

        $this->articleRepositoryMock
            ->expects($this->once())
            ->method('getById')
            ->with(1)
            ->willReturn($articleMock);

        $getServiceMock = $this->getGetServiceMock(1);

        $this->assertNull(
            $getServiceMock->getArticleToAdminPreviewByArticleId(1)
        );
    }

    public function testGetArticleToAdminPreviewByArticleId(): void
    {
        $articleMock = (new Article())
            ->setSlug(new Slug());

        $this->articleRepositoryMock
            ->expects($this->once())
            ->method('getById')
            ->with(1)
            ->willReturn($articleMock);

        $getServiceMock = $this->getGetServiceMock();
        $result = $getServiceMock->getArticleToAdminPreviewByArticleId(1);

        $this->assertInstanceOf(ResponseDTO::class, $result);
    }

    private function getGetServiceMock(): GetService
    {
        return new GetService(
            $this->articleFilterGetServiceMock,
            $this->settingServiceMock,
            $this->articleRepositoryMock,
        );
    }
}
