<?php

namespace Tests\Unit\app\Services\Article\Image;

use App\Enums\FilesystemDiskEnum;
use App\Models\Article;
use App\Models\ArticleImageThumbnail;
use App\Repositories\ArticleImageThumbnailRepositoryInterface;
use App\Services\Article\Image\DeleteService;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Filesystem\FilesystemManager;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;

class DeleteServiceTest extends TestCase
{
    private MockObject $filesystemManagerMock;

    private MockObject $articleImageThumbnailRepositoryMock;

    private MockObject $filesystemMock;

    private MockObject $articleMock;

    private MockObject $articleImageThumbnail;

    protected function setUp(): void
    {
        parent::setUp();

        $this->filesystemManagerMock = $this->createMock(FilesystemManager::class);
        $this->articleImageThumbnailRepositoryMock = $this->createMock(ArticleImageThumbnailRepositoryInterface::class);

        $this->filesystemMock = $this->createMock(Filesystem::class);
        $this->articleMock = $this->createMock(Article::class);
        $this->articleImageThumbnail = $this->createMock(ArticleImageThumbnail::class);
    }

    public function testDeleteAllImagesOfArticle(): void
    {
        $this->filesystemManagerMock
            ->expects($this->once())
            ->method('disk')
            ->with(FilesystemDiskEnum::articleImages->value)
            ->willReturn($this->filesystemMock);

        $this->articleMock
            ->expects($this->exactly(2))
            ->method('getImage')
            ->willReturn('imageFilename');

        $this->filesystemMock
            ->expects($this->exactly(2))
            ->method('delete')
            ->withAnyParameters()
            ->willReturn(true);

        $this->articleMock
            ->expects($this->once())
            ->method('getImageThumbnails')
            ->willReturn(new Collection([$this->articleImageThumbnail]));

        $this->articleImageThumbnail
            ->expects($this->once())
            ->method('getFilename')
            ->willReturn('imageThumbnail');

        $this->articleImageThumbnailRepositoryMock
            ->expects($this->once())
            ->method('delete')
            ->with($this->articleImageThumbnail)
            ->willReturn(true);

        $deleteServiceMock = $this->getDeleteServiceMock();
        $deleteServiceMock->deleteAllImagesOfArticle($this->articleMock);
    }

    private function getDeleteServiceMock(): DeleteService
    {
        return new DeleteService(
            $this->filesystemManagerMock,
            $this->articleImageThumbnailRepositoryMock,
        );
    }
}
