<?php

namespace Tests\Unit\app\Services\Article\Image;

use App\Enums\Article\ImageThumbnailEnum;
use App\Enums\FilesystemDiskEnum;
use App\Exceptions\ArticleException;
use App\Models\Article;
use App\Models\ArticleImageThumbnail;
use App\Repositories\ArticleImageThumbnailRepositoryInterface;
use App\Repositories\ArticleRepositoryInterface;
use App\Services\Article\Image\ThumbnailService;
use Illuminate\Cache\Repository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Filesystem\FilesystemManager;
use Intervention\Image\Image;
use Intervention\Image\ImageManager;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\Unit\TestCase;

class ThumbnailServiceTest extends TestCase
{
    private MockObject $articleRepositoryMock;

    private MockObject $filesystemManagerMock;

    private MockObject $imageManagerMock;

    private MockObject $articleImageThumbnailRepositoryMock;

    private MockObject $laravelCacheMock;

    private MockObject $articleMock;

    private MockObject $filesystemMock;

    private MockObject $imageMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->articleRepositoryMock = $this->createMock(ArticleRepositoryInterface::class);
        $this->filesystemManagerMock = $this->createMock(FilesystemManager::class);
        $this->imageManagerMock = $this->createMock(ImageManager::class);
        $this->articleImageThumbnailRepositoryMock = $this->createMock(ArticleImageThumbnailRepositoryInterface::class);
        $this->laravelCacheMock = $this->createMock(Repository::class);

        $this->articleMock = $this->createMock(Article::class);
        $this->filesystemMock = $this->createMock(FilesystemAdapter::class);
        $this->imageMock = $this->getMockBuilder(Image::class)
            ->addMethods(['resize'])
            ->onlyMethods(['save'])
            ->getMock();
    }

    public function testCreateOrUpdateThumbnailsByArticleIdWhenArticleNotExists(): void
    {
        $this->articleRepositoryMock
            ->expects($this->once())
            ->method('getById')
            ->with(1)
            ->willReturn(null);

        $thumbnailServiceMock = $this->getThumbnailServiceMock();

        $this->expectException(ArticleException::class);

        $thumbnailServiceMock->createOrUpdateThumbnailsByArticleId(1);
    }

    public function testCreateOrUpdateThumbnailsByArticleIdWhenArticleNotHasSetImage(): void
    {
        $this->articleMock
            ->expects($this->once())
            ->method('getImage')
            ->willReturn(null);

        $this->articleRepositoryMock
            ->expects($this->once())
            ->method('getById')
            ->with(1)
            ->willReturn($this->articleMock);

        $thumbnailServiceMock = $this->getThumbnailServiceMock();

        $this->assertNull(
            $thumbnailServiceMock->createOrUpdateThumbnailsByArticleId(1)
        );
    }

    public function testCreateOrUpdateThumbnailsByArticleIdWhenArticleHasSetImage(): void
    {
        $this->articleMock
            ->method('getImage')
            ->willReturn('articleImageFilename');

        $this->articleMock
            ->method('getId')
            ->willReturn(1);

        $this->articleMock
            ->method('getTitle')
            ->willReturn('articleTitle');

        $this->articleMock
            ->expects($this->once())
            ->method('getImageThumbnails')
            ->willReturn(new Collection([
                (new ArticleImageThumbnail())
                    ->setWidth(ImageThumbnailEnum::desktopHomeRecent->getWidth())
                    ->setFilename('diffFilename'),
                (new ArticleImageThumbnail())
                    ->setWidth(ImageThumbnailEnum::desktopArticleRecent->getWidth())
                    ->setFilename('articletitle_306.webp'),
            ]));

        $this->articleRepositoryMock
            ->expects($this->once())
            ->method('getById')
            ->with(1)
            ->willReturn($this->articleMock);

        $this->imageManagerMock
            ->expects($this->once())
            ->method('configure')
            ->with(['driver' => 'imagick'])
            ->willReturnSelf();

        $this->imageMock
            ->method('resize')
            ->withAnyParameters()
            ->willReturn($this->imageMock);

        $this->imageMock
            ->method('save')
            ->withAnyParameters()
            ->willReturn($this->imageMock);

        $this->imageManagerMock
            ->method('make')
            ->with('filesystemPath')
            ->willReturn($this->imageMock);

        $this->filesystemMock
            ->method('path')
            ->withAnyParameters()
            ->willReturn('filesystemPath');

        $this->filesystemMock
            ->method('delete')
            ->withAnyParameters()
            ->willReturn(true);

        $this->filesystemManagerMock
            ->expects($this->once())
            ->method('disk')
            ->with(FilesystemDiskEnum::articleImages->value)
            ->willReturn($this->filesystemMock);

        $this->articleImageThumbnailRepositoryMock
            ->method('new')
            ->willReturn(new ArticleImageThumbnail());

        $this->articleImageThumbnailRepositoryMock
            ->method('save')
            ->withAnyParameters()
            ->willReturn(true);

        $this->laravelCacheMock
            ->method('forget')
            ->withAnyParameters()
            ->willReturn(true);

        $thumbnailServiceMock = $this->getThumbnailServiceMock();

        $this->assertNull(
            $thumbnailServiceMock->createOrUpdateThumbnailsByArticleId(1)
        );
    }

    private function getThumbnailServiceMock(): ThumbnailService
    {
        return new ThumbnailService(
            $this->articleRepositoryMock,
            $this->filesystemManagerMock,
            $this->imageManagerMock,
            $this->articleImageThumbnailRepositoryMock,
            $this->laravelCacheMock,
        );
    }
}
