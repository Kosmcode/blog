<?php

namespace Tests\Unit\app\Services\Article\Recent;

use App\Enums\Cache\KeyEnum;
use App\Repositories\ArticleRepositoryInterface;
use App\Services\Article\Recent\GetService;
use Illuminate\Cache\Repository;
use Illuminate\Support\Collection;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;

class GetServiceTest extends TestCase
{
    private MockObject $articleRepositoryMock;

    private MockObject $laravelCacheMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->articleRepositoryMock = $this->createMock(ArticleRepositoryInterface::class);
        $this->laravelCacheMock = $this->createMock(Repository::class);
    }

    public function testGetRecentArticlesWhenArgumentCachedIsFalse(): void
    {
        $this->articleRepositoryMock
            ->expects($this->once())
            ->method('getRecentArticles')
            ->willReturn(new Collection());

        $getServiceMock = $this->getGetServiceMock();
        $result = $getServiceMock->getRecentArticles(false);

        $this->assertInstanceOf(Collection::class, $result);
    }

    public function testGetRecentArticlesWhenArgumentCachedIsTrueAndIsCached(): void
    {
        $this->laravelCacheMock
            ->expects($this->once())
            ->method('get')
            ->with(KeyEnum::recentArticles->value)
            ->willReturn(new Collection());

        $getServiceMock = $this->getGetServiceMock();
        $result = $getServiceMock->getRecentArticles();

        $this->assertInstanceOf(Collection::class, $result);
    }

    public function testGetRecentArticlesWhenArgumentCachedIsTrueAndIsNotCached(): void
    {
        $this->laravelCacheMock
            ->expects($this->once())
            ->method('get')
            ->with(KeyEnum::recentArticles->value)
            ->willReturn(null);

        $this->laravelCacheMock
            ->expects($this->once())
            ->method('forever')
            ->withAnyParameters()
            ->willReturn(new Collection());

        $getServiceMock = $this->getGetServiceMock();
        $result = $getServiceMock->getRecentArticles();

        $this->assertInstanceOf(Collection::class, $result);
    }

    private function getGetServiceMock(): GetService
    {
        return new GetService(
            $this->articleRepositoryMock,
            $this->laravelCacheMock,
        );
    }
}
