<?php

namespace Tests\Unit\app\Services\Article\Tag;

use App\Dto\ArticlesFiltersDTO;
use App\Dto\ResponseDTO;
use App\Models\Tag;
use App\Repositories\TagRepositoryInterface;
use App\Services\Article\Filter\GetServiceInterface;
use App\Services\Article\Tag\GetService;
use App\Services\SettingServiceInterface;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Tests\TestCase;

class GetServiceTest extends TestCase
{
    private MockObject $settingServiceMock;

    private MockObject $tagRepositoryMock;

    private MockObject $articleFilterGetServiceMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->settingServiceMock = $this->createMock(SettingServiceInterface::class);
        $this->tagRepositoryMock = $this->createMock(TagRepositoryInterface::class);
        $this->articleFilterGetServiceMock = $this->createMock(GetServiceInterface::class);
    }

    public function testGetArticleByTagSlugWhenNotFoundTag(): void
    {
        $this->tagRepositoryMock
            ->expects($this->once())
            ->method('findBySlug')
            ->with('notExistsArticleTag')
            ->willReturn(null);

        $getServiceMock = $this->getGetServiceMock();

        $this->expectException(HttpException::class);

        $getServiceMock->getArticleByTagSlug('notExistsArticleTag');
    }

    public function testGetArticleByTagSlug(): void
    {
        $tagMock = (new Tag())
            ->setName('tagName');

        $this->tagRepositoryMock
            ->expects($this->once())
            ->method('findBySlug')
            ->with('articleTag')
            ->willReturn($tagMock);

        $this->articleFilterGetServiceMock
            ->expects($this->once())
            ->method('getArticlesFiltersDTOByParameters')
            ->withAnyParameters()
            ->willReturn(new ArticlesFiltersDTO());

        $this->settingServiceMock
            ->method('get')
            ->withAnyParameters()
            ->willReturn('settingValue');

        $getServiceMock = $this->getGetServiceMock();
        $result = $getServiceMock->getArticleByTagSlug('articleTag');

        $this->assertInstanceOf(ResponseDTO::class, $result);
    }

    private function getGetServiceMock(): GetService
    {
        return new GetService(
            $this->settingServiceMock,
            $this->tagRepositoryMock,
            $this->articleFilterGetServiceMock,
        );
    }
}
