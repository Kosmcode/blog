<?php

namespace Tests\Unit\app\Services\Contact;

use App\Enums\Setting\KeyEnum;
use App\Repositories\ContactRepositoryInterface;
use App\Services\Contact\AvailableSentService;
use App\Services\SettingServiceInterface;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;

class AvailableSentServiceTest extends TestCase
{
    private MockObject $contactRepositoryMock;

    private MockObject $settingServiceMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->contactRepositoryMock = $this->createMock(ContactRepositoryInterface::class);
        $this->settingServiceMock = $this->createMock(SettingServiceInterface::class);
    }

    public function testAvailableSendByIpAddressWhenSettingValueIsWrong(): void
    {
        $this->settingServiceMock
            ->expects($this->once())
            ->method('get')
            ->with(KeyEnum::contactCanSendTimeInMinutes->value)
            ->willReturn(null);

        $availableSentServiceMock = $this->getAvailableSentServiceMock();

        $this->assertTrue(
            $availableSentServiceMock->availableSendByIpAddress('127.0.0.1')
        );
    }

    public function testAvailableSendByIpAddressWhenIsNotAvailableToSend(): void
    {
        $this->settingServiceMock
            ->expects($this->once())
            ->method('get')
            ->with(KeyEnum::contactCanSendTimeInMinutes->value)
            ->willReturn(5);

        $this->contactRepositoryMock
            ->expects($this->once())
            ->method('existsByIpAddressAndCreatedAt')
            ->withAnyParameters()
            ->willReturn(true);

        $availableSentServiceMock = $this->getAvailableSentServiceMock();

        $this->assertFalse(
            $availableSentServiceMock->availableSendByIpAddress('127.0.0.1')
        );
    }

    public function testAvailableSendByIpAddressWhenIsAvailableToSend(): void
    {
        $this->settingServiceMock
            ->expects($this->once())
            ->method('get')
            ->with(KeyEnum::contactCanSendTimeInMinutes->value)
            ->willReturn(5);

        $this->contactRepositoryMock
            ->expects($this->once())
            ->method('existsByIpAddressAndCreatedAt')
            ->withAnyParameters()
            ->willReturn(false);

        $availableSentServiceMock = $this->getAvailableSentServiceMock();

        $this->assertTrue(
            $availableSentServiceMock->availableSendByIpAddress('127.0.0.1')
        );
    }

    private function getAvailableSentServiceMock(): AvailableSentService
    {
        return new AvailableSentService(
            $this->contactRepositoryMock,
            $this->settingServiceMock,
        );
    }
}
