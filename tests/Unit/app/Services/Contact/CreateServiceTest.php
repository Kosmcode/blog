<?php

namespace Tests\Unit\app\Services\Contact;

use App\Dto\ResponseDTO;
use App\Http\Requests\Contact\CreateRequest;
use App\Models\Contact;
use App\Models\PageElement;
use App\Repositories\ContactRepositoryInterface;
use App\Services\Contact\CreateService;
use App\Services\Page\Element\GetServiceInterface;
use App\Services\SettingServiceInterface;
use Illuminate\Contracts\Queue\Queue;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;

class CreateServiceTest extends TestCase
{
    private MockObject $contactRepositoryMock;

    private MockObject $settingServiceMock;

    private MockObject $laravelQueueMock;

    private MockObject $elementServiceMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->contactRepositoryMock = $this->createMock(ContactRepositoryInterface::class);
        $this->settingServiceMock = $this->createMock(SettingServiceInterface::class);
        $this->laravelQueueMock = $this->createMock(Queue::class);
        $this->elementServiceMock = $this->createMock(GetServiceInterface::class);
    }

    public function testFromCreateRequestWhenNotCreatedContact(): void
    {
        $createRequestMock = new CreateRequest();

        $this->contactRepositoryMock
            ->expects($this->once())
            ->method('createByContactStoreRequest')
            ->with($createRequestMock)
            ->willReturn(null);

        $this->settingServiceMock
            ->method('get')
            ->withAnyParameters()
            ->willReturn('settingValue');

        $createServiceMock = $this->getCreateServiceMock();
        $result = $createServiceMock->fromCreateRequest($createRequestMock);

        $this->assertInstanceOf(ResponseDTO::class, $result);
    }

    public function testFromCreateRequestWhenCreatedContactAndSettingSendEmailIsOff(): void
    {
        $createRequestMock = new CreateRequest();

        $this->contactRepositoryMock
            ->expects($this->once())
            ->method('createByContactStoreRequest')
            ->with($createRequestMock)
            ->willReturn(new Contact());

        $this->settingServiceMock
            ->method('get')
            ->withAnyParameters()
            ->willReturnOnConsecutiveCalls(
                'settingValue',
                'settingValue',
                'settingValue',
                false
            );

        $this->elementServiceMock
            ->method('getByElementEnum')
            ->withAnyParameters()
            ->willReturn(new PageElement());

        $createServiceMock = $this->getCreateServiceMock();
        $result = $createServiceMock->fromCreateRequest($createRequestMock);

        $this->assertInstanceOf(ResponseDTO::class, $result);
    }

    public function testFromCreateRequestWhenCreatedContactAndSettingSendEmailIsOn(): void
    {
        $createRequestMock = new CreateRequest();

        $this->contactRepositoryMock
            ->expects($this->once())
            ->method('createByContactStoreRequest')
            ->with($createRequestMock)
            ->willReturn(
                (new Contact())
                    ->setId(1)
            );

        $this->settingServiceMock
            ->method('get')
            ->withAnyParameters()
            ->willReturnOnConsecutiveCalls(
                'settingValue',
                'settingValue',
                'settingValue',
                true
            );

        $this->elementServiceMock
            ->method('getByElementEnum')
            ->withAnyParameters()
            ->willReturn(new PageElement());

        $this->laravelQueueMock
            ->expects($this->once())
            ->method('push')
            ->withAnyParameters();

        $createServiceMock = $this->getCreateServiceMock();
        $result = $createServiceMock->fromCreateRequest($createRequestMock);

        $this->assertInstanceOf(ResponseDTO::class, $result);
    }

    private function getCreateServiceMock(): CreateService
    {
        return new CreateService(
            $this->contactRepositoryMock,
            $this->settingServiceMock,
            $this->laravelQueueMock,
            $this->elementServiceMock,
        );
    }
}
