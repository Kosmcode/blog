<?php

namespace Tests\Unit\app\Services\Contact;

use App\Dto\ResponseDTO;
use App\Services\Contact\AvailableSentServiceInterface;
use App\Services\Contact\GetService;
use App\Services\Page\Element\GetServiceInterface;
use App\Services\SettingServiceInterface;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;

class GetServiceTest extends TestCase
{
    private MockObject $contactCheckServiceMock;

    private MockObject $settingServiceMock;

    private MockObject $pageElementGetServiceMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->contactCheckServiceMock = $this->createMock(AvailableSentServiceInterface::class);
        $this->settingServiceMock = $this->createMock(SettingServiceInterface::class);
        $this->pageElementGetServiceMock = $this->createMock(GetServiceInterface::class);
    }

    public function testGetToInertia(): void
    {
        $this->contactCheckServiceMock
            ->expects($this->once())
            ->method('availableSendByIpAddress')
            ->with('127.0.0.1')
            ->willReturn(true);

        $this->pageElementGetServiceMock
            ->method('getByElementEnum')
            ->withAnyParameters()
            ->willReturn(null);

        $this->settingServiceMock
            ->method('get')
            ->withAnyParameters()
            ->willReturn('settingValue');

        $getServiceMock = $this->getGetServiceMock();
        $result = $getServiceMock->getToInertia('127.0.0.1');

        $this->assertInstanceOf(ResponseDTO::class, $result);
    }

    private function getGetServiceMock(): GetService
    {
        return new GetService(
            $this->contactCheckServiceMock,
            $this->settingServiceMock,
            $this->pageElementGetServiceMock,
        );
    }
}
