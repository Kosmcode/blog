<?php

namespace Tests\Unit\app\Services\Contact\Job;

use App\Enums\Log\ChannelEnum;
use App\Enums\Setting\KeyEnum;
use App\Models\Contact;
use App\Repositories\ContactRepositoryInterface;
use App\Services\Contact\Job\SendMailService;
use App\Services\SettingServiceInterface;
use Illuminate\Log\LogManager;
use Illuminate\Mail\Mailer;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;

class SendMailServiceTest extends TestCase
{
    private MockObject $laravelLogMock;

    private MockObject $settingServiceMock;

    private MockObject $contactRepositoryMock;

    private MockObject $laravelMailMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->laravelLogMock = $this->createMock(LogManager::class);
        $this->settingServiceMock = $this->createMock(SettingServiceInterface::class);
        $this->contactRepositoryMock = $this->createMock(ContactRepositoryInterface::class);
        $this->laravelMailMock = $this->createMock(Mailer::class);
    }

    public function testSendMailFromJobByContactIdWhenSendMailIsNotEnabled(): void
    {
        $this->laravelLogMock
            ->expects($this->once())
            ->method('channel')
            ->with(ChannelEnum::contactQueue->value)
            ->willReturnSelf();

        $this->laravelLogMock
            ->expects($this->once())
            ->method('info')
            ->withAnyParameters();

        $this->settingServiceMock
            ->expects($this->once())
            ->method('get')
            ->with(KeyEnum::contactSendToMailEnabled->value)
            ->willReturn('0');

        $sendMailServiceMock = $this->getSendMailServiceMock();

        $this->assertNull(
            $sendMailServiceMock->sendMailFromJobByContactId(1)
        );
    }

    public function testSendMailFromJobByContactIdWhenMailToSendIsNotValid(): void
    {
        $this->laravelLogMock
            ->expects($this->once())
            ->method('channel')
            ->with(ChannelEnum::contactQueue->value)
            ->willReturnSelf();

        $this->laravelLogMock
            ->expects($this->once())
            ->method('info')
            ->withAnyParameters();

        $this->laravelLogMock
            ->expects($this->once())
            ->method('error')
            ->withAnyParameters();

        $this->settingServiceMock
            ->expects($this->exactly(2))
            ->method('get')
            ->withAnyParameters()
            ->willReturnOnConsecutiveCalls(
                '1',
                'not.valid@email'
            );

        $sendMailServiceMock = $this->getSendMailServiceMock();

        $this->assertNull(
            $sendMailServiceMock->sendMailFromJobByContactId(1)
        );
    }

    public function testSendMailFromJobByContactIdWhenContactNotExists(): void
    {
        $this->laravelLogMock
            ->expects($this->once())
            ->method('channel')
            ->with(ChannelEnum::contactQueue->value)
            ->willReturnSelf();

        $this->laravelLogMock
            ->expects($this->once())
            ->method('info')
            ->withAnyParameters();

        $this->settingServiceMock
            ->expects($this->exactly(2))
            ->method('get')
            ->withAnyParameters()
            ->willReturnOnConsecutiveCalls(
                '1',
                'valid@email.com'
            );

        $this->contactRepositoryMock
            ->expects($this->once())
            ->method('getById')
            ->with(1)
            ->willReturn(null);

        $this->laravelLogMock
            ->expects($this->once())
            ->method('error')
            ->withAnyParameters();

        $sendMailServiceMock = $this->getSendMailServiceMock();

        $this->assertNull(
            $sendMailServiceMock->sendMailFromJobByContactId(1)
        );
    }

    public function testSendMailFromJobByContactIdWhenSend(): void
    {
        $this->laravelLogMock
            ->expects($this->once())
            ->method('channel')
            ->with(ChannelEnum::contactQueue->value)
            ->willReturnSelf();

        $this->laravelLogMock
            ->expects($this->exactly(2))
            ->method('info')
            ->withAnyParameters();

        $this->settingServiceMock
            ->expects($this->exactly(2))
            ->method('get')
            ->withAnyParameters()
            ->willReturnOnConsecutiveCalls(
                '1',
                'valid@email.com'
            );

        $this->contactRepositoryMock
            ->expects($this->once())
            ->method('getById')
            ->with(1)
            ->willReturn(
                (new Contact([
                    'email' => 'test@test.com',
                    'subject' => 'subject',

                ]))
            );

        $this->contactRepositoryMock
            ->expects($this->once())
            ->method('save')
            ->withAnyParameters()
            ->willReturn(true);

        $sendMailServiceMock = $this->getSendMailServiceMock();

        $this->assertNull(
            $sendMailServiceMock->sendMailFromJobByContactId(1)
        );
    }

    private function getSendMailServiceMock(): SendMailService
    {
        return new SendMailService(
            $this->laravelLogMock,
            $this->settingServiceMock,
            $this->contactRepositoryMock,
            $this->laravelMailMock,
        );
    }
}
