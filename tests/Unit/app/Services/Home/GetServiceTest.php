<?php

namespace Tests\Unit\app\Services\Home;

use App\Dto\ResponseDTO;
use App\Models\PageElement;
use App\Services\Article\Recent\GetServiceInterface as ArticleRecentGetServiceInterface;
use App\Services\Home\GetService;
use App\Services\Page\Element\GetServiceInterface as PageElementGetServiceInterface;
use App\Services\SettingServiceInterface;
use Illuminate\Support\Collection;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;

class GetServiceTest extends TestCase
{
    private MockObject $settingServiceMock;

    private MockObject $pageElementServiceMock;

    private MockObject $recentArticleServiceMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->settingServiceMock = $this->createMock(SettingServiceInterface::class);
        $this->pageElementServiceMock = $this->createMock(PageElementGetServiceInterface::class);
        $this->recentArticleServiceMock = $this->createMock(ArticleRecentGetServiceInterface::class);
    }

    public function testGetResponseDTO(): void
    {
        $this->recentArticleServiceMock
            ->expects($this->once())
            ->method('getRecentArticles')
            ->willReturn(new Collection());

        $this->pageElementServiceMock
            ->expects($this->exactly(2))
            ->method('getByElementEnum')
            ->withAnyParameters()
            ->willReturn(new PageElement());

        $this->settingServiceMock
            ->method('get')
            ->withAnyParameters()
            ->willReturn('settingValue');

        $getServiceMock = $this->getGetServiceMock();
        $result = $getServiceMock->getResponseDTO();

        $this->assertInstanceOf(ResponseDTO::class, $result);
    }

    private function getGetServiceMock(): GetService
    {
        return new GetService(
            $this->settingServiceMock,
            $this->pageElementServiceMock,
            $this->recentArticleServiceMock,
        );
    }
}
