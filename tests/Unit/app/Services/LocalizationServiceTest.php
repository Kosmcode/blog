<?php

namespace Tests\Unit\app\Services;

use App\Services\LocalizationService;
use Illuminate\Cache\Repository as LaravelCache;
use Illuminate\Log\LogManager as LaravelLog;
use Illuminate\Translation\Translator as LaravelLang;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use Psr\Log\LoggerInterface;
use Tests\Unit\TestCase;

class LocalizationServiceTest extends TestCase
{
    use ProphecyTrait;

    private ObjectProphecy $laravelLang;

    private ObjectProphecy $laravelLog;

    private ObjectProphecy $laravelCache;

    private ObjectProphecy $logger;

    public function setUp(): void
    {
        parent::setUp();

        $this->laravelLang = $this->prophesize(LaravelLang::class);
        $this->laravelLog = $this->prophesize(LaravelLog::class);
        $this->laravelCache = $this->prophesize(LaravelCache::class);

        $this->logger = $this->prophesize(LoggerInterface::class);
    }

    public function testSetLangPattern(): void
    {
        $localizationServiceMock = $this->getLocalizationServiceMock();

        $result = $localizationServiceMock->setLangPattern('langPattern');

        $this->assertInstanceOf(LocalizationService::class, $result);
    }

    public function testGetWhenLocalizationResultIsAnArray(): void
    {
        $this->laravelLang
            ->get(
                Argument::type('String'),
                Argument::type('Array'),
                Argument::type('String'),
                Argument::type('Bool')
            )
            ->willReturn(['key' => 'translate', 'key2' => 'translate2']);

        $this->logger
            ->error(Argument::type('String'));

        $localizationServiceMock = $this->getLocalizationServiceMock();

        $localizationServiceMock->setLangPattern('testLangPattern');

        $result = $localizationServiceMock->get('localizationKey');

        $this->assertIsString($result);
        $this->assertEquals('', $result);
    }

    public function testGetWhenLocalizationResultIsSameAsLocalizationKey(): void
    {
        $this->laravelLang
            ->get(
                Argument::type('String'),
                Argument::type('Array'),
                Argument::type('String'),
                Argument::type('Bool')
            )
            ->willReturn('localizationKey');

        $this->logger
            ->error(Argument::type('String'));

        $localizationServiceMock = $this->getLocalizationServiceMock();

        $result = $localizationServiceMock->get('localizationKey');

        $this->assertIsString($result);
        $this->assertEquals('', $result);
    }

    public function testGetWhenLocalizationResultIsCorrect(): void
    {
        $this->laravelLang
            ->get(
                Argument::type('String'),
                Argument::type('Array'),
                Argument::type('String'),
                Argument::type('Bool')
            )
            ->willReturn('correctTranslateLocalization');

        $this->logger
            ->error(Argument::type('String'));

        $localizationServiceMock = $this->getLocalizationServiceMock();

        $result = $localizationServiceMock->get('localizationKey');

        $this->assertIsString($result);
        $this->assertEquals('correctTranslateLocalization', $result);
    }

    private function getLocalizationServiceMock(): LocalizationService
    {
        $this->laravelLog
            ->channel(Argument::type('String'))
            ->willReturn($this->logger->reveal());

        $this->laravelLang
            ->getLocale()
            ->willReturn('en');

        return new LocalizationService(
            $this->laravelLang->reveal(),
            $this->laravelLog->reveal(),
            $this->laravelCache->reveal()
        );
    }
}
