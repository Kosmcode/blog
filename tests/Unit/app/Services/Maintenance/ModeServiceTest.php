<?php

namespace Tests\Unit\app\Services\Maintenance;

use App\Enums\Setting\KeyEnum;
use App\Services\Maintenance\ModeService;
use App\Services\SettingServiceInterface;
use App\Services\User\CheckServiceInterface;
use Illuminate\Contracts\Routing\ResponseFactory as LaravelResponse;
use Illuminate\Http\Response;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use Tests\Unit\TestCase;

class ModeServiceTest extends TestCase
{
    use ProphecyTrait;

    private ObjectProphecy $settingService;

    private ObjectProphecy $laravelResponse;

    private ObjectProphecy $userCheckService;

    public function setUp(): void
    {
        parent::setUp();

        $this->settingService = $this->prophesize(SettingServiceInterface::class);
        $this->laravelResponse = $this->prophesize(LaravelResponse::class);
        $this->userCheckService = $this->prophesize(CheckServiceInterface::class);
    }

    public function testIsEnabledWhenUserIsAdmin()
    {
        $this->userCheckService
            ->userHasAdminRole()
            ->willReturn(true);

        $mockedMaintenanceModeService = $this->getMockedMaintenanceModeService();

        $result = $mockedMaintenanceModeService->isEnabled();

        $this->assertIsBool($result);
        $this->assertFalse($result);
    }

    public function testIsEnabledWhenUserIsNotAdminAndIsNotEnabled()
    {
        $this->userCheckService
            ->userHasAdminRole()
            ->willReturn(false);

        $this->settingService
            ->get(Argument::type('String'))
            ->willReturn(false);

        $mockedMaintenanceModeService = $this->getMockedMaintenanceModeService();

        $result = $mockedMaintenanceModeService->isEnabled();

        $this->assertIsBool($result);
        $this->assertFalse($result);
    }

    public function testIsEnabledWhenUserIsNotAdminAndIsEnabled()
    {
        $this->userCheckService
            ->userHasAdminRole()
            ->willReturn(false);

        $this->settingService
            ->get(Argument::type('String'))
            ->willReturn(true);

        $mockedMaintenanceModeService = $this->getMockedMaintenanceModeService();

        $result = $mockedMaintenanceModeService->isEnabled();

        $this->assertIsBool($result);
        $this->assertTrue($result);
    }

    public function testGetMaintenanceView()
    {
        $this->settingService
            ->get(Argument::type('String'))
            ->willReturn('appName');
        $this->settingService
            ->get(Argument::type('String'))
            ->willReturn('maintenanceModeMessage');

        $this->laravelResponse
            ->view(Argument::type('String'), Argument::type('Array'), Argument::type('Integer'))
            ->willReturn(new Response());

        $mockedMaintenanceModeService = $this->getMockedMaintenanceModeService();

        $result = $mockedMaintenanceModeService->getMaintenanceView();

        $this->assertInstanceOf(Response::class, $result);
    }

    public function testSetMaintenanceMode()
    {
        $this->settingService
            ->setBySettingKeyEnum(
                KeyEnum::maintenanceModeEnabled,
                Argument::type('Bool')
            );

        $mockedMaintenanceModeService = $this->getMockedMaintenanceModeService();

        $this->assertNull(
            $mockedMaintenanceModeService->setMaintenanceMode(true)
        );
    }

    private function getMockedMaintenanceModeService(): ModeService
    {
        return new ModeService(
            $this->settingService->reveal(),
            $this->laravelResponse->reveal(),
            $this->userCheckService->reveal()
        );
    }
}
