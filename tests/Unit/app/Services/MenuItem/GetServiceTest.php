<?php

namespace Tests\Unit\app\Services\MenuItem;

use App\Enums\Cache\KeyEnum;
use App\Repositories\MenuItemRepositoryInterface;
use App\Services\MenuItem\GetService;
use Illuminate\Cache\Repository;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;

class GetServiceTest extends TestCase
{
    private MockObject $laravelCacheMock;

    private MockObject $menuItemRepositoryMock;

    private MockObject $laravelRequestMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->laravelCacheMock = $this->createMock(Repository::class);
        $this->menuItemRepositoryMock = $this->createMock(MenuItemRepositoryInterface::class);
        $this->laravelRequestMock = $this->createMock(Request::class);
    }

    public function testGetToInertiaNavigationWhenGetCachedAndExists(): void
    {
        $this->laravelCacheMock
            ->expects($this->once())
            ->method('get')
            ->with(KeyEnum::menuItem->value)
            ->willReturn(['menuItems']);

        $getServiceMock = $this->getGetServiceMock();

        $result = $getServiceMock->getToInertiaNavigation();

        $this->assertIsArray($result);
        $this->assertSame(['menuItems'], $result);
    }

    public function testGetToInertiaNavigationWhenGetCachedAndNotExists(): void
    {
        $this->laravelCacheMock
            ->expects($this->once())
            ->method('get')
            ->with(KeyEnum::menuItem->value)
            ->willReturn(null);

        $this->menuItemRepositoryMock
            ->expects($this->once())
            ->method('getMainParentsLinksWithChildless')
            ->willReturn(new Collection());

        $this->laravelCacheMock
            ->expects($this->once())
            ->method('forever')
            ->withAnyParameters()
            ->willReturn([]);

        $getServiceMock = $this->getGetServiceMock();

        $result = $getServiceMock->getToInertiaNavigation();

        $this->assertIsArray($result);
        $this->assertSame([], $result);
    }

    public function testGetToInertiaNavigationWhenNotGetCached(): void
    {
        $this->menuItemRepositoryMock
            ->expects($this->once())
            ->method('getMainParentsLinksWithChildless')
            ->willReturn(new Collection());

        $getServiceMock = $this->getGetServiceMock();

        $result = $getServiceMock->getToInertiaNavigation(false);

        $this->assertIsArray($result);
        $this->assertSame([], $result);
    }

    private function getGetServiceMock(): GetService
    {
        return new GetService(
            $this->laravelCacheMock,
            $this->menuItemRepositoryMock,
            $this->laravelRequestMock,
        );
    }
}
