<?php

namespace Tests\Unit\app\Services\MenuItem\Internal\LinkFactories;

use App\Models\Category;
use App\Repositories\CategoryRepositoryInterface;
use App\Services\MenuItem\Internal\LinkFactories\ArticleCategoryFactory;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Collection;
use PHPUnit\Framework\MockObject\MockObject;
use stdClass;
use Tests\TestCase;

class ArticleCategoryFactoryTest extends TestCase
{
    private MockObject $categoryRepositoryMock;

    private MockObject $urlGeneratorMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->categoryRepositoryMock = $this->createMock(CategoryRepositoryInterface::class);
        $this->urlGeneratorMock = $this->createMock(UrlGenerator::class);
    }

    public function testToArrayToAdmin(): void
    {
        $categoryMock = (new Category())
            ->setSlug('categorySlug')
            ->setName('categoryName');

        $this->categoryRepositoryMock
            ->expects($this->once())
            ->method('getToMenuItemsLinks')
            ->willReturn(new Collection([
                $categoryMock,
                new stdClass(),
            ]));

        $this->urlGeneratorMock
            ->expects($this->once())
            ->method('route')
            ->withAnyParameters()
            ->willReturn('routeUrl');

        $articleCategoryFactoryMock = $this->getArticleCategoryFactoryMock();
        $result = $articleCategoryFactoryMock->toArrayToAdmin();

        $this->assertIsArray($result);
        $this->assertSame(
            ['routeUrl' => 'Article category: categoryName'],
            $result
        );
    }

    private function getArticleCategoryFactoryMock(): ArticleCategoryFactory
    {
        return new ArticleCategoryFactory(
            $this->categoryRepositoryMock,
            $this->urlGeneratorMock,
        );
    }
}
