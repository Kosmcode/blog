<?php

namespace Tests\Unit\app\Services\MenuItem\Internal\LinkFactories;

use App\Models\Tag;
use App\Repositories\TagRepositoryInterface;
use App\Services\MenuItem\Internal\LinkFactories\ArticleTagFactory;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Collection;
use PHPUnit\Framework\MockObject\MockObject;
use stdClass;
use Tests\TestCase;

class ArticleTagFactoryTest extends TestCase
{
    private MockObject $tagRepositoryMock;

    private MockObject $urlGeneratorMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->tagRepositoryMock = $this->createMock(TagRepositoryInterface::class);
        $this->urlGeneratorMock = $this->createMock(UrlGenerator::class);
    }

    public function testToArrayToAdmin(): void
    {
        $tagMock = (new Tag())
            ->setName('tagName')
            ->setSlug('tagSlug');

        $this->tagRepositoryMock
            ->expects($this->once())
            ->method('getToMenuItemsLinks')
            ->willReturn(new Collection([
                $tagMock,
                new stdClass(),
            ]));

        $this->urlGeneratorMock
            ->expects($this->once())
            ->method('route')
            ->withAnyParameters()
            ->willReturn('routeUrl');

        $articleTagFactoryMock = $this->getArticleTagFactoryMock();
        $result = $articleTagFactoryMock->toArrayToAdmin();

        $this->assertIsArray($result);
        $this->assertSame(
            ['routeUrl' => 'Article tag: tagName'],
            $result
        );
    }

    private function getArticleTagFactoryMock(): ArticleTagFactory
    {
        return new ArticleTagFactory(
            $this->tagRepositoryMock,
            $this->urlGeneratorMock,
        );
    }
}
