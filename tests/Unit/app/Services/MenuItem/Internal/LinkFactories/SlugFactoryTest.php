<?php

namespace Tests\Unit\app\Services\MenuItem\Internal\LinkFactories;

use App\Models\Slug;
use App\Repositories\SlugRepositoryInterface;
use App\Services\MenuItem\Internal\LinkFactories\SlugFactory;
use App\Services\Slug\Factory\Factories\ContentableFactoryInterface;
use App\Services\Slug\Factory\SlugFactoryInterface;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Collection;
use PHPUnit\Framework\MockObject\MockObject;
use stdClass;
use Tests\TestCase;

class SlugFactoryTest extends TestCase
{
    private MockObject $slugRepositoryMock;

    private MockObject $urlGeneratorMock;

    private MockObject $sluggableFactoryMock;

    private MockObject $sluggableMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->slugRepositoryMock = $this->createMock(SlugRepositoryInterface::class);
        $this->urlGeneratorMock = $this->createMock(UrlGenerator::class);
        $this->sluggableFactoryMock = $this->createMock(SlugFactoryInterface::class);

        $this->sluggableMock = $this->createMock(ContentableFactoryInterface::class);
    }

    public function testToArrayToAdmin(): void
    {
        $this->slugRepositoryMock
            ->expects($this->once())
            ->method('getToMenuItemsLinks')
            ->willReturn(new Collection([
                new stdClass(),
                (new Slug())
                    ->setSlug('slugSlug'),
            ]));

        $this->sluggableFactoryMock
            ->expects($this->exactly(1))
            ->method('getBySlug')
            ->withAnyParameters()
            ->willReturn($this->sluggableMock);

        $this->sluggableMock
            ->expects($this->once())
            ->method('getNameToAdminLinksBySlug')
            ->withAnyParameters()
            ->willReturn('slugContentableName');

        $this->urlGeneratorMock
            ->expects($this->once())
            ->method('route')
            ->withAnyParameters()
            ->willReturn('routeUrl');

        $slugFactoryMock = $this->getSlugFactoryMock();
        $result = $slugFactoryMock->toArrayToAdmin();

        $this->assertIsArray($result);
        $this->assertSame(['routeUrl' => 'slugContentableName'], $result);
    }

    private function getSlugFactoryMock(): SlugFactory
    {
        return new SlugFactory(
            $this->slugRepositoryMock,
            $this->urlGeneratorMock,
            $this->sluggableFactoryMock,
        );
    }
}
