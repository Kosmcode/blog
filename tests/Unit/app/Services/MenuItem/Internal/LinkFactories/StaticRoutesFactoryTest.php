<?php

namespace Tests\Unit\app\Services\MenuItem\Internal\LinkFactories;

use App\Enums\Route\SlugEnum;
use App\Services\MenuItem\Internal\LinkFactories\LinkFactoryInterface;
use App\Services\MenuItem\Internal\LinkFactories\StaticRoutesFactory;
use Tests\Unit\TestCase;

class StaticRoutesFactoryTest extends TestCase
{
    public function testToArrayToAdmin(): void
    {
        $staticRoutesFactoryMock = $this->getStaticRoutesFactoryMock();

        $result = $staticRoutesFactoryMock->toArrayToAdmin();

        $this->assertIsArray($result);
        $this->assertSame(
            SlugEnum::getRoutesToMenuItemInternalLinks(),
            $result
        );

    }

    protected function getStaticRoutesFactoryMock(): LinkFactoryInterface
    {
        return new StaticRoutesFactory();
    }
}
