<?php

namespace Tests\Unit\app\Services\MenuItem\Internal;

use App\Services\MenuItem\Internal\LinkFactories\LinkFactoryInterface;
use App\Services\MenuItem\Internal\LinkFactories\StaticRoutesFactory;
use App\Services\MenuItem\Internal\LinkFactory;
use Illuminate\Contracts\Foundation\Application;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;

class LinkFactoryTest extends TestCase
{
    private iterable $factoriesMock;

    private MockObject $applicationMock;

    private MockObject $factoryMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->factoriesMock = [StaticRoutesFactory::class];
        $this->applicationMock = $this->createMock(Application::class);

        $this->factoryMock = $this->createMock(LinkFactoryInterface::class);
    }

    public function testGetArrayLinksToAdmin(): void
    {
        $this->applicationMock
            ->expects($this->once())
            ->method('make')
            ->with(StaticRoutesFactory::class)
            ->willReturn($this->factoryMock);

        $this->factoryMock
            ->expects($this->once())
            ->method('toArrayToAdmin')
            ->willReturn(['test']);

        $linkFactoryMock = $this->getLinkFactoryMock();
        $result = $linkFactoryMock->getArrayLinksToAdmin();

        $this->assertIsArray($result);
        $this->assertSame(['test'], $result);
    }

    private function getLinkFactoryMock(): LinkFactory
    {
        return new LinkFactory(
            $this->factoriesMock,
            $this->applicationMock,
        );
    }
}
