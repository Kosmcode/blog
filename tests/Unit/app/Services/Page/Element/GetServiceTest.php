<?php

namespace Tests\Unit\app\Services\Page\Element;

use App\Enums\Page\ElementEnum;
use App\Models\PageElement;
use App\Repositories\PageElementRepositoryInterface;
use App\Services\Page\Element\GetService;
use Illuminate\Cache\Repository;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;

class GetServiceTest extends TestCase
{
    private MockObject $laravelCacheMock;

    private MockObject $pageElementRepositoryMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->laravelCacheMock = $this->createMock(Repository::class);
        $this->pageElementRepositoryMock = $this->createMock(PageElementRepositoryInterface::class);
    }

    public function testGetByElementEnumWhenIsCachedArgumentAndExistCachedResource(): void
    {
        $this->laravelCacheMock
            ->expects($this->once())
            ->method('get')
            ->withAnyParameters()
            ->willReturn(new PageElement());

        $getServiceMock = $this->getGetServiceMock();
        $result = $getServiceMock->getByElementEnum(ElementEnum::homePageTop);

        $this->assertInstanceOf(PageElement::class, $result);
    }

    public function testGetByElementEnumIsCachedArgumentNotExistCachedResourceAndPageElementNotExistsByTitle(): void
    {
        $this->laravelCacheMock
            ->expects($this->once())
            ->method('get')
            ->withAnyParameters()
            ->willReturn(null);

        $this->pageElementRepositoryMock
            ->expects($this->once())
            ->method('getByTitle')
            ->with(ElementEnum::homePageTop->value)
            ->willReturn(null);

        $getServiceMock = $this->getGetServiceMock();
        $result = $getServiceMock->getByElementEnum(ElementEnum::homePageTop);

        $this->assertNull($result);
    }

    public function testGetByElementEnumIsCachedArgumentNotExistCachedResourceAndPageElementExistsByTitle(): void
    {
        $this->laravelCacheMock
            ->expects($this->once())
            ->method('get')
            ->withAnyParameters()
            ->willReturn(null);

        $this->pageElementRepositoryMock
            ->expects($this->once())
            ->method('getByTitle')
            ->with(ElementEnum::homePageTop->value)
            ->willReturn(new PageElement());

        $this->laravelCacheMock
            ->expects($this->once())
            ->method('forever')
            ->withAnyParameters()
            ->willReturn(new PageElement());

        $getServiceMock = $this->getGetServiceMock();
        $result = $getServiceMock->getByElementEnum(ElementEnum::homePageTop);

        $this->assertInstanceOf(PageElement::class, $result);
    }

    public function testGetByElementEnumWhenIsNotCachedArgument(): void
    {
        $this->pageElementRepositoryMock
            ->expects($this->once())
            ->method('getByTitle')
            ->with(ElementEnum::homePageTop->value)
            ->willReturn(new PageElement());

        $getServiceMock = $this->getGetServiceMock();
        $result = $getServiceMock->getByElementEnum(ElementEnum::homePageTop, false);

        $this->assertInstanceOf(PageElement::class, $result);
    }

    private function getGetServiceMock(): GetService
    {
        return new GetService(
            $this->laravelCacheMock,
            $this->pageElementRepositoryMock,
        );
    }
}
