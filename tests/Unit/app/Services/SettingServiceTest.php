<?php

namespace Tests\Unit\app\Services;

use App\Enums\Setting\KeyEnum;
use App\Models\Setting;
use App\Repositories\SettingRepositoryInterface;
use App\Services\SettingService;
use Illuminate\Cache\Repository as LaravelCache;
use Illuminate\Config\Repository as LaravelConfig;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;

class SettingServiceTest extends TestCase
{
    private MockObject $laravelCacheMock;

    private MockObject $settingRepositoryMock;

    private MockObject $laravelConfigMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->laravelCacheMock = $this->createMock(LaravelCache::class);
        $this->settingRepositoryMock = $this->createMock(SettingRepositoryInterface::class);
        $this->laravelConfigMock = $this->createMock(LaravelConfig::class);
    }

    public function testGetWhenCachedArgumentIsFalseAndGetFromSetting(): void
    {
        $this->settingRepositoryMock
            ->expects($this->once())
            ->method('findByKey')
            ->with('testKey')
            ->willReturn(
                (new Setting())
                    ->setValue('expectedValue')
            );

        $settingServiceMock = $this->getSettingServiceMock();
        $result = $settingServiceMock->get('testKey', false);

        $this->assertIsString($result);
        $this->assertSame('expectedValue', $result);
    }

    public function testGetWhenCachedArgumentIsFalseAndGetFromConfig(): void
    {
        $this->settingRepositoryMock
            ->expects($this->once())
            ->method('findByKey')
            ->with('testKey')
            ->willReturn(null);

        $this->laravelConfigMock
            ->expects($this->once())
            ->method('get')
            ->with('testKey')
            ->willReturn('expectedValue');

        $settingServiceMock = $this->getSettingServiceMock();
        $result = $settingServiceMock->get('testKey', false);

        $this->assertIsString($result);
        $this->assertSame('expectedValue', $result);
    }

    public function testGetWhenCachedArgumentIsTrueTrueAndCacheExists(): void
    {
        $this->laravelCacheMock
            ->expects($this->once())
            ->method('get')
            ->withAnyParameters()
            ->willReturn('expectedValue');

        $settingServiceMock = $this->getSettingServiceMock();
        $result = $settingServiceMock->get('testKey');

        $this->assertIsString($result);
        $this->assertSame('expectedValue', $result);
    }

    public function testGetWhenCachedArgumentIsTrueIsNotCachedAndGetFromSetting(): void
    {
        $this->laravelCacheMock
            ->expects($this->once())
            ->method('get')
            ->withAnyParameters()
            ->willReturn(null);

        $this->settingRepositoryMock
            ->expects($this->once())
            ->method('findByKey')
            ->with('testKey')
            ->willReturn(
                (new Setting())
                    ->setValue('expectedValue')
            );

        $this->laravelCacheMock
            ->expects($this->once())
            ->method('forever')
            ->withAnyParameters();

        $settingServiceMock = $this->getSettingServiceMock();
        $result = $settingServiceMock->get('testKey');

        $this->assertIsString($result);
        $this->assertSame('expectedValue', $result);
    }

    public function testGetWhenCachedArgumentIsTrueIsNotCachedAndGetFromConfig(): void
    {
        $this->laravelCacheMock
            ->expects($this->once())
            ->method('get')
            ->withAnyParameters()
            ->willReturn(null);

        $this->settingRepositoryMock
            ->expects($this->once())
            ->method('findByKey')
            ->with('testKey')
            ->willReturn(null);

        $this->laravelConfigMock
            ->expects($this->once())
            ->method('get')
            ->with('testKey')
            ->willReturn('expectedValue');

        $this->laravelCacheMock
            ->expects($this->once())
            ->method('forever')
            ->withAnyParameters();

        $settingServiceMock = $this->getSettingServiceMock();
        $result = $settingServiceMock->get('testKey');

        $this->assertIsString($result);
        $this->assertSame('expectedValue', $result);
    }

    public function testSetBySettingKeyEnumWhenNotFoundSettingByKey(): void
    {
        $this->settingRepositoryMock
            ->expects($this->once())
            ->method('findByKey')
            ->with(KeyEnum::appName->value)
            ->willReturn(null);

        $settingServiceMock = $this->getSettingServiceMock();

        $this->assertNull(
            $settingServiceMock->setBySettingKeyEnum(KeyEnum::appName, 'testValue')
        );
    }

    public function testSetBySettingKeyEnumWhenFoundSettingByKey(): void
    {
        $this->settingRepositoryMock
            ->expects($this->once())
            ->method('findByKey')
            ->with(KeyEnum::appName->value)
            ->willReturn(
                (new Setting())
                    ->setSettingKeyName(KeyEnum::appName->value)
            );

        $this->settingRepositoryMock
            ->expects($this->once())
            ->method('setValueByKey')
            ->with(KeyEnum::appName->value, 'testValue')
            ->willReturn(true);

        $this->laravelCacheMock
            ->expects($this->once())
            ->method('forever')
            ->withAnyParameters();

        $settingServiceMock = $this->getSettingServiceMock();

        $this->assertNull(
            $settingServiceMock->setBySettingKeyEnum(KeyEnum::appName, 'testValue')
        );
    }

    private function getSettingServiceMock(): SettingService
    {
        return new SettingService(
            $this->laravelCacheMock,
            $this->settingRepositoryMock,
            $this->laravelConfigMock,
        );
    }
}
