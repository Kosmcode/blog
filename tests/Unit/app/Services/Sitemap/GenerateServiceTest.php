<?php

namespace Tests\Unit\app\Services\Sitemap;

use App\Enums\FilesystemDiskEnum;
use App\Services\MenuItem\Internal\LinkFactoryInterface;
use App\Services\SettingServiceInterface;
use App\Services\Sitemap\GenerateService;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Filesystem\FilesystemManager;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;

class GenerateServiceTest extends TestCase
{
    private MockObject $linkFactoryMock;

    private MockObject $settingServiceMock;

    private MockObject $laravelStorageMock;

    private MockObject $filesystemMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->linkFactoryMock = $this->createMock(LinkFactoryInterface::class);
        $this->settingServiceMock = $this->createMock(SettingServiceInterface::class);
        $this->laravelStorageMock = $this->createMock(FilesystemManager::class);

        $this->filesystemMock = $this->createMock(Filesystem::class);
    }

    public function testGenerateSimpleTextSitemap(): void
    {
        $this->settingServiceMock
            ->method('get')
            ->withAnyParameters()
            ->willReturn('setting value');

        $this->linkFactoryMock
            ->expects($this->once())
            ->method('getArrayLinksToAdmin')
            ->willReturn(['uriLink' => 'uri name']);

        $this->laravelStorageMock
            ->expects($this->once())
            ->method('drive')
            ->with(FilesystemDiskEnum::public->value)
            ->willReturn($this->filesystemMock);

        $this->filesystemMock
            ->expects($this->once())
            ->method('put')
            ->withAnyParameters()
            ->willReturn(true);

        $generateServiceMock = $this->getGenerateServiceMock();
        $result = $generateServiceMock->generateSimpleTextSitemap();

        $this->assertTrue($result);
    }

    private function getGenerateServiceMock(): GenerateService
    {
        return new GenerateService(
            $this->linkFactoryMock,
            $this->settingServiceMock,
            $this->laravelStorageMock,
        );
    }
}
