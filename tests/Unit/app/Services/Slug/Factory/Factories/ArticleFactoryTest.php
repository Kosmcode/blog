<?php

namespace Tests\Unit\app\Services\Slug\Factory\Factories;

use App\Dto\ResponseDTO;
use App\Enums\Article\StatusEnum;
use App\Exceptions\SlugException;
use App\Models\Article;
use App\Models\Slug;
use App\Services\Article\Recent\GetServiceInterface;
use App\Services\Slug\Factory\Factories\ArticleFactory;
use App\Services\Slug\Factory\Factories\ContentableFactoryInterface;
use Illuminate\Support\Collection;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Tests\TestCase;

class ArticleFactoryTest extends TestCase
{
    private MockObject $articleRecentGetServiceMock;

    private MockObject $slugMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->articleRecentGetServiceMock = $this->createMock(GetServiceInterface::class);

        $this->slugMock = $this->createMock(Slug::class);
    }

    public function testSupportedModel(): void
    {
        $articleFactoryMock = $this->getArticleFactoryMock();
        $result = $articleFactoryMock->supportedModel();

        $this->assertIsString($result);
        $this->assertSame(Article::class, $result);
    }

    public function testGetResponseDTOBySlugWhenSlugContentableIsNotSupportedModel(): void
    {
        $this->slugMock
            ->expects($this->once())
            ->method('getContentable')
            ->willReturn(null);

        $articleFactoryMock = $this->getArticleFactoryMock();

        $this->expectException(SlugException::class);

        $articleFactoryMock->getResponseDTOBySlug($this->slugMock);
    }

    public function testGetResponseDTOBySlugWhenSlugContentableIsSupportedModelAndArticleIsNotPublished(): void
    {
        $this->slugMock
            ->expects($this->once())
            ->method('getContentable')
            ->willReturn(new Article());

        $articleFactoryMock = $this->getArticleFactoryMock();

        $this->expectException(HttpException::class);
        $this->expectExceptionCode(Response::HTTP_NOT_FOUND);
        $this->expectExceptionMessage('Article not exists');

        $articleFactoryMock->getResponseDTOBySlug($this->slugMock);
    }

    public function testGetResponseDTOBySlugWhenSlugContentableIsSupportedModel(): void
    {
        $this->slugMock
            ->expects($this->once())
            ->method('getContentable')
            ->willReturn(
                (new Article())
                    ->setStatusByStatusEnum(StatusEnum::published)
            );

        $this->articleRecentGetServiceMock
            ->expects($this->once())
            ->method('getRecentArticles')
            ->willReturn(new Collection());

        $articleFactoryMock = $this->getArticleFactoryMock();
        $result = $articleFactoryMock->getResponseDTOBySlug($this->slugMock);

        $this->assertInstanceOf(ResponseDTO::class, $result);
    }

    public function testGetNameToAdminLinksBySlugWhenSlugContentableIsNotSupportedModel(): void
    {
        $this->slugMock
            ->expects($this->once())
            ->method('getContentable')
            ->willReturn(null);

        $articleFactoryMock = $this->getArticleFactoryMock();

        $this->expectException(SlugException::class);

        $articleFactoryMock->getNameToAdminLinksBySlug($this->slugMock);
    }

    public function testGetNameToAdminLinksBySlugWhenSlugContentableIsSupportedModel(): void
    {
        $this->slugMock
            ->expects($this->once())
            ->method('getContentable')
            ->willReturn((new Article())->setTitle('articleTitle'));

        $articleFactoryMock = $this->getArticleFactoryMock();
        $result = $articleFactoryMock->getNameToAdminLinksBySlug($this->slugMock);

        $this->assertIsString($result);
        $this->assertSame('Article: articleTitle', $result);
    }

    private function getArticleFactoryMock(): ContentableFactoryInterface
    {
        return new ArticleFactory(
            $this->articleRecentGetServiceMock,
        );
    }
}
