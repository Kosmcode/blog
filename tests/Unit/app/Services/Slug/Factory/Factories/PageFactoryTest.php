<?php

namespace Tests\Unit\app\Services\Slug\Factory\Factories;

use App\Dto\ResponseDTO;
use App\Exceptions\SlugException;
use App\Models\Page;
use App\Models\Slug;
use App\Services\Slug\Factory\Factories\ContentableFactoryInterface;
use App\Services\Slug\Factory\Factories\PageFactory;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Tests\TestCase;

class PageFactoryTest extends TestCase
{
    private MockObject $slugMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->slugMock = $this->createMock(Slug::class);
    }

    public function testSupportedModel(): void
    {
        $articleFactoryMock = $this->getArticleFactoryMock();
        $result = $articleFactoryMock->supportedModel();

        $this->assertIsString($result);
        $this->assertSame(Page::class, $result);
    }

    public function testGetResponseDTOBySlugWhenSlugContentableIsNotSupportedModel(): void
    {
        $this->slugMock
            ->expects($this->once())
            ->method('getContentable')
            ->willReturn(null);

        $articleFactoryMock = $this->getArticleFactoryMock();

        $this->expectException(SlugException::class);

        $articleFactoryMock->getResponseDTOBySlug($this->slugMock);
    }

    public function testGetResponseDTOBySlugWhenPageIsNotPublished(): void
    {
        $this->slugMock
            ->expects($this->once())
            ->method('getContentable')
            ->willReturn(
                (new Page())
                    ->setPublished(false)
            );

        $articleFactoryMock = $this->getArticleFactoryMock();

        $this->expectException(HttpException::class);

        $articleFactoryMock->getResponseDTOBySlug($this->slugMock);
    }

    public function testGetResponseDTOBySlugWhenSlugContentableIsSupportedModel(): void
    {
        $this->slugMock
            ->expects($this->once())
            ->method('getContentable')
            ->willReturn(
                (new Page())
                    ->setPublished(true)
            );

        $articleFactoryMock = $this->getArticleFactoryMock();
        $result = $articleFactoryMock->getResponseDTOBySlug($this->slugMock);

        $this->assertInstanceOf(ResponseDTO::class, $result);
    }

    public function testGetNameToAdminLinksBySlugWhenSlugContentableIsNotSupportedModel(): void
    {
        $this->slugMock
            ->expects($this->once())
            ->method('getContentable')
            ->willReturn(null);

        $articleFactoryMock = $this->getArticleFactoryMock();

        $this->expectException(SlugException::class);

        $articleFactoryMock->getNameToAdminLinksBySlug($this->slugMock);
    }

    public function testGetNameToAdminLinksBySlugWhenSlugContentableIsSupportedModel(): void
    {
        $this->slugMock
            ->expects($this->once())
            ->method('getContentable')
            ->willReturn((new Page())->setName('pageName'));

        $articleFactoryMock = $this->getArticleFactoryMock();
        $result = $articleFactoryMock->getNameToAdminLinksBySlug($this->slugMock);

        $this->assertIsString($result);
        $this->assertSame('Page: pageName', $result);
    }

    private function getArticleFactoryMock(): ContentableFactoryInterface
    {
        return new PageFactory();
    }
}
