<?php

namespace Tests\Unit\app\Services\Slug\Factory;

use App\Exceptions\SlugException;
use App\Models\Article;
use App\Models\Slug;
use App\Services\Slug\Factory\Factories\ArticleFactory;
use App\Services\Slug\Factory\Factories\ContentableFactoryInterface;
use App\Services\Slug\Factory\Factories\PageFactory;
use App\Services\Slug\Factory\SlugFactory;
use Illuminate\Contracts\Foundation\Application;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;

class SlugFactoryTest extends TestCase
{
    private iterable $factoriesMock;

    private MockObject $applicationMock;

    private MockObject $factoryMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->factoryMock = $this->createMock(ContentableFactoryInterface::class);

        $this->factoriesMock = [PageFactory::class, ArticleFactory::class];
        $this->applicationMock = $this->createMock(Application::class);
    }

    public function testGetBySlugWhenGetBySupportedModel(): void
    {
        $slugMock = (new Slug())
            ->setContentableType(Article::class);

        $this->applicationMock
            ->expects($this->once())
            ->method('make')
            ->with(ArticleFactory::class)
            ->willReturn($this->factoryMock);

        $slugFactoryMock = $this->getSlugFactoryMock();
        $result = $slugFactoryMock->getBySlug($slugMock);

        $this->assertInstanceOf(ContentableFactoryInterface::class, $result);
    }

    public function testGetBySlugWhenNotGetBySupportedModel(): void
    {
        $slugMock = (new Slug())
            ->setContentableType(Slug::class);

        $slugFactoryMock = $this->getSlugFactoryMock();

        $this->expectException(SlugException::class);

        $slugFactoryMock->getBySlug($slugMock);
    }

    private function getSlugFactoryMock(): SlugFactory
    {
        return new SlugFactory(
            $this->factoriesMock,
            $this->applicationMock,
        );
    }
}
