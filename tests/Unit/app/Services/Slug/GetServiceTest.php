<?php

namespace Tests\Unit\app\Services\Slug;

use App\Dto\ResponseDTO;
use App\Models\Slug;
use App\Repositories\SlugRepositoryInterface;
use App\Services\Slug\Factory\SlugFactoryInterface;
use App\Services\Slug\GetService;
use Illuminate\Cache\Repository;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Tests\TestCase;

class GetServiceTest extends TestCase
{
    private MockObject $slugRepositoryMock;

    private MockObject $sluggableFactoryMock;

    private MockObject $laravelCacheMock;

    private MockObject $slugFactoryMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->slugRepositoryMock = $this->createMock(SlugRepositoryInterface::class);
        $this->sluggableFactoryMock = $this->createMock(SlugFactoryInterface::class);
        $this->laravelCacheMock = $this->createMock(Repository::class);

        $this->slugFactoryMock = $this->createMock(\App\Services\Slug\Factory\Factories\ContentableFactoryInterface::class);
    }

    public function testGetResponseDTOWhenCacheExist(): void
    {
        $this->laravelCacheMock
            ->expects($this->once())
            ->method('get')
            ->withAnyParameters()
            ->willReturn(new ResponseDTO());

        $getServiceMock = $this->getGetServiceMock();
        $result = $getServiceMock->getResponseDTO('slugUri');

        $this->assertInstanceOf(ResponseDTO::class, $result);
    }

    public function testGetResponseDTOWhenCacheNotExistAndNotFoundSlug(): void
    {
        $this->laravelCacheMock
            ->expects($this->once())
            ->method('get')
            ->withAnyParameters()
            ->willReturn(null);

        $this->slugRepositoryMock
            ->expects($this->once())
            ->method('findBySlug')
            ->with('slugUri')
            ->willReturn(null);

        $getServiceMock = $this->getGetServiceMock();

        $this->expectException(HttpException::class);

        $getServiceMock->getResponseDTO('slugUri');
    }

    public function testGetResponseDTOWhenCacheNotExist(): void
    {
        $slugMock = (new Slug());

        $this->laravelCacheMock
            ->expects($this->once())
            ->method('get')
            ->withAnyParameters()
            ->willReturn(null);

        $this->slugRepositoryMock
            ->expects($this->once())
            ->method('findBySlug')
            ->with('slugUri')
            ->willReturn($slugMock);

        $this->sluggableFactoryMock
            ->expects($this->once())
            ->method('getBySlug')
            ->with($slugMock)
            ->willReturn($this->slugFactoryMock);

        $this->slugFactoryMock
            ->expects($this->once())
            ->method('getResponseDTOBySlug')
            ->with($slugMock)
            ->willReturn(new ResponseDTO());

        $this->laravelCacheMock
            ->expects($this->once())
            ->method('forever')
            ->withAnyParameters();

        $getServiceMock = $this->getGetServiceMock();
        $result = $getServiceMock->getResponseDTO('slugUri');

        $this->assertInstanceOf(ResponseDTO::class, $result);
    }

    private function getGetServiceMock(): GetService
    {
        return new GetService(
            $this->slugRepositoryMock,
            $this->sluggableFactoryMock,
            $this->laravelCacheMock,
        );
    }
}
