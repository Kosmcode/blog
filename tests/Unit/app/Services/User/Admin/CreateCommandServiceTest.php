<?php

namespace Tests\Unit\app\Services\User\Admin;

use App\Models\ModelHasRole;
use App\Models\Role;
use App\Models\User;
use App\Repositories\ModelHasRoleRepositoryInterface;
use App\Repositories\RoleRepositoryInterface;
use App\Services\LocalizationServiceInterface;
use App\Services\User\Admin\CreateCommandService;
use App\Services\User\CreateServiceInterface;
use App\Services\Validation\UserEmailValidationService;
use App\Services\Validation\UsernameValidationService;
use App\Services\Validation\UserPasswordsValidationService;
use App\Services\Validation\ValidationServiceInterface;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use Tests\Unit\TestCase;

class CreateCommandServiceTest extends TestCase
{
    use ProphecyTrait;

    private const EXAMPLE_MESSAGE_ERROR = 'messageError';

    private const EXAMPLE_MESSAGE_SUCCESS = 'messageSucces';

    private ObjectProphecy $roleRepositoryMock;

    private ObjectProphecy $modelHasRoleRepositoryMock;

    private ObjectProphecy $localizationServiceMock;

    private ObjectProphecy $userCreateServiceMock;

    private ObjectProphecy $validationServiceMock;

    private ObjectProphecy $roleModelMock;

    private ObjectProphecy $userModelMock;

    private ObjectProphecy $modelHasRoleModelMock;

    public function setUp(): void
    {
        parent::setUp();

        $this->roleRepositoryMock = $this->prophesize(RoleRepositoryInterface::class);
        $this->modelHasRoleRepositoryMock = $this->prophesize(ModelHasRoleRepositoryInterface::class);
        $this->localizationServiceMock = $this->prophesize(LocalizationServiceInterface::class);
        $this->userCreateServiceMock = $this->prophesize(CreateServiceInterface::class);
        $this->validationServiceMock = $this->prophesize(ValidationServiceInterface::class);

        $this->roleModelMock = $this->prophesize(Role::class);
        $this->userModelMock = $this->prophesize(User::class);
        $this->modelHasRoleModelMock = $this->prophesize(ModelHasRole::class);
    }

    public function testIsCorrectUserEmailWhenIsNotCorrect(): void
    {
        $this->validationServiceMock
            ->isValid(Argument::type(UserEmailValidationService::class))
            ->willReturn(false);

        $this->localizationServiceMock
            ->get(Argument::type('String'))
            ->willReturn(self::EXAMPLE_MESSAGE_ERROR);

        $this->validationServiceMock
            ->getNotification()
            ->willReturn(self::EXAMPLE_MESSAGE_ERROR);

        $userCreateAdminCommandServiceMock = $this->getCreateCommandServiceMock();

        $this->assertNull($userCreateAdminCommandServiceMock->getNotification());
        $this->assertFalse($userCreateAdminCommandServiceMock->isCorrectUserEmail('wrongUserEmail'));
        $this->assertEquals(self::EXAMPLE_MESSAGE_ERROR, $userCreateAdminCommandServiceMock->getNotification());
    }

    public function testIsCorrectUserEmailWhenIsCorrect(): void
    {
        $this->validationServiceMock
            ->isValid(Argument::type(UserEmailValidationService::class))
            ->willReturn(true);

        $userCreateAdminCommandServiceMock = $this->getCreateCommandServiceMock();

        $this->assertNull($userCreateAdminCommandServiceMock->getNotification());
        $this->assertTrue($userCreateAdminCommandServiceMock->isCorrectUserEmail('goodUserEmail'));
        $this->assertNull($userCreateAdminCommandServiceMock->getNotification());
    }

    public function testIsCorrectUsernameWhenIsNotCorrect(): void
    {
        $this->validationServiceMock
            ->isValid(Argument::type(UsernameValidationService::class))
            ->willReturn(false);

        $this->localizationServiceMock
            ->get(Argument::type('String'))
            ->willReturn(self::EXAMPLE_MESSAGE_ERROR);

        $this->validationServiceMock
            ->getNotification()
            ->willReturn(self::EXAMPLE_MESSAGE_ERROR);

        $userCreateAdminCommandServiceMock = $this->getCreateCommandServiceMock();

        $this->assertNull($userCreateAdminCommandServiceMock->getNotification());
        $this->assertFalse($userCreateAdminCommandServiceMock->isCorrectUsername('wrongUsername'));
        $this->assertEquals(self::EXAMPLE_MESSAGE_ERROR, $userCreateAdminCommandServiceMock->getNotification());
    }

    public function testIsCorrectUsernameWhenIsCorrect(): void
    {
        $this->validationServiceMock
            ->isValid(Argument::type(UsernameValidationService::class))
            ->willReturn(true);

        $userCreateAdminCommandServiceMock = $this->getCreateCommandServiceMock();

        $this->assertNull($userCreateAdminCommandServiceMock->getNotification());
        $this->assertTrue($userCreateAdminCommandServiceMock->isCorrectUsername('goodUsername'));
        $this->assertNull($userCreateAdminCommandServiceMock->getNotification());
    }

    public function testIsCorrectUserPasswordWhenIsNotCorrect(): void
    {
        $this->validationServiceMock
            ->isValid(Argument::type(UserPasswordsValidationService::class))
            ->willReturn(false);

        $this->localizationServiceMock
            ->get(Argument::type('String'))
            ->willReturn(self::EXAMPLE_MESSAGE_ERROR);

        $this->validationServiceMock
            ->getNotification()
            ->willReturn(self::EXAMPLE_MESSAGE_ERROR);

        $userCreateAdminCommandServiceMock = $this->getCreateCommandServiceMock();

        $this->assertNull($userCreateAdminCommandServiceMock->getNotification());
        $this->assertFalse(
            $userCreateAdminCommandServiceMock->isCorrectUserPassword(
                'wrongUserPassword',
                'wrongUserPasswordConfirm'
            )
        );
        $this->assertEquals(self::EXAMPLE_MESSAGE_ERROR, $userCreateAdminCommandServiceMock->getNotification());
    }

    public function testIsCorrectUserPasswordWhenIsCorrect(): void
    {
        $this->validationServiceMock
            ->isValid(Argument::type(UserPasswordsValidationService::class))
            ->willReturn(true);

        $userCreateAdminCommandServiceMock = $this->getCreateCommandServiceMock();

        $this->assertNull($userCreateAdminCommandServiceMock->getNotification());
        $this->assertTrue(
            $userCreateAdminCommandServiceMock->isCorrectUserPassword(
                'wrongUserPassword',
                'wrongUserPasswordConfirm'
            )
        );
        $this->assertNull($userCreateAdminCommandServiceMock->getNotification());
    }

    public function testCreateUserWithAdminRoleWhenNotExistAdminRole(): void
    {
        $this->roleRepositoryMock
            ->findByName(Argument::type('String'))
            ->willReturn(null);

        $this->localizationServiceMock
            ->get(Argument::type('String'))
            ->willReturn(self::EXAMPLE_MESSAGE_ERROR);

        $userCreateAdminCommandServiceMock = $this->getCreateCommandServiceMock();

        $this->assertNull($userCreateAdminCommandServiceMock->getNotification());
        $this->assertFalse($userCreateAdminCommandServiceMock->createUserWithAdminRole(
            Argument::type('String'),
            Argument::type('String'),
            Argument::type('String')
        ));
        $this->assertEquals(self::EXAMPLE_MESSAGE_ERROR, $userCreateAdminCommandServiceMock->getNotification());
    }

    public function testCreateUserWithAdminRoleSuccess(): void
    {
        $this->roleModelMock
            ->getId()
            ->willReturn(1);

        $this->roleRepositoryMock
            ->findByName(Argument::type('String'))
            ->willReturn($this->roleModelMock->reveal());

        $this->userModelMock
            ->getId()
            ->willReturn(1);

        $this->userCreateServiceMock
            ->create(Argument::type('String'), Argument::type('String'), Argument::type('String'))
            ->willReturn($this->userModelMock->reveal());

        $this->modelHasRoleRepositoryMock
            ->create(Argument::type('Array'))
            ->willReturn($this->modelHasRoleModelMock->reveal());

        $this->localizationServiceMock
            ->get(Argument::type('String'))
            ->willReturn(self::EXAMPLE_MESSAGE_SUCCESS);

        $userCreateAdminCommandServiceMock = $this->getCreateCommandServiceMock();

        $this->assertNull($userCreateAdminCommandServiceMock->getNotification());
        $this->assertTrue($userCreateAdminCommandServiceMock->createUserWithAdminRole(
            Argument::type('String'),
            Argument::type('String'),
            Argument::type('String')
        ));
        $this->assertEquals(self::EXAMPLE_MESSAGE_SUCCESS, $userCreateAdminCommandServiceMock->getNotification());
    }

    private function getCreateCommandServiceMock(): CreateCommandService
    {
        $this->localizationServiceMock->setLangPattern(Argument::type('String'));

        return new CreateCommandService(
            $this->roleRepositoryMock->reveal(),
            $this->modelHasRoleRepositoryMock->reveal(),
            $this->localizationServiceMock->reveal(),
            $this->userCreateServiceMock->reveal(),
            $this->validationServiceMock->reveal()
        );
    }
}
