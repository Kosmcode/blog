<?php

namespace Tests\Unit\app\Services\User;

use App\Models\User;
use App\Services\User\CheckService;
use Illuminate\Contracts\Auth\Guard as LaravelAuth;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use Tests\Unit\TestCase;

class UserCheckServiceTest extends TestCase
{
    use ProphecyTrait;

    private const EXAMPLE_ROLES = ['role1', 'role2', 'role3'];

    private ObjectProphecy $laravelAuth;

    private ObjectProphecy $userModel;

    public function setUp(): void
    {
        parent::setUp();

        $this->laravelAuth = $this->prophesize(LaravelAuth::class);

        $this->userModel = $this->prophesize(User::class);
    }

    public function testUserHasAdminRoleWhenNotGivenUserAndIsNotLogged()
    {
        $this->laravelAuth
            ->check()
            ->willReturn(false);

        $mockedUserCheckService = $this->getMockedUserCheckService();

        $result = $mockedUserCheckService->userHasAdminRole();

        $this->assertIsBool($result);
        $this->assertFalse($result);
    }

    public function testUserHasAdminRoleWhenNotGivenUserAndIsLoggedAndNotGetUserInstance()
    {
        $this->laravelAuth
            ->check()
            ->willReturn(true);

        $this->laravelAuth
            ->user()
            ->willReturn(null);

        $mockedUserCheckService = $this->getMockedUserCheckService();

        $result = $mockedUserCheckService->userHasAdminRole();

        $this->assertIsBool($result);
        $this->assertFalse($result);
    }

    public function testUserHasAdminRoleWhenNotGivenUserAndIsLoggedAndNotIsAdmin()
    {
        $this->laravelAuth
            ->check()
            ->willReturn(true);

        $this->userModel
            ->hasRole(Argument::type('String'))
            ->willReturn(false);

        $this->laravelAuth
            ->user()
            ->willReturn($this->userModel->reveal());

        $mockedUserCheckService = $this->getMockedUserCheckService();

        $result = $mockedUserCheckService->userHasAdminRole();

        $this->assertIsBool($result);
        $this->assertFalse($result);
    }

    public function testUserHasAdminRoleWhenNotGivenUserAndIsLoggedAndIsAdmin()
    {
        $this->laravelAuth
            ->check()
            ->willReturn(true);

        $this->userModel
            ->hasRole(Argument::type('String'))
            ->willReturn(true);

        $this->laravelAuth
            ->user()
            ->willReturn($this->userModel->reveal());

        $mockedUserCheckService = $this->getMockedUserCheckService();

        $result = $mockedUserCheckService->userHasAdminRole();

        $this->assertIsBool($result);
        $this->assertTrue($result);
    }

    public function testUserHasAdminRoleWhenGivenUserAndNotHasAdminRole()
    {
        $this->userModel
            ->hasRole(Argument::type('String'))
            ->willReturn(false);

        $mockedUserCheckService = $this->getMockedUserCheckService();

        $result = $mockedUserCheckService->userHasAdminRole($this->userModel->reveal());

        $this->assertIsBool($result);
        $this->assertFalse($result);
    }

    public function testUserHasAdminRoleWhenGivenUserAndHasAdminRole()
    {
        $this->userModel
            ->hasRole(Argument::type('String'))
            ->willReturn(true);

        $mockedUserCheckService = $this->getMockedUserCheckService();

        $result = $mockedUserCheckService->userHasAdminRole($this->userModel->reveal());

        $this->assertIsBool($result);
        $this->assertTrue($result);
    }

    public function testUserHasAdminOrRoleWhenIsNotSetUserAndNotHasAuthorize()
    {
        $this->laravelAuth
            ->check()
            ->willReturn(false);

        $mockedUserCheckService = $this->getMockedUserCheckService();

        $result = $mockedUserCheckService->userHasAdminOrRole(self::EXAMPLE_ROLES);

        $this->assertIsBool($result);
        $this->assertFalse($result);
    }

    public function testUserHasAdminOrRoleWhenIsNotSetUserAndHasAuthorizeAndNotHasRoleOrAdmin()
    {
        $this->userModel
            ->hasRole(Argument::type('Array'))
            ->willReturn(false);

        $this->laravelAuth
            ->check()
            ->willReturn(true);

        $this->laravelAuth
            ->user()
            ->willReturn($this->userModel->reveal());

        $mockedUserCheckService = $this->getMockedUserCheckService();

        $result = $mockedUserCheckService->userHasAdminOrRole(self::EXAMPLE_ROLES);

        $this->assertIsBool($result);
        $this->assertFalse($result);
    }

    public function testUserHasAdminOrRoleWhenIsNotSetUserAndHasAuthorizeAndHasRoleOrAdmin()
    {
        $this->userModel
            ->hasRole(Argument::type('Array'))
            ->willReturn(true);

        $this->laravelAuth
            ->check()
            ->willReturn(true);

        $this->laravelAuth
            ->user()
            ->willReturn($this->userModel->reveal());

        $mockedUserCheckService = $this->getMockedUserCheckService();

        $result = $mockedUserCheckService->userHasAdminOrRole(self::EXAMPLE_ROLES);

        $this->assertIsBool($result);
        $this->assertTrue($result);
    }

    public function testUserHasAdminOrRoleWhenIsSetUserAndNotHasRoleOrAdmin()
    {
        $this->userModel
            ->hasRole(Argument::type('Array'))
            ->willReturn(false);

        $mockedUserCheckService = $this->getMockedUserCheckService();

        $result = $mockedUserCheckService->userHasAdminOrRole(self::EXAMPLE_ROLES, $this->userModel->reveal());

        $this->assertIsBool($result);
        $this->assertFalse($result);
    }

    public function testUserHasAdminOrRoleWhenIsSetUserAndHasRoleOrAdmin()
    {
        $this->userModel
            ->hasRole(Argument::type('Array'))
            ->willReturn(true);

        $mockedUserCheckService = $this->getMockedUserCheckService();

        $result = $mockedUserCheckService->userHasAdminOrRole(self::EXAMPLE_ROLES, $this->userModel->reveal());

        $this->assertIsBool($result);
        $this->assertTrue($result);
    }

    private function getMockedUserCheckService(): CheckService
    {
        return new CheckService(
            $this->laravelAuth->reveal()
        );
    }
}
