<?php

namespace Tests\Unit\app\Services\User;

use App\Models\User;
use App\Repositories\UserRepositoryInterface;
use App\Services\User\CreateService;
use Illuminate\Contracts\Hashing\Hasher as LaravelHash;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use Tests\Unit\TestCase;

class UserCreateServiceTest extends TestCase
{
    use ProphecyTrait;

    private ObjectProphecy $userRepositoryMock;

    private ObjectProphecy $laravelHashMock;

    private ObjectProphecy $userModelMock;

    public function setUp(): void
    {
        parent::setUp();

        $this->userRepositoryMock = $this->prophesize(UserRepositoryInterface::class);
        $this->laravelHashMock = $this->prophesize(LaravelHash::class);

        $this->userModelMock = $this->prophesize(User::class);
    }

    public function testCreate(): void
    {
        $this->userModelMock
            ->getName()
            ->willReturn('username');
        $this->userModelMock
            ->getEmail()
            ->willReturn('user@email.com');
        $this->userModelMock
            ->getPassword()
            ->willReturn('userPasswordHashed');

        $this->laravelHashMock
            ->make(Argument::type('String'))
            ->willReturn('userPasswordHashed');

        $this->userRepositoryMock
            ->create(Argument::type('Array'))
            ->willReturn($this->userModelMock->reveal());

        $userCreateServiceMock = $this->getUserCreateServiceMock();

        $result = $userCreateServiceMock->create('username', 'user@email.com', 'userPassword');

        $this->assertInstanceOf(User::class, $result);
        $this->assertEquals('username', $result->getName());
        $this->assertEquals('user@email.com', $result->getEmail());
        $this->assertEquals('userPasswordHashed', $result->getPassword());
    }

    private function getUserCreateServiceMock(): CreateService
    {
        return new CreateService(
            $this->userRepositoryMock->reveal(),
            $this->laravelHashMock->reveal()
        );
    }
}
