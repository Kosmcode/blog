<?php

namespace Tests\Unit\app\Services\User;

use App\Enums\User\RequestParameterEnum;
use App\Http\Requests\Auth\StoreRequest;
use App\Models\User;
use App\Services\User\CreateServiceInterface;
use App\Services\User\RegisterService;
use Illuminate\Auth\Events\Registered;
use Illuminate\Events\Dispatcher as LaravelEvent;
use Illuminate\Support\Facades\Auth;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use Tests\Unit\TestCase;

class UserRegisterServiceTest extends TestCase
{
    use ProphecyTrait;

    private ObjectProphecy $userCreateServiceMock;

    private ObjectProphecy $laravelEventMock;

    private ObjectProphecy $userStoreRequestMock;

    private ObjectProphecy $userModelMock;

    public function setUp(): void
    {
        parent::setUp();

        $this->userCreateServiceMock = $this->prophesize(CreateServiceInterface::class);
        $this->laravelEventMock = $this->prophesize(LaravelEvent::class);

        $this->userStoreRequestMock = $this->prophesize(StoreRequest::class);
        $this->userModelMock = $this->prophesize(User::class);
    }

    public function testCreateAndLogin(): void
    {
        $this->userStoreRequestMock
            ->all()
            ->willReturn([
                RequestParameterEnum::name->value => 'username',
                RequestParameterEnum::email->value => 'userEmail',
                RequestParameterEnum::password->value => 'userPasswrod',
            ]);

        $this->userCreateServiceMock
            ->create(Argument::type('String'), Argument::type('String'), Argument::type('String'))
            ->willReturn($this->userModelMock->reveal());

        $this->laravelEventMock
            ->dispatch(Argument::type(Registered::class));

        Auth::shouldReceive('login')
            ->once();

        $userRegisterServiceMock = $this->getUserRegisterServiceMock();

        $this->assertNull($userRegisterServiceMock->createAndLogin($this->userStoreRequestMock->reveal()));
    }

    private function getUserRegisterServiceMock(): RegisterService
    {
        return new RegisterService(
            $this->userCreateServiceMock->reveal(),
            $this->laravelEventMock->reveal()
        );
    }
}
