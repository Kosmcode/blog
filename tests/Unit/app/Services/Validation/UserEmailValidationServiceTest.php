<?php

namespace Tests\Unit\app\Services\Validation;

use App\Services\Validation\UserEmailValidationService;
use Illuminate\Support\Facades\App;
use Illuminate\Support\MessageBag;
use Illuminate\Validation\Factory as LaravelValidator;
use Illuminate\Validation\Validator as LaravelValidatorInstance;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use Tests\Unit\TestCase;

class UserEmailValidationServiceTest extends TestCase
{
    use ProphecyTrait;

    private const EXAMPLE_ERROR_MESSAGE = 'wrongEmailMessage';

    private ObjectProphecy $laravelValidatorMock;

    private ObjectProphecy $laravelValidatorInstanceMock;

    private ObjectProphecy $messageBagMock;

    public function setUp(): void
    {
        parent::setUp();

        $this->laravelValidatorMock = $this->prophesize(LaravelValidator::class);
        $this->laravelValidatorInstanceMock = $this->prophesize(LaravelValidatorInstance::class);
        $this->messageBagMock = $this->prophesize(MessageBag::class);
    }

    public function testIsCorrectWhenWrongUserEmail(): void
    {
        $this->messageBagMock
            ->first()
            ->willReturn(self::EXAMPLE_ERROR_MESSAGE);

        $this->laravelValidatorInstanceMock
            ->errors()
            ->willReturn($this->messageBagMock->reveal());

        $this->laravelValidatorInstanceMock
            ->passes()
            ->willReturn(false);

        $this->laravelValidatorMock
            ->make(Argument::type('Array'), Argument::type('Array'))
            ->willReturn($this->laravelValidatorInstanceMock->reveal());

        App::shouldReceive('make')
            ->once()
            ->andReturn($this->laravelValidatorMock->reveal());

        $userEmailValidationServiceMock = $this->getUserEmailValidationServiceMock('wrongEmail');

        $this->assertFalse($userEmailValidationServiceMock->isCorrect());
        $this->assertEquals(self::EXAMPLE_ERROR_MESSAGE, $userEmailValidationServiceMock->getNotification());
    }

    public function testIsCorrectWhenCorrectUserEmail(): void
    {
        $this->laravelValidatorInstanceMock
            ->passes()
            ->willReturn(true);

        $this->laravelValidatorMock
            ->make(Argument::type('Array'), Argument::type('Array'))
            ->willReturn($this->laravelValidatorInstanceMock->reveal());

        App::shouldReceive('make')
            ->once()
            ->andReturn($this->laravelValidatorMock->reveal());

        $userEmailValidationServiceMock = $this->getUserEmailValidationServiceMock('good@user.mail');

        $this->assertNull($userEmailValidationServiceMock->getNotification());
        $this->assertTrue($userEmailValidationServiceMock->isCorrect());
        $this->assertNull($userEmailValidationServiceMock->getNotification());
    }

    private function getUserEmailValidationServiceMock(string $userEmail): UserEmailValidationService
    {
        return new UserEmailValidationService($userEmail);
    }
}
