<?php

namespace Tests\Unit\app\Services\Validation;

use App\Services\Validation\ValidatableServiceInterface;
use App\Services\Validation\ValidationService;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use Tests\Unit\TestCase;

class ValidationServiceTest extends TestCase
{
    use ProphecyTrait;

    private const EXAMPLE_ERROR_MESSAGE = 'errorMessage';

    private ObjectProphecy $validationServiceMock;

    public function setUp(): void
    {
        parent::setUp();

        $this->validationServiceMock = $this->prophesize(ValidatableServiceInterface::class);
    }

    public function testIsValidWhenIsNotCorrectData(): void
    {
        $this->validationServiceMock
            ->isCorrect()
            ->willReturn(false);

        $this->validationServiceMock
            ->getNotification()
            ->willReturn(self::EXAMPLE_ERROR_MESSAGE);

        $validationServiceMock = $this->getValidationServiceMock();

        $this->assertNull($validationServiceMock->getNotification());
        $this->assertFalse($validationServiceMock->isValid($this->validationServiceMock->reveal()));
        $this->assertEquals(self::EXAMPLE_ERROR_MESSAGE, $validationServiceMock->getNotification());
    }

    public function testIsValidWhenIsCorrectData(): void
    {
        $this->validationServiceMock
            ->isCorrect()
            ->willReturn(true);

        $validationServiceMock = $this->getValidationServiceMock();

        $this->assertNull($validationServiceMock->getNotification());
        $this->assertTrue($validationServiceMock->isValid($this->validationServiceMock->reveal()));
        $this->assertNull($validationServiceMock->getNotification());
    }

    private function getValidationServiceMock(): ValidationService
    {
        return new ValidationService();
    }
}
