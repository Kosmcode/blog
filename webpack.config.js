const path = require('path');
const CompressionPlugin = require("compression-webpack-plugin");
let webpack = require("webpack");
const productionGzipExtensions = ['js', 'css']

module.exports = {
    resolve: {
        alias: {
            '@': path.resolve('resources/js'),
        },
    },
    externals: {
        "jquery": "jQuery"
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jquery: 'jquery',
            'window.jQuery': 'jquery',
            jQuery: 'jquery'
        }),
        new CompressionPlugin({
            filename: '[file].gz[query]',
            algorithm: 'gzip',
            test: new RegExp('\\.(' + productionGzipExtensions.join('|') + ')$'),
            threshold: 10240,
            minRatio: 0.8
        })
    ]
};
